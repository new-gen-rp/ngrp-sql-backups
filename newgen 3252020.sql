-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 26, 2020 at 03:11 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `newgen`
--

-- --------------------------------------------------------

--
-- Table structure for table `addon_account`
--

CREATE TABLE `addon_account` (
  `name` varchar(60) COLLATE utf8mb4_bin NOT NULL,
  `label` varchar(100) COLLATE utf8mb4_bin NOT NULL,
  `shared` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `addon_account`
--

INSERT INTO `addon_account` (`name`, `label`, `shared`) VALUES
('bank_savings', 'Bank Savings', 0),
('caution', 'Caution', 0),
('property_black_money', 'Black Money', 0),
('society_ambulance', 'EMS', 1),
('society_banker', 'Bank', 1),
('society_burgershot', 'Burgershot', 1),
('society_cardealer', 'Cardealer', 1),
('society_mechanic', 'Mechanic', 1),
('society_police', 'Police', 1),
('society_taxi', 'Taxi', 1);

-- --------------------------------------------------------

--
-- Table structure for table `addon_account_data`
--

CREATE TABLE `addon_account_data` (
  `id` int(11) NOT NULL,
  `account_name` varchar(100) COLLATE utf8mb4_bin DEFAULT NULL,
  `money` int(11) NOT NULL,
  `owner` varchar(100) COLLATE utf8mb4_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `addon_account_data`
--

INSERT INTO `addon_account_data` (`id`, `account_name`, `money`, `owner`) VALUES
(2, 'society_ambulance', 0, NULL),
(3, 'society_banker', 0, NULL),
(4, 'society_cardealer', 0, NULL),
(5, 'society_mechanic', 1000000, NULL),
(6, 'society_police', 1001130, NULL),
(7, 'society_taxi', 0, NULL),
(11, 'bank_savings', 0, 'steam:110000111d0b1aa'),
(12, 'caution', 0, 'steam:110000111d0b1aa'),
(13, 'property_black_money', 0, 'steam:110000111d0b1aa'),
(14, 'bank_savings', 0, 'steam:1100001081d1893'),
(15, 'caution', 0, 'steam:1100001081d1893'),
(16, 'property_black_money', 0, 'steam:1100001081d1893'),
(17, 'bank_savings', 0, 'steam:11000010b49a743'),
(18, 'caution', 0, 'steam:11000010b49a743'),
(19, 'property_black_money', 0, 'steam:11000010b49a743'),
(20, 'bank_savings', 0, 'steam:11000013e7edea3'),
(21, 'caution', 0, 'steam:11000013e7edea3'),
(22, 'property_black_money', 0, 'steam:11000013e7edea3'),
(23, 'bank_savings', 0, 'steam:110000111c332ed'),
(24, 'caution', 0, 'steam:110000111c332ed'),
(25, 'property_black_money', 0, 'steam:110000111c332ed'),
(26, 'bank_savings', 0, 'steam:11000010a01bdb9'),
(27, 'property_black_money', 0, 'steam:11000010a01bdb9'),
(28, 'caution', 0, 'steam:11000010a01bdb9'),
(29, 'bank_savings', 0, 'steam:11000013e9d4b99'),
(30, 'caution', 0, 'steam:11000013e9d4b99'),
(31, 'property_black_money', 0, 'steam:11000013e9d4b99'),
(32, 'bank_savings', 0, 'steam:110000107941620'),
(33, 'caution', 0, 'steam:110000107941620'),
(34, 'property_black_money', 0, 'steam:110000107941620'),
(35, 'bank_savings', 0, 'steam:11000010fe595d0'),
(36, 'caution', 0, 'steam:11000010fe595d0'),
(37, 'property_black_money', 0, 'steam:11000010fe595d0'),
(38, 'bank_savings', 0, 'steam:11000010e2a62e2'),
(39, 'caution', 0, 'steam:11000010e2a62e2'),
(40, 'property_black_money', 0, 'steam:11000010e2a62e2'),
(41, 'bank_savings', 0, 'steam:11000010b88b452'),
(42, 'caution', 0, 'steam:11000010b88b452'),
(43, 'property_black_money', 0, 'steam:11000010b88b452'),
(44, 'society_burgershot', 0, NULL),
(45, 'bank_savings', 0, 'steam:110000105ed368b'),
(46, 'caution', 0, 'steam:110000105ed368b'),
(47, 'property_black_money', 0, 'steam:110000105ed368b'),
(48, 'bank_savings', 0, 'steam:1100001183c7077'),
(49, 'caution', 0, 'steam:1100001183c7077'),
(50, 'property_black_money', 0, 'steam:1100001183c7077'),
(51, 'bank_savings', 0, 'steam:1100001139319c1'),
(52, 'property_black_money', 0, 'steam:1100001139319c1'),
(53, 'caution', 0, 'steam:1100001139319c1'),
(54, 'bank_savings', 0, 'steam:110000132580eb0'),
(55, 'caution', 0, 'steam:110000132580eb0'),
(56, 'property_black_money', 0, 'steam:110000132580eb0'),
(57, 'bank_savings', 0, 'steam:110000119e7384d'),
(58, 'caution', 0, 'steam:110000119e7384d'),
(59, 'property_black_money', 0, 'steam:110000119e7384d'),
(60, 'bank_savings', 0, 'steam:110000139d9adf9'),
(61, 'property_black_money', 0, 'steam:110000139d9adf9'),
(62, 'caution', 0, 'steam:110000139d9adf9'),
(63, 'bank_savings', 0, 'steam:11000013eaca305'),
(64, 'caution', 0, 'steam:11000013eaca305'),
(65, 'property_black_money', 0, 'steam:11000013eaca305'),
(66, 'bank_savings', 0, 'steam:11000013efa68e1'),
(67, 'caution', 0, 'steam:11000013efa68e1'),
(68, 'property_black_money', 0, 'steam:11000013efa68e1'),
(69, 'bank_savings', 0, 'steam:1100001176cddfb'),
(70, 'caution', 0, 'steam:1100001176cddfb'),
(71, 'property_black_money', 0, 'steam:1100001176cddfb'),
(72, 'bank_savings', 0, 'steam:11000013f6b85e1'),
(73, 'caution', 0, 'steam:11000013f6b85e1'),
(74, 'property_black_money', 0, 'steam:11000013f6b85e1'),
(75, 'bank_savings', 0, 'steam:11000013e33b934'),
(76, 'caution', 0, 'steam:11000013e33b934'),
(77, 'property_black_money', 0, 'steam:11000013e33b934'),
(78, 'bank_savings', 0, 'steam:11000013517b942'),
(79, 'caution', 0, 'steam:11000013517b942'),
(80, 'property_black_money', 0, 'steam:11000013517b942'),
(81, 'bank_savings', 0, 'steam:110000134e3ca1a'),
(82, 'property_black_money', 0, 'steam:110000134e3ca1a'),
(83, 'caution', 0, 'steam:110000134e3ca1a'),
(84, 'bank_savings', 0, 'steam:1100001013142e0'),
(85, 'caution', 0, 'steam:1100001013142e0'),
(86, 'property_black_money', 0, 'steam:1100001013142e0'),
(87, 'bank_savings', 0, 'steam:110000133d93ea2'),
(88, 'caution', 0, 'steam:110000133d93ea2'),
(89, 'property_black_money', 0, 'steam:110000133d93ea2'),
(90, 'bank_savings', 0, 'steam:11000011531f6b5'),
(91, 'caution', 0, 'steam:11000011531f6b5'),
(92, 'property_black_money', 0, 'steam:11000011531f6b5'),
(93, 'bank_savings', 0, 'steam:11000010cfb10c1'),
(94, 'property_black_money', 0, 'steam:11000010cfb10c1'),
(95, 'caution', 0, 'steam:11000010cfb10c1'),
(96, 'bank_savings', 0, 'steam:110000135e316b5'),
(97, 'caution', 0, 'steam:110000135e316b5'),
(98, 'property_black_money', 0, 'steam:110000135e316b5'),
(99, 'bank_savings', 0, 'steam:1100001156cfac9'),
(100, 'property_black_money', 0, 'steam:1100001156cfac9'),
(101, 'caution', 0, 'steam:1100001156cfac9'),
(102, 'bank_savings', 0, 'steam:110000136177a4e'),
(103, 'caution', 0, 'steam:110000136177a4e'),
(104, 'property_black_money', 0, 'steam:110000136177a4e'),
(105, 'bank_savings', 0, 'steam:110000116e48a9b'),
(106, 'property_black_money', 0, 'steam:110000116e48a9b'),
(107, 'caution', 0, 'steam:110000116e48a9b'),
(108, 'bank_savings', 0, 'steam:110000106197c1a'),
(109, 'caution', 0, 'steam:110000106197c1a'),
(110, 'property_black_money', 0, 'steam:110000106197c1a'),
(111, 'bank_savings', 0, 'steam:110000113ec9482'),
(112, 'caution', 0, 'steam:110000113ec9482'),
(113, 'property_black_money', 0, 'steam:110000113ec9482'),
(114, 'bank_savings', 0, 'steam:1100001351e28d7'),
(115, 'caution', 0, 'steam:1100001351e28d7'),
(116, 'property_black_money', 0, 'steam:1100001351e28d7'),
(117, 'bank_savings', 0, 'steam:11000013f478ad1'),
(118, 'caution', 0, 'steam:11000013f478ad1'),
(119, 'property_black_money', 0, 'steam:11000013f478ad1'),
(120, 'caution', 2020, 'steam:11000010d651395'),
(121, 'property_black_money', 0, 'steam:11000010d651395'),
(122, 'bank_savings', 0, 'steam:11000010d651395'),
(123, 'bank_savings', 0, 'steam:1100001177ff1be'),
(124, 'caution', 0, 'steam:1100001177ff1be'),
(125, 'property_black_money', 0, 'steam:1100001177ff1be'),
(126, 'bank_savings', 0, 'steam:11000013ea40bc0'),
(127, 'caution', 0, 'steam:11000013ea40bc0'),
(128, 'property_black_money', 0, 'steam:11000013ea40bc0'),
(129, 'bank_savings', 0, 'steam:110000118024cc1'),
(130, 'caution', 0, 'steam:110000118024cc1'),
(131, 'property_black_money', 0, 'steam:110000118024cc1');

-- --------------------------------------------------------

--
-- Table structure for table `addon_inventory`
--

CREATE TABLE `addon_inventory` (
  `name` varchar(60) COLLATE utf8mb4_bin NOT NULL,
  `label` varchar(100) COLLATE utf8mb4_bin NOT NULL,
  `shared` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `addon_inventory`
--

INSERT INTO `addon_inventory` (`name`, `label`, `shared`) VALUES
('property', 'Property', 0),
('society_ambulance', 'EMS', 1),
('society_burgershot', 'Burgershot', 1),
('society_burgershot_fridge', 'Burgershot(frigorifico)', 1),
('society_cardealer', 'Cardealer', 1),
('society_mechanic', 'Mechanic', 1),
('society_police', 'Police', 1),
('society_taxi', 'Taxi', 1);

-- --------------------------------------------------------

--
-- Table structure for table `addon_inventory_items`
--

CREATE TABLE `addon_inventory_items` (
  `id` int(11) NOT NULL,
  `inventory_name` varchar(100) COLLATE utf8mb4_bin NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_bin NOT NULL,
  `count` int(11) NOT NULL,
  `owner` varchar(60) COLLATE utf8mb4_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `addon_inventory_items`
--

INSERT INTO `addon_inventory_items` (`id`, `inventory_name`, `name`, `count`, `owner`) VALUES
(1, 'society_mechanic', 'cannabis', 26, NULL),
(2, 'society_taxi', 'bread', 0, NULL),
(3, 'society_taxi', 'water', 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `billing`
--

CREATE TABLE `billing` (
  `id` int(11) NOT NULL,
  `identifier` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `sender` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `target_type` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `target` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `amount` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `billing`
--

INSERT INTO `billing` (`id`, `identifier`, `sender`, `target_type`, `target`, `label`, `amount`) VALUES
(7, 'steam:110000134e3ca1a', 'steam:110000111c332ed', 'player', 'steam:110000111c332ed', 'Fine: Illegal Passing', 100),
(26, 'steam:1100001177ff1be', 'steam:11000010b49a743', 'society', 'society_police', 'Fine: Assault of an LEO', 1878);

-- --------------------------------------------------------

--
-- Table structure for table `cardealer_vehicles`
--

CREATE TABLE `cardealer_vehicles` (
  `id` int(11) NOT NULL,
  `vehicle` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `price` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- --------------------------------------------------------

--
-- Table structure for table `characters`
--

CREATE TABLE `characters` (
  `id` int(11) NOT NULL,
  `identifier` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `firstname` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `lastname` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `dateofbirth` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `sex` varchar(1) COLLATE utf8mb4_bin NOT NULL DEFAULT 'M',
  `height` varchar(128) COLLATE utf8mb4_bin NOT NULL,
  `ems_rank` int(11) DEFAULT -1,
  `leo_rank` int(11) DEFAULT -1,
  `tow_rank` int(11) DEFAULT -1,
  `fire_rank` int(11) DEFAULT -1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `characters`
--

INSERT INTO `characters` (`id`, `identifier`, `firstname`, `lastname`, `dateofbirth`, `sex`, `height`, `ems_rank`, `leo_rank`, `tow_rank`, `fire_rank`) VALUES
(2, 'steam:11000010a01bdb9', 'Tommie', 'Pickles', '1988-12-28', 'm', '172', -1, -1, -1, -1),
(4, 'steam:1100001081d1893', 'Godrick', 'Twinkletoes', '1994-09-12', 'm', '200', -1, 6, -1, -1),
(5, 'steam:110000133d93ea2', 'Nicholas ', 'Silver', '1989/03/17', 'm', '150', -1, -1, -1, -1),
(6, 'steam:1100001139319c1', 'Michael', 'Thomas', '1994-06-12', 'm', '176', -1, -1, -1, -1),
(7, 'steam:1100001156cfac9', 'Joey ', 'Mcboby', '08-12-99', 'm', '188', -1, -1, -1, -1),
(8, 'steam:11000013ea40bc0', 'Freddy', 'Lungus', '1997/03/14', 'm', '157', -1, -1, -1, -1),
(9, 'steam:110000106197c1a', 'Austin', 'Nocera', '1995-06-09', 'm', '200', -1, -1, -1, -1),
(10, 'steam:110000116e48a9b', 'Nate', 'Redmond', '1997-04-29', 'm', '160', -1, 3, -1, -1),
(11, 'steam:11000010b49a743', 'Killian', 'Oconnell', '19960529', 'm', '175', -1, -1, -1, -1),
(12, 'steam:110000111c332ed', 'Darryl', 'Normil', '1987-29-08', 'm', '160', -1, -1, -1, -1),
(13, 'steam:110000133d93ea2', 'Grant ', 'Cummings', '1990/03/17', 'm', '150', -1, -1, -1, -1);

-- --------------------------------------------------------

--
-- Table structure for table `datastore`
--

CREATE TABLE `datastore` (
  `name` varchar(60) COLLATE utf8mb4_bin NOT NULL,
  `label` varchar(100) COLLATE utf8mb4_bin NOT NULL,
  `shared` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `datastore`
--

INSERT INTO `datastore` (`name`, `label`, `shared`) VALUES
('property', 'Property', 0),
('society_ambulance', 'EMS', 1),
('society_burgershot', 'Burgershot', 1),
('society_police', 'Police', 1),
('society_taxi', 'Taxi', 1),
('user_ears', 'Ears', 0),
('user_glasses', 'Glasses', 0),
('user_helmet', 'Helmet', 0),
('user_mask', 'Mask', 0);

-- --------------------------------------------------------

--
-- Table structure for table `datastore_data`
--

CREATE TABLE `datastore_data` (
  `id` int(11) NOT NULL,
  `name` varchar(60) COLLATE utf8mb4_bin NOT NULL,
  `owner` varchar(60) COLLATE utf8mb4_bin DEFAULT NULL,
  `data` longtext COLLATE utf8mb4_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `datastore_data`
--

INSERT INTO `datastore_data` (`id`, `name`, `owner`, `data`) VALUES
(2, 'society_ambulance', NULL, '{}'),
(3, 'society_police', NULL, '{}'),
(4, 'society_taxi', NULL, '{}'),
(10, 'property', 'steam:110000111d0b1aa', '{}'),
(11, 'user_ears', 'steam:110000111d0b1aa', '{}'),
(12, 'user_glasses', 'steam:110000111d0b1aa', '{}'),
(13, 'user_helmet', 'steam:110000111d0b1aa', '{}'),
(14, 'user_mask', 'steam:110000111d0b1aa', '{}'),
(15, 'property', 'steam:1100001081d1893', '{}'),
(16, 'user_ears', 'steam:1100001081d1893', '{}'),
(17, 'user_glasses', 'steam:1100001081d1893', '{}'),
(18, 'user_helmet', 'steam:1100001081d1893', '{}'),
(19, 'user_mask', 'steam:1100001081d1893', '{}'),
(20, 'property', 'steam:11000010b49a743', '{}'),
(21, 'user_ears', 'steam:11000010b49a743', '{}'),
(22, 'user_glasses', 'steam:11000010b49a743', '{}'),
(23, 'user_helmet', 'steam:11000010b49a743', '{}'),
(24, 'user_mask', 'steam:11000010b49a743', '{}'),
(25, 'property', 'steam:11000013e7edea3', '{}'),
(26, 'user_ears', 'steam:11000013e7edea3', '{}'),
(27, 'user_glasses', 'steam:11000013e7edea3', '{}'),
(28, 'user_helmet', 'steam:11000013e7edea3', '{}'),
(29, 'user_mask', 'steam:11000013e7edea3', '{}'),
(30, 'property', 'steam:110000111c332ed', '{}'),
(31, 'user_ears', 'steam:110000111c332ed', '{}'),
(32, 'user_glasses', 'steam:110000111c332ed', '{}'),
(33, 'user_helmet', 'steam:110000111c332ed', '{}'),
(34, 'user_mask', 'steam:110000111c332ed', '{}'),
(35, 'user_ears', 'steam:11000010a01bdb9', '{}'),
(36, 'property', 'steam:11000010a01bdb9', '{}'),
(37, 'user_glasses', 'steam:11000010a01bdb9', '{}'),
(38, 'user_helmet', 'steam:11000010a01bdb9', '{}'),
(39, 'user_mask', 'steam:11000010a01bdb9', '{}'),
(40, 'property', 'steam:11000013e9d4b99', '{}'),
(41, 'user_ears', 'steam:11000013e9d4b99', '{}'),
(42, 'user_glasses', 'steam:11000013e9d4b99', '{}'),
(43, 'user_helmet', 'steam:11000013e9d4b99', '{}'),
(44, 'user_mask', 'steam:11000013e9d4b99', '{}'),
(45, 'user_ears', 'steam:110000107941620', '{}'),
(46, 'property', 'steam:110000107941620', '{}'),
(47, 'user_glasses', 'steam:110000107941620', '{}'),
(48, 'user_mask', 'steam:110000107941620', '{}'),
(49, 'user_helmet', 'steam:110000107941620', '{}'),
(50, 'property', 'steam:11000010fe595d0', '{}'),
(51, 'user_ears', 'steam:11000010fe595d0', '{}'),
(52, 'user_glasses', 'steam:11000010fe595d0', '{}'),
(53, 'user_mask', 'steam:11000010fe595d0', '{}'),
(54, 'user_helmet', 'steam:11000010fe595d0', '{}'),
(55, 'property', 'steam:11000010e2a62e2', '{}'),
(56, 'user_ears', 'steam:11000010e2a62e2', '{}'),
(57, 'user_glasses', 'steam:11000010e2a62e2', '{}'),
(58, 'user_helmet', 'steam:11000010e2a62e2', '{}'),
(59, 'user_mask', 'steam:11000010e2a62e2', '{}'),
(60, 'property', 'steam:11000010b88b452', '{}'),
(61, 'user_ears', 'steam:11000010b88b452', '{}'),
(62, 'user_glasses', 'steam:11000010b88b452', '{}'),
(63, 'user_helmet', 'steam:11000010b88b452', '{}'),
(64, 'user_mask', 'steam:11000010b88b452', '{}'),
(65, 'society_burgershot', NULL, '{}'),
(66, 'property', 'steam:110000105ed368b', '{}'),
(67, 'user_ears', 'steam:110000105ed368b', '{}'),
(68, 'user_glasses', 'steam:110000105ed368b', '{}'),
(69, 'user_helmet', 'steam:110000105ed368b', '{}'),
(70, 'user_mask', 'steam:110000105ed368b', '{}'),
(71, 'property', 'steam:1100001183c7077', '{}'),
(72, 'user_ears', 'steam:1100001183c7077', '{}'),
(73, 'user_glasses', 'steam:1100001183c7077', '{}'),
(74, 'user_helmet', 'steam:1100001183c7077', '{}'),
(75, 'user_mask', 'steam:1100001183c7077', '{}'),
(76, 'property', 'steam:1100001139319c1', '{}'),
(77, 'user_ears', 'steam:1100001139319c1', '{}'),
(78, 'user_helmet', 'steam:1100001139319c1', '{}'),
(79, 'user_mask', 'steam:1100001139319c1', '{}'),
(80, 'user_glasses', 'steam:1100001139319c1', '{}'),
(81, 'property', 'steam:110000132580eb0', '{}'),
(82, 'user_ears', 'steam:110000132580eb0', '{}'),
(83, 'user_glasses', 'steam:110000132580eb0', '{}'),
(84, 'user_helmet', 'steam:110000132580eb0', '{}'),
(85, 'user_mask', 'steam:110000132580eb0', '{}'),
(86, 'user_helmet', 'steam:110000119e7384d', '{}'),
(87, 'user_mask', 'steam:110000119e7384d', '{}'),
(88, 'property', 'steam:110000119e7384d', '{}'),
(89, 'user_glasses', 'steam:110000119e7384d', '{}'),
(90, 'user_ears', 'steam:110000119e7384d', '{}'),
(91, 'property', 'steam:110000139d9adf9', '{}'),
(92, 'user_glasses', 'steam:110000139d9adf9', '{}'),
(93, 'user_mask', 'steam:110000139d9adf9', '{}'),
(94, 'user_ears', 'steam:110000139d9adf9', '{}'),
(95, 'user_helmet', 'steam:110000139d9adf9', '{}'),
(96, 'property', 'steam:11000013eaca305', '{}'),
(97, 'user_ears', 'steam:11000013eaca305', '{}'),
(98, 'user_glasses', 'steam:11000013eaca305', '{}'),
(99, 'user_helmet', 'steam:11000013eaca305', '{}'),
(100, 'user_mask', 'steam:11000013eaca305', '{}'),
(101, 'property', 'steam:11000013efa68e1', '{}'),
(102, 'user_ears', 'steam:11000013efa68e1', '{}'),
(103, 'user_glasses', 'steam:11000013efa68e1', '{}'),
(104, 'user_helmet', 'steam:11000013efa68e1', '{}'),
(105, 'user_mask', 'steam:11000013efa68e1', '{}'),
(106, 'property', 'steam:1100001176cddfb', '{}'),
(107, 'user_glasses', 'steam:1100001176cddfb', '{}'),
(108, 'user_ears', 'steam:1100001176cddfb', '{}'),
(109, 'user_helmet', 'steam:1100001176cddfb', '{}'),
(110, 'user_mask', 'steam:1100001176cddfb', '{}'),
(111, 'property', 'steam:11000013f6b85e1', '{}'),
(112, 'user_ears', 'steam:11000013f6b85e1', '{}'),
(113, 'user_glasses', 'steam:11000013f6b85e1', '{}'),
(114, 'user_helmet', 'steam:11000013f6b85e1', '{}'),
(115, 'user_mask', 'steam:11000013f6b85e1', '{}'),
(116, 'user_helmet', 'steam:11000013e33b934', '{}'),
(117, 'user_mask', 'steam:11000013e33b934', '{}'),
(118, 'user_ears', 'steam:11000013e33b934', '{}'),
(119, 'property', 'steam:11000013e33b934', '{}'),
(120, 'user_glasses', 'steam:11000013e33b934', '{}'),
(121, 'property', 'steam:11000013517b942', '{}'),
(122, 'user_ears', 'steam:11000013517b942', '{}'),
(123, 'user_glasses', 'steam:11000013517b942', '{}'),
(124, 'user_helmet', 'steam:11000013517b942', '{}'),
(125, 'user_mask', 'steam:11000013517b942', '{}'),
(126, 'property', 'steam:110000134e3ca1a', '{}'),
(127, 'user_ears', 'steam:110000134e3ca1a', '{}'),
(128, 'user_helmet', 'steam:110000134e3ca1a', '{}'),
(129, 'user_mask', 'steam:110000134e3ca1a', '{}'),
(130, 'user_glasses', 'steam:110000134e3ca1a', '{}'),
(131, 'user_helmet', 'steam:1100001013142e0', '{}'),
(132, 'user_mask', 'steam:1100001013142e0', '{}'),
(133, 'property', 'steam:1100001013142e0', '{}'),
(134, 'user_ears', 'steam:1100001013142e0', '{}'),
(135, 'user_glasses', 'steam:1100001013142e0', '{}'),
(136, 'user_helmet', 'steam:110000133d93ea2', '{}'),
(137, 'user_mask', 'steam:110000133d93ea2', '{}'),
(138, 'property', 'steam:110000133d93ea2', '{}'),
(139, 'user_ears', 'steam:110000133d93ea2', '{}'),
(140, 'user_glasses', 'steam:110000133d93ea2', '{}'),
(141, 'property', 'steam:11000011531f6b5', '{}'),
(142, 'user_ears', 'steam:11000011531f6b5', '{}'),
(143, 'user_glasses', 'steam:11000011531f6b5', '{}'),
(144, 'user_mask', 'steam:11000011531f6b5', '{}'),
(145, 'user_helmet', 'steam:11000011531f6b5', '{}'),
(146, 'property', 'steam:11000010cfb10c1', '{}'),
(147, 'user_ears', 'steam:11000010cfb10c1', '{}'),
(148, 'user_glasses', 'steam:11000010cfb10c1', '{}'),
(149, 'user_mask', 'steam:11000010cfb10c1', '{}'),
(150, 'user_helmet', 'steam:11000010cfb10c1', '{}'),
(151, 'property', 'steam:110000135e316b5', '{}'),
(152, 'user_glasses', 'steam:110000135e316b5', '{}'),
(153, 'user_ears', 'steam:110000135e316b5', '{}'),
(154, 'user_mask', 'steam:110000135e316b5', '{}'),
(155, 'user_helmet', 'steam:110000135e316b5', '{}'),
(156, 'user_helmet', 'steam:1100001156cfac9', '{}'),
(157, 'user_mask', 'steam:1100001156cfac9', '{}'),
(158, 'property', 'steam:1100001156cfac9', '{}'),
(159, 'user_ears', 'steam:1100001156cfac9', '{}'),
(160, 'user_glasses', 'steam:1100001156cfac9', '{}'),
(161, 'property', 'steam:110000136177a4e', '{}'),
(162, 'user_ears', 'steam:110000136177a4e', '{}'),
(163, 'user_glasses', 'steam:110000136177a4e', '{}'),
(164, 'user_helmet', 'steam:110000136177a4e', '{}'),
(165, 'user_mask', 'steam:110000136177a4e', '{}'),
(166, 'property', 'steam:110000116e48a9b', '{}'),
(167, 'user_ears', 'steam:110000116e48a9b', '{}'),
(168, 'user_glasses', 'steam:110000116e48a9b', '{}'),
(169, 'user_helmet', 'steam:110000116e48a9b', '{}'),
(170, 'user_mask', 'steam:110000116e48a9b', '{}'),
(171, 'user_helmet', 'steam:110000106197c1a', '{}'),
(172, 'property', 'steam:110000106197c1a', '{}'),
(173, 'user_ears', 'steam:110000106197c1a', '{}'),
(174, 'user_glasses', 'steam:110000106197c1a', '{}'),
(175, 'user_mask', 'steam:110000106197c1a', '{}'),
(176, 'property', 'steam:110000113ec9482', '{}'),
(177, 'user_ears', 'steam:110000113ec9482', '{}'),
(178, 'user_glasses', 'steam:110000113ec9482', '{}'),
(179, 'user_mask', 'steam:110000113ec9482', '{}'),
(180, 'user_helmet', 'steam:110000113ec9482', '{}'),
(181, 'property', 'steam:1100001351e28d7', '{}'),
(182, 'user_ears', 'steam:1100001351e28d7', '{}'),
(183, 'user_mask', 'steam:1100001351e28d7', '{}'),
(184, 'user_glasses', 'steam:1100001351e28d7', '{}'),
(185, 'user_helmet', 'steam:1100001351e28d7', '{}'),
(186, 'property', 'steam:11000013f478ad1', '{}'),
(187, 'user_ears', 'steam:11000013f478ad1', '{}'),
(188, 'user_mask', 'steam:11000013f478ad1', '{}'),
(189, 'user_glasses', 'steam:11000013f478ad1', '{}'),
(190, 'user_helmet', 'steam:11000013f478ad1', '{}'),
(191, 'property', 'steam:11000010d651395', '{}'),
(192, 'user_ears', 'steam:11000010d651395', '{}'),
(193, 'user_glasses', 'steam:11000010d651395', '{}'),
(194, 'user_helmet', 'steam:11000010d651395', '{}'),
(195, 'user_mask', 'steam:11000010d651395', '{}'),
(196, 'property', 'steam:1100001177ff1be', '{}'),
(197, 'user_ears', 'steam:1100001177ff1be', '{}'),
(198, 'user_glasses', 'steam:1100001177ff1be', '{}'),
(199, 'user_mask', 'steam:1100001177ff1be', '{}'),
(200, 'user_helmet', 'steam:1100001177ff1be', '{}'),
(201, 'user_ears', 'steam:11000013ea40bc0', '{}'),
(202, 'user_glasses', 'steam:11000013ea40bc0', '{}'),
(203, 'user_helmet', 'steam:11000013ea40bc0', '{}'),
(204, 'user_mask', 'steam:11000013ea40bc0', '{}'),
(205, 'property', 'steam:11000013ea40bc0', '{}'),
(206, 'property', 'steam:110000118024cc1', '{}'),
(207, 'user_ears', 'steam:110000118024cc1', '{}'),
(208, 'user_glasses', 'steam:110000118024cc1', '{}'),
(209, 'user_helmet', 'steam:110000118024cc1', '{}'),
(210, 'user_mask', 'steam:110000118024cc1', '{}');

-- --------------------------------------------------------

--
-- Table structure for table `dpkeybinds`
--

CREATE TABLE `dpkeybinds` (
  `id` varchar(50) DEFAULT NULL,
  `keybind1` varchar(50) DEFAULT 'num4',
  `emote1` varchar(255) DEFAULT '',
  `keybind2` varchar(50) DEFAULT 'num5',
  `emote2` varchar(255) DEFAULT '',
  `keybind3` varchar(50) DEFAULT 'num6',
  `emote3` varchar(255) DEFAULT '',
  `keybind4` varchar(50) DEFAULT 'num7',
  `emote4` varchar(255) DEFAULT '',
  `keybind5` varchar(50) DEFAULT 'num8',
  `emote5` varchar(255) DEFAULT '',
  `keybind6` varchar(50) DEFAULT 'num9',
  `emote6` varchar(255) DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dpkeybinds`
--

INSERT INTO `dpkeybinds` (`id`, `keybind1`, `emote1`, `keybind2`, `emote2`, `keybind3`, `emote3`, `keybind4`, `emote4`, `keybind5`, `emote5`, `keybind6`, `emote6`) VALUES
('steam:110000111d0b1aa', 'num4', '', 'num5', '', 'num6', '', 'num7', '', 'num8', '', 'num9', ''),
('steam:110000111c332ed', 'num4', '', 'num5', '', 'num6', '', 'num7', '', 'num8', '', 'num9', ''),
('steam:110000105ed368b', 'num4', '', 'num5', '', 'num6', '', 'num7', '', 'num8', '', 'num9', ''),
('steam:11000010e2a62e2', 'num4', '', 'num5', '', 'num6', '', 'num7', '', 'num8', '', 'num9', ''),
('steam:11000010b49a743', 'num4', '', 'num5', '', 'num6', '', 'num7', '', 'num8', '', 'num9', ''),
('steam:1100001081d1893', 'num4', 'medic2', 'num5', 'notepad2', 'num6', 'passout', 'num7', '', 'num8', '', 'num9', ''),
('steam:110000119e7384d', 'num4', '', 'num5', '', 'num6', '', 'num7', '', 'num8', '', 'num9', ''),
('steam:110000139d9adf9', 'num4', '', 'num5', '', 'num6', '', 'num7', '', 'num8', '', 'num9', ''),
('steam:11000013eaca305', 'num4', '', 'num5', '', 'num6', '', 'num7', '', 'num8', '', 'num9', ''),
('steam:11000013efa68e1', 'num4', '', 'num5', '', 'num6', '', 'num7', '', 'num8', '', 'num9', ''),
('steam:110000132580eb0', 'num4', '', 'num5', '', 'num6', '', 'num7', '', 'num8', '', 'num9', ''),
('steam:1100001139319c1', 'num4', '', 'num5', '', 'num6', '', 'num7', '', 'num8', '', 'num9', ''),
('steam:1100001183c7077', 'num4', '', 'num5', '', 'num6', '', 'num7', '', 'num8', '', 'num9', ''),
('steam:11000013e7edea3', 'num4', '', 'num5', '', 'num6', '', 'num7', '', 'num8', '', 'num9', ''),
('steam:110000107941620', 'num4', '', 'num5', '', 'num6', '', 'num7', '', 'num8', '', 'num9', ''),
('steam:1100001176cddfb', 'num4', '', 'num5', '', 'num6', '', 'num7', '', 'num8', '', 'num9', ''),
('steam:11000010fe595d0', 'num4', '', 'num5', '', 'num6', '', 'num7', '', 'num8', '', 'num9', ''),
('steam:11000013f6b85e1', 'num4', '', 'num5', '', 'num6', '', 'num7', '', 'num8', '', 'num9', ''),
('steam:11000013e33b934', 'num4', '', 'num5', '', 'num6', '', 'num7', '', 'num8', '', 'num9', ''),
('steam:11000013517b942', 'num4', '', 'num5', '', 'num6', '', 'num7', '', 'num8', '', 'num9', ''),
('steam:110000134e3ca1a', 'num4', '', 'num5', '', 'num6', '', 'num7', '', 'num8', '', 'num9', ''),
('steam:1100001013142e0', 'num4', '', 'num5', '', 'num6', '', 'num7', '', 'num8', '', 'num9', ''),
('steam:110000133d93ea2', 'num4', '', 'num5', '', 'num6', '', 'num7', '', 'num8', '', 'num9', ''),
('steam:11000011531f6b5', 'num4', '', 'num5', '', 'num6', '', 'num7', '', 'num8', '', 'num9', ''),
('steam:11000010cfb10c1', 'num4', '', 'num5', '', 'num6', '', 'num7', '', 'num8', '', 'num9', ''),
('steam:110000135e316b5', 'num4', '', 'num5', '', 'num6', '', 'num7', '', 'num8', '', 'num9', ''),
('steam:1100001156cfac9', 'num4', '', 'num5', '', 'num6', '', 'num7', '', 'num8', '', 'num9', ''),
('steam:110000136177a4e', 'num4', '', 'num5', '', 'num6', '', 'num7', '', 'num8', '', 'num9', ''),
('steam:110000116e48a9b', 'num4', '', 'num5', '', 'num6', '', 'num7', '', 'num8', '', 'num9', ''),
('steam:110000106197c1a', 'num4', '', 'num5', '', 'num6', '', 'num7', '', 'num8', '', 'num9', ''),
('steam:11000010a01bdb9', 'num4', '', 'num5', '', 'num6', '', 'num7', '', 'num8', '', 'num9', ''),
('steam:110000113ec9482', 'num4', '', 'num5', '', 'num6', '', 'num7', '', 'num8', '', 'num9', ''),
('steam:1100001351e28d7', 'num4', '', 'num5', '', 'num6', '', 'num7', '', 'num8', '', 'num9', ''),
('steam:11000013f478ad1', 'num4', '', 'num5', '', 'num6', '', 'num7', '', 'num8', '', 'num9', ''),
('steam:11000010d651395', 'num4', '', 'num5', '', 'num6', '', 'num7', '', 'num8', '', 'num9', ''),
('steam:1100001177ff1be', 'num4', '', 'num5', '', 'num6', '', 'num7', '', 'num8', '', 'num9', ''),
('steam:11000013ea40bc0', 'num4', '', 'num5', '', 'num6', '', 'num7', '', 'num8', '', 'num9', ''),
('steam:110000118024cc1', 'num4', '', 'num5', '', 'num6', '', 'num7', '', 'num8', '', 'num9', '');

-- --------------------------------------------------------

--
-- Table structure for table `fine_types`
--

CREATE TABLE `fine_types` (
  `id` int(11) NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `category` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `fine_types`
--

INSERT INTO `fine_types` (`id`, `label`, `amount`, `category`) VALUES
(1, 'Misuse of a horn', 30, 0),
(2, 'Illegally Crossing a continuous Line', 40, 0),
(3, 'Driving on the wrong side of the road', 250, 0),
(4, 'Illegal U-Turn', 250, 0),
(5, 'Illegally Driving Off-road', 170, 0),
(6, 'Refusing a Lawful Command', 30, 0),
(7, 'Illegally Stopping a Vehicle', 150, 0),
(8, 'Illegal Parking', 70, 0),
(9, 'Failing to Yield to the right', 70, 0),
(10, 'Failure to comply with Vehicle Information', 90, 0),
(11, 'Failing to stop at a Stop Sign ', 105, 0),
(12, 'Failing to stop at a Red Light', 130, 0),
(13, 'Illegal Passing', 100, 0),
(14, 'Driving an illegal Vehicle', 100, 0),
(15, 'Driving without a License', 1500, 0),
(16, 'Hit and Run', 800, 0),
(17, 'Exceeding Speeds Over < 5 mph', 90, 0),
(18, 'Exceeding Speeds Over 5-15 mph', 120, 0),
(19, 'Exceeding Speeds Over 15-30 mph', 180, 0),
(20, 'Exceeding Speeds Over > 30 mph', 300, 0),
(21, 'Impeding traffic flow', 110, 1),
(22, 'Public Intoxication', 90, 1),
(23, 'Disorderly conduct', 90, 1),
(24, 'Obstruction of Justice', 130, 1),
(25, 'Insults towards Civilans', 75, 1),
(26, 'Disrespecting of an LEO', 110, 1),
(27, 'Verbal Threat towards a Civilan', 90, 1),
(28, 'Verbal Threat towards an LEO', 150, 1),
(29, 'Providing False Information', 250, 1),
(30, 'Attempt of Corruption', 1500, 1),
(31, 'Brandishing a weapon in city Limits', 120, 2),
(32, 'Brandishing a Lethal Weapon in city Limits', 300, 2),
(33, 'No Firearms License', 600, 2),
(34, 'Possession of an Illegal Weapon', 700, 2),
(35, 'Possession of Burglary Tools', 300, 2),
(36, 'Grand Theft Auto', 1800, 2),
(37, 'Intent to Sell/Distrube of an illegal Substance', 1500, 2),
(38, 'Frabrication of an Illegal Substance', 1500, 2),
(39, 'Possession of an Illegal Substance ', 650, 2),
(40, 'Kidnapping of a Civilan', 1500, 2),
(41, 'Kidnapping of an LEO', 2000, 2),
(42, 'Robbery', 650, 2),
(43, 'Armed Robbery of a Store', 650, 2),
(44, 'Armed Robbery of a Bank', 1500, 2),
(45, 'Assault on a Civilian', 2000, 3),
(46, 'Assault of an LEO', 2500, 3),
(47, 'Attempt of Murder of a Civilian', 3000, 3),
(48, 'Attempt of Murder of an LEO', 5000, 3),
(49, 'Murder of a Civilian', 10000, 3),
(50, 'Murder of an LEO', 30000, 3),
(51, 'Involuntary manslaughter', 1800, 3),
(52, 'Fraud', 2000, 2);

-- --------------------------------------------------------

--
-- Table structure for table `glovebox_inventory`
--

CREATE TABLE `glovebox_inventory` (
  `id` int(11) NOT NULL,
  `plate` varchar(8) NOT NULL,
  `data` text NOT NULL,
  `owned` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `glovebox_inventory`
--

INSERT INTO `glovebox_inventory` (`id`, `plate`, `data`, `owned`) VALUES
(44, 'NZT 687 ', '{\"black_money\":[{\"amount\":10108}],\"coffres\":[{\"name\":\"armour\",\"count\":10},{\"name\":\"clip\",\"count\":27},{\"name\":\"weed_pooch\",\"count\":1}],\"weapons\":[{\"ammo\":140,\"label\":\"Carbine rifle\",\"name\":\"WEAPON_CARBINERIFLE\"},{\"ammo\":44,\"label\":\"Pump shotgun\",\"name\":\"WEAPON_PUMPSHOTGUN\"},{\"ammo\":100,\"label\":\"Taser\",\"name\":\"WEAPON_STUNGUN\"},{\"ammo\":105,\"label\":\"Pistol\",\"name\":\"WEAPON_PISTOL\"},{\"ammo\":0,\"label\":\"Flashlight\",\"name\":\"WEAPON_FLASHLIGHT\"},{\"ammo\":25,\"label\":\"Flare gun\",\"name\":\"WEAPON_FLARE\"},{\"ammo\":0,\"label\":\"Nightstick\",\"name\":\"WEAPON_NIGHTSTICK\"},{\"ammo\":140,\"label\":\"Pump shotgun\",\"name\":\"WEAPON_PUMPSHOTGUN\"},{\"ammo\":0,\"label\":\"Combat pistol\",\"name\":\"WEAPON_COMBATPISTOL\"},{\"ammo\":0,\"label\":\"Nightstick\",\"name\":\"WEAPON_NIGHTSTICK\"},{\"ammo\":97,\"label\":\"Carbine rifle\",\"name\":\"WEAPON_CARBINERIFLE\"},{\"ammo\":0,\"label\":\"Fire extinguisher\",\"name\":\"WEAPON_FIREEXTINGUISHER\"},{\"ammo\":100,\"label\":\"Taser\",\"name\":\"WEAPON_STUNGUN\"},{\"ammo\":0,\"label\":\"Flashlight\",\"name\":\"WEAPON_FLASHLIGHT\"},{\"ammo\":25,\"label\":\"Flare gun\",\"name\":\"WEAPON_FLARE\"},{\"ammo\":68,\"label\":\"Combat pistol\",\"name\":\"WEAPON_COMBATPISTOL\"},{\"ammo\":0,\"label\":\"Fire extinguisher\",\"name\":\"WEAPON_FIREEXTINGUISHER\"}]}', 1),
(63, 'CMF 528 ', '{}', 0),
(64, '09XQR472', '{}', 0),
(65, '23HYO478', '{}', 0);

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

CREATE TABLE `items` (
  `name` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `label` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `weight` int(11) NOT NULL DEFAULT 1,
  `limit` int(10) NOT NULL DEFAULT 0,
  `rare` tinyint(1) NOT NULL DEFAULT 0,
  `can_remove` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `items`
--

INSERT INTO `items` (`name`, `label`, `weight`, `limit`, `rare`, `can_remove`) VALUES
('alive_chicken', 'Live Chicken', 1, 20, 0, 1),
('armour', 'Armor', 1, -1, 0, 1),
('bag', 'Bag', 1, 1, 0, 1),
('bandage', 'Bandage', 2, 0, 0, 1),
('beer', 'Beer', 1, 30, 0, 1),
('blowpipe', 'Blowtorch', 2, 0, 0, 1),
('bread', 'Bread', 1, 10, 0, 1),
('breathalyzer', 'Breathalyzer', 1, 10, 0, 1),
('burger', 'Bacon Burgare', 1, -1, 0, 1),
('cannabis', 'Cannabis', 3, 0, 0, 1),
('carokit', 'Body Kit', 3, 0, 0, 1),
('carotool', 'Tools', 2, 0, 0, 1),
('ccheese', 'Feta de queso', 1, 40, 0, 1),
('cheese', 'Queso', 1, 10, 0, 1),
('cheesebows', 'OLW Ostbågar', 1, -1, 0, 1),
('chips', 'OLW 3xLök Chips', 1, -1, 0, 1),
('cigarett', 'Cigarett', 1, -1, 0, 1),
('clettuce', 'Lechuga cortada', 1, 40, 0, 1),
('clip', 'Ammo', 1, -1, 0, 1),
('clothe', 'Cloth', 1, 40, 0, 1),
('coca', 'CocaPlant', 1, 150, 0, 1),
('cocacola', 'Coca Cola', 1, -1, 0, 1),
('coffee', 'Coffee', 1, -1, 0, 1),
('coke', 'Coke (1G)', 1, 0, 0, 1),
('coke_pooch', 'Bag of coke (28G)', 2, 0, 0, 1),
('copper', 'Copper', 1, 56, 0, 1),
('crack', 'Crack', 1, 25, 0, 1),
('ctomato', 'Tomate cortado', 1, 40, 0, 1),
('cutted_wood', 'Cut Wood', 1, 20, 0, 1),
('dabs', 'Dabs', 1, 50, 0, 1),
('diamond', 'Diamond', 1, 50, 0, 1),
('donut', 'Policemans best Friend', 1, -1, 0, 1),
('drugtest', 'DrugTest', 1, 10, 0, 1),
('ephedra', 'Ephedra', 1, 100, 0, 1),
('ephedrine', 'Ephedrine', 1, 100, 0, 1),
('essence', 'Essence', 1, 24, 0, 1),
('fabric', 'Fabric', 1, 80, 0, 1),
('fakepee', 'Fake Pee', 1, 5, 0, 1),
('fanta', 'Fanta Exotic', 1, -1, 0, 1),
('fburger', 'Hamburguesa congelada', 1, 20, 0, 1),
('fish', 'Fish', 1, 100, 0, 1),
('fixkit', 'Repair Kit', 3, 0, 0, 1),
('fixtool', 'Repair Tools', 2, 0, 0, 1),
('fvburger', 'Hamburguesa veggie congelada', 1, 20, 0, 1),
('gazbottle', 'Gas Bottle', 2, 0, 0, 1),
('gold', 'Gold', 1, 21, 0, 1),
('heroine', 'Heroine', 1, 10, 0, 1),
('iron', 'Iron', 1, 42, 0, 1),
('jewels', 'Jewels', 1, 0, 0, 1),
('lettuce', 'Lechuga', 1, 10, 0, 1),
('lighter', 'Tändare', 1, -1, 0, 1),
('loka', 'Loka Crush', 1, -1, 0, 1),
('lotteryticket', 'Trisslott', 1, -1, 0, 1),
('macka', 'Skinkmacka', 1, -1, 0, 1),
('marabou', 'Marabou Mjölkchoklad', 1, -1, 0, 1),
('marijuana', 'Marijuana', 2, 0, 0, 1),
('medikit', 'Medikit', 2, 0, 0, 1),
('meth', 'Meth (1G)', 1, 0, 0, 1),
('meth_pooch', 'Bag of meth (28G)', 2, 0, 0, 1),
('narcan', 'Narcan', 1, 10, 0, 1),
('nugget', 'Nugget', 1, 40, 0, 1),
('nuggets10', 'Nuggets x10', 1, 4, 0, 1),
('nuggets4', 'Nuggets x4', 1, 10, 0, 1),
('opium', 'Opium (1G)', 1, 0, 0, 1),
('opium_pooch', 'Bag of opium (28G)', 2, 0, 0, 1),
('packaged_chicken', 'Packaged Chicken', 1, 100, 0, 1),
('packaged_plank', 'Packaged Plank', 1, 100, 0, 1),
('painkiller', 'Painkiller', 1, 10, 0, 1),
('pastacarbonara', 'Pasta Carbonara', 1, -1, 0, 1),
('pcp', 'PCP', 1, 25, 0, 1),
('petrol', 'Petrol', 1, 24, 0, 1),
('petrol_raffin', 'Refined Petrol', 1, 24, 0, 1),
('pizza', 'Kebab Pizza', 1, -1, 0, 1),
('poppy', 'Poppy', 1, 100, 0, 1),
('potato', 'Papa', 1, 10, 0, 1),
('radio', 'Radio', 1, 1, 0, 0),
('shamburger', 'Hamburguesa simple', 1, 5, 0, 1),
('shotgun_shells', 'Shotgun Shells', 1, 20, 0, 1),
('slaughtered_chicken', 'Slaughtered Chicken', 1, 20, 0, 1),
('sprite', 'Sprite', 1, -1, 0, 1),
('stone', 'Stone', 1, 7, 0, 1),
('tequila', 'Tequila', 1, 10, 0, 1),
('tomato', 'Tomate', 1, 10, 0, 1),
('vbread', 'Pan veggie', 1, 20, 0, 1),
('vhamburger', 'hamburguesa veggie', 1, 5, 0, 1),
('vodka', 'Vodka', 1, 10, 0, 1),
('washed_stone', 'Washed Stone', 1, 7, 0, 1),
('water', 'Water', 1, 5, 0, 1),
('weed', 'Weed (1G)', 1, 0, 0, 1),
('weed_pooch', 'Bag of weed (28G)', 2, 0, 0, 1),
('whiskey', 'Whiskey', 1, 10, 0, 1),
('wood', 'Wood', 1, 20, 0, 1),
('wool', 'Wool', 1, 40, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

CREATE TABLE `jobs` (
  `name` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `label` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL,
  `whitelisted` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `jobs`
--

INSERT INTO `jobs` (`name`, `label`, `whitelisted`) VALUES
('ambulance', 'EMS', 1),
('banker', 'Banker', 1),
('burgershot', 'Burgershot', 1),
('cardealer', 'Cardealer', 1),
('fisherman', 'Fisherman', 0),
('fueler', 'Fueler', 0),
('lumberjack', 'Lumberjack', 0),
('mechanic', 'Mechanic', 0),
('miner', 'Miner', 0),
('police', 'Police', 1),
('reporter', 'Journalist', 0),
('slaughterer', 'Slaughterer', 0),
('tailor', 'Tailor', 0),
('taxi', 'Taxi', 0),
('unemployed', 'Unemployed', 0);

-- --------------------------------------------------------

--
-- Table structure for table `job_grades`
--

CREATE TABLE `job_grades` (
  `id` int(11) NOT NULL,
  `job_name` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL,
  `grade` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `label` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `salary` int(11) NOT NULL,
  `skin_male` longtext COLLATE utf8mb4_bin NOT NULL,
  `skin_female` longtext COLLATE utf8mb4_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `job_grades`
--

INSERT INTO `job_grades` (`id`, `job_name`, `grade`, `name`, `label`, `salary`, `skin_male`, `skin_female`) VALUES
(1, 'unemployed', 0, 'unemployed', 'Unemployed', 200, '{}', '{}'),
(2, 'banker', 0, 'advisor', 'Advisor', 10, '{}', '{}'),
(3, 'banker', 1, 'banker', 'Banker', 20, '{}', '{}'),
(4, 'banker', 2, 'business_banker', 'Business Banker', 30, '{}', '{}'),
(5, 'banker', 3, 'trader', 'Trader', 40, '{}', '{}'),
(6, 'banker', 4, 'boss', 'Boss', 0, '{}', '{}'),
(7, 'lumberjack', 0, 'employee', 'Employee', 0, '{}', '{}'),
(8, 'fisherman', 0, 'employee', 'Employee', 0, '{}', '{}'),
(9, 'fueler', 0, 'employee', 'Employee', 0, '{}', '{}'),
(10, 'reporter', 0, 'employee', 'Employee', 0, '{}', '{}'),
(11, 'tailor', 0, 'employee', 'Employee', 0, '{\"mask_1\":0,\"arms\":1,\"glasses_1\":0,\"hair_color_2\":4,\"makeup_1\":0,\"face\":19,\"glasses\":0,\"mask_2\":0,\"makeup_3\":0,\"skin\":29,\"helmet_2\":0,\"lipstick_4\":0,\"sex\":0,\"torso_1\":24,\"makeup_2\":0,\"bags_2\":0,\"chain_2\":0,\"ears_1\":-1,\"bags_1\":0,\"bproof_1\":0,\"shoes_2\":0,\"lipstick_2\":0,\"chain_1\":0,\"tshirt_1\":0,\"eyebrows_3\":0,\"pants_2\":0,\"beard_4\":0,\"torso_2\":0,\"beard_2\":6,\"ears_2\":0,\"hair_2\":0,\"shoes_1\":36,\"tshirt_2\":0,\"beard_3\":0,\"hair_1\":2,\"hair_color_1\":0,\"pants_1\":48,\"helmet_1\":-1,\"bproof_2\":0,\"eyebrows_4\":0,\"eyebrows_2\":0,\"decals_1\":0,\"age_2\":0,\"beard_1\":5,\"shoes\":10,\"lipstick_1\":0,\"eyebrows_1\":0,\"glasses_2\":0,\"makeup_4\":0,\"decals_2\":0,\"lipstick_3\":0,\"age_1\":0}', '{\"mask_1\":0,\"arms\":5,\"glasses_1\":5,\"hair_color_2\":4,\"makeup_1\":0,\"face\":19,\"glasses\":0,\"mask_2\":0,\"makeup_3\":0,\"skin\":29,\"helmet_2\":0,\"lipstick_4\":0,\"sex\":1,\"torso_1\":52,\"makeup_2\":0,\"bags_2\":0,\"chain_2\":0,\"ears_1\":-1,\"bags_1\":0,\"bproof_1\":0,\"shoes_2\":1,\"lipstick_2\":0,\"chain_1\":0,\"tshirt_1\":23,\"eyebrows_3\":0,\"pants_2\":0,\"beard_4\":0,\"torso_2\":0,\"beard_2\":6,\"ears_2\":0,\"hair_2\":0,\"shoes_1\":42,\"tshirt_2\":4,\"beard_3\":0,\"hair_1\":2,\"hair_color_1\":0,\"pants_1\":36,\"helmet_1\":-1,\"bproof_2\":0,\"eyebrows_4\":0,\"eyebrows_2\":0,\"decals_1\":0,\"age_2\":0,\"beard_1\":5,\"shoes\":10,\"lipstick_1\":0,\"eyebrows_1\":0,\"glasses_2\":0,\"makeup_4\":0,\"decals_2\":0,\"lipstick_3\":0,\"age_1\":0}'),
(12, 'miner', 0, 'employee', 'Employee', 0, '{\"tshirt_2\":1,\"ears_1\":8,\"glasses_1\":15,\"torso_2\":0,\"ears_2\":2,\"glasses_2\":3,\"shoes_2\":1,\"pants_1\":75,\"shoes_1\":51,\"bags_1\":0,\"helmet_2\":0,\"pants_2\":7,\"torso_1\":71,\"tshirt_1\":59,\"arms\":2,\"bags_2\":0,\"helmet_1\":0}', '{}'),
(13, 'slaughterer', 0, 'employee', 'Employee', 0, '{\"age_1\":0,\"glasses_2\":0,\"beard_1\":5,\"decals_2\":0,\"beard_4\":0,\"shoes_2\":0,\"tshirt_2\":0,\"lipstick_2\":0,\"hair_2\":0,\"arms\":67,\"pants_1\":36,\"skin\":29,\"eyebrows_2\":0,\"shoes\":10,\"helmet_1\":-1,\"lipstick_1\":0,\"helmet_2\":0,\"hair_color_1\":0,\"glasses\":0,\"makeup_4\":0,\"makeup_1\":0,\"hair_1\":2,\"bproof_1\":0,\"bags_1\":0,\"mask_1\":0,\"lipstick_3\":0,\"chain_1\":0,\"eyebrows_4\":0,\"sex\":0,\"torso_1\":56,\"beard_2\":6,\"shoes_1\":12,\"decals_1\":0,\"face\":19,\"lipstick_4\":0,\"tshirt_1\":15,\"mask_2\":0,\"age_2\":0,\"eyebrows_3\":0,\"chain_2\":0,\"glasses_1\":0,\"ears_1\":-1,\"bags_2\":0,\"ears_2\":0,\"torso_2\":0,\"bproof_2\":0,\"makeup_2\":0,\"eyebrows_1\":0,\"makeup_3\":0,\"pants_2\":0,\"beard_3\":0,\"hair_color_2\":4}', '{\"age_1\":0,\"glasses_2\":0,\"beard_1\":5,\"decals_2\":0,\"beard_4\":0,\"shoes_2\":0,\"tshirt_2\":0,\"lipstick_2\":0,\"hair_2\":0,\"arms\":72,\"pants_1\":45,\"skin\":29,\"eyebrows_2\":0,\"shoes\":10,\"helmet_1\":-1,\"lipstick_1\":0,\"helmet_2\":0,\"hair_color_1\":0,\"glasses\":0,\"makeup_4\":0,\"makeup_1\":0,\"hair_1\":2,\"bproof_1\":0,\"bags_1\":0,\"mask_1\":0,\"lipstick_3\":0,\"chain_1\":0,\"eyebrows_4\":0,\"sex\":1,\"torso_1\":49,\"beard_2\":6,\"shoes_1\":24,\"decals_1\":0,\"face\":19,\"lipstick_4\":0,\"tshirt_1\":9,\"mask_2\":0,\"age_2\":0,\"eyebrows_3\":0,\"chain_2\":0,\"glasses_1\":5,\"ears_1\":-1,\"bags_2\":0,\"ears_2\":0,\"torso_2\":0,\"bproof_2\":0,\"makeup_2\":0,\"eyebrows_1\":0,\"makeup_3\":0,\"pants_2\":0,\"beard_3\":0,\"hair_color_2\":4}'),
(14, 'mechanic', 0, 'recrue', 'Recruit', 150, '{}', '{}'),
(15, 'mechanic', 1, 'novice', 'Novice', 300, '{}', '{}'),
(16, 'mechanic', 2, 'experimente', 'Experienced', 400, '{}', '{}'),
(17, 'mechanic', 3, 'chief', 'Leader', 500, '{}', '{}'),
(18, 'mechanic', 4, 'boss', 'Boss', 0, '{}', '{}'),
(19, 'taxi', 0, 'recrue', 'Recruit', 650, '{\"hair_2\":0,\"hair_color_2\":0,\"torso_1\":32,\"bags_1\":0,\"helmet_2\":0,\"chain_2\":0,\"eyebrows_3\":0,\"makeup_3\":0,\"makeup_2\":0,\"tshirt_1\":31,\"makeup_1\":0,\"bags_2\":0,\"makeup_4\":0,\"eyebrows_4\":0,\"chain_1\":0,\"lipstick_4\":0,\"bproof_2\":0,\"hair_color_1\":0,\"decals_2\":0,\"pants_2\":0,\"age_2\":0,\"glasses_2\":0,\"ears_2\":0,\"arms\":27,\"lipstick_1\":0,\"ears_1\":-1,\"mask_2\":0,\"sex\":0,\"lipstick_3\":0,\"helmet_1\":-1,\"shoes_2\":0,\"beard_2\":0,\"beard_1\":0,\"lipstick_2\":0,\"beard_4\":0,\"glasses_1\":0,\"bproof_1\":0,\"mask_1\":0,\"decals_1\":1,\"hair_1\":0,\"eyebrows_2\":0,\"beard_3\":0,\"age_1\":0,\"tshirt_2\":0,\"skin\":0,\"torso_2\":0,\"eyebrows_1\":0,\"face\":0,\"shoes_1\":10,\"pants_1\":24}', '{\"hair_2\":0,\"hair_color_2\":0,\"torso_1\":57,\"bags_1\":0,\"helmet_2\":0,\"chain_2\":0,\"eyebrows_3\":0,\"makeup_3\":0,\"makeup_2\":0,\"tshirt_1\":38,\"makeup_1\":0,\"bags_2\":0,\"makeup_4\":0,\"eyebrows_4\":0,\"chain_1\":0,\"lipstick_4\":0,\"bproof_2\":0,\"hair_color_1\":0,\"decals_2\":0,\"pants_2\":1,\"age_2\":0,\"glasses_2\":0,\"ears_2\":0,\"arms\":21,\"lipstick_1\":0,\"ears_1\":-1,\"mask_2\":0,\"sex\":1,\"lipstick_3\":0,\"helmet_1\":-1,\"shoes_2\":0,\"beard_2\":0,\"beard_1\":0,\"lipstick_2\":0,\"beard_4\":0,\"glasses_1\":5,\"bproof_1\":0,\"mask_1\":0,\"decals_1\":1,\"hair_1\":0,\"eyebrows_2\":0,\"beard_3\":0,\"age_1\":0,\"tshirt_2\":0,\"skin\":0,\"torso_2\":0,\"eyebrows_1\":0,\"face\":0,\"shoes_1\":49,\"pants_1\":11}'),
(20, 'taxi', 1, 'novice', 'Cabby', 850, '{\"hair_2\":0,\"hair_color_2\":0,\"torso_1\":32,\"bags_1\":0,\"helmet_2\":0,\"chain_2\":0,\"eyebrows_3\":0,\"makeup_3\":0,\"makeup_2\":0,\"tshirt_1\":31,\"makeup_1\":0,\"bags_2\":0,\"makeup_4\":0,\"eyebrows_4\":0,\"chain_1\":0,\"lipstick_4\":0,\"bproof_2\":0,\"hair_color_1\":0,\"decals_2\":0,\"pants_2\":0,\"age_2\":0,\"glasses_2\":0,\"ears_2\":0,\"arms\":27,\"lipstick_1\":0,\"ears_1\":-1,\"mask_2\":0,\"sex\":0,\"lipstick_3\":0,\"helmet_1\":-1,\"shoes_2\":0,\"beard_2\":0,\"beard_1\":0,\"lipstick_2\":0,\"beard_4\":0,\"glasses_1\":0,\"bproof_1\":0,\"mask_1\":0,\"decals_1\":1,\"hair_1\":0,\"eyebrows_2\":0,\"beard_3\":0,\"age_1\":0,\"tshirt_2\":0,\"skin\":0,\"torso_2\":0,\"eyebrows_1\":0,\"face\":0,\"shoes_1\":10,\"pants_1\":24}', '{\"hair_2\":0,\"hair_color_2\":0,\"torso_1\":57,\"bags_1\":0,\"helmet_2\":0,\"chain_2\":0,\"eyebrows_3\":0,\"makeup_3\":0,\"makeup_2\":0,\"tshirt_1\":38,\"makeup_1\":0,\"bags_2\":0,\"makeup_4\":0,\"eyebrows_4\":0,\"chain_1\":0,\"lipstick_4\":0,\"bproof_2\":0,\"hair_color_1\":0,\"decals_2\":0,\"pants_2\":1,\"age_2\":0,\"glasses_2\":0,\"ears_2\":0,\"arms\":21,\"lipstick_1\":0,\"ears_1\":-1,\"mask_2\":0,\"sex\":1,\"lipstick_3\":0,\"helmet_1\":-1,\"shoes_2\":0,\"beard_2\":0,\"beard_1\":0,\"lipstick_2\":0,\"beard_4\":0,\"glasses_1\":5,\"bproof_1\":0,\"mask_1\":0,\"decals_1\":1,\"hair_1\":0,\"eyebrows_2\":0,\"beard_3\":0,\"age_1\":0,\"tshirt_2\":0,\"skin\":0,\"torso_2\":0,\"eyebrows_1\":0,\"face\":0,\"shoes_1\":49,\"pants_1\":11}'),
(21, 'taxi', 2, 'experimente', 'Experienced', 1000, '{\"hair_2\":0,\"hair_color_2\":0,\"torso_1\":26,\"bags_1\":0,\"helmet_2\":0,\"chain_2\":0,\"eyebrows_3\":0,\"makeup_3\":0,\"makeup_2\":0,\"tshirt_1\":57,\"makeup_1\":0,\"bags_2\":0,\"makeup_4\":0,\"eyebrows_4\":0,\"chain_1\":0,\"lipstick_4\":0,\"bproof_2\":0,\"hair_color_1\":0,\"decals_2\":0,\"pants_2\":4,\"age_2\":0,\"glasses_2\":0,\"ears_2\":0,\"arms\":11,\"lipstick_1\":0,\"ears_1\":-1,\"mask_2\":0,\"sex\":0,\"lipstick_3\":0,\"helmet_1\":-1,\"shoes_2\":0,\"beard_2\":0,\"beard_1\":0,\"lipstick_2\":0,\"beard_4\":0,\"glasses_1\":0,\"bproof_1\":0,\"mask_1\":0,\"decals_1\":0,\"hair_1\":0,\"eyebrows_2\":0,\"beard_3\":0,\"age_1\":0,\"tshirt_2\":0,\"skin\":0,\"torso_2\":0,\"eyebrows_1\":0,\"face\":0,\"shoes_1\":10,\"pants_1\":24}', '{\"hair_2\":0,\"hair_color_2\":0,\"torso_1\":57,\"bags_1\":0,\"helmet_2\":0,\"chain_2\":0,\"eyebrows_3\":0,\"makeup_3\":0,\"makeup_2\":0,\"tshirt_1\":38,\"makeup_1\":0,\"bags_2\":0,\"makeup_4\":0,\"eyebrows_4\":0,\"chain_1\":0,\"lipstick_4\":0,\"bproof_2\":0,\"hair_color_1\":0,\"decals_2\":0,\"pants_2\":1,\"age_2\":0,\"glasses_2\":0,\"ears_2\":0,\"arms\":21,\"lipstick_1\":0,\"ears_1\":-1,\"mask_2\":0,\"sex\":1,\"lipstick_3\":0,\"helmet_1\":-1,\"shoes_2\":0,\"beard_2\":0,\"beard_1\":0,\"lipstick_2\":0,\"beard_4\":0,\"glasses_1\":5,\"bproof_1\":0,\"mask_1\":0,\"decals_1\":1,\"hair_1\":0,\"eyebrows_2\":0,\"beard_3\":0,\"age_1\":0,\"tshirt_2\":0,\"skin\":0,\"torso_2\":0,\"eyebrows_1\":0,\"face\":0,\"shoes_1\":49,\"pants_1\":11}'),
(22, 'taxi', 3, 'uber', 'Uber Cabby', 1250, '{\"hair_2\":0,\"hair_color_2\":0,\"torso_1\":26,\"bags_1\":0,\"helmet_2\":0,\"chain_2\":0,\"eyebrows_3\":0,\"makeup_3\":0,\"makeup_2\":0,\"tshirt_1\":57,\"makeup_1\":0,\"bags_2\":0,\"makeup_4\":0,\"eyebrows_4\":0,\"chain_1\":0,\"lipstick_4\":0,\"bproof_2\":0,\"hair_color_1\":0,\"decals_2\":0,\"pants_2\":4,\"age_2\":0,\"glasses_2\":0,\"ears_2\":0,\"arms\":11,\"lipstick_1\":0,\"ears_1\":-1,\"mask_2\":0,\"sex\":0,\"lipstick_3\":0,\"helmet_1\":-1,\"shoes_2\":0,\"beard_2\":0,\"beard_1\":0,\"lipstick_2\":0,\"beard_4\":0,\"glasses_1\":0,\"bproof_1\":0,\"mask_1\":0,\"decals_1\":0,\"hair_1\":0,\"eyebrows_2\":0,\"beard_3\":0,\"age_1\":0,\"tshirt_2\":0,\"skin\":0,\"torso_2\":0,\"eyebrows_1\":0,\"face\":0,\"shoes_1\":10,\"pants_1\":24}', '{\"hair_2\":0,\"hair_color_2\":0,\"torso_1\":57,\"bags_1\":0,\"helmet_2\":0,\"chain_2\":0,\"eyebrows_3\":0,\"makeup_3\":0,\"makeup_2\":0,\"tshirt_1\":38,\"makeup_1\":0,\"bags_2\":0,\"makeup_4\":0,\"eyebrows_4\":0,\"chain_1\":0,\"lipstick_4\":0,\"bproof_2\":0,\"hair_color_1\":0,\"decals_2\":0,\"pants_2\":1,\"age_2\":0,\"glasses_2\":0,\"ears_2\":0,\"arms\":21,\"lipstick_1\":0,\"ears_1\":-1,\"mask_2\":0,\"sex\":1,\"lipstick_3\":0,\"helmet_1\":-1,\"shoes_2\":0,\"beard_2\":0,\"beard_1\":0,\"lipstick_2\":0,\"beard_4\":0,\"glasses_1\":5,\"bproof_1\":0,\"mask_1\":0,\"decals_1\":1,\"hair_1\":0,\"eyebrows_2\":0,\"beard_3\":0,\"age_1\":0,\"tshirt_2\":0,\"skin\":0,\"torso_2\":0,\"eyebrows_1\":0,\"face\":0,\"shoes_1\":49,\"pants_1\":11}'),
(23, 'taxi', 4, 'boss', 'Lead Cabby', 1500, '{\"hair_2\":0,\"hair_color_2\":0,\"torso_1\":29,\"bags_1\":0,\"helmet_2\":0,\"chain_2\":0,\"eyebrows_3\":0,\"makeup_3\":0,\"makeup_2\":0,\"tshirt_1\":31,\"makeup_1\":0,\"bags_2\":0,\"makeup_4\":0,\"eyebrows_4\":0,\"chain_1\":0,\"lipstick_4\":0,\"bproof_2\":0,\"hair_color_1\":0,\"decals_2\":0,\"pants_2\":4,\"age_2\":0,\"glasses_2\":0,\"ears_2\":0,\"arms\":1,\"lipstick_1\":0,\"ears_1\":-1,\"mask_2\":0,\"sex\":0,\"lipstick_3\":0,\"helmet_1\":-1,\"shoes_2\":0,\"beard_2\":0,\"beard_1\":0,\"lipstick_2\":0,\"beard_4\":0,\"glasses_1\":0,\"bproof_1\":0,\"mask_1\":0,\"decals_1\":0,\"hair_1\":0,\"eyebrows_2\":0,\"beard_3\":0,\"age_1\":0,\"tshirt_2\":0,\"skin\":0,\"torso_2\":4,\"eyebrows_1\":0,\"face\":0,\"shoes_1\":10,\"pants_1\":24}', '{\"hair_2\":0,\"hair_color_2\":0,\"torso_1\":57,\"bags_1\":0,\"helmet_2\":0,\"chain_2\":0,\"eyebrows_3\":0,\"makeup_3\":0,\"makeup_2\":0,\"tshirt_1\":38,\"makeup_1\":0,\"bags_2\":0,\"makeup_4\":0,\"eyebrows_4\":0,\"chain_1\":0,\"lipstick_4\":0,\"bproof_2\":0,\"hair_color_1\":0,\"decals_2\":0,\"pants_2\":1,\"age_2\":0,\"glasses_2\":0,\"ears_2\":0,\"arms\":21,\"lipstick_1\":0,\"ears_1\":-1,\"mask_2\":0,\"sex\":1,\"lipstick_3\":0,\"helmet_1\":-1,\"shoes_2\":0,\"beard_2\":0,\"beard_1\":0,\"lipstick_2\":0,\"beard_4\":0,\"glasses_1\":5,\"bproof_1\":0,\"mask_1\":0,\"decals_1\":1,\"hair_1\":0,\"eyebrows_2\":0,\"beard_3\":0,\"age_1\":0,\"tshirt_2\":0,\"skin\":0,\"torso_2\":0,\"eyebrows_1\":0,\"face\":0,\"shoes_1\":49,\"pants_1\":11}'),
(24, 'cardealer', 0, 'recruit', 'Recruit', 10, '{}', '{}'),
(25, 'cardealer', 1, 'novice', 'Novice', 25, '{}', '{}'),
(26, 'cardealer', 2, 'experienced', 'Experienced', 40, '{}', '{}'),
(27, 'cardealer', 3, 'boss', 'Boss', 0, '{}', '{}'),
(28, 'ambulance', 0, 'ambulance', 'Jr. EMT', 20, '{\"tshirt_2\":0,\"hair_color_1\":5,\"glasses_2\":3,\"shoes\":9,\"torso_2\":3,\"hair_color_2\":0,\"pants_1\":24,\"glasses_1\":4,\"hair_1\":2,\"sex\":0,\"decals_2\":0,\"tshirt_1\":15,\"helmet_1\":8,\"helmet_2\":0,\"arms\":92,\"face\":19,\"decals_1\":60,\"torso_1\":13,\"hair_2\":0,\"skin\":34,\"pants_2\":5}', '{\"tshirt_2\":3,\"decals_2\":0,\"glasses\":0,\"hair_1\":2,\"torso_1\":73,\"shoes\":1,\"hair_color_2\":0,\"glasses_1\":19,\"skin\":13,\"face\":6,\"pants_2\":5,\"tshirt_1\":75,\"pants_1\":37,\"helmet_1\":57,\"torso_2\":0,\"arms\":14,\"sex\":1,\"glasses_2\":0,\"decals_1\":0,\"hair_2\":0,\"helmet_2\":0,\"hair_color_1\":0}'),
(29, 'ambulance', 1, 'doctor', 'EMT', 40, '{\"tshirt_2\":0,\"hair_color_1\":5,\"glasses_2\":3,\"shoes\":9,\"torso_2\":3,\"hair_color_2\":0,\"pants_1\":24,\"glasses_1\":4,\"hair_1\":2,\"sex\":0,\"decals_2\":0,\"tshirt_1\":15,\"helmet_1\":8,\"helmet_2\":0,\"arms\":92,\"face\":19,\"decals_1\":60,\"torso_1\":13,\"hair_2\":0,\"skin\":34,\"pants_2\":5}', '{\"tshirt_2\":3,\"decals_2\":0,\"glasses\":0,\"hair_1\":2,\"torso_1\":73,\"shoes\":1,\"hair_color_2\":0,\"glasses_1\":19,\"skin\":13,\"face\":6,\"pants_2\":5,\"tshirt_1\":75,\"pants_1\":37,\"helmet_1\":57,\"torso_2\":0,\"arms\":14,\"sex\":1,\"glasses_2\":0,\"decals_1\":0,\"hair_2\":0,\"helmet_2\":0,\"hair_color_1\":0}'),
(30, 'ambulance', 2, 'chief_doctor', 'Sr. EMT', 60, '{\"tshirt_2\":0,\"hair_color_1\":5,\"glasses_2\":3,\"shoes\":9,\"torso_2\":3,\"hair_color_2\":0,\"pants_1\":24,\"glasses_1\":4,\"hair_1\":2,\"sex\":0,\"decals_2\":0,\"tshirt_1\":15,\"helmet_1\":8,\"helmet_2\":0,\"arms\":92,\"face\":19,\"decals_1\":60,\"torso_1\":13,\"hair_2\":0,\"skin\":34,\"pants_2\":5}', '{\"tshirt_2\":3,\"decals_2\":0,\"glasses\":0,\"hair_1\":2,\"torso_1\":73,\"shoes\":1,\"hair_color_2\":0,\"glasses_1\":19,\"skin\":13,\"face\":6,\"pants_2\":5,\"tshirt_1\":75,\"pants_1\":37,\"helmet_1\":57,\"torso_2\":0,\"arms\":14,\"sex\":1,\"glasses_2\":0,\"decals_1\":0,\"hair_2\":0,\"helmet_2\":0,\"hair_color_1\":0}'),
(31, 'ambulance', 3, 'boss', 'EMT Supervisor', 80, '{\"tshirt_2\":0,\"hair_color_1\":5,\"glasses_2\":3,\"shoes\":9,\"torso_2\":3,\"hair_color_2\":0,\"pants_1\":24,\"glasses_1\":4,\"hair_1\":2,\"sex\":0,\"decals_2\":0,\"tshirt_1\":15,\"helmet_1\":8,\"helmet_2\":0,\"arms\":92,\"face\":19,\"decals_1\":60,\"torso_1\":13,\"hair_2\":0,\"skin\":34,\"pants_2\":5}', '{\"tshirt_2\":3,\"decals_2\":0,\"glasses\":0,\"hair_1\":2,\"torso_1\":73,\"shoes\":1,\"hair_color_2\":0,\"glasses_1\":19,\"skin\":13,\"face\":6,\"pants_2\":5,\"tshirt_1\":75,\"pants_1\":37,\"helmet_1\":57,\"torso_2\":0,\"arms\":14,\"sex\":1,\"glasses_2\":0,\"decals_1\":0,\"hair_2\":0,\"helmet_2\":0,\"hair_color_1\":0}'),
(32, 'police', 0, 'recruit', 'Recruit', 250, '{}', '{}'),
(33, 'police', 1, 'officer', 'Officer', 350, '{}', '{}'),
(34, 'police', 2, 'corporal', 'Corporal', 550, '{}', '{}'),
(35, 'police', 3, 'sergeant', 'Sergeant', 750, '{}', '{}'),
(36, 'police', 4, 'lieutenant', 'Lieutenant', 950, '{}', '{}'),
(37, 'police', 5, 'boss', 'Captain', 1150, '{}', '{}'),
(38, 'police', 6, 'boss', 'Chief', 10000, '{}', '{}');

-- --------------------------------------------------------

--
-- Table structure for table `licenses`
--

CREATE TABLE `licenses` (
  `type` varchar(60) COLLATE utf8mb4_bin NOT NULL,
  `label` varchar(60) COLLATE utf8mb4_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `licenses`
--

INSERT INTO `licenses` (`type`, `label`) VALUES
('boat', 'Boat License'),
('dmv', 'Driving Permit'),
('drive', 'Drivers License'),
('drive_bike', 'Motorcycle License'),
('drive_truck', 'Commercial Drivers License'),
('weapon', 'Weapon Permit'),
('weed_processing', 'Weed Processing License');

-- --------------------------------------------------------

--
-- Table structure for table `old_owned_vehicles`
--

CREATE TABLE `old_owned_vehicles` (
  `owner` varchar(22) COLLATE utf8mb4_bin NOT NULL,
  `plate` varchar(12) COLLATE utf8mb4_bin NOT NULL,
  `vehicle` longtext COLLATE utf8mb4_bin DEFAULT NULL,
  `type` varchar(20) COLLATE utf8mb4_bin NOT NULL DEFAULT 'car',
  `job` varchar(20) COLLATE utf8mb4_bin DEFAULT NULL,
  `stored` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `old_owned_vehicles`
--

INSERT INTO `old_owned_vehicles` (`owner`, `plate`, `vehicle`, `type`, `job`, `stored`) VALUES
('steam:1100001081d1893', 'AFA 021', '{\"modFrontBumper\":-1,\"modTrunk\":-1,\"modDashboard\":-1,\"wheels\":1,\"modTurbo\":false,\"modHydrolic\":-1,\"modSteeringWheel\":-1,\"neonEnabled\":[false,false,false,false],\"plate\":\"AFA 021\",\"dirtLevel\":0.0,\"modAPlate\":-1,\"color2\":134,\"modDial\":-1,\"color1\":134,\"modRightFender\":-1,\"pearlescentColor\":156,\"modHorns\":-1,\"fuelLevel\":75.3,\"modTrimB\":-1,\"modFender\":-1,\"modWindows\":-1,\"modDoorSpeaker\":-1,\"modSeats\":-1,\"modVanityPlate\":-1,\"modAerials\":-1,\"modAirFilter\":-1,\"plateIndex\":4,\"modLivery\":3,\"extras\":{\"2\":false,\"1\":false,\"12\":false,\"11\":false,\"8\":false,\"7\":false,\"10\":true,\"9\":false,\"4\":false,\"3\":false,\"6\":true,\"5\":false},\"neonColor\":[255,0,255],\"modExhaust\":-1,\"modShifterLeavers\":-1,\"modRearBumper\":-1,\"modTank\":-1,\"modFrame\":-1,\"tyreSmokeColor\":[255,255,255],\"modSuspension\":-1,\"modEngineBlock\":-1,\"wheelColor\":156,\"modSmokeEnabled\":false,\"modTransmission\":-1,\"modGrille\":-1,\"bodyHealth\":1000.0,\"modRoof\":-1,\"modBrakes\":-1,\"modTrimA\":-1,\"model\":1745872177,\"engineHealth\":1000.0,\"modHood\":-1,\"modFrontWheels\":-1,\"modArmor\":-1,\"modBackWheels\":-1,\"windowTint\":-1,\"modSideSkirt\":-1,\"modArchCover\":-1,\"modXenon\":false,\"modSpeakers\":-1,\"modStruts\":-1,\"modPlateHolder\":-1,\"modEngine\":-1,\"modSpoilers\":-1,\"modOrnaments\":-1}', 'car', 'police', 1),
('steam:1100001139319c1', 'AJB 215', '{\"neonColor\":[255,0,255],\"modLivery\":3,\"modTrunk\":-1,\"modAPlate\":-1,\"modHood\":-1,\"modFender\":-1,\"modGrille\":-1,\"modSpoilers\":-1,\"modAerials\":-1,\"modExhaust\":-1,\"modTransmission\":-1,\"modArchCover\":-1,\"modRightFender\":-1,\"modFrontBumper\":-1,\"modFrontWheels\":-1,\"modSeats\":-1,\"modVanityPlate\":-1,\"dirtLevel\":0.0,\"modAirFilter\":-1,\"modPlateHolder\":-1,\"plate\":\"AJB 215\",\"extras\":{\"6\":false,\"5\":false,\"10\":false,\"7\":false,\"2\":true,\"1\":false,\"4\":false,\"3\":false,\"11\":false,\"12\":false,\"9\":false,\"8\":false},\"modEngineBlock\":-1,\"modRearBumper\":-1,\"modBackWheels\":-1,\"modSteeringWheel\":-1,\"modTrimB\":-1,\"modDashboard\":-1,\"modFrame\":-1,\"pearlescentColor\":156,\"color1\":134,\"engineHealth\":1000.0,\"model\":-1616359240,\"modEngine\":-1,\"modArmor\":-1,\"windowTint\":-1,\"modTank\":-1,\"modSuspension\":-1,\"modSideSkirt\":-1,\"modTrimA\":-1,\"modBrakes\":-1,\"plateIndex\":4,\"modTurbo\":false,\"modSpeakers\":-1,\"modSmokeEnabled\":false,\"neonEnabled\":[false,false,false,false],\"modXenon\":false,\"modDial\":-1,\"wheels\":1,\"modOrnaments\":-1,\"wheelColor\":156,\"modDoorSpeaker\":-1,\"modRoof\":-1,\"modHydrolic\":-1,\"modStruts\":-1,\"fuelLevel\":87.6,\"modShifterLeavers\":-1,\"bodyHealth\":1000.0,\"tyreSmokeColor\":[255,255,255],\"modHorns\":-1,\"color2\":134,\"modWindows\":-1}', 'car', 'police', 1),
('steam:11000010b49a743', 'BRL 379', '{\"modDoorSpeaker\":-1,\"modFrontWheels\":-1,\"modTransmission\":-1,\"plateIndex\":4,\"plate\":\"BRL 379\",\"tyreSmokeColor\":[255,255,255],\"modSuspension\":-1,\"modWindows\":-1,\"modRoof\":-1,\"modAerials\":-1,\"modSideSkirt\":-1,\"fuelLevel\":76.4,\"modSteeringWheel\":-1,\"modOrnaments\":-1,\"modSmokeEnabled\":false,\"model\":1357997983,\"modEngine\":-1,\"modXenon\":false,\"modArchCover\":-1,\"modArmor\":-1,\"color2\":134,\"color1\":134,\"neonEnabled\":[false,false,false,false],\"modBrakes\":-1,\"modTank\":-1,\"pearlescentColor\":156,\"modFender\":-1,\"modLivery\":1,\"windowTint\":-1,\"modShifterLeavers\":-1,\"bodyHealth\":1000.0,\"modTrimB\":-1,\"modDashboard\":-1,\"modBackWheels\":-1,\"modSpeakers\":-1,\"modGrille\":-1,\"modTrimA\":-1,\"modTurbo\":false,\"modSeats\":-1,\"dirtLevel\":0.0,\"modEngineBlock\":-1,\"modHorns\":-1,\"modDial\":-1,\"modHydrolic\":-1,\"modRearBumper\":-1,\"neonColor\":[255,0,255],\"modHood\":-1,\"extras\":{\"12\":false,\"8\":false,\"11\":false,\"10\":false,\"2\":false,\"3\":false,\"9\":false,\"1\":false,\"6\":false,\"7\":false,\"4\":false,\"5\":true},\"modExhaust\":-1,\"modTrunk\":-1,\"engineHealth\":1000.0,\"modPlateHolder\":-1,\"modStruts\":-1,\"modAirFilter\":-1,\"modSpoilers\":-1,\"wheelColor\":156,\"modFrontBumper\":-1,\"modVanityPlate\":-1,\"modRightFender\":-1,\"modFrame\":-1,\"modAPlate\":-1,\"wheels\":1}', 'car', 'police', 1),
('steam:11000013f6b85e1', 'BRT 622', '{\"windowTint\":-1,\"modSmokeEnabled\":false,\"modDial\":-1,\"modPlateHolder\":-1,\"dirtLevel\":0.0,\"modOrnaments\":-1,\"modTransmission\":-1,\"extras\":{\"10\":false,\"12\":false,\"6\":false,\"7\":true,\"9\":false,\"1\":false,\"8\":false,\"11\":false,\"4\":false,\"5\":false,\"2\":false,\"3\":false},\"modRightFender\":-1,\"engineHealth\":1000.0,\"modVanityPlate\":-1,\"modAPlate\":-1,\"modAerials\":-1,\"modTrunk\":-1,\"modTurbo\":false,\"modEngineBlock\":-1,\"modAirFilter\":-1,\"model\":-1616359240,\"neonEnabled\":[false,false,false,false],\"modStruts\":-1,\"modTrimB\":-1,\"modHorns\":-1,\"modTank\":-1,\"modShifterLeavers\":-1,\"modDashboard\":-1,\"modHydrolic\":-1,\"modSpoilers\":-1,\"modSteeringWheel\":-1,\"modRoof\":-1,\"plate\":\"BRT 622\",\"modArchCover\":-1,\"plateIndex\":4,\"modSideSkirt\":-1,\"modGrille\":-1,\"modBackWheels\":-1,\"color1\":134,\"neonColor\":[255,0,255],\"wheels\":1,\"modFrame\":-1,\"modXenon\":false,\"modBrakes\":-1,\"modFrontBumper\":-1,\"wheelColor\":156,\"tyreSmokeColor\":[255,255,255],\"modTrimA\":-1,\"modExhaust\":-1,\"modLivery\":0,\"modArmor\":-1,\"modSeats\":-1,\"modWindows\":-1,\"color2\":134,\"modHood\":-1,\"modFrontWheels\":-1,\"modDoorSpeaker\":-1,\"modSuspension\":-1,\"modFender\":-1,\"modEngine\":-1,\"fuelLevel\":81.4,\"bodyHealth\":1000.0,\"modSpeakers\":-1,\"modRearBumper\":-1,\"pearlescentColor\":156}', 'car', 'police', 1),
('steam:11000010b49a743', 'CFJ 046', '{\"modWindows\":-1,\"modHorns\":-1,\"bodyHealth\":1000.0,\"modRoof\":-1,\"modDashboard\":-1,\"modFrame\":-1,\"modXenon\":false,\"modLivery\":1,\"modSmokeEnabled\":false,\"modFender\":-1,\"modBackWheels\":-1,\"modSideSkirt\":-1,\"modTrunk\":-1,\"modGrille\":-1,\"windowTint\":-1,\"modAerials\":-1,\"neonColor\":[255,0,255],\"modTurbo\":false,\"modAirFilter\":-1,\"dirtLevel\":1.0,\"color2\":134,\"modSuspension\":-1,\"modSpeakers\":-1,\"model\":161178935,\"modFrontWheels\":-1,\"modHood\":-1,\"plateIndex\":4,\"modFrontBumper\":-1,\"engineHealth\":1000.0,\"modSteeringWheel\":-1,\"extras\":{\"12\":false,\"9\":false,\"10\":false,\"11\":false,\"3\":false,\"2\":false,\"1\":true,\"8\":false,\"7\":false,\"6\":false,\"5\":false,\"4\":false},\"pearlescentColor\":156,\"wheelColor\":156,\"modBrakes\":-1,\"wheels\":1,\"modDial\":-1,\"neonEnabled\":[false,false,false,false],\"modOrnaments\":-1,\"modArchCover\":-1,\"tyreSmokeColor\":[255,255,255],\"modSeats\":-1,\"plate\":\"CFJ 046\",\"modTank\":-1,\"fuelLevel\":78.4,\"modAPlate\":-1,\"modEngine\":-1,\"modRightFender\":-1,\"modTransmission\":-1,\"color1\":134,\"modArmor\":-1,\"modDoorSpeaker\":-1,\"modTrimB\":-1,\"modHydrolic\":-1,\"modVanityPlate\":-1,\"modTrimA\":-1,\"modPlateHolder\":-1,\"modSpoilers\":-1,\"modEngineBlock\":-1,\"modStruts\":-1,\"modExhaust\":-1,\"modShifterLeavers\":-1,\"modRearBumper\":-1}', 'car', 'police', 1),
('steam:110000133d93ea2', 'CXP 038', '{\"modBrakes\":-1,\"modFrontBumper\":-1,\"modEngineBlock\":-1,\"modPlateHolder\":-1,\"modAirFilter\":-1,\"wheelColor\":156,\"modArmor\":-1,\"modFender\":-1,\"modWindows\":-1,\"modBackWheels\":-1,\"modDoorSpeaker\":-1,\"modSideSkirt\":-1,\"plate\":\"CXP 038\",\"wheels\":0,\"modGrille\":-1,\"modHood\":-1,\"modSteeringWheel\":-1,\"modStruts\":-1,\"modHydrolic\":-1,\"modDashboard\":-1,\"modDial\":-1,\"pearlescentColor\":1,\"tyreSmokeColor\":[255,255,255],\"color1\":0,\"modTurbo\":false,\"modRightFender\":-1,\"modExhaust\":-1,\"modSuspension\":-1,\"modArchCover\":-1,\"modOrnaments\":-1,\"modHorns\":-1,\"fuelLevel\":64.3,\"modTransmission\":-1,\"modSpoilers\":-1,\"color2\":0,\"modEngine\":1,\"modAerials\":-1,\"extras\":[],\"modSmokeEnabled\":false,\"bodyHealth\":1000.0,\"neonEnabled\":[false,false,false,false],\"modXenon\":1,\"windowTint\":1,\"engineHealth\":1000.0,\"modSpeakers\":-1,\"modVanityPlate\":-1,\"dirtLevel\":0.0,\"plateIndex\":1,\"modRoof\":-1,\"modTank\":-1,\"modSeats\":-1,\"modLivery\":2,\"modRearBumper\":-1,\"modFrame\":-1,\"modAPlate\":-1,\"modTrimB\":-1,\"neonColor\":[255,0,255],\"modTrunk\":-1,\"model\":-1513691047,\"modShifterLeavers\":-1,\"modFrontWheels\":-1,\"modTrimA\":-1}', 'car', NULL, 1),
('steam:110000133d93ea2', 'DAZ 673', '{\"modWindows\":-1,\"modHydrolic\":-1,\"modTurbo\":false,\"modArchCover\":-1,\"modRearBumper\":-1,\"windowTint\":-1,\"modFrontWheels\":-1,\"modAPlate\":-1,\"modTrimA\":-1,\"modPlateHolder\":-1,\"dirtLevel\":0.0,\"modDoorSpeaker\":-1,\"modAerials\":-1,\"modShifterLeavers\":-1,\"modSmokeEnabled\":false,\"modTrunk\":-1,\"modFender\":-1,\"modSuspension\":-1,\"fuelLevel\":65.0,\"modTransmission\":-1,\"modSideSkirt\":-1,\"engineHealth\":1000.0,\"plate\":\"DAZ 673\",\"modStruts\":-1,\"modBackWheels\":-1,\"tyreSmokeColor\":[255,255,255],\"modXenon\":false,\"modHood\":-1,\"modFrontBumper\":-1,\"modTank\":-1,\"modDial\":-1,\"modLivery\":-1,\"neonColor\":[255,0,255],\"modBrakes\":-1,\"modRoof\":-1,\"modEngine\":-1,\"modSpoilers\":-1,\"modGrille\":-1,\"modTrimB\":-1,\"modVanityPlate\":-1,\"neonEnabled\":[false,false,false,false],\"modDashboard\":-1,\"modHorns\":-1,\"modExhaust\":-1,\"pearlescentColor\":5,\"color1\":0,\"plateIndex\":0,\"wheelColor\":112,\"modEngineBlock\":-1,\"modArmor\":-1,\"modFrame\":-1,\"modRightFender\":-1,\"modOrnaments\":-1,\"modAirFilter\":-1,\"modSeats\":-1,\"extras\":[],\"wheels\":5,\"bodyHealth\":1000.0,\"model\":1549126457,\"modSpeakers\":-1,\"color2\":4,\"modSteeringWheel\":-1}', 'car', NULL, 1),
('steam:11000010fe595d0', 'DDU 263', '{\"modXenon\":false,\"modTrunk\":-1,\"modDashboard\":-1,\"wheels\":1,\"wheelColor\":156,\"modHydrolic\":-1,\"modSteeringWheel\":-1,\"neonEnabled\":[false,false,false,false],\"plate\":\"DDU 263\",\"modTrimB\":-1,\"modAPlate\":-1,\"color2\":134,\"modDial\":-1,\"modPlateHolder\":-1,\"pearlescentColor\":156,\"bodyHealth\":1000.0,\"modHorns\":-1,\"fuelLevel\":81.1,\"modWindows\":-1,\"modFender\":-1,\"modTank\":-1,\"modDoorSpeaker\":-1,\"modSeats\":-1,\"modVanityPlate\":-1,\"modAerials\":-1,\"modArmor\":-1,\"plateIndex\":4,\"modLivery\":0,\"extras\":{\"2\":false,\"1\":false,\"12\":false,\"11\":false,\"8\":true,\"7\":true,\"10\":false,\"9\":false,\"4\":false,\"3\":false,\"6\":false,\"5\":false},\"neonColor\":[255,0,255],\"modExhaust\":-1,\"modSpeakers\":-1,\"modRearBumper\":-1,\"tyreSmokeColor\":[255,255,255],\"modFrame\":-1,\"modFrontBumper\":-1,\"modSuspension\":-1,\"modEngineBlock\":-1,\"modTurbo\":false,\"modSmokeEnabled\":false,\"modTransmission\":-1,\"modGrille\":-1,\"modAirFilter\":-1,\"modRoof\":-1,\"modBrakes\":-1,\"modTrimA\":-1,\"model\":1027958099,\"engineHealth\":1000.0,\"modHood\":-1,\"modFrontWheels\":-1,\"modRightFender\":-1,\"modBackWheels\":-1,\"windowTint\":-1,\"color1\":134,\"modArchCover\":-1,\"modShifterLeavers\":-1,\"modSideSkirt\":-1,\"modStruts\":-1,\"dirtLevel\":6.0,\"modEngine\":-1,\"modSpoilers\":-1,\"modOrnaments\":-1}', 'car', 'police', 1),
('steam:11000013e33b934', 'DPN 185', '{\"modTrimA\":-1,\"modBrakes\":-1,\"modFender\":-1,\"modHood\":-1,\"modSmokeEnabled\":false,\"color1\":29,\"modTrunk\":-1,\"modAirFilter\":-1,\"modGrille\":-1,\"modPlateHolder\":-1,\"plate\":\"DPN 185\",\"modLivery\":-1,\"modRoof\":-1,\"modTrimB\":-1,\"modAPlate\":-1,\"modSideSkirt\":-1,\"modTurbo\":false,\"fuelLevel\":65.0,\"wheels\":3,\"modAerials\":-1,\"model\":-301519603,\"modXenon\":false,\"dirtLevel\":9.0,\"modRightFender\":-1,\"modBackWheels\":-1,\"modFrontWheels\":-1,\"modSpoilers\":-1,\"pearlescentColor\":29,\"modDial\":-1,\"modDoorSpeaker\":-1,\"tyreSmokeColor\":[255,255,255],\"color2\":0,\"bodyHealth\":1000.0,\"neonEnabled\":[false,false,false,false],\"modSteeringWheel\":-1,\"modSpeakers\":-1,\"modTransmission\":-1,\"modSeats\":-1,\"modVanityPlate\":-1,\"modRearBumper\":-1,\"modStruts\":-1,\"modExhaust\":-1,\"modOrnaments\":-1,\"plateIndex\":4,\"modShifterLeavers\":-1,\"modFrontBumper\":-1,\"modSuspension\":-1,\"modArchCover\":-1,\"modArmor\":-1,\"modDashboard\":-1,\"modHorns\":-1,\"modEngine\":-1,\"modHydrolic\":-1,\"windowTint\":-1,\"extras\":{\"3\":true,\"2\":true,\"1\":true},\"modFrame\":-1,\"neonColor\":[255,0,255],\"modEngineBlock\":-1,\"modTank\":-1,\"engineHealth\":1000.0,\"wheelColor\":0,\"modWindows\":-1}', 'car', NULL, 1),
('steam:1100001081d1893', 'DUH 545', '{\"modSideSkirt\":-1,\"wheelColor\":156,\"modSteeringWheel\":-1,\"bodyHealth\":1000.0,\"modGrille\":-1,\"modSuspension\":-1,\"modFrame\":-1,\"windowTint\":-1,\"plate\":\"DUH 545\",\"dirtLevel\":0.0,\"modSmokeEnabled\":false,\"modAirFilter\":-1,\"modAerials\":-1,\"modTrimA\":-1,\"modDoorSpeaker\":-1,\"modArmor\":-1,\"modSeats\":-1,\"plateIndex\":4,\"extras\":{\"1\":false,\"12\":false,\"11\":true,\"10\":false,\"5\":true,\"4\":false,\"7\":false,\"6\":false,\"9\":false,\"8\":false,\"2\":false,\"3\":false},\"modSpeakers\":-1,\"modHorns\":-1,\"modTransmission\":-1,\"modEngineBlock\":-1,\"model\":1745872177,\"modWindows\":-1,\"engineHealth\":1000.0,\"modAPlate\":-1,\"modPlateHolder\":-1,\"modEngine\":-1,\"fuelLevel\":84.8,\"modVanityPlate\":-1,\"modTurbo\":false,\"modRearBumper\":-1,\"modFrontBumper\":-1,\"modExhaust\":-1,\"modRightFender\":-1,\"modTank\":-1,\"color2\":134,\"modDashboard\":-1,\"color1\":134,\"modDial\":-1,\"neonEnabled\":[false,false,false,false],\"neonColor\":[255,0,255],\"modXenon\":false,\"modSpoilers\":-1,\"modShifterLeavers\":-1,\"modRoof\":-1,\"modBrakes\":-1,\"modHood\":-1,\"modFender\":-1,\"modLivery\":0,\"modTrimB\":-1,\"tyreSmokeColor\":[255,255,255],\"modBackWheels\":-1,\"modTrunk\":-1,\"modHydrolic\":-1,\"pearlescentColor\":156,\"modArchCover\":-1,\"wheels\":1,\"modStruts\":-1,\"modOrnaments\":-1,\"modFrontWheels\":-1}', 'car', 'police', 1),
('steam:11000010fe595d0', 'FCJ 140', '{\"windowTint\":-1,\"modSmokeEnabled\":false,\"modDial\":-1,\"modPlateHolder\":-1,\"dirtLevel\":0.0,\"color1\":134,\"modTransmission\":-1,\"extras\":{\"10\":false,\"11\":true,\"6\":false,\"7\":false,\"9\":false,\"1\":false,\"8\":false,\"12\":false,\"4\":false,\"5\":false,\"2\":true,\"3\":false},\"modOrnaments\":-1,\"engineHealth\":1000.0,\"modArchCover\":-1,\"modAPlate\":-1,\"modAerials\":-1,\"modDashboard\":-1,\"modTurbo\":false,\"modEngineBlock\":-1,\"modAirFilter\":-1,\"modTrunk\":-1,\"modRearBumper\":-1,\"modStruts\":-1,\"modTrimB\":-1,\"fuelLevel\":84.1,\"modLivery\":1,\"modShifterLeavers\":-1,\"neonColor\":[255,0,255],\"modHydrolic\":-1,\"modSpoilers\":-1,\"modSteeringWheel\":-1,\"modVanityPlate\":-1,\"modDoorSpeaker\":-1,\"modTrimA\":-1,\"modGrille\":-1,\"modSideSkirt\":-1,\"modRoof\":-1,\"modBackWheels\":-1,\"modWindows\":-1,\"modHorns\":-1,\"wheels\":1,\"modSpeakers\":-1,\"modXenon\":false,\"model\":1357997983,\"modFrontBumper\":-1,\"modSeats\":-1,\"tyreSmokeColor\":[255,255,255],\"wheelColor\":156,\"plateIndex\":4,\"modEngine\":-1,\"modArmor\":-1,\"modBrakes\":-1,\"pearlescentColor\":156,\"color2\":134,\"modHood\":-1,\"modFrontWheels\":-1,\"modExhaust\":-1,\"modSuspension\":-1,\"modFender\":-1,\"neonEnabled\":[false,false,false,false],\"modRightFender\":-1,\"bodyHealth\":1000.0,\"modTank\":-1,\"modFrame\":-1,\"plate\":\"FCJ 140\"}', 'car', 'police', 1),
('steam:11000010b49a743', 'FHQ 779', '{\"modSteeringWheel\":-1,\"modRightFender\":-1,\"modRearBumper\":-1,\"modSmokeEnabled\":false,\"modFrame\":-1,\"modShifterLeavers\":-1,\"modExhaust\":-1,\"neonColor\":[255,0,255],\"modSuspension\":-1,\"modTrimB\":-1,\"modWindows\":-1,\"modTransmission\":-1,\"modSpeakers\":-1,\"color2\":134,\"modEngine\":-1,\"modAerials\":-1,\"modSeats\":-1,\"pearlescentColor\":156,\"modDoorSpeaker\":-1,\"modFrontBumper\":-1,\"wheels\":0,\"plateIndex\":4,\"extras\":{\"5\":false,\"4\":false,\"7\":false,\"6\":false,\"9\":false,\"8\":false,\"12\":true,\"10\":false,\"11\":false,\"1\":false,\"3\":false,\"2\":false},\"modFender\":-1,\"modAirFilter\":-1,\"modEngineBlock\":-1,\"modFrontWheels\":-1,\"tyreSmokeColor\":[255,255,255],\"modSpoilers\":-1,\"modBrakes\":-1,\"modHorns\":-1,\"modDial\":-1,\"modTrunk\":-1,\"modHood\":-1,\"bodyHealth\":1000.0,\"modTank\":-1,\"modTurbo\":false,\"modOrnaments\":-1,\"modVanityPlate\":-1,\"dirtLevel\":0.0,\"fuelLevel\":89.6,\"modArchCover\":-1,\"modXenon\":false,\"modGrille\":-1,\"plate\":\"FHQ 779\",\"modStruts\":-1,\"modPlateHolder\":-1,\"neonEnabled\":[false,false,false,false],\"engineHealth\":1000.0,\"modAPlate\":-1,\"modRoof\":-1,\"model\":-1059115956,\"modSideSkirt\":-1,\"modBackWheels\":-1,\"modHydrolic\":-1,\"color1\":134,\"modTrimA\":-1,\"modLivery\":3,\"modDashboard\":-1,\"windowTint\":-1,\"modArmor\":-1,\"wheelColor\":156}', 'car', 'police', 1),
('steam:1100001081d1893', 'GJV 215', '{\"modSeats\":-1,\"modTrimA\":-1,\"modHorns\":-1,\"fuelLevel\":73.2,\"modHydrolic\":-1,\"plateIndex\":3,\"tyreSmokeColor\":[255,255,255],\"modDashboard\":-1,\"modBackWheels\":-1,\"modGrille\":-1,\"modTrimB\":-1,\"modArchCover\":-1,\"modSideSkirt\":-1,\"modSteeringWheel\":-1,\"modTank\":-1,\"neonEnabled\":[false,false,false,false],\"modFender\":-1,\"modSpeakers\":-1,\"modDial\":-1,\"modRearBumper\":-1,\"modShifterLeavers\":-1,\"bodyHealth\":998.3,\"modRightFender\":-1,\"modTransmission\":-1,\"modAirFilter\":-1,\"modFrame\":-1,\"modAPlate\":-1,\"pearlescentColor\":23,\"modAerials\":-1,\"modExhaust\":-1,\"extras\":[],\"color1\":3,\"color2\":3,\"modEngine\":-1,\"modLivery\":-1,\"modWindows\":-1,\"modRoof\":-1,\"modDoorSpeaker\":-1,\"windowTint\":-1,\"modVanityPlate\":-1,\"modSmokeEnabled\":false,\"modHood\":-1,\"modBrakes\":-1,\"modFrontBumper\":-1,\"modFrontWheels\":-1,\"wheelColor\":156,\"dirtLevel\":7.2,\"neonColor\":[255,0,255],\"modSuspension\":-1,\"modTrunk\":-1,\"modOrnaments\":-1,\"modTurbo\":false,\"modSpoilers\":-1,\"wheels\":5,\"engineHealth\":997.4,\"modStruts\":-1,\"modArmor\":-1,\"plate\":\"GJV 215\",\"modPlateHolder\":-1,\"model\":1373123368,\"modEngineBlock\":-1,\"modXenon\":false}', 'car', NULL, 1),
('steam:11000010fe595d0', 'GNN 441', '{\"modXenon\":false,\"modTrunk\":-1,\"modDashboard\":-1,\"wheels\":1,\"wheelColor\":156,\"modHydrolic\":-1,\"modSteeringWheel\":-1,\"neonEnabled\":[false,false,false,false],\"plate\":\"GNN 441\",\"modTrimB\":-1,\"modAPlate\":-1,\"color2\":134,\"modDial\":-1,\"modPlateHolder\":-1,\"pearlescentColor\":156,\"bodyHealth\":1000.0,\"modHorns\":-1,\"fuelLevel\":87.2,\"modWindows\":-1,\"modFender\":-1,\"modTank\":-1,\"modDoorSpeaker\":-1,\"modSeats\":-1,\"modVanityPlate\":-1,\"modAerials\":-1,\"modArmor\":-1,\"plateIndex\":4,\"modLivery\":1,\"extras\":{\"2\":false,\"1\":true,\"12\":false,\"11\":true,\"8\":false,\"7\":false,\"10\":false,\"9\":false,\"4\":false,\"3\":false,\"6\":false,\"5\":false},\"neonColor\":[255,0,255],\"modExhaust\":-1,\"modSpeakers\":-1,\"modRearBumper\":-1,\"tyreSmokeColor\":[255,255,255],\"modFrame\":-1,\"modFrontBumper\":-1,\"modSuspension\":-1,\"modEngineBlock\":-1,\"modTurbo\":false,\"modSmokeEnabled\":false,\"modTransmission\":-1,\"modGrille\":-1,\"modAirFilter\":-1,\"modRoof\":-1,\"modBrakes\":-1,\"modTrimA\":-1,\"model\":1357997983,\"engineHealth\":1000.0,\"modHood\":-1,\"modFrontWheels\":-1,\"modRightFender\":-1,\"modBackWheels\":-1,\"windowTint\":-1,\"color1\":134,\"modArchCover\":-1,\"modShifterLeavers\":-1,\"modSideSkirt\":-1,\"modStruts\":-1,\"dirtLevel\":0.0,\"modEngine\":-1,\"modSpoilers\":-1,\"modOrnaments\":-1}', 'car', 'police', 1),
('steam:11000010e2a62e2', 'HEZ 969', '{\"modPlateHolder\":-1,\"modArchCover\":-1,\"bodyHealth\":1000.0,\"model\":1032823388,\"modAPlate\":-1,\"modBrakes\":-1,\"color1\":31,\"modXenon\":false,\"modDoorSpeaker\":-1,\"plate\":\"HEZ 969\",\"windowTint\":-1,\"modTurbo\":false,\"modHorns\":-1,\"modTransmission\":-1,\"modExhaust\":-1,\"modFrame\":-1,\"modSmokeEnabled\":false,\"neonColor\":[255,0,255],\"modOrnaments\":-1,\"modSpeakers\":-1,\"modTank\":-1,\"modRightFender\":-1,\"modEngineBlock\":-1,\"pearlescentColor\":32,\"modStruts\":-1,\"modFrontBumper\":-1,\"modEngine\":-1,\"modVanityPlate\":-1,\"modRoof\":-1,\"modWindows\":-1,\"modSuspension\":-1,\"modDial\":-1,\"modFender\":-1,\"modSpoilers\":-1,\"wheels\":7,\"modAerials\":-1,\"modArmor\":-1,\"modSeats\":-1,\"modGrille\":-1,\"tyreSmokeColor\":[255,255,255],\"modBackWheels\":-1,\"modTrimA\":-1,\"modFrontWheels\":-1,\"modSideSkirt\":-1,\"fuelLevel\":65.0,\"modSteeringWheel\":-1,\"modRearBumper\":-1,\"modHydrolic\":-1,\"dirtLevel\":9.0,\"modShifterLeavers\":-1,\"neonEnabled\":[false,false,false,false],\"modHood\":-1,\"modAirFilter\":-1,\"engineHealth\":1000.0,\"color2\":0,\"wheelColor\":156,\"modLivery\":-1,\"extras\":{\"10\":false,\"12\":true},\"plateIndex\":0,\"modTrunk\":-1,\"modTrimB\":-1,\"modDashboard\":-1}', 'car', NULL, 1),
('steam:110000133d93ea2', 'HHT 377', '{\"modExhaust\":-1,\"modHydrolic\":-1,\"modTurbo\":false,\"modArchCover\":-1,\"modRearBumper\":-1,\"windowTint\":-1,\"modTrimB\":-1,\"modAPlate\":-1,\"modTrimA\":-1,\"modPlateHolder\":-1,\"dirtLevel\":0.0,\"modDoorSpeaker\":-1,\"modRightFender\":-1,\"tyreSmokeColor\":[255,255,255],\"modHood\":-1,\"modFender\":-1,\"color2\":88,\"modGrille\":-1,\"modLivery\":-1,\"modWindows\":-1,\"modSideSkirt\":-1,\"engineHealth\":1000.0,\"modOrnaments\":-1,\"modStruts\":-1,\"fuelLevel\":65.0,\"modDial\":-1,\"modXenon\":false,\"modTank\":-1,\"modFrontBumper\":-1,\"plate\":\"HHT 377\",\"modBackWheels\":-1,\"modAirFilter\":-1,\"neonColor\":[255,0,255],\"modDashboard\":-1,\"modSmokeEnabled\":false,\"modEngine\":-1,\"modSpoilers\":-1,\"modTransmission\":-1,\"modArmor\":-1,\"modVanityPlate\":-1,\"neonEnabled\":[false,false,false,false],\"modShifterLeavers\":-1,\"modHorns\":-1,\"modBrakes\":-1,\"pearlescentColor\":88,\"color1\":88,\"plateIndex\":3,\"wheelColor\":156,\"modSuspension\":-1,\"modFrontWheels\":-1,\"modFrame\":-1,\"modTrunk\":-1,\"modRoof\":-1,\"modEngineBlock\":-1,\"modSeats\":-1,\"modSpeakers\":-1,\"wheels\":6,\"bodyHealth\":1000.0,\"model\":1672195559,\"extras\":{\"1\":true,\"4\":true,\"9\":true},\"modAerials\":-1,\"modSteeringWheel\":-1}', 'car', NULL, 1),
('steam:11000010b49a743', 'HKZ 176', '{\"modTank\":-1,\"modHorns\":-1,\"modEngineBlock\":-1,\"dirtLevel\":0.0,\"modSeats\":-1,\"bodyHealth\":1000.0,\"modArmor\":-1,\"wheels\":1,\"model\":-1616359240,\"windowTint\":-1,\"modSpeakers\":-1,\"modArchCover\":-1,\"plateIndex\":4,\"modAirFilter\":-1,\"modBrakes\":-1,\"modExhaust\":-1,\"modHydrolic\":-1,\"modFrontBumper\":-1,\"wheelColor\":156,\"modAerials\":-1,\"modBackWheels\":-1,\"fuelLevel\":88.2,\"modFender\":-1,\"modShifterLeavers\":-1,\"modTrimB\":-1,\"modRoof\":-1,\"modSpoilers\":-1,\"neonColor\":[255,0,255],\"modOrnaments\":-1,\"neonEnabled\":[false,false,false,false],\"modXenon\":false,\"modDial\":-1,\"modSmokeEnabled\":false,\"modSuspension\":-1,\"modLivery\":0,\"modDashboard\":-1,\"color1\":134,\"modTrimA\":-1,\"pearlescentColor\":156,\"modRightFender\":-1,\"modGrille\":-1,\"modTrunk\":-1,\"modSteeringWheel\":-1,\"modTurbo\":false,\"plate\":\"HKZ 176\",\"modAPlate\":-1,\"modEngine\":-1,\"modVanityPlate\":-1,\"modPlateHolder\":-1,\"modSideSkirt\":-1,\"color2\":134,\"tyreSmokeColor\":[255,255,255],\"modRearBumper\":-1,\"modHood\":-1,\"engineHealth\":1000.0,\"modDoorSpeaker\":-1,\"modStruts\":-1,\"modFrontWheels\":-1,\"modWindows\":-1,\"extras\":{\"2\":false,\"1\":false,\"4\":false,\"3\":false,\"7\":true,\"9\":false,\"12\":false,\"11\":false,\"6\":false,\"5\":false,\"8\":true,\"10\":true},\"modFrame\":-1,\"modTransmission\":-1}', 'car', 'police', 1),
('steam:11000013f6b85e1', 'HQR 873', '{\"modStruts\":-1,\"modTank\":-1,\"modAPlate\":-1,\"tyreSmokeColor\":[255,255,255],\"modTrunk\":-1,\"modXenon\":false,\"modSeats\":-1,\"color1\":134,\"modDoorSpeaker\":-1,\"plate\":\"HQR 873\",\"modTransmission\":-1,\"modAirFilter\":-1,\"modTrimB\":-1,\"modArmor\":-1,\"engineHealth\":1000.0,\"wheels\":1,\"modShifterLeavers\":-1,\"color2\":134,\"modOrnaments\":-1,\"modArchCover\":-1,\"extras\":{\"12\":false,\"1\":false,\"4\":false,\"10\":false,\"5\":false,\"11\":true,\"3\":false,\"2\":false,\"9\":false,\"8\":false,\"7\":false,\"6\":true},\"modDial\":-1,\"modExhaust\":-1,\"modGrille\":-1,\"modFrontBumper\":-1,\"modEngine\":-1,\"modHydrolic\":-1,\"plateIndex\":4,\"modTurbo\":false,\"modRearBumper\":-1,\"modFender\":-1,\"windowTint\":-1,\"modSpeakers\":-1,\"modHood\":-1,\"modVanityPlate\":-1,\"neonColor\":[255,0,255],\"modPlateHolder\":-1,\"modSuspension\":-1,\"modBackWheels\":-1,\"modRightFender\":-1,\"modBrakes\":-1,\"modDashboard\":-1,\"modFrontWheels\":-1,\"modHorns\":-1,\"bodyHealth\":1000.0,\"modEngineBlock\":-1,\"modSmokeEnabled\":false,\"neonEnabled\":[false,false,false,false],\"modWindows\":-1,\"pearlescentColor\":156,\"modSteeringWheel\":-1,\"dirtLevel\":0.0,\"fuelLevel\":88.8,\"modSideSkirt\":-1,\"wheelColor\":156,\"modAerials\":-1,\"model\":1745872177,\"modRoof\":-1,\"modLivery\":2,\"modFrame\":-1,\"modTrimA\":-1,\"modSpoilers\":-1}', 'car', 'police', 1),
('steam:1100001081d1893', 'IRW 235', '{\"modSeats\":-1,\"modTrimA\":-1,\"modHorns\":-1,\"fuelLevel\":75.2,\"modHydrolic\":-1,\"plateIndex\":0,\"tyreSmokeColor\":[255,255,255],\"modDashboard\":-1,\"modBackWheels\":-1,\"modGrille\":-1,\"modTrimB\":-1,\"modArchCover\":-1,\"modSideSkirt\":-1,\"modSteeringWheel\":-1,\"modTank\":-1,\"neonEnabled\":[false,false,false,false],\"modFender\":-1,\"modSpeakers\":-1,\"modDial\":-1,\"modRearBumper\":-1,\"modShifterLeavers\":-1,\"bodyHealth\":1000.0,\"modRightFender\":-1,\"modTransmission\":-1,\"modAirFilter\":-1,\"modFrame\":-1,\"modAPlate\":-1,\"pearlescentColor\":64,\"modAerials\":-1,\"modExhaust\":-1,\"extras\":[],\"color1\":64,\"color2\":0,\"modEngine\":-1,\"modLivery\":-1,\"modWindows\":-1,\"modRoof\":-1,\"modDoorSpeaker\":-1,\"windowTint\":-1,\"modVanityPlate\":-1,\"modSmokeEnabled\":false,\"modHood\":-1,\"modBrakes\":-1,\"modFrontBumper\":-1,\"modFrontWheels\":-1,\"wheelColor\":156,\"dirtLevel\":7.0,\"neonColor\":[255,255,255],\"modSuspension\":-1,\"modTrunk\":-1,\"modOrnaments\":-1,\"modTurbo\":false,\"modSpoilers\":-1,\"wheels\":6,\"engineHealth\":1000.0,\"modStruts\":-1,\"modArmor\":-1,\"plate\":\"IRW 235\",\"modPlateHolder\":-1,\"model\":-114291515,\"modEngineBlock\":-1,\"modXenon\":false}', 'car', NULL, 1),
('steam:11000010b49a743', 'ISR 383', '{\"modTank\":-1,\"modHorns\":-1,\"modEngineBlock\":-1,\"dirtLevel\":0.0,\"modSeats\":-1,\"bodyHealth\":1000.0,\"modArmor\":-1,\"wheels\":1,\"model\":1418298348,\"windowTint\":-1,\"modSpeakers\":-1,\"modArchCover\":-1,\"plateIndex\":4,\"modAirFilter\":-1,\"modBrakes\":-1,\"modExhaust\":-1,\"modHydrolic\":-1,\"modFrontBumper\":-1,\"wheelColor\":156,\"modAerials\":-1,\"modBackWheels\":-1,\"fuelLevel\":75.7,\"modFender\":-1,\"modShifterLeavers\":-1,\"modTrimB\":-1,\"modRoof\":-1,\"modSpoilers\":-1,\"neonColor\":[255,0,255],\"modOrnaments\":-1,\"neonEnabled\":[false,false,false,false],\"modXenon\":false,\"modDial\":-1,\"modSmokeEnabled\":false,\"modSuspension\":-1,\"modLivery\":3,\"modDashboard\":-1,\"color1\":134,\"modTrimA\":-1,\"pearlescentColor\":156,\"modRightFender\":-1,\"modGrille\":-1,\"modTrunk\":-1,\"modSteeringWheel\":-1,\"modTurbo\":false,\"plate\":\"ISR 383\",\"modAPlate\":-1,\"modEngine\":-1,\"modVanityPlate\":-1,\"modPlateHolder\":-1,\"modSideSkirt\":-1,\"color2\":134,\"tyreSmokeColor\":[255,255,255],\"modRearBumper\":-1,\"modHood\":-1,\"engineHealth\":1000.0,\"modDoorSpeaker\":-1,\"modStruts\":-1,\"modFrontWheels\":-1,\"modWindows\":-1,\"extras\":{\"2\":true,\"1\":false,\"4\":false,\"3\":false,\"7\":false,\"9\":false,\"12\":true,\"11\":false,\"6\":false,\"5\":false,\"8\":false,\"10\":false},\"modFrame\":-1,\"modTransmission\":-1}', 'car', 'police', 1),
('steam:11000010b49a743', 'IVL 836', '{\"model\":1027958099,\"modBrakes\":-1,\"bodyHealth\":1000.0,\"modStruts\":-1,\"modTrimA\":-1,\"extras\":{\"1\":true,\"2\":false,\"3\":false,\"4\":false,\"5\":false,\"6\":false,\"7\":false,\"8\":false,\"9\":false,\"10\":false,\"11\":true,\"12\":false},\"plate\":\"IVL 836\",\"dirtLevel\":4.0,\"neonColor\":[255,0,255],\"modArchCover\":-1,\"tyreSmokeColor\":[255,255,255],\"modAirFilter\":-1,\"fuelLevel\":75.3,\"modSpeakers\":-1,\"modPlateHolder\":-1,\"pearlescentColor\":156,\"modBackWheels\":-1,\"modTrimB\":-1,\"modEngine\":-1,\"modFender\":-1,\"modTurbo\":false,\"modTank\":-1,\"engineHealth\":1000.0,\"color1\":134,\"modRightFender\":-1,\"color2\":134,\"modHydrolic\":-1,\"modExhaust\":-1,\"modHorns\":-1,\"modXenon\":false,\"modWindows\":-1,\"wheelColor\":156,\"modVanityPlate\":-1,\"modTrunk\":-1,\"modSeats\":-1,\"neonEnabled\":[false,false,false,false],\"modAerials\":-1,\"modAPlate\":-1,\"modTransmission\":-1,\"modGrille\":-1,\"modDashboard\":-1,\"modFrontBumper\":-1,\"modLivery\":2,\"modSuspension\":-1,\"modEngineBlock\":-1,\"windowTint\":-1,\"modSmokeEnabled\":false,\"modFrontWheels\":-1,\"modSideSkirt\":-1,\"modFrame\":-1,\"wheels\":1,\"modSpoilers\":-1,\"modDial\":-1,\"modShifterLeavers\":-1,\"modDoorSpeaker\":-1,\"modRearBumper\":-1,\"modOrnaments\":-1,\"modArmor\":-1,\"modSteeringWheel\":-1,\"modHood\":-1,\"modRoof\":-1,\"plateIndex\":4}', 'car', 'police', 1),
('steam:11000013f6b85e1', 'IXW 287', '{\"windowTint\":-1,\"modSmokeEnabled\":false,\"modDial\":-1,\"modPlateHolder\":-1,\"dirtLevel\":0.0,\"modOrnaments\":-1,\"modTransmission\":-1,\"extras\":{\"10\":false,\"12\":false,\"6\":false,\"7\":false,\"9\":false,\"1\":true,\"8\":false,\"11\":true,\"4\":false,\"5\":false,\"2\":false,\"3\":false},\"modRightFender\":-1,\"engineHealth\":1000.0,\"modVanityPlate\":-1,\"modAPlate\":-1,\"modAerials\":-1,\"modTrunk\":-1,\"modTurbo\":false,\"modEngineBlock\":-1,\"modAirFilter\":-1,\"model\":1027958099,\"neonEnabled\":[false,false,false,false],\"modStruts\":-1,\"modTrimB\":-1,\"modHorns\":-1,\"modTank\":-1,\"modShifterLeavers\":-1,\"modDashboard\":-1,\"modHydrolic\":-1,\"modSpoilers\":-1,\"modSteeringWheel\":-1,\"modRoof\":-1,\"plate\":\"IXW 287\",\"modArchCover\":-1,\"plateIndex\":4,\"modSideSkirt\":-1,\"modGrille\":-1,\"modBackWheels\":-1,\"color1\":134,\"neonColor\":[255,0,255],\"wheels\":1,\"modFrame\":-1,\"modXenon\":false,\"modBrakes\":-1,\"modFrontBumper\":-1,\"wheelColor\":156,\"tyreSmokeColor\":[255,255,255],\"modTrimA\":-1,\"modExhaust\":-1,\"modLivery\":3,\"modArmor\":-1,\"modSeats\":-1,\"modWindows\":-1,\"color2\":134,\"modHood\":-1,\"modFrontWheels\":-1,\"modDoorSpeaker\":-1,\"modSuspension\":-1,\"modFender\":-1,\"modEngine\":-1,\"fuelLevel\":76.5,\"bodyHealth\":1000.0,\"modSpeakers\":-1,\"modRearBumper\":-1,\"pearlescentColor\":156}', 'car', 'police', 1),
('steam:11000013f6b85e1', 'JDP 890', '{\"modStruts\":-1,\"modTank\":-1,\"modAPlate\":-1,\"tyreSmokeColor\":[255,255,255],\"modTrunk\":-1,\"modXenon\":false,\"modSeats\":-1,\"color1\":134,\"modDoorSpeaker\":-1,\"plate\":\"JDP 890\",\"modTransmission\":-1,\"modAirFilter\":-1,\"modTrimB\":-1,\"modArmor\":-1,\"engineHealth\":1000.0,\"wheels\":1,\"modShifterLeavers\":-1,\"color2\":134,\"modOrnaments\":-1,\"modArchCover\":-1,\"extras\":{\"12\":false,\"1\":false,\"4\":false,\"10\":false,\"5\":false,\"11\":true,\"3\":false,\"2\":false,\"9\":false,\"8\":false,\"7\":true,\"6\":false},\"modDial\":-1,\"modExhaust\":-1,\"modGrille\":-1,\"modFrontBumper\":-1,\"modEngine\":-1,\"modHydrolic\":-1,\"plateIndex\":4,\"modTurbo\":false,\"modRearBumper\":-1,\"modFender\":-1,\"windowTint\":-1,\"modSpeakers\":-1,\"modHood\":-1,\"modVanityPlate\":-1,\"neonColor\":[255,0,255],\"modPlateHolder\":-1,\"modSuspension\":-1,\"modBackWheels\":-1,\"modRightFender\":-1,\"modBrakes\":-1,\"modDashboard\":-1,\"modFrontWheels\":-1,\"modHorns\":-1,\"bodyHealth\":1000.0,\"modEngineBlock\":-1,\"modSmokeEnabled\":false,\"neonEnabled\":[false,false,false,false],\"modWindows\":-1,\"pearlescentColor\":156,\"modSteeringWheel\":-1,\"dirtLevel\":0.0,\"fuelLevel\":78.0,\"modSideSkirt\":-1,\"wheelColor\":156,\"modAerials\":-1,\"model\":-1616359240,\"modRoof\":-1,\"modLivery\":2,\"modFrame\":-1,\"modTrimA\":-1,\"modSpoilers\":-1}', 'car', 'police', 1),
('steam:11000013f6b85e1', 'KGN 356', '{\"modXenon\":false,\"modTrunk\":-1,\"modDashboard\":-1,\"wheels\":1,\"modShifterLeavers\":-1,\"modHydrolic\":-1,\"modSteeringWheel\":-1,\"neonEnabled\":[false,false,false,false],\"plate\":\"KGN 356\",\"neonColor\":[255,0,255],\"modAPlate\":-1,\"color2\":134,\"modDial\":-1,\"color1\":134,\"modFender\":-1,\"pearlescentColor\":156,\"modHorns\":-1,\"fuelLevel\":94.5,\"modTrimB\":-1,\"modTurbo\":false,\"modWindows\":-1,\"modDoorSpeaker\":-1,\"modSeats\":-1,\"modVanityPlate\":-1,\"modAerials\":-1,\"modRightFender\":-1,\"plateIndex\":4,\"modLivery\":1,\"modArmor\":-1,\"extras\":{\"2\":false,\"1\":false,\"12\":false,\"11\":false,\"8\":false,\"7\":false,\"10\":true,\"9\":false,\"4\":false,\"3\":false,\"6\":false,\"5\":true},\"modExhaust\":-1,\"modSpeakers\":-1,\"modRearBumper\":-1,\"tyreSmokeColor\":[255,255,255],\"modFrame\":-1,\"modGrille\":-1,\"modSuspension\":-1,\"modEngineBlock\":-1,\"modAirFilter\":-1,\"modSmokeEnabled\":false,\"modTransmission\":-1,\"modTank\":-1,\"modFrontBumper\":-1,\"modRoof\":-1,\"modBrakes\":-1,\"modTrimA\":-1,\"model\":1027958099,\"engineHealth\":1000.0,\"modHood\":-1,\"modFrontWheels\":-1,\"modSideSkirt\":-1,\"modBackWheels\":-1,\"windowTint\":-1,\"modPlateHolder\":-1,\"modArchCover\":-1,\"dirtLevel\":0.0,\"wheelColor\":156,\"modStruts\":-1,\"bodyHealth\":1000.0,\"modEngine\":-1,\"modSpoilers\":-1,\"modOrnaments\":-1}', 'car', 'police', 1),
('steam:110000105ed368b', 'LRR 177', '{\"modTank\":-1,\"neonColor\":[255,0,255],\"modEngineBlock\":-1,\"dirtLevel\":3.0,\"modSeats\":-1,\"bodyHealth\":1000.0,\"modArmor\":-1,\"wheels\":1,\"model\":161178935,\"windowTint\":-1,\"modSpeakers\":-1,\"neonEnabled\":[false,false,false,false],\"plateIndex\":4,\"modAirFilter\":-1,\"modBrakes\":-1,\"modExhaust\":-1,\"modShifterLeavers\":-1,\"modHydrolic\":-1,\"modArchCover\":-1,\"modAerials\":-1,\"modSpoilers\":-1,\"fuelLevel\":89.2,\"modFender\":-1,\"modHorns\":-1,\"modVanityPlate\":-1,\"modRoof\":-1,\"modBackWheels\":-1,\"modLivery\":0,\"modOrnaments\":-1,\"modTurbo\":false,\"modXenon\":false,\"modSuspension\":-1,\"modSmokeEnabled\":false,\"color1\":134,\"wheelColor\":156,\"modRearBumper\":-1,\"modPlateHolder\":-1,\"modTrimA\":-1,\"pearlescentColor\":156,\"modRightFender\":-1,\"modGrille\":-1,\"modTrunk\":-1,\"modSteeringWheel\":-1,\"modTrimB\":-1,\"plate\":\"LRR 177\",\"modAPlate\":-1,\"modEngine\":-1,\"modFrontBumper\":-1,\"modSideSkirt\":-1,\"modDial\":-1,\"color2\":134,\"modDashboard\":-1,\"tyreSmokeColor\":[255,255,255],\"modHood\":-1,\"engineHealth\":1000.0,\"modDoorSpeaker\":-1,\"modStruts\":-1,\"modFrontWheels\":-1,\"modWindows\":-1,\"extras\":{\"2\":false,\"1\":true,\"4\":false,\"3\":false,\"10\":false,\"9\":false,\"12\":false,\"11\":false,\"6\":false,\"5\":false,\"8\":false,\"7\":false},\"modFrame\":-1,\"modTransmission\":-1}', 'car', 'police', 1),
('steam:11000010b49a743', 'MGL 230', '{\"modTank\":-1,\"modHorns\":-1,\"modEngineBlock\":-1,\"dirtLevel\":2.0,\"modSeats\":-1,\"bodyHealth\":1000.0,\"modArmor\":-1,\"wheels\":0,\"model\":-1059115956,\"windowTint\":-1,\"modSpeakers\":-1,\"modArchCover\":-1,\"plateIndex\":4,\"modAirFilter\":-1,\"modBrakes\":-1,\"modExhaust\":-1,\"modHydrolic\":-1,\"modFrontBumper\":-1,\"wheelColor\":156,\"modAerials\":-1,\"modBackWheels\":-1,\"fuelLevel\":90.5,\"modFender\":-1,\"modShifterLeavers\":-1,\"modTrimB\":-1,\"modRoof\":-1,\"modSpoilers\":-1,\"neonColor\":[255,0,255],\"modOrnaments\":-1,\"neonEnabled\":[false,false,false,false],\"modXenon\":false,\"modDial\":-1,\"modSmokeEnabled\":false,\"modSuspension\":-1,\"modLivery\":3,\"modDashboard\":-1,\"color1\":134,\"modTrimA\":-1,\"pearlescentColor\":156,\"modRightFender\":-1,\"modGrille\":-1,\"modTrunk\":-1,\"modSteeringWheel\":-1,\"modTurbo\":false,\"plate\":\"MGL 230\",\"modAPlate\":-1,\"modEngine\":-1,\"modVanityPlate\":-1,\"modPlateHolder\":-1,\"modSideSkirt\":-1,\"color2\":134,\"tyreSmokeColor\":[255,255,255],\"modRearBumper\":-1,\"modHood\":-1,\"engineHealth\":1000.0,\"modDoorSpeaker\":-1,\"modStruts\":-1,\"modFrontWheels\":-1,\"modWindows\":-1,\"extras\":{\"2\":true,\"1\":false,\"4\":false,\"3\":false,\"7\":false,\"9\":false,\"12\":false,\"11\":false,\"6\":false,\"5\":false,\"8\":false,\"10\":false},\"modFrame\":-1,\"modTransmission\":-1}', 'car', 'police', 1),
('steam:11000010b49a743', 'MPV 719', '{\"modSteeringWheel\":-1,\"modRightFender\":-1,\"modRearBumper\":-1,\"modSmokeEnabled\":false,\"modFrame\":-1,\"modShifterLeavers\":-1,\"modExhaust\":-1,\"neonColor\":[255,0,255],\"modSuspension\":-1,\"modTrimB\":-1,\"modWindows\":-1,\"modTransmission\":-1,\"modSpeakers\":-1,\"color2\":134,\"modEngine\":-1,\"modAerials\":-1,\"modSeats\":-1,\"pearlescentColor\":156,\"modDoorSpeaker\":-1,\"modFrontBumper\":-1,\"wheels\":0,\"plateIndex\":4,\"extras\":{\"5\":false,\"4\":false,\"7\":false,\"6\":false,\"9\":false,\"8\":false,\"12\":true,\"10\":false,\"11\":false,\"1\":false,\"3\":false,\"2\":false},\"modFender\":-1,\"modAirFilter\":-1,\"modEngineBlock\":-1,\"modFrontWheels\":-1,\"tyreSmokeColor\":[255,255,255],\"modSpoilers\":-1,\"modBrakes\":-1,\"modHorns\":-1,\"modDial\":-1,\"modTrunk\":-1,\"modHood\":-1,\"bodyHealth\":1000.0,\"modTank\":-1,\"modTurbo\":false,\"modOrnaments\":-1,\"modVanityPlate\":-1,\"dirtLevel\":0.0,\"fuelLevel\":76.2,\"modArchCover\":-1,\"modXenon\":false,\"modGrille\":-1,\"plate\":\"MPV 719\",\"modStruts\":-1,\"modPlateHolder\":-1,\"neonEnabled\":[false,false,false,false],\"engineHealth\":1000.0,\"modAPlate\":-1,\"modRoof\":-1,\"model\":-1059115956,\"modSideSkirt\":-1,\"modBackWheels\":-1,\"modHydrolic\":-1,\"color1\":134,\"modTrimA\":-1,\"modLivery\":2,\"modDashboard\":-1,\"windowTint\":-1,\"modArmor\":-1,\"wheelColor\":156}', 'car', 'police', 1),
('steam:11000010b49a743', 'OIL 252', '{\"modDoorSpeaker\":-1,\"modFrontWheels\":-1,\"modTransmission\":-1,\"plateIndex\":4,\"plate\":\"OIL 252\",\"tyreSmokeColor\":[255,255,255],\"modSuspension\":-1,\"modWindows\":-1,\"modRoof\":-1,\"modAerials\":-1,\"modSideSkirt\":-1,\"fuelLevel\":87.7,\"modSteeringWheel\":-1,\"modOrnaments\":-1,\"modSmokeEnabled\":false,\"model\":1745872177,\"modEngine\":-1,\"modXenon\":false,\"modArchCover\":-1,\"modArmor\":-1,\"color2\":134,\"color1\":134,\"neonEnabled\":[false,false,false,false],\"modBrakes\":-1,\"modTank\":-1,\"pearlescentColor\":156,\"modFender\":-1,\"modLivery\":2,\"windowTint\":-1,\"modShifterLeavers\":-1,\"bodyHealth\":1000.0,\"modTrimB\":-1,\"modDashboard\":-1,\"modBackWheels\":-1,\"modSpeakers\":-1,\"modGrille\":-1,\"modTrimA\":-1,\"modTurbo\":false,\"modSeats\":-1,\"dirtLevel\":0.0,\"modEngineBlock\":-1,\"modHorns\":-1,\"modDial\":-1,\"modHydrolic\":-1,\"modRearBumper\":-1,\"neonColor\":[255,0,255],\"modHood\":-1,\"extras\":{\"12\":false,\"8\":false,\"11\":false,\"10\":true,\"2\":false,\"3\":false,\"9\":false,\"1\":false,\"6\":false,\"7\":false,\"4\":true,\"5\":false},\"modExhaust\":-1,\"modTrunk\":-1,\"engineHealth\":1000.0,\"modPlateHolder\":-1,\"modStruts\":-1,\"modAirFilter\":-1,\"modSpoilers\":-1,\"wheelColor\":156,\"modFrontBumper\":-1,\"modVanityPlate\":-1,\"modRightFender\":-1,\"modFrame\":-1,\"modAPlate\":-1,\"wheels\":1}', 'car', 'police', 1),
('steam:1100001139319c1', 'ORM 791', '{\"neonColor\":[255,0,255],\"modLivery\":0,\"modTrunk\":-1,\"modAPlate\":-1,\"modHood\":-1,\"modFender\":-1,\"modGrille\":-1,\"modSpoilers\":-1,\"modRightFender\":-1,\"modExhaust\":-1,\"modTransmission\":-1,\"modArchCover\":-1,\"color2\":134,\"modFrontBumper\":-1,\"modFrontWheels\":-1,\"modSeats\":-1,\"model\":598074270,\"modVanityPlate\":-1,\"modAirFilter\":-1,\"modPlateHolder\":-1,\"plate\":\"ORM 791\",\"extras\":{\"6\":true,\"5\":false,\"10\":true,\"7\":false,\"2\":false,\"1\":false,\"4\":false,\"3\":false,\"11\":false,\"9\":false,\"8\":false},\"modEngineBlock\":-1,\"modRearBumper\":-1,\"modBackWheels\":-1,\"modSteeringWheel\":-1,\"modTrimB\":-1,\"modDashboard\":-1,\"engineHealth\":1000.0,\"pearlescentColor\":156,\"color1\":134,\"modXenon\":false,\"modEngine\":-1,\"modTank\":-1,\"modFrame\":-1,\"windowTint\":-1,\"modWindows\":-1,\"modSuspension\":-1,\"modTurbo\":false,\"modArmor\":-1,\"modBrakes\":-1,\"plateIndex\":4,\"modDoorSpeaker\":-1,\"modSpeakers\":-1,\"modSmokeEnabled\":false,\"neonEnabled\":[false,false,false,false],\"tyreSmokeColor\":[255,255,255],\"modDial\":-1,\"wheels\":1,\"modOrnaments\":-1,\"wheelColor\":156,\"modRoof\":-1,\"bodyHealth\":1000.0,\"modHydrolic\":-1,\"modStruts\":-1,\"fuelLevel\":76.8,\"modShifterLeavers\":-1,\"modAerials\":-1,\"modSideSkirt\":-1,\"modHorns\":-1,\"modTrimA\":-1,\"dirtLevel\":0.0}', 'car', 'police', 1),
('steam:11000013f6b85e1', 'QIK 955', '{\"modXenon\":false,\"modTrunk\":-1,\"modDashboard\":-1,\"wheels\":1,\"modShifterLeavers\":-1,\"modHydrolic\":-1,\"modSteeringWheel\":-1,\"neonEnabled\":[false,false,false,false],\"plate\":\"QIK 955\",\"neonColor\":[255,0,255],\"modAPlate\":-1,\"color2\":134,\"modDial\":-1,\"color1\":134,\"modFender\":-1,\"pearlescentColor\":156,\"modHorns\":-1,\"fuelLevel\":93.5,\"modTrimB\":-1,\"modTurbo\":false,\"modWindows\":-1,\"modDoorSpeaker\":-1,\"modSeats\":-1,\"modVanityPlate\":-1,\"modAerials\":-1,\"modRightFender\":-1,\"plateIndex\":4,\"modLivery\":0,\"modArmor\":-1,\"extras\":{\"2\":false,\"1\":false,\"12\":true,\"11\":false,\"8\":false,\"7\":false,\"10\":false,\"9\":false,\"4\":true,\"3\":false,\"6\":false,\"5\":false},\"modExhaust\":-1,\"modSpeakers\":-1,\"modRearBumper\":-1,\"tyreSmokeColor\":[255,255,255],\"modFrame\":-1,\"modGrille\":-1,\"modSuspension\":-1,\"modEngineBlock\":-1,\"modAirFilter\":-1,\"modSmokeEnabled\":false,\"modTransmission\":-1,\"modTank\":-1,\"modFrontBumper\":-1,\"modRoof\":-1,\"modBrakes\":-1,\"modTrimA\":-1,\"model\":1357997983,\"engineHealth\":1000.0,\"modHood\":-1,\"modFrontWheels\":-1,\"modSideSkirt\":-1,\"modBackWheels\":-1,\"windowTint\":-1,\"modPlateHolder\":-1,\"modArchCover\":-1,\"dirtLevel\":0.0,\"wheelColor\":156,\"modStruts\":-1,\"bodyHealth\":1000.0,\"modEngine\":-1,\"modSpoilers\":-1,\"modOrnaments\":-1}', 'car', 'police', 1),
('steam:1100001081d1893', 'QSX 486', '{\"plate\":\"QSX 486\",\"pearlescentColor\":156,\"modTransmission\":-1,\"plateIndex\":4,\"neonEnabled\":[false,false,false,false],\"color1\":134,\"modPlateHolder\":-1,\"bodyHealth\":1000.0,\"modArchCover\":-1,\"engineHealth\":1000.0,\"modTurbo\":false,\"modRearBumper\":-1,\"modSuspension\":-1,\"modHood\":-1,\"modEngineBlock\":-1,\"modFrame\":-1,\"modGrille\":-1,\"modHydrolic\":-1,\"tyreSmokeColor\":[255,255,255],\"modOrnaments\":-1,\"modBrakes\":-1,\"wheelColor\":156,\"modHorns\":-1,\"modTrimB\":-1,\"modAPlate\":-1,\"modRightFender\":-1,\"neonColor\":[255,0,255],\"modRoof\":-1,\"modWindows\":-1,\"color2\":134,\"model\":1745872177,\"modTrimA\":-1,\"modSpoilers\":-1,\"modVanityPlate\":-1,\"wheels\":1,\"modAerials\":-1,\"modSideSkirt\":-1,\"modStruts\":-1,\"modFrontBumper\":-1,\"modBackWheels\":-1,\"modFender\":-1,\"modTrunk\":-1,\"modFrontWheels\":-1,\"modAirFilter\":-1,\"extras\":{\"11\":true,\"10\":false,\"1\":false,\"12\":false,\"4\":false,\"5\":false,\"2\":false,\"3\":false,\"8\":false,\"9\":false,\"6\":false,\"7\":true},\"modSteeringWheel\":-1,\"modDashboard\":-1,\"modDial\":-1,\"modShifterLeavers\":-1,\"fuelLevel\":86.4,\"modLivery\":0,\"windowTint\":-1,\"modExhaust\":-1,\"dirtLevel\":0.0,\"modTank\":-1,\"modSmokeEnabled\":false,\"modDoorSpeaker\":-1,\"modArmor\":-1,\"modSpeakers\":-1,\"modEngine\":-1,\"modSeats\":-1,\"modXenon\":false}', 'car', 'police', 1),
('steam:11000013f6b85e1', 'RNC 414', '{\"windowTint\":-1,\"modSmokeEnabled\":false,\"modDial\":-1,\"modPlateHolder\":-1,\"dirtLevel\":1.0,\"modOrnaments\":-1,\"modTransmission\":-1,\"extras\":{\"10\":false,\"12\":false,\"6\":true,\"7\":false,\"9\":false,\"1\":false,\"8\":false,\"11\":false,\"4\":false,\"5\":false,\"2\":false,\"3\":false},\"modRightFender\":-1,\"engineHealth\":1000.0,\"modVanityPlate\":-1,\"modAPlate\":-1,\"modAerials\":-1,\"modTrunk\":-1,\"modTurbo\":false,\"modEngineBlock\":-1,\"modAirFilter\":-1,\"model\":-1059115956,\"neonEnabled\":[false,false,false,false],\"modStruts\":-1,\"modTrimB\":-1,\"modHorns\":-1,\"modTank\":-1,\"modShifterLeavers\":-1,\"modDashboard\":-1,\"modHydrolic\":-1,\"modSpoilers\":-1,\"modSteeringWheel\":-1,\"modRoof\":-1,\"plate\":\"RNC 414\",\"modArchCover\":-1,\"plateIndex\":4,\"modSideSkirt\":-1,\"modGrille\":-1,\"modBackWheels\":-1,\"color1\":134,\"neonColor\":[255,0,255],\"wheels\":0,\"modFrame\":-1,\"modXenon\":false,\"modBrakes\":-1,\"modFrontBumper\":-1,\"wheelColor\":156,\"tyreSmokeColor\":[255,255,255],\"modTrimA\":-1,\"modExhaust\":-1,\"modLivery\":3,\"modArmor\":-1,\"modSeats\":-1,\"modWindows\":-1,\"color2\":134,\"modHood\":-1,\"modFrontWheels\":-1,\"modDoorSpeaker\":-1,\"modSuspension\":-1,\"modFender\":-1,\"modEngine\":-1,\"fuelLevel\":75.1,\"bodyHealth\":1000.0,\"modSpeakers\":-1,\"modRearBumper\":-1,\"pearlescentColor\":156}', 'car', 'police', 1),
('steam:1100001081d1893', 'RPC 234', '{\"modSteeringWheel\":-1,\"modRightFender\":-1,\"modRearBumper\":-1,\"modSmokeEnabled\":false,\"model\":-1059115956,\"modFrame\":-1,\"modExhaust\":-1,\"neonColor\":[255,0,255],\"modSuspension\":-1,\"modTrimB\":-1,\"modWindows\":-1,\"modXenon\":false,\"modSpeakers\":-1,\"color2\":134,\"modEngine\":-1,\"modAerials\":-1,\"engineHealth\":1000.0,\"pearlescentColor\":156,\"modDoorSpeaker\":-1,\"modArmor\":-1,\"modAPlate\":-1,\"modTransmission\":-1,\"extras\":{\"5\":false,\"4\":false,\"7\":false,\"6\":false,\"9\":false,\"8\":false,\"12\":false,\"10\":false,\"11\":false,\"1\":false,\"3\":false,\"2\":true},\"wheels\":0,\"modAirFilter\":-1,\"modEngineBlock\":-1,\"modFrontWheels\":-1,\"tyreSmokeColor\":[255,255,255],\"modSpoilers\":-1,\"modFender\":-1,\"modHorns\":-1,\"modDial\":-1,\"modTrunk\":-1,\"modHood\":-1,\"bodyHealth\":1000.0,\"modTank\":-1,\"modTurbo\":false,\"modOrnaments\":-1,\"modVanityPlate\":-1,\"dirtLevel\":3.0,\"fuelLevel\":78.3,\"modArchCover\":-1,\"modHydrolic\":-1,\"modBrakes\":-1,\"plate\":\"RPC 234\",\"modSeats\":-1,\"modPlateHolder\":-1,\"modGrille\":-1,\"modStruts\":-1,\"modShifterLeavers\":-1,\"modRoof\":-1,\"neonEnabled\":[false,false,false,false],\"modSideSkirt\":-1,\"modFrontBumper\":-1,\"modDashboard\":-1,\"color1\":134,\"plateIndex\":4,\"modTrimA\":-1,\"modBackWheels\":-1,\"windowTint\":-1,\"modLivery\":3,\"wheelColor\":156}', 'car', 'police', 1),
('steam:110000111c332ed', 'RRJ 481', '{\"modSideSkirt\":-1,\"wheelColor\":156,\"modSteeringWheel\":-1,\"modAPlate\":-1,\"modGrille\":-1,\"modSuspension\":-1,\"modFrame\":-1,\"plateIndex\":4,\"plate\":\"RRJ 481\",\"dirtLevel\":0.0,\"modWindows\":-1,\"modXenon\":false,\"modAerials\":-1,\"bodyHealth\":1000.0,\"modSmokeEnabled\":false,\"modArmor\":-1,\"modSeats\":-1,\"modEngineBlock\":-1,\"extras\":{\"1\":false,\"12\":false,\"11\":false,\"10\":true,\"5\":false,\"4\":false,\"7\":true,\"6\":false,\"9\":false,\"8\":false,\"2\":false,\"3\":false},\"modTrimA\":-1,\"modHorns\":-1,\"modBrakes\":-1,\"model\":-1616359240,\"modTransmission\":-1,\"fuelLevel\":76.8,\"engineHealth\":1000.0,\"modRearBumper\":-1,\"modTurbo\":false,\"modEngine\":-1,\"modDoorSpeaker\":-1,\"modVanityPlate\":-1,\"tyreSmokeColor\":[255,255,255],\"modRightFender\":-1,\"modFrontBumper\":-1,\"modExhaust\":-1,\"windowTint\":-1,\"modDashboard\":-1,\"color2\":134,\"modLivery\":0,\"color1\":134,\"modDial\":-1,\"neonEnabled\":[false,false,false,false],\"neonColor\":[255,0,255],\"modTrimB\":-1,\"modRoof\":-1,\"modShifterLeavers\":-1,\"modHood\":-1,\"modSpeakers\":-1,\"modFender\":-1,\"modTank\":-1,\"modSpoilers\":-1,\"modPlateHolder\":-1,\"modArchCover\":-1,\"modAirFilter\":-1,\"modTrunk\":-1,\"modHydrolic\":-1,\"pearlescentColor\":156,\"modBackWheels\":-1,\"wheels\":1,\"modStruts\":-1,\"modOrnaments\":-1,\"modFrontWheels\":-1}', 'car', 'police', 1),
('steam:1100001139319c1', 'RVK 344', '{\"neonColor\":[255,0,255],\"modLivery\":0,\"modTrunk\":-1,\"modAPlate\":-1,\"modHood\":-1,\"modFender\":-1,\"modGrille\":-1,\"modSpoilers\":-1,\"modRightFender\":-1,\"modExhaust\":-1,\"modTransmission\":-1,\"modArchCover\":-1,\"color2\":134,\"modFrontBumper\":-1,\"modFrontWheels\":-1,\"modSeats\":-1,\"model\":1745872177,\"modVanityPlate\":-1,\"modAirFilter\":-1,\"modPlateHolder\":-1,\"plate\":\"RVK 344\",\"extras\":{\"6\":false,\"5\":false,\"10\":true,\"7\":true,\"2\":false,\"1\":false,\"4\":false,\"3\":false,\"11\":false,\"12\":false,\"9\":false,\"8\":true},\"modEngineBlock\":-1,\"modRearBumper\":-1,\"modBackWheels\":-1,\"modSteeringWheel\":-1,\"modTrimB\":-1,\"modDashboard\":-1,\"engineHealth\":1000.0,\"pearlescentColor\":156,\"color1\":134,\"modXenon\":false,\"modEngine\":-1,\"modTank\":-1,\"modFrame\":-1,\"windowTint\":-1,\"modWindows\":-1,\"modSuspension\":-1,\"modTurbo\":false,\"modArmor\":-1,\"modBrakes\":-1,\"plateIndex\":4,\"modDoorSpeaker\":-1,\"modSpeakers\":-1,\"modSmokeEnabled\":false,\"neonEnabled\":[false,false,false,false],\"tyreSmokeColor\":[255,255,255],\"modDial\":-1,\"wheels\":1,\"modOrnaments\":-1,\"wheelColor\":156,\"modRoof\":-1,\"bodyHealth\":1000.0,\"modHydrolic\":-1,\"modStruts\":-1,\"fuelLevel\":80.5,\"modShifterLeavers\":-1,\"modAerials\":-1,\"modSideSkirt\":-1,\"modHorns\":-1,\"modTrimA\":-1,\"dirtLevel\":0.0}', 'car', 'police', 1);
INSERT INTO `old_owned_vehicles` (`owner`, `plate`, `vehicle`, `type`, `job`, `stored`) VALUES
('steam:110000133d93ea2', 'RWS 405', '{\"dirtLevel\":0.0,\"engineHealth\":1000.0,\"modOrnaments\":-1,\"modSpoilers\":-1,\"pearlescentColor\":88,\"model\":1672195559,\"modDashboard\":-1,\"modFrontBumper\":-1,\"modAerials\":-1,\"wheels\":6,\"modStruts\":-1,\"modDial\":-1,\"modTransmission\":-1,\"modTrunk\":-1,\"windowTint\":-1,\"modSpeakers\":-1,\"modWindows\":-1,\"modRightFender\":-1,\"modDoorSpeaker\":-1,\"modLivery\":-1,\"modTrimA\":-1,\"modGrille\":-1,\"modEngine\":-1,\"neonColor\":[255,0,255],\"modTrimB\":-1,\"modVanityPlate\":-1,\"modFender\":-1,\"modSteeringWheel\":-1,\"modArmor\":-1,\"modExhaust\":-1,\"modEngineBlock\":-1,\"modXenon\":false,\"modFrame\":-1,\"bodyHealth\":1000.0,\"modArchCover\":-1,\"plate\":\"RWS 405\",\"tyreSmokeColor\":[255,255,255],\"modBrakes\":-1,\"modTurbo\":false,\"plateIndex\":3,\"modShifterLeavers\":-1,\"modHorns\":-1,\"color2\":88,\"modHood\":-1,\"modSuspension\":-1,\"modAPlate\":-1,\"modTank\":-1,\"modAirFilter\":-1,\"modFrontWheels\":-1,\"modRoof\":-1,\"modBackWheels\":-1,\"neonEnabled\":[false,false,false,false],\"modRearBumper\":-1,\"extras\":{\"9\":true,\"4\":true,\"1\":true},\"modSmokeEnabled\":false,\"color1\":88,\"modPlateHolder\":-1,\"fuelLevel\":65.0,\"modSideSkirt\":-1,\"modSeats\":-1,\"modHydrolic\":-1,\"wheelColor\":156}', 'car', NULL, 1),
('steam:110000133d93ea2', 'TDI 236', '{\"modPlateHolder\":-1,\"modShifterLeavers\":-1,\"bodyHealth\":1000.0,\"pearlescentColor\":38,\"modAPlate\":-1,\"modGrille\":-1,\"color1\":29,\"modExhaust\":-1,\"modDoorSpeaker\":-1,\"modFrame\":-1,\"windowTint\":-1,\"modTurbo\":false,\"plateIndex\":4,\"modTransmission\":-1,\"modFender\":-1,\"modAerials\":-1,\"modSmokeEnabled\":false,\"neonColor\":[255,0,255],\"modOrnaments\":-1,\"modFrontWheels\":-1,\"modTank\":-1,\"modRightFender\":-1,\"modHorns\":-1,\"modHood\":-1,\"modStruts\":-1,\"modFrontBumper\":-1,\"modEngine\":-1,\"modVanityPlate\":-1,\"modRoof\":-1,\"model\":1131912276,\"modSuspension\":-1,\"modBrakes\":-1,\"modArchCover\":-1,\"modSpoilers\":-1,\"wheelColor\":156,\"modEngineBlock\":-1,\"modWindows\":-1,\"modSeats\":-1,\"extras\":[],\"tyreSmokeColor\":[255,255,255],\"engineHealth\":1000.0,\"modBackWheels\":-1,\"modXenon\":false,\"modSideSkirt\":-1,\"fuelLevel\":0.0,\"modSteeringWheel\":-1,\"modRearBumper\":-1,\"modHydrolic\":-1,\"modDial\":-1,\"plate\":\"TDI 236\",\"neonEnabled\":[false,false,false,false],\"modLivery\":-1,\"modAirFilter\":-1,\"dirtLevel\":0.0,\"color2\":0,\"modTrimB\":-1,\"wheels\":6,\"modArmor\":-1,\"modTrimA\":-1,\"modTrunk\":-1,\"modSpeakers\":-1,\"modDashboard\":-1}', 'car', NULL, 1),
('steam:110000106197c1a', 'TJK 073', '{\"modBackWheels\":-1,\"color2\":0,\"modSideSkirt\":-1,\"modTransmission\":-1,\"modSmokeEnabled\":false,\"modEngineBlock\":-1,\"modBrakes\":-1,\"modEngine\":-1,\"bodyHealth\":1000.0,\"modRoof\":-1,\"wheelColor\":6,\"modFrontWheels\":-1,\"modAirFilter\":-1,\"modDial\":-1,\"modVanityPlate\":-1,\"modSteeringWheel\":-1,\"color1\":5,\"engineHealth\":1000.0,\"modArchCover\":-1,\"windowTint\":-1,\"modLivery\":-1,\"neonEnabled\":[false,false,false,false],\"modArmor\":-1,\"modFender\":-1,\"modFrontBumper\":-1,\"modXenon\":false,\"modTrimA\":-1,\"modTurbo\":false,\"neonColor\":[255,0,255],\"modPlateHolder\":-1,\"modTrunk\":-1,\"fuelLevel\":65.0,\"modDoorSpeaker\":-1,\"modTrimB\":-1,\"modHydrolic\":-1,\"modHood\":-1,\"modRearBumper\":-1,\"modAerials\":-1,\"modSuspension\":-1,\"model\":2067820283,\"modAPlate\":-1,\"plateIndex\":0,\"modHorns\":-1,\"modGrille\":-1,\"modTank\":-1,\"modFrame\":-1,\"plate\":\"TJK 073\",\"modExhaust\":-1,\"modSpeakers\":-1,\"modWindows\":-1,\"modDashboard\":-1,\"pearlescentColor\":5,\"modSpoilers\":-1,\"modRightFender\":-1,\"dirtLevel\":4.0,\"modShifterLeavers\":-1,\"modSeats\":-1,\"extras\":[],\"tyreSmokeColor\":[255,255,255],\"wheels\":7,\"modOrnaments\":-1,\"modStruts\":-1}', 'car', NULL, 1),
('steam:11000010b49a743', 'TJZ 019', '{\"modTank\":-1,\"modHorns\":-1,\"modEngineBlock\":-1,\"dirtLevel\":0.0,\"modSeats\":-1,\"bodyHealth\":1000.0,\"modArmor\":-1,\"wheels\":1,\"model\":598074270,\"windowTint\":-1,\"modSpeakers\":-1,\"modArchCover\":-1,\"plateIndex\":4,\"modAirFilter\":-1,\"modBrakes\":-1,\"modExhaust\":-1,\"modHydrolic\":-1,\"modFrontBumper\":-1,\"wheelColor\":156,\"modAerials\":-1,\"modBackWheels\":-1,\"fuelLevel\":94.9,\"modFender\":-1,\"modShifterLeavers\":-1,\"modTrimB\":-1,\"modRoof\":-1,\"modSpoilers\":-1,\"neonColor\":[255,0,255],\"modOrnaments\":-1,\"neonEnabled\":[false,false,false,false],\"modXenon\":false,\"modDial\":-1,\"modSmokeEnabled\":false,\"modSuspension\":-1,\"modLivery\":1,\"modDashboard\":-1,\"color1\":134,\"modTrimA\":-1,\"pearlescentColor\":156,\"modRightFender\":-1,\"modGrille\":-1,\"modTrunk\":-1,\"modSteeringWheel\":-1,\"modTurbo\":false,\"plate\":\"TJZ 019\",\"modAPlate\":-1,\"modEngine\":-1,\"modVanityPlate\":-1,\"modPlateHolder\":-1,\"modSideSkirt\":-1,\"color2\":134,\"tyreSmokeColor\":[255,255,255],\"modRearBumper\":-1,\"modHood\":-1,\"engineHealth\":1000.0,\"modDoorSpeaker\":-1,\"modStruts\":-1,\"modFrontWheels\":-1,\"modWindows\":-1,\"extras\":{\"2\":false,\"1\":false,\"4\":false,\"3\":true,\"9\":false,\"7\":false,\"11\":true,\"6\":false,\"5\":false,\"8\":false,\"10\":false},\"modFrame\":-1,\"modTransmission\":-1}', 'car', 'police', 1),
('steam:11000010b49a743', 'TPN 884', '{\"modWindows\":-1,\"modHorns\":-1,\"bodyHealth\":1000.0,\"modRoof\":-1,\"modDashboard\":-1,\"modFrame\":-1,\"modXenon\":false,\"modLivery\":0,\"modSmokeEnabled\":false,\"modFender\":-1,\"modBackWheels\":-1,\"modSideSkirt\":-1,\"modTrunk\":-1,\"modGrille\":-1,\"windowTint\":-1,\"modAerials\":-1,\"neonColor\":[255,0,255],\"modTurbo\":false,\"modAirFilter\":-1,\"dirtLevel\":0.0,\"color2\":134,\"modSuspension\":-1,\"modSpeakers\":-1,\"model\":1745872177,\"modFrontWheels\":-1,\"modHood\":-1,\"plateIndex\":4,\"modFrontBumper\":-1,\"engineHealth\":1000.0,\"modSteeringWheel\":-1,\"extras\":{\"12\":false,\"9\":false,\"10\":false,\"11\":false,\"3\":false,\"2\":false,\"1\":false,\"8\":true,\"7\":true,\"6\":false,\"5\":false,\"4\":false},\"pearlescentColor\":156,\"wheelColor\":156,\"modBrakes\":-1,\"wheels\":1,\"modDial\":-1,\"neonEnabled\":[false,false,false,false],\"modOrnaments\":-1,\"modArchCover\":-1,\"tyreSmokeColor\":[255,255,255],\"modSeats\":-1,\"plate\":\"TPN 884\",\"modTank\":-1,\"fuelLevel\":84.2,\"modAPlate\":-1,\"modEngine\":-1,\"modRightFender\":-1,\"modTransmission\":-1,\"color1\":134,\"modArmor\":-1,\"modDoorSpeaker\":-1,\"modTrimB\":-1,\"modHydrolic\":-1,\"modVanityPlate\":-1,\"modTrimA\":-1,\"modPlateHolder\":-1,\"modSpoilers\":-1,\"modEngineBlock\":-1,\"modStruts\":-1,\"modExhaust\":-1,\"modShifterLeavers\":-1,\"modRearBumper\":-1}', 'car', 'police', 1),
('steam:11000010fe595d0', 'TQV 422', '{\"windowTint\":-1,\"modSmokeEnabled\":false,\"modDial\":-1,\"modPlateHolder\":-1,\"dirtLevel\":0.0,\"modRightFender\":-1,\"modTransmission\":-1,\"extras\":{\"10\":false,\"12\":false,\"6\":false,\"7\":false,\"9\":false,\"1\":false,\"8\":false,\"11\":true,\"4\":false,\"5\":false,\"2\":false,\"3\":true},\"modOrnaments\":-1,\"engineHealth\":1000.0,\"modFrame\":-1,\"modAPlate\":-1,\"modAerials\":-1,\"modVanityPlate\":-1,\"modTurbo\":false,\"modEngineBlock\":-1,\"modAirFilter\":-1,\"model\":1357997983,\"neonEnabled\":[false,false,false,false],\"modStruts\":-1,\"modTrimB\":-1,\"modDashboard\":-1,\"modTank\":-1,\"fuelLevel\":75.3,\"pearlescentColor\":156,\"modHydrolic\":-1,\"modSpoilers\":-1,\"modSteeringWheel\":-1,\"modTrunk\":-1,\"plate\":\"TQV 422\",\"modLivery\":0,\"neonColor\":[255,0,255],\"modSideSkirt\":-1,\"modGrille\":-1,\"modBackWheels\":-1,\"modWindows\":-1,\"modHorns\":-1,\"wheels\":1,\"plateIndex\":4,\"modXenon\":false,\"wheelColor\":156,\"modFrontBumper\":-1,\"modShifterLeavers\":-1,\"tyreSmokeColor\":[255,255,255],\"modRearBumper\":-1,\"modExhaust\":-1,\"modRoof\":-1,\"modArmor\":-1,\"modBrakes\":-1,\"color1\":134,\"color2\":134,\"modHood\":-1,\"modFrontWheels\":-1,\"modDoorSpeaker\":-1,\"modSuspension\":-1,\"modFender\":-1,\"modEngine\":-1,\"modSeats\":-1,\"bodyHealth\":1000.0,\"modSpeakers\":-1,\"modTrimA\":-1,\"modArchCover\":-1}', 'car', 'police', 1),
('steam:1100001081d1893', 'UCU 505', '{\"modTank\":-1,\"modHorns\":-1,\"modEngineBlock\":-1,\"dirtLevel\":0.0,\"modSeats\":-1,\"bodyHealth\":1000.0,\"modArmor\":-1,\"wheels\":1,\"model\":1745872177,\"windowTint\":-1,\"modSpeakers\":-1,\"modAirFilter\":-1,\"plateIndex\":4,\"modHydrolic\":-1,\"modBrakes\":-1,\"modExhaust\":-1,\"neonColor\":[255,0,255],\"engineHealth\":1000.0,\"wheelColor\":156,\"modAerials\":-1,\"modShifterLeavers\":-1,\"modEngine\":-1,\"modFender\":-1,\"modSpoilers\":-1,\"modTrimB\":-1,\"modRoof\":-1,\"modVanityPlate\":-1,\"neonEnabled\":[false,false,false,false],\"modOrnaments\":-1,\"modBackWheels\":-1,\"modXenon\":false,\"modSuspension\":-1,\"modSmokeEnabled\":false,\"modLivery\":1,\"color1\":134,\"modPlateHolder\":-1,\"fuelLevel\":82.2,\"modTrimA\":-1,\"modArchCover\":-1,\"modRightFender\":-1,\"modGrille\":-1,\"modTrunk\":-1,\"modSteeringWheel\":-1,\"modDial\":-1,\"plate\":\"UCU 505\",\"modAPlate\":-1,\"modDashboard\":-1,\"modRearBumper\":-1,\"modSideSkirt\":-1,\"modTurbo\":false,\"color2\":134,\"pearlescentColor\":156,\"tyreSmokeColor\":[255,255,255],\"modHood\":-1,\"modFrontBumper\":-1,\"modDoorSpeaker\":-1,\"modStruts\":-1,\"modFrontWheels\":-1,\"modWindows\":-1,\"extras\":{\"2\":false,\"1\":false,\"4\":false,\"3\":false,\"7\":false,\"9\":false,\"12\":false,\"11\":false,\"6\":false,\"5\":true,\"8\":false,\"10\":false},\"modFrame\":-1,\"modTransmission\":-1}', 'car', 'police', 1),
('steam:110000133d93ea2', 'UJT 115', '{\"modAerials\":-1,\"color2\":0,\"modTrimA\":-1,\"modGrille\":-1,\"engineHealth\":1000.0,\"modExhaust\":-1,\"modRoof\":-1,\"modWindows\":-1,\"modTrunk\":-1,\"modSideSkirt\":-1,\"windowTint\":-1,\"dirtLevel\":5.0,\"pearlescentColor\":38,\"modSuspension\":-1,\"modVanityPlate\":-1,\"modTransmission\":-1,\"modOrnaments\":-1,\"modEngine\":-1,\"modFrame\":-1,\"model\":1131912276,\"modSpoilers\":-1,\"color1\":29,\"modSpeakers\":-1,\"modSmokeEnabled\":false,\"modDial\":-1,\"modStruts\":-1,\"tyreSmokeColor\":[255,255,255],\"modRightFender\":-1,\"modDoorSpeaker\":-1,\"modEngineBlock\":-1,\"bodyHealth\":1000.0,\"plateIndex\":4,\"plate\":\"UJT 115\",\"modArchCover\":-1,\"modBrakes\":-1,\"wheels\":6,\"modSteeringWheel\":-1,\"modLivery\":-1,\"modAPlate\":-1,\"modArmor\":-1,\"modBackWheels\":-1,\"fuelLevel\":0.0,\"modRearBumper\":-1,\"modShifterLeavers\":-1,\"modHood\":-1,\"extras\":[],\"modSeats\":-1,\"modAirFilter\":-1,\"modPlateHolder\":-1,\"modFrontWheels\":-1,\"modXenon\":false,\"wheelColor\":156,\"modDashboard\":-1,\"modFender\":-1,\"modHydrolic\":-1,\"modTrimB\":-1,\"modFrontBumper\":-1,\"modTank\":-1,\"modHorns\":-1,\"neonColor\":[255,0,255],\"neonEnabled\":[false,false,false,false],\"modTurbo\":false}', 'car', NULL, 1),
('steam:11000010b49a743', 'VAV 851', '{\"modTank\":-1,\"modHorns\":-1,\"modEngineBlock\":-1,\"dirtLevel\":4.0,\"modSeats\":-1,\"bodyHealth\":1000.0,\"modArmor\":-1,\"wheels\":1,\"model\":1027958099,\"windowTint\":-1,\"modSpeakers\":-1,\"modArchCover\":-1,\"plateIndex\":4,\"modAirFilter\":-1,\"modBrakes\":-1,\"modExhaust\":-1,\"modHydrolic\":-1,\"modFrontBumper\":-1,\"wheelColor\":156,\"modAerials\":-1,\"modBackWheels\":-1,\"fuelLevel\":87.0,\"modFender\":-1,\"modShifterLeavers\":-1,\"modTrimB\":-1,\"modRoof\":-1,\"modSpoilers\":-1,\"neonColor\":[255,0,255],\"modOrnaments\":-1,\"neonEnabled\":[false,false,false,false],\"modXenon\":false,\"modDial\":-1,\"modSmokeEnabled\":false,\"modSuspension\":-1,\"modLivery\":0,\"modDashboard\":-1,\"color1\":134,\"modTrimA\":-1,\"pearlescentColor\":156,\"modRightFender\":-1,\"modGrille\":-1,\"modTrunk\":-1,\"modSteeringWheel\":-1,\"modTurbo\":false,\"plate\":\"VAV 851\",\"modAPlate\":-1,\"modEngine\":-1,\"modVanityPlate\":-1,\"modPlateHolder\":-1,\"modSideSkirt\":-1,\"color2\":134,\"tyreSmokeColor\":[255,255,255],\"modRearBumper\":-1,\"modHood\":-1,\"engineHealth\":1000.0,\"modDoorSpeaker\":-1,\"modStruts\":-1,\"modFrontWheels\":-1,\"modWindows\":-1,\"extras\":{\"2\":false,\"1\":false,\"4\":false,\"3\":false,\"7\":true,\"9\":false,\"12\":false,\"11\":true,\"6\":false,\"5\":false,\"8\":false,\"10\":false},\"modFrame\":-1,\"modTransmission\":-1}', 'car', 'police', 1),
('steam:110000133d93ea2', 'WUX 779', '{\"modAerials\":-1,\"color2\":0,\"modTrimA\":-1,\"modShifterLeavers\":-1,\"modWindows\":-1,\"modExhaust\":-1,\"modRoof\":-1,\"modGrille\":-1,\"modTransmission\":-1,\"bodyHealth\":1000.0,\"modFender\":-1,\"dirtLevel\":7.0,\"pearlescentColor\":38,\"modSuspension\":-1,\"modVanityPlate\":-1,\"modStruts\":-1,\"modOrnaments\":-1,\"modSteeringWheel\":-1,\"modPlateHolder\":-1,\"neonColor\":[255,0,255],\"modFrontBumper\":-1,\"modXenon\":false,\"modSpeakers\":-1,\"modTurbo\":false,\"model\":1131912276,\"modSideSkirt\":-1,\"tyreSmokeColor\":[255,255,255],\"neonEnabled\":[false,false,false,false],\"modDoorSpeaker\":-1,\"modEngineBlock\":-1,\"modRightFender\":-1,\"plateIndex\":4,\"plate\":\"WUX 779\",\"modTrunk\":-1,\"modBrakes\":-1,\"modArchCover\":-1,\"modEngine\":-1,\"wheelColor\":156,\"modFrame\":-1,\"modArmor\":-1,\"modBackWheels\":-1,\"fuelLevel\":86.0,\"modRearBumper\":-1,\"modSmokeEnabled\":false,\"modHood\":-1,\"modAPlate\":-1,\"modSeats\":-1,\"modHydrolic\":-1,\"color1\":29,\"modFrontWheels\":-1,\"modSpoilers\":-1,\"modDial\":-1,\"modDashboard\":-1,\"extras\":[],\"modAirFilter\":-1,\"modTrimB\":-1,\"modLivery\":-1,\"engineHealth\":1000.0,\"modHorns\":-1,\"windowTint\":-1,\"wheels\":6,\"modTank\":-1}', 'car', NULL, 1),
('steam:110000111c332ed', 'XFW 709', '{\"wheelColor\":156,\"modRoof\":-1,\"fuelLevel\":75.1,\"modFrontWheels\":-1,\"modSteeringWheel\":-1,\"bodyHealth\":1000.0,\"modDashboard\":-1,\"plateIndex\":4,\"wheels\":0,\"modXenon\":false,\"modSpoilers\":-1,\"modEngine\":-1,\"tyreSmokeColor\":[255,255,255],\"model\":-1059115956,\"modVanityPlate\":-1,\"modTurbo\":false,\"modArmor\":-1,\"modGrille\":-1,\"modShifterLeavers\":-1,\"modTrimB\":-1,\"modRightFender\":-1,\"modHorns\":-1,\"windowTint\":-1,\"modFrontBumper\":-1,\"modPlateHolder\":-1,\"modTransmission\":-1,\"modBrakes\":-1,\"modEngineBlock\":-1,\"modFender\":-1,\"modExhaust\":-1,\"engineHealth\":1000.0,\"pearlescentColor\":156,\"modBackWheels\":-1,\"modAPlate\":-1,\"dirtLevel\":0.0,\"modWindows\":-1,\"modHydrolic\":-1,\"modDial\":-1,\"modTrunk\":-1,\"modDoorSpeaker\":-1,\"modSideSkirt\":-1,\"modAirFilter\":-1,\"modRearBumper\":-1,\"color2\":134,\"modOrnaments\":-1,\"extras\":{\"4\":false,\"3\":false,\"2\":false,\"1\":false,\"12\":false,\"11\":false,\"10\":false,\"9\":false,\"8\":false,\"7\":false,\"6\":true,\"5\":false},\"modTrimA\":-1,\"modTank\":-1,\"plate\":\"XFW 709\",\"modArchCover\":-1,\"modAerials\":-1,\"neonColor\":[255,0,255],\"modSmokeEnabled\":false,\"modFrame\":-1,\"neonEnabled\":[false,false,false,false],\"color1\":134,\"modStruts\":-1,\"modLivery\":0,\"modSuspension\":-1,\"modSeats\":-1,\"modHood\":-1,\"modSpeakers\":-1}', 'car', 'police', 1),
('steam:11000013f6b85e1', 'XJS 557', '{\"windowTint\":-1,\"modSmokeEnabled\":false,\"modDial\":-1,\"modPlateHolder\":-1,\"dirtLevel\":0.0,\"modOrnaments\":-1,\"modTransmission\":-1,\"extras\":{\"10\":false,\"12\":true,\"6\":false,\"7\":false,\"9\":false,\"1\":false,\"8\":false,\"11\":false,\"4\":true,\"5\":false,\"2\":false,\"3\":false},\"modRightFender\":-1,\"engineHealth\":1000.0,\"modVanityPlate\":-1,\"modAPlate\":-1,\"modAerials\":-1,\"modTrunk\":-1,\"modTurbo\":false,\"modEngineBlock\":-1,\"modAirFilter\":-1,\"model\":1357997983,\"neonEnabled\":[false,false,false,false],\"modStruts\":-1,\"modTrimB\":-1,\"modHorns\":-1,\"modTank\":-1,\"modShifterLeavers\":-1,\"modDashboard\":-1,\"modHydrolic\":-1,\"modSpoilers\":-1,\"modSteeringWheel\":-1,\"modRoof\":-1,\"plate\":\"XJS 557\",\"modArchCover\":-1,\"plateIndex\":4,\"modSideSkirt\":-1,\"modGrille\":-1,\"modBackWheels\":-1,\"color1\":134,\"neonColor\":[255,0,255],\"wheels\":1,\"modFrame\":-1,\"modXenon\":false,\"modBrakes\":-1,\"modFrontBumper\":-1,\"wheelColor\":156,\"tyreSmokeColor\":[255,255,255],\"modTrimA\":-1,\"modExhaust\":-1,\"modLivery\":1,\"modArmor\":-1,\"modSeats\":-1,\"modWindows\":-1,\"color2\":134,\"modHood\":-1,\"modFrontWheels\":-1,\"modDoorSpeaker\":-1,\"modSuspension\":-1,\"modFender\":-1,\"modEngine\":-1,\"fuelLevel\":82.2,\"bodyHealth\":1000.0,\"modSpeakers\":-1,\"modRearBumper\":-1,\"pearlescentColor\":156}', 'car', 'police', 1),
('steam:11000010b49a743', 'YCM 152', '{\"modTank\":-1,\"modHorns\":-1,\"modEngineBlock\":-1,\"dirtLevel\":0.0,\"modSeats\":-1,\"bodyHealth\":1000.0,\"modArmor\":-1,\"wheels\":1,\"model\":1745872177,\"windowTint\":-1,\"modSpeakers\":-1,\"modArchCover\":-1,\"plateIndex\":4,\"modAirFilter\":-1,\"modBrakes\":-1,\"modExhaust\":-1,\"modHydrolic\":-1,\"modFrontBumper\":-1,\"wheelColor\":156,\"modAerials\":-1,\"modBackWheels\":-1,\"fuelLevel\":85.4,\"modFender\":-1,\"modShifterLeavers\":-1,\"modTrimB\":-1,\"modRoof\":-1,\"modSpoilers\":-1,\"neonColor\":[255,0,255],\"modOrnaments\":-1,\"neonEnabled\":[false,false,false,false],\"modXenon\":false,\"modDial\":-1,\"modSmokeEnabled\":false,\"modSuspension\":-1,\"modLivery\":2,\"modDashboard\":-1,\"color1\":134,\"modTrimA\":-1,\"pearlescentColor\":156,\"modRightFender\":-1,\"modGrille\":-1,\"modTrunk\":-1,\"modSteeringWheel\":-1,\"modTurbo\":false,\"plate\":\"YCM 152\",\"modAPlate\":-1,\"modEngine\":-1,\"modVanityPlate\":-1,\"modPlateHolder\":-1,\"modSideSkirt\":-1,\"color2\":134,\"tyreSmokeColor\":[255,255,255],\"modRearBumper\":-1,\"modHood\":-1,\"engineHealth\":1000.0,\"modDoorSpeaker\":-1,\"modStruts\":-1,\"modFrontWheels\":-1,\"modWindows\":-1,\"extras\":{\"2\":false,\"1\":false,\"4\":false,\"3\":true,\"7\":false,\"9\":false,\"12\":false,\"11\":false,\"6\":false,\"5\":false,\"8\":false,\"10\":true},\"modFrame\":-1,\"modTransmission\":-1}', 'car', 'police', 1),
('steam:11000010fe595d0', 'YMJ 060', '{\"modStruts\":-1,\"modTank\":-1,\"modAPlate\":-1,\"tyreSmokeColor\":[255,255,255],\"modTrunk\":-1,\"extras\":{\"12\":false,\"1\":false,\"4\":false,\"10\":false,\"5\":false,\"11\":false,\"3\":true,\"2\":false,\"9\":false,\"8\":false,\"7\":false,\"6\":false},\"modSeats\":-1,\"color1\":134,\"modDoorSpeaker\":-1,\"plate\":\"YMJ 060\",\"modTransmission\":-1,\"modAirFilter\":-1,\"modFrontBumper\":-1,\"modArmor\":-1,\"engineHealth\":1000.0,\"wheels\":1,\"modTrimB\":-1,\"color2\":134,\"modOrnaments\":-1,\"modArchCover\":-1,\"modShifterLeavers\":-1,\"windowTint\":-1,\"modDial\":-1,\"modGrille\":-1,\"modVanityPlate\":-1,\"modFender\":-1,\"modHydrolic\":-1,\"plateIndex\":4,\"modLivery\":0,\"modRearBumper\":-1,\"modFrontWheels\":-1,\"modXenon\":false,\"modSpeakers\":-1,\"modHood\":-1,\"modDashboard\":-1,\"modEngine\":-1,\"modPlateHolder\":-1,\"modSuspension\":-1,\"modTurbo\":false,\"modRightFender\":-1,\"modBrakes\":-1,\"modBackWheels\":-1,\"bodyHealth\":1000.0,\"modHorns\":-1,\"modWindows\":-1,\"modEngineBlock\":-1,\"modSmokeEnabled\":false,\"neonEnabled\":[false,false,false,false],\"pearlescentColor\":156,\"neonColor\":[255,0,255],\"modSteeringWheel\":-1,\"dirtLevel\":0.0,\"fuelLevel\":88.9,\"modSideSkirt\":-1,\"wheelColor\":156,\"modAerials\":-1,\"model\":161178935,\"modRoof\":-1,\"modExhaust\":-1,\"modFrame\":-1,\"modTrimA\":-1,\"modSpoilers\":-1}', 'car', 'police', 1),
('steam:1100001081d1893', 'ZCK 658', '{\"plate\":\"ZCK 658\",\"pearlescentColor\":156,\"modTransmission\":-1,\"plateIndex\":4,\"modXenon\":false,\"color1\":134,\"modPlateHolder\":-1,\"bodyHealth\":1000.0,\"tyreSmokeColor\":[255,255,255],\"engineHealth\":1000.0,\"modArmor\":-1,\"modSideSkirt\":-1,\"modSuspension\":-1,\"modRightFender\":-1,\"modEngineBlock\":-1,\"modFrame\":-1,\"modRearBumper\":-1,\"modHydrolic\":-1,\"modTrimA\":-1,\"modBrakes\":-1,\"modAerials\":-1,\"wheelColor\":156,\"modHorns\":-1,\"modTrimB\":-1,\"modAPlate\":-1,\"modGrille\":-1,\"neonColor\":[255,0,255],\"modRoof\":-1,\"modWindows\":-1,\"modHood\":-1,\"model\":1745872177,\"fuelLevel\":81.1,\"modSpoilers\":-1,\"modVanityPlate\":-1,\"wheels\":1,\"modFender\":-1,\"modFrontBumper\":-1,\"modStruts\":-1,\"modBackWheels\":-1,\"modFrontWheels\":-1,\"dirtLevel\":0.0,\"modTrunk\":-1,\"modSteeringWheel\":-1,\"modAirFilter\":-1,\"extras\":{\"11\":false,\"10\":false,\"1\":false,\"12\":false,\"4\":false,\"5\":false,\"2\":true,\"3\":false,\"8\":false,\"9\":false,\"6\":false,\"7\":false},\"color2\":134,\"modDashboard\":-1,\"modDial\":-1,\"modShifterLeavers\":-1,\"modArchCover\":-1,\"modLivery\":1,\"windowTint\":-1,\"modExhaust\":-1,\"modTank\":-1,\"neonEnabled\":[false,false,false,false],\"modSmokeEnabled\":false,\"modDoorSpeaker\":-1,\"modTurbo\":false,\"modSpeakers\":-1,\"modEngine\":-1,\"modSeats\":-1,\"modOrnaments\":-1}', 'car', 'police', 1),
('steam:1100001139319c1', 'ZFQ 624', '{\"neonColor\":[255,0,255],\"modLivery\":1,\"modTrunk\":-1,\"modAPlate\":-1,\"modHood\":-1,\"modFender\":-1,\"modGrille\":-1,\"modSpoilers\":-1,\"modRightFender\":-1,\"modExhaust\":-1,\"modTransmission\":-1,\"modArchCover\":-1,\"color2\":134,\"modFrontBumper\":-1,\"modFrontWheels\":-1,\"modSeats\":-1,\"model\":598074270,\"modVanityPlate\":-1,\"modAirFilter\":-1,\"modPlateHolder\":-1,\"plate\":\"ZFQ 624\",\"extras\":{\"6\":false,\"5\":true,\"10\":true,\"7\":false,\"2\":false,\"1\":false,\"4\":false,\"3\":false,\"11\":false,\"9\":false,\"8\":false},\"modEngineBlock\":-1,\"modRearBumper\":-1,\"modBackWheels\":-1,\"modSteeringWheel\":-1,\"modTrimB\":-1,\"modDashboard\":-1,\"engineHealth\":1000.0,\"pearlescentColor\":156,\"color1\":134,\"modXenon\":false,\"modEngine\":-1,\"modTank\":-1,\"modFrame\":-1,\"windowTint\":-1,\"modWindows\":-1,\"modSuspension\":-1,\"modTurbo\":false,\"modArmor\":-1,\"modBrakes\":-1,\"plateIndex\":4,\"modDoorSpeaker\":-1,\"modSpeakers\":-1,\"modSmokeEnabled\":false,\"neonEnabled\":[false,false,false,false],\"tyreSmokeColor\":[255,255,255],\"modDial\":-1,\"wheels\":1,\"modOrnaments\":-1,\"wheelColor\":156,\"modRoof\":-1,\"bodyHealth\":1000.0,\"modHydrolic\":-1,\"modStruts\":-1,\"fuelLevel\":81.5,\"modShifterLeavers\":-1,\"modAerials\":-1,\"modSideSkirt\":-1,\"modHorns\":-1,\"modTrimA\":-1,\"dirtLevel\":0.0}', 'car', 'police', 1),
('steam:1100001081d1893', 'ZQO 189', '{\"modBackWheels\":-1,\"modSmokeEnabled\":false,\"modHydrolic\":-1,\"model\":-344943009,\"modSideSkirt\":-1,\"modBrakes\":-1,\"modSpeakers\":-1,\"plate\":\"ZQO 189\",\"modExhaust\":-1,\"wheelColor\":156,\"modFrame\":-1,\"engineHealth\":999.6,\"modPlateHolder\":-1,\"modHood\":-1,\"modDoorSpeaker\":-1,\"modShifterLeavers\":-1,\"modTurbo\":false,\"modRoof\":-1,\"wheels\":0,\"modEngine\":-1,\"modTrunk\":-1,\"pearlescentColor\":111,\"modVanityPlate\":-1,\"modTransmission\":-1,\"modRearBumper\":-1,\"modAerials\":-1,\"modWindows\":-1,\"color1\":6,\"modRightFender\":-1,\"modTrimA\":-1,\"neonEnabled\":[false,false,false,false],\"color2\":0,\"modDial\":-1,\"dirtLevel\":11.2,\"modOrnaments\":-1,\"modEngineBlock\":-1,\"modSteeringWheel\":-1,\"modArchCover\":-1,\"modXenon\":false,\"bodyHealth\":999.8,\"plateIndex\":0,\"modGrille\":-1,\"extras\":{\"12\":false,\"10\":true},\"modTank\":-1,\"tyreSmokeColor\":[255,255,255],\"modFender\":-1,\"modFrontWheels\":-1,\"windowTint\":-1,\"modSeats\":-1,\"modArmor\":-1,\"modTrimB\":-1,\"modAPlate\":-1,\"modHorns\":-1,\"modFrontBumper\":-1,\"fuelLevel\":77.4,\"modDashboard\":-1,\"modAirFilter\":-1,\"modStruts\":-1,\"modSuspension\":-1,\"neonColor\":[255,0,255],\"modSpoilers\":-1,\"modLivery\":-1}', 'car', NULL, 1),
('steam:11000010b49a743', 'ZSR 928', '{\"model\":-1059115956,\"modBrakes\":-1,\"bodyHealth\":1000.0,\"modStruts\":-1,\"modTrimA\":-1,\"extras\":{\"1\":false,\"2\":false,\"3\":false,\"4\":false,\"5\":false,\"6\":false,\"7\":true,\"8\":false,\"9\":false,\"10\":false,\"11\":false,\"12\":false},\"plate\":\"ZSR 928\",\"dirtLevel\":1.0,\"neonColor\":[255,0,255],\"modArchCover\":-1,\"tyreSmokeColor\":[255,255,255],\"modAirFilter\":-1,\"fuelLevel\":77.8,\"modSpeakers\":-1,\"modPlateHolder\":-1,\"pearlescentColor\":156,\"modBackWheels\":-1,\"modTrimB\":-1,\"modEngine\":-1,\"modFender\":-1,\"modTurbo\":false,\"modTank\":-1,\"engineHealth\":1000.0,\"color1\":134,\"modRightFender\":-1,\"color2\":134,\"modHydrolic\":-1,\"modExhaust\":-1,\"modHorns\":-1,\"modXenon\":false,\"modWindows\":-1,\"wheelColor\":156,\"modVanityPlate\":-1,\"modTrunk\":-1,\"modSeats\":-1,\"neonEnabled\":[false,false,false,false],\"modAerials\":-1,\"modAPlate\":-1,\"modTransmission\":-1,\"modGrille\":-1,\"modDashboard\":-1,\"modFrontBumper\":-1,\"modLivery\":3,\"modSuspension\":-1,\"modEngineBlock\":-1,\"windowTint\":-1,\"modSmokeEnabled\":false,\"modFrontWheels\":-1,\"modSideSkirt\":-1,\"modFrame\":-1,\"wheels\":0,\"modSpoilers\":-1,\"modDial\":-1,\"modShifterLeavers\":-1,\"modDoorSpeaker\":-1,\"modRearBumper\":-1,\"modOrnaments\":-1,\"modArmor\":-1,\"modSteeringWheel\":-1,\"modHood\":-1,\"modRoof\":-1,\"plateIndex\":4}', 'car', 'police', 1);

-- --------------------------------------------------------

--
-- Table structure for table `old_users`
--

CREATE TABLE `old_users` (
  `identifier` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `license` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL,
  `money` int(11) DEFAULT NULL,
  `name` varchar(50) COLLATE utf8mb4_bin DEFAULT '',
  `skin` longtext COLLATE utf8mb4_bin DEFAULT NULL,
  `job` varchar(50) COLLATE utf8mb4_bin DEFAULT 'unemployed',
  `job_grade` int(11) DEFAULT 0,
  `loadout` longtext COLLATE utf8mb4_bin DEFAULT NULL,
  `position` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `bank` int(11) DEFAULT NULL,
  `permission_level` int(11) DEFAULT NULL,
  `group` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL,
  `firstname` varchar(50) COLLATE utf8mb4_bin DEFAULT '',
  `lastname` varchar(50) COLLATE utf8mb4_bin DEFAULT '',
  `dateofbirth` varchar(25) COLLATE utf8mb4_bin DEFAULT '',
  `sex` varchar(10) COLLATE utf8mb4_bin DEFAULT '',
  `height` varchar(5) COLLATE utf8mb4_bin DEFAULT '',
  `status` longtext COLLATE utf8mb4_bin DEFAULT NULL,
  `last_property` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `is_dead` tinyint(1) DEFAULT 0,
  `phone_number` varchar(10) COLLATE utf8mb4_bin DEFAULT NULL,
  `jail_time` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `old_users`
--

INSERT INTO `old_users` (`identifier`, `license`, `money`, `name`, `skin`, `job`, `job_grade`, `loadout`, `position`, `bank`, `permission_level`, `group`, `firstname`, `lastname`, `dateofbirth`, `sex`, `height`, `status`, `last_property`, `is_dead`, `phone_number`, `jail_time`) VALUES
('steam:1100001013142e0', 'license:e30e0bd16a8b01da8b6e91ce04e69827aa98aed0', 0, 'ThombStone', '{\"blush_3\":0,\"beard_3\":0,\"bags_1\":0,\"makeup_2\":0,\"makeup_3\":0,\"mask_2\":0,\"arms_2\":0,\"glasses_2\":0,\"decals_1\":0,\"moles_2\":0,\"sun_2\":0,\"shoes_2\":0,\"tshirt_2\":0,\"hair_2\":0,\"lipstick_2\":0,\"pants_1\":0,\"makeup_4\":0,\"chest_2\":0,\"chain_2\":0,\"lipstick_3\":0,\"pants_2\":0,\"torso_2\":0,\"bracelets_2\":0,\"blemishes_1\":0,\"bproof_1\":0,\"bags_2\":0,\"eyebrows_1\":0,\"bodyb_2\":0,\"beard_4\":0,\"chain_1\":0,\"sun_1\":0,\"hair_color_2\":0,\"blush_2\":0,\"chest_3\":0,\"face\":0,\"blemishes_2\":0,\"chest_1\":0,\"makeup_1\":0,\"shoes_1\":0,\"moles_1\":0,\"beard_2\":0,\"decals_2\":0,\"age_1\":0,\"hair_color_1\":0,\"bproof_2\":0,\"complexion_2\":0,\"torso_1\":0,\"lipstick_4\":0,\"watches_2\":0,\"bodyb_1\":0,\"arms\":0,\"eyebrows_2\":0,\"ears_1\":-1,\"eye_color\":0,\"ears_2\":0,\"blush_1\":0,\"sex\":0,\"age_2\":0,\"bracelets_1\":-1,\"glasses_1\":0,\"hair_1\":0,\"lipstick_1\":0,\"eyebrows_3\":0,\"mask_1\":0,\"complexion_1\":0,\"watches_1\":-1,\"helmet_1\":-1,\"beard_1\":0,\"eyebrows_4\":0,\"skin\":0,\"tshirt_1\":0,\"helmet_2\":0}', 'unemployed', 0, '[]', '{\"x\":-838.1,\"z\":215.0,\"y\":4181.6}', 0, 0, 'user', 'Roy', 'Moshkovich', '1996-07-04', 'm', '180', '[{\"name\":\"hunger\",\"percent\":98.4,\"val\":984000},{\"name\":\"thirst\",\"percent\":98.8,\"val\":988000},{\"name\":\"drunk\",\"percent\":0.0,\"val\":0}]', NULL, 0, '933-5393', 0),
('steam:110000105ed368b', 'license:468b43146dc337352c6c210541d2fe1a4794c7e4', 14971, 'Joker', '{\"blush_2\":0,\"torso_2\":0,\"chest_2\":0,\"bproof_1\":34,\"bracelets_1\":-1,\"complexion_2\":0,\"skin\":13,\"pants_2\":1,\"watches_2\":0,\"hair_color_1\":18,\"decals_1\":0,\"chest_3\":0,\"arms_2\":0,\"hair_1\":19,\"beard_4\":0,\"watches_1\":-1,\"hair_2\":4,\"bags_2\":0,\"complexion_1\":0,\"lipstick_1\":0,\"bags_1\":0,\"makeup_2\":0,\"chest_1\":0,\"blush_1\":0,\"bodyb_2\":0,\"makeup_3\":0,\"sex\":0,\"bodyb_1\":0,\"chain_1\":28,\"ears_1\":2,\"shoes_2\":0,\"age_1\":0,\"tshirt_1\":96,\"age_2\":0,\"mask_1\":0,\"decals_2\":0,\"beard_1\":18,\"lipstick_2\":0,\"blemishes_1\":0,\"torso_1\":115,\"lipstick_4\":0,\"eyebrows_1\":3,\"bproof_2\":0,\"blemishes_2\":0,\"mask_2\":0,\"blush_3\":0,\"eyebrows_2\":10,\"bracelets_2\":0,\"moles_1\":0,\"lipstick_3\":0,\"makeup_1\":0,\"eyebrows_4\":0,\"helmet_1\":-1,\"shoes_1\":2,\"glasses_1\":3,\"tshirt_2\":17,\"moles_2\":0,\"ears_2\":0,\"helmet_2\":0,\"beard_2\":10,\"eyebrows_3\":26,\"eye_color\":0,\"face\":12,\"hair_color_2\":0,\"sun_2\":0,\"pants_1\":1,\"chain_2\":9,\"makeup_4\":0,\"glasses_2\":0,\"arms\":6,\"sun_1\":0,\"beard_3\":0}', 'unemployed', 0, '[{\"label\":\"Knife\",\"name\":\"WEAPON_KNIFE\",\"components\":[],\"ammo\":0},{\"label\":\"Nightstick\",\"name\":\"WEAPON_NIGHTSTICK\",\"components\":[],\"ammo\":0},{\"label\":\"Hammer\",\"name\":\"WEAPON_HAMMER\",\"components\":[],\"ammo\":0},{\"label\":\"Bat\",\"name\":\"WEAPON_BAT\",\"components\":[],\"ammo\":0},{\"label\":\"Golf club\",\"name\":\"WEAPON_GOLFCLUB\",\"components\":[],\"ammo\":0},{\"label\":\"Crow bar\",\"name\":\"WEAPON_CROWBAR\",\"components\":[],\"ammo\":0},{\"label\":\"Pistol\",\"name\":\"WEAPON_PISTOL\",\"components\":[\"clip_default\"],\"ammo\":1000},{\"label\":\"Combat pistol\",\"name\":\"WEAPON_COMBATPISTOL\",\"components\":[\"clip_default\",\"flashlight\"],\"ammo\":1000},{\"label\":\"AP pistol\",\"name\":\"WEAPON_APPISTOL\",\"components\":[\"clip_default\"],\"ammo\":1000},{\"label\":\"Pistol .50\",\"name\":\"WEAPON_PISTOL50\",\"components\":[\"clip_default\"],\"ammo\":1000},{\"label\":\"Heavy revolver\",\"name\":\"WEAPON_REVOLVER\",\"components\":[],\"ammo\":1000},{\"label\":\"Sns pistol\",\"name\":\"WEAPON_SNSPISTOL\",\"components\":[\"clip_default\"],\"ammo\":1000},{\"label\":\"Heavy pistol\",\"name\":\"WEAPON_HEAVYPISTOL\",\"components\":[\"clip_default\"],\"ammo\":1000},{\"label\":\"Vintage pistol\",\"name\":\"WEAPON_VINTAGEPISTOL\",\"components\":[\"clip_default\"],\"ammo\":1000},{\"label\":\"Micro SMG\",\"name\":\"WEAPON_MICROSMG\",\"components\":[\"clip_default\"],\"ammo\":1000},{\"label\":\"SMG\",\"name\":\"WEAPON_SMG\",\"components\":[\"clip_default\"],\"ammo\":1000},{\"label\":\"Assault SMG\",\"name\":\"WEAPON_ASSAULTSMG\",\"components\":[\"clip_default\"],\"ammo\":1000},{\"label\":\"Mini smg\",\"name\":\"WEAPON_MINISMG\",\"components\":[\"clip_default\"],\"ammo\":1000},{\"label\":\"Machine pistol\",\"name\":\"WEAPON_MACHINEPISTOL\",\"components\":[\"clip_default\"],\"ammo\":1000},{\"label\":\"Combat pdw\",\"name\":\"WEAPON_COMBATPDW\",\"components\":[\"clip_default\"],\"ammo\":1000},{\"label\":\"Pump shotgun\",\"name\":\"WEAPON_PUMPSHOTGUN\",\"components\":[],\"ammo\":1000},{\"label\":\"Sawed off shotgun\",\"name\":\"WEAPON_SAWNOFFSHOTGUN\",\"components\":[],\"ammo\":1000},{\"label\":\"Assault shotgun\",\"name\":\"WEAPON_ASSAULTSHOTGUN\",\"components\":[\"clip_default\"],\"ammo\":1000},{\"label\":\"Bullpup shotgun\",\"name\":\"WEAPON_BULLPUPSHOTGUN\",\"components\":[],\"ammo\":1000},{\"label\":\"Heavy shotgun\",\"name\":\"WEAPON_HEAVYSHOTGUN\",\"components\":[\"clip_default\"],\"ammo\":1000},{\"label\":\"Assault rifle\",\"name\":\"WEAPON_ASSAULTRIFLE\",\"components\":[\"clip_default\"],\"ammo\":1000},{\"label\":\"Carbine rifle\",\"name\":\"WEAPON_CARBINERIFLE\",\"components\":[\"clip_default\"],\"ammo\":1000},{\"label\":\"Advanced rifle\",\"name\":\"WEAPON_ADVANCEDRIFLE\",\"components\":[\"clip_default\"],\"ammo\":1000},{\"label\":\"Special carbine\",\"name\":\"WEAPON_SPECIALCARBINE\",\"components\":[\"clip_default\"],\"ammo\":1000},{\"label\":\"Bullpup rifle\",\"name\":\"WEAPON_BULLPUPRIFLE\",\"components\":[\"clip_default\"],\"ammo\":1000},{\"label\":\"Compact rifle\",\"name\":\"WEAPON_COMPACTRIFLE\",\"components\":[\"clip_default\"],\"ammo\":1000},{\"label\":\"MG\",\"name\":\"WEAPON_MG\",\"components\":[\"clip_default\"],\"ammo\":1000},{\"label\":\"Combat MG\",\"name\":\"WEAPON_COMBATMG\",\"components\":[\"clip_default\"],\"ammo\":1000},{\"label\":\"Gusenberg sweeper\",\"name\":\"WEAPON_GUSENBERG\",\"components\":[\"clip_default\"],\"ammo\":1000},{\"label\":\"Sniper rifle\",\"name\":\"WEAPON_SNIPERRIFLE\",\"components\":[\"scope\"],\"ammo\":994},{\"label\":\"Heavy sniper\",\"name\":\"WEAPON_HEAVYSNIPER\",\"components\":[\"scope_advanced\"],\"ammo\":994},{\"label\":\"Marksman rifle\",\"name\":\"WEAPON_MARKSMANRIFLE\",\"components\":[\"clip_default\",\"scope\"],\"ammo\":994},{\"label\":\"Grenade launcher\",\"name\":\"WEAPON_GRENADELAUNCHER\",\"components\":[],\"ammo\":20},{\"label\":\"Rocket launcher\",\"name\":\"WEAPON_RPG\",\"components\":[],\"ammo\":20},{\"label\":\"Minigun\",\"name\":\"WEAPON_MINIGUN\",\"components\":[],\"ammo\":1000},{\"label\":\"Grenade\",\"name\":\"WEAPON_GRENADE\",\"components\":[],\"ammo\":24},{\"label\":\"Sticky bomb\",\"name\":\"WEAPON_STICKYBOMB\",\"components\":[],\"ammo\":25},{\"label\":\"Smoke grenade\",\"name\":\"WEAPON_SMOKEGRENADE\",\"components\":[],\"ammo\":25},{\"label\":\"Bz gas\",\"name\":\"WEAPON_BZGAS\",\"components\":[],\"ammo\":25},{\"label\":\"Molotov cocktail\",\"name\":\"WEAPON_MOLOTOV\",\"components\":[],\"ammo\":25},{\"label\":\"Fire extinguisher\",\"name\":\"WEAPON_FIREEXTINGUISHER\",\"components\":[],\"ammo\":2000},{\"label\":\"Jerrycan\",\"name\":\"WEAPON_PETROLCAN\",\"components\":[],\"ammo\":3279},{\"label\":\"Ball\",\"name\":\"WEAPON_BALL\",\"components\":[],\"ammo\":1},{\"label\":\"Bottle\",\"name\":\"WEAPON_BOTTLE\",\"components\":[],\"ammo\":0},{\"label\":\"Dagger\",\"name\":\"WEAPON_DAGGER\",\"components\":[],\"ammo\":0},{\"label\":\"Firework\",\"name\":\"WEAPON_FIREWORK\",\"components\":[],\"ammo\":20},{\"label\":\"Musket\",\"name\":\"WEAPON_MUSKET\",\"components\":[],\"ammo\":1000},{\"label\":\"Taser\",\"name\":\"WEAPON_STUNGUN\",\"components\":[],\"ammo\":1000},{\"label\":\"Homing launcher\",\"name\":\"WEAPON_HOMINGLAUNCHER\",\"components\":[],\"ammo\":10},{\"label\":\"Snow ball\",\"name\":\"WEAPON_SNOWBALL\",\"components\":[],\"ammo\":10},{\"label\":\"Flaregun\",\"name\":\"WEAPON_FLAREGUN\",\"components\":[],\"ammo\":15},{\"label\":\"Marksman pistol\",\"name\":\"WEAPON_MARKSMANPISTOL\",\"components\":[],\"ammo\":1000},{\"label\":\"Knuckledusters\",\"name\":\"WEAPON_KNUCKLE\",\"components\":[],\"ammo\":0},{\"label\":\"Hatchet\",\"name\":\"WEAPON_HATCHET\",\"components\":[],\"ammo\":0},{\"label\":\"Railgun\",\"name\":\"WEAPON_RAILGUN\",\"components\":[],\"ammo\":20},{\"label\":\"Machete\",\"name\":\"WEAPON_MACHETE\",\"components\":[],\"ammo\":0},{\"label\":\"Switchblade\",\"name\":\"WEAPON_SWITCHBLADE\",\"components\":[],\"ammo\":0},{\"label\":\"Double barrel shotgun\",\"name\":\"WEAPON_DBSHOTGUN\",\"components\":[],\"ammo\":1000},{\"label\":\"Auto shotgun\",\"name\":\"WEAPON_AUTOSHOTGUN\",\"components\":[],\"ammo\":1000},{\"label\":\"Battle axe\",\"name\":\"WEAPON_BATTLEAXE\",\"components\":[],\"ammo\":0},{\"label\":\"Compact launcher\",\"name\":\"WEAPON_COMPACTLAUNCHER\",\"components\":[],\"ammo\":20},{\"label\":\"Pipe bomb\",\"name\":\"WEAPON_PIPEBOMB\",\"components\":[],\"ammo\":10},{\"label\":\"Pool cue\",\"name\":\"WEAPON_POOLCUE\",\"components\":[],\"ammo\":0},{\"label\":\"Pipe wrench\",\"name\":\"WEAPON_WRENCH\",\"components\":[],\"ammo\":0},{\"label\":\"Flashlight\",\"name\":\"WEAPON_FLASHLIGHT\",\"components\":[],\"ammo\":0},{\"label\":\"Flare gun\",\"name\":\"WEAPON_FLARE\",\"components\":[],\"ammo\":25},{\"label\":\"Double-Action Revolver\",\"name\":\"WEAPON_DOUBLEACTION\",\"components\":[],\"ammo\":1000}]', '{\"z\":38.8,\"x\":1597.2,\"y\":3596.0}', 270875, 1, 'superadmin', 'Dean', 'Conners', '1985-07-14', 'm', '160', '[{\"name\":\"hunger\",\"val\":709500,\"percent\":70.95},{\"name\":\"thirst\",\"val\":557125,\"percent\":55.7125},{\"name\":\"drunk\",\"val\":0,\"percent\":0.0}]', NULL, 0, '501-1190', 0),
('steam:110000106197c1a', 'license:a8f78a1b293e00697b6d5296a0cbbe4cef9b08c0', 277899, 'billsrock35', '{\"ears_2\":0,\"torso_2\":0,\"eyebrows_2\":0,\"hair_color_2\":0,\"skin\":0,\"makeup_3\":0,\"glasses_2\":0,\"hair_color_1\":0,\"sun_1\":0,\"blush_1\":0,\"sex\":0,\"eyebrows_1\":0,\"blush_2\":0,\"beard_3\":0,\"beard_2\":0,\"chest_2\":0,\"bodyb_1\":0,\"mask_1\":0,\"beard_1\":0,\"hair_1\":0,\"complexion_1\":0,\"makeup_4\":0,\"moles_1\":0,\"ears_1\":-1,\"bproof_2\":0,\"pants_1\":0,\"moles_2\":0,\"age_2\":0,\"tshirt_1\":0,\"shoes_1\":0,\"helmet_2\":0,\"eyebrows_4\":0,\"glasses_1\":0,\"arms\":0,\"bracelets_2\":0,\"chain_2\":0,\"mask_2\":0,\"makeup_2\":0,\"bracelets_1\":-1,\"face\":0,\"complexion_2\":0,\"lipstick_4\":0,\"lipstick_2\":0,\"chain_1\":0,\"pants_2\":0,\"decals_1\":0,\"age_1\":0,\"eyebrows_3\":0,\"bags_2\":0,\"bags_1\":0,\"shoes_2\":0,\"lipstick_1\":0,\"bodyb_2\":0,\"tshirt_2\":0,\"helmet_1\":-1,\"sun_2\":0,\"makeup_1\":0,\"watches_1\":-1,\"bproof_1\":0,\"blush_3\":0,\"torso_1\":0,\"lipstick_3\":0,\"blemishes_1\":0,\"decals_2\":0,\"chest_3\":0,\"blemishes_2\":0,\"hair_2\":0,\"beard_4\":0,\"eye_color\":0,\"chest_1\":0,\"arms_2\":0,\"watches_2\":0}', 'mechanic', 1, '[{\"label\":\"Pistol\",\"name\":\"WEAPON_PISTOL\",\"ammo\":252,\"components\":[\"clip_default\"]},{\"label\":\"Combat pistol\",\"name\":\"WEAPON_COMBATPISTOL\",\"ammo\":252,\"components\":[\"clip_default\"]},{\"label\":\"Pistol .50\",\"name\":\"WEAPON_PISTOL50\",\"ammo\":252,\"components\":[\"clip_default\"]},{\"label\":\"Heavy revolver\",\"name\":\"WEAPON_REVOLVER\",\"ammo\":252,\"components\":[]},{\"label\":\"Heavy pistol\",\"name\":\"WEAPON_HEAVYPISTOL\",\"ammo\":252,\"components\":[\"clip_default\"]},{\"label\":\"SMG\",\"name\":\"WEAPON_SMG\",\"ammo\":42,\"components\":[\"clip_default\"]},{\"label\":\"Pump shotgun\",\"name\":\"WEAPON_PUMPSHOTGUN\",\"ammo\":10,\"components\":[]},{\"label\":\"Assault rifle\",\"name\":\"WEAPON_ASSAULTRIFLE\",\"ammo\":126,\"components\":[\"clip_default\"]},{\"label\":\"Carbine rifle\",\"name\":\"WEAPON_CARBINERIFLE\",\"ammo\":126,\"components\":[\"clip_default\"]},{\"label\":\"Advanced rifle\",\"name\":\"WEAPON_ADVANCEDRIFLE\",\"ammo\":126,\"components\":[\"clip_default\"]},{\"label\":\"Sniper rifle\",\"name\":\"WEAPON_SNIPERRIFLE\",\"ammo\":42,\"components\":[\"scope\"]},{\"label\":\"Smoke grenade\",\"name\":\"WEAPON_SMOKEGRENADE\",\"ammo\":25,\"components\":[]},{\"label\":\"Machete\",\"name\":\"WEAPON_MACHETE\",\"ammo\":0,\"components\":[]},{\"label\":\"Flare gun\",\"name\":\"WEAPON_FLARE\",\"ammo\":25,\"components\":[]}]', '{\"x\":1294.1,\"z\":33.5,\"y\":3665.1}', 3600, 0, 'user', 'Austin', 'Nocera', '1995-06-09', 'm', '200', '[{\"val\":258900,\"percent\":25.89,\"name\":\"hunger\"},{\"val\":319175,\"percent\":31.9175,\"name\":\"thirst\"},{\"val\":0,\"percent\":0.0,\"name\":\"drunk\"}]', NULL, 0, '367-8401', 60),
('steam:110000107941620', 'license:c5d34e7a8849335a40f53f3d2612c6d801a8fdac', 800, 'Jedi', '{\"decals_2\":0,\"eyebrows_4\":0,\"makeup_4\":0,\"hair_1\":0,\"tshirt_2\":0,\"hair_2\":0,\"bracelets_1\":-1,\"torso_1\":0,\"pants_2\":0,\"chest_1\":0,\"decals_1\":0,\"hair_color_2\":0,\"chest_3\":0,\"shoes_2\":0,\"complexion_1\":0,\"blemishes_2\":0,\"arms_2\":0,\"blush_1\":0,\"sun_1\":0,\"sex\":0,\"bproof_1\":0,\"helmet_1\":-1,\"hair_color_1\":0,\"bags_2\":0,\"lipstick_2\":0,\"blush_3\":0,\"moles_1\":0,\"arms\":0,\"moles_2\":0,\"skin\":0,\"sun_2\":0,\"shoes_1\":0,\"blush_2\":0,\"ears_1\":-1,\"blemishes_1\":0,\"mask_2\":0,\"torso_2\":0,\"eyebrows_2\":0,\"face\":0,\"glasses_2\":0,\"glasses_1\":0,\"makeup_3\":0,\"helmet_2\":0,\"chain_2\":0,\"eyebrows_1\":0,\"pants_1\":0,\"beard_4\":0,\"chain_1\":0,\"beard_1\":0,\"mask_1\":0,\"tshirt_1\":0,\"watches_2\":0,\"makeup_2\":0,\"bracelets_2\":0,\"bodyb_1\":0,\"age_1\":0,\"complexion_2\":0,\"bags_1\":0,\"bodyb_2\":0,\"eyebrows_3\":0,\"lipstick_4\":0,\"ears_2\":0,\"chest_2\":0,\"makeup_1\":0,\"watches_1\":-1,\"beard_2\":0,\"lipstick_1\":0,\"bproof_2\":0,\"eye_color\":0,\"lipstick_3\":0,\"beard_3\":0,\"age_2\":0}', 'taxi', 0, '[{\"ammo\":985,\"label\":\"Sns pistol\",\"name\":\"WEAPON_SNSPISTOL\",\"components\":[\"clip_default\"]},{\"ammo\":4375,\"label\":\"Jerrycan\",\"name\":\"WEAPON_PETROLCAN\",\"components\":[]},{\"ammo\":1000,\"label\":\"Taser\",\"name\":\"WEAPON_STUNGUN\",\"components\":[]}]', '{\"y\":3318.9,\"x\":1783.2,\"z\":40.9}', 137720, 0, 'user', 'Jon', 'Smtih', '7/23/1993', 'm', '200', '[{\"name\":\"hunger\",\"percent\":66.11,\"val\":661100},{\"name\":\"thirst\",\"percent\":45.7425,\"val\":457425},{\"name\":\"drunk\",\"percent\":0.0,\"val\":0}]', NULL, 0, '427-1473', 0),
('steam:1100001081d1893', 'license:335b97a11f294fa23e5ab4446ddf5f9d9452b275', 521802, '456', '{\"lipstick_4\":0,\"watches_2\":0,\"mask_1\":0,\"pants_1\":0,\"glasses_2\":0,\"beard_3\":0,\"makeup_4\":0,\"chest_1\":0,\"skin\":0,\"arms\":0,\"decals_2\":0,\"moles_1\":0,\"moles_2\":0,\"blemishes_1\":0,\"chain_2\":0,\"bproof_2\":0,\"mask_2\":0,\"blush_3\":0,\"sun_2\":0,\"complexion_2\":0,\"lipstick_3\":0,\"torso_1\":0,\"shoes_1\":0,\"age_1\":0,\"ears_1\":-1,\"tshirt_1\":0,\"complexion_1\":0,\"bracelets_2\":0,\"shoes_2\":0,\"blush_1\":0,\"glasses_1\":0,\"eyebrows_1\":0,\"hair_color_1\":0,\"hair_2\":0,\"age_2\":0,\"ears_2\":0,\"helmet_1\":-1,\"beard_1\":0,\"bags_2\":0,\"tshirt_2\":0,\"chain_1\":0,\"makeup_1\":0,\"blush_2\":0,\"beard_4\":0,\"chest_3\":0,\"bodyb_1\":0,\"watches_1\":-1,\"hair_color_2\":0,\"hair_1\":2,\"torso_2\":0,\"chest_2\":0,\"bags_1\":0,\"blemishes_2\":0,\"eyebrows_3\":0,\"pants_2\":0,\"bodyb_2\":0,\"face\":0,\"lipstick_2\":0,\"bracelets_1\":-1,\"eyebrows_2\":0,\"arms_2\":0,\"sex\":0,\"lipstick_1\":0,\"makeup_2\":0,\"eyebrows_4\":0,\"sun_1\":0,\"helmet_2\":0,\"eye_color\":0,\"bproof_1\":0,\"beard_2\":0,\"decals_1\":0,\"makeup_3\":0}', 'police', 6, '[{\"ammo\":0,\"label\":\"Nightstick\",\"name\":\"WEAPON_NIGHTSTICK\",\"components\":[]},{\"ammo\":73,\"label\":\"Combat pistol\",\"name\":\"WEAPON_COMBATPISTOL\",\"components\":[\"clip_default\",\"flashlight\"]},{\"ammo\":46,\"label\":\"Pump shotgun\",\"name\":\"WEAPON_PUMPSHOTGUN\",\"components\":[\"flashlight\"]},{\"ammo\":150,\"label\":\"Carbine rifle\",\"name\":\"WEAPON_CARBINERIFLE\",\"components\":[\"clip_extended\",\"flashlight\"]},{\"ammo\":0,\"label\":\"Fire extinguisher\",\"name\":\"WEAPON_FIREEXTINGUISHER\",\"components\":[]},{\"ammo\":100,\"label\":\"Taser\",\"name\":\"WEAPON_STUNGUN\",\"components\":[]},{\"ammo\":0,\"label\":\"Flashlight\",\"name\":\"WEAPON_FLASHLIGHT\",\"components\":[]}]', '{\"y\":3672.6,\"z\":33.7,\"x\":1852.0}', 1395600, 1, 'superadmin', 'Godrick', 'Twinkletoes', '1994-09-12', 'm', '200', '[{\"val\":922900,\"name\":\"hunger\",\"percent\":92.29},{\"val\":941425,\"name\":\"thirst\",\"percent\":94.1425},{\"val\":0,\"name\":\"drunk\",\"percent\":0.0}]', NULL, 0, '531-3502', 0),
('steam:11000010a01bdb9', 'license:cb5765d5073314f0d95e9e69ab51cf5cc5e16d7d', 0, 'stickybombz', '{\"tshirt_2\":0,\"eyebrows_1\":0,\"arms\":0,\"shoes_2\":0,\"age_1\":0,\"hair_1\":0,\"hair_color_1\":0,\"skin\":0,\"glasses_2\":0,\"sun_1\":0,\"hair_2\":0,\"blemishes_2\":0,\"chest_2\":0,\"complexion_1\":0,\"torso_1\":0,\"bags_1\":0,\"shoes_1\":0,\"beard_3\":0,\"arms_2\":0,\"beard_1\":0,\"chest_1\":0,\"decals_1\":0,\"ears_2\":0,\"makeup_3\":0,\"eyebrows_4\":0,\"pants_2\":0,\"bags_2\":0,\"bodyb_2\":0,\"hair_color_2\":0,\"watches_2\":0,\"moles_1\":0,\"face\":0,\"sun_2\":0,\"sex\":0,\"chain_1\":0,\"lipstick_4\":0,\"moles_2\":0,\"beard_2\":0,\"decals_2\":0,\"lipstick_2\":0,\"blush_1\":0,\"makeup_2\":0,\"mask_1\":0,\"chest_3\":0,\"bproof_2\":0,\"torso_2\":0,\"lipstick_3\":0,\"ears_1\":-1,\"helmet_1\":-1,\"bproof_1\":0,\"complexion_2\":0,\"eyebrows_3\":0,\"lipstick_1\":0,\"blush_3\":0,\"bracelets_1\":-1,\"bodyb_1\":0,\"eyebrows_2\":0,\"pants_1\":0,\"makeup_1\":0,\"blush_2\":0,\"helmet_2\":0,\"mask_2\":0,\"age_2\":0,\"beard_4\":0,\"bracelets_2\":0,\"tshirt_1\":0,\"makeup_4\":0,\"watches_1\":-1,\"chain_2\":0,\"blemishes_1\":0,\"glasses_1\":0,\"eye_color\":0}', 'police', 0, '[]', '{\"z\":31.2,\"x\":-243.4,\"y\":-958.5}', 1790, 0, 'user', 'Tommie', 'Pickles', '1988-12-28', 'm', '172', '[{\"val\":125900,\"percent\":12.59,\"name\":\"hunger\"},{\"val\":344425,\"percent\":34.4425,\"name\":\"thirst\"},{\"val\":0,\"percent\":0.0,\"name\":\"drunk\"}]', NULL, 0, '873-7104', 0),
('steam:11000010b49a743', 'license:e5d2acf163c043923bfb9d0a3117fb456b890766', 188778, 'killian2134', '{\"chest_3\":0,\"bodyb_1\":0,\"face\":0,\"sex\":0,\"bproof_2\":0,\"chest_1\":0,\"bags_1\":0,\"ears_1\":-1,\"torso_1\":0,\"ears_2\":0,\"eyebrows_2\":0,\"skin\":0,\"moles_2\":0,\"chain_2\":0,\"helmet_1\":-1,\"sun_1\":0,\"watches_1\":-1,\"chest_2\":0,\"decals_2\":0,\"makeup_3\":0,\"arms\":0,\"eyebrows_4\":0,\"beard_1\":0,\"hair_color_2\":0,\"hair_color_1\":0,\"eye_color\":0,\"bracelets_2\":0,\"makeup_2\":0,\"blemishes_2\":0,\"torso_2\":0,\"watches_2\":0,\"tshirt_2\":0,\"bproof_1\":0,\"shoes_1\":24,\"lipstick_2\":0,\"blush_3\":0,\"beard_3\":0,\"eyebrows_1\":0,\"blush_1\":0,\"sun_2\":0,\"decals_1\":0,\"mask_2\":0,\"hair_2\":0,\"age_2\":0,\"blush_2\":0,\"tshirt_1\":0,\"bracelets_1\":-1,\"bags_2\":0,\"glasses_1\":0,\"helmet_2\":0,\"bodyb_2\":0,\"shoes_2\":0,\"blemishes_1\":0,\"beard_4\":0,\"pants_1\":0,\"beard_2\":0,\"mask_1\":0,\"glasses_2\":0,\"arms_2\":0,\"pants_2\":0,\"eyebrows_3\":0,\"makeup_4\":0,\"lipstick_3\":0,\"complexion_1\":0,\"age_1\":0,\"makeup_1\":0,\"complexion_2\":0,\"lipstick_4\":0,\"chain_1\":0,\"lipstick_1\":0,\"moles_1\":0,\"hair_1\":4}', 'police', 4, '[{\"label\":\"Knife\",\"ammo\":0,\"components\":[],\"name\":\"WEAPON_KNIFE\"},{\"label\":\"Nightstick\",\"ammo\":0,\"components\":[],\"name\":\"WEAPON_NIGHTSTICK\"},{\"label\":\"Combat pistol\",\"ammo\":1000,\"components\":[\"clip_default\",\"flashlight\"],\"name\":\"WEAPON_COMBATPISTOL\"},{\"label\":\"SMG\",\"ammo\":1000,\"components\":[\"clip_extended\",\"flashlight\",\"scope\"],\"name\":\"WEAPON_SMG\"},{\"label\":\"Pump shotgun\",\"ammo\":1000,\"components\":[\"flashlight\"],\"name\":\"WEAPON_PUMPSHOTGUN\"},{\"label\":\"Carbine rifle\",\"ammo\":974,\"components\":[\"clip_default\",\"flashlight\",\"scope\",\"grip\"],\"name\":\"WEAPON_CARBINERIFLE\"},{\"label\":\"Heavy sniper\",\"ammo\":998,\"components\":[\"scope_advanced\"],\"name\":\"WEAPON_HEAVYSNIPER\"},{\"label\":\"Bz gas\",\"ammo\":25,\"components\":[],\"name\":\"WEAPON_BZGAS\"},{\"label\":\"Fire extinguisher\",\"ammo\":2000,\"components\":[],\"name\":\"WEAPON_FIREEXTINGUISHER\"},{\"label\":\"Taser\",\"ammo\":1000,\"components\":[],\"name\":\"WEAPON_STUNGUN\"},{\"label\":\"Flashlight\",\"ammo\":0,\"components\":[],\"name\":\"WEAPON_FLASHLIGHT\"},{\"label\":\"Flare gun\",\"ammo\":25,\"components\":[],\"name\":\"WEAPON_FLARE\"}]', '{\"x\":1865.9,\"z\":34.3,\"y\":3688.9}', 1092535, 1, 'superadmin', 'Killian', 'Oconnell', '19960529', 'm', '155', '[{\"val\":788900,\"name\":\"hunger\",\"percent\":78.89},{\"val\":842125,\"name\":\"thirst\",\"percent\":84.2125},{\"val\":0,\"name\":\"drunk\",\"percent\":0.0}]', NULL, 0, '906-8228', 0),
('steam:11000010b88b452', 'license:45a5d3fb073a103ebd2dfbe1b39779508c21967a', 0, 'Ebaklak', NULL, 'unemployed', 0, '[]', '{\"x\":-2057.8,\"z\":32.8,\"y\":2835.4}', 200, 0, 'user', 'Sashka', 'Gbbrg', '20000101', 'm', '190', '[{\"percent\":93.35,\"val\":933500,\"name\":\"hunger\"},{\"percent\":95.0125,\"val\":950125,\"name\":\"thirst\"},{\"percent\":0.0,\"val\":0,\"name\":\"drunk\"}]', NULL, 1, '183-6771', 0),
('steam:11000010cfb10c1', 'license:1d01db020b3634fa219da748f05a60bf6ce33e88', 994, 'Knightrider164', NULL, 'fueler', 0, '[{\"ammo\":39,\"label\":\"Pistol\",\"components\":[\"clip_default\"],\"name\":\"WEAPON_PISTOL\"},{\"ammo\":4494,\"label\":\"Jerrycan\",\"components\":[],\"name\":\"WEAPON_PETROLCAN\"}]', '{\"x\":1651.4,\"y\":2856.3,\"z\":41.0}', 28000, 0, 'user', 'Tommy', 'Callahan', '1994-06-08', 'm', '165', '[{\"val\":445100,\"name\":\"hunger\",\"percent\":44.51},{\"val\":308825,\"name\":\"thirst\",\"percent\":30.8825},{\"val\":0,\"name\":\"drunk\",\"percent\":0.0}]', NULL, 1, '288-7796', 0),
('steam:11000010d651395', 'license:03af6ed28bbd61a7b9caaa082b27755547e5da0e', 1181, 'tmfaucoin', '{\"glasses_2\":0,\"bproof_1\":0,\"decals_1\":0,\"age_1\":0,\"makeup_1\":0,\"tshirt_2\":0,\"glasses_1\":0,\"bodyb_1\":0,\"watches_2\":0,\"age_2\":0,\"watches_1\":-1,\"bodyb_2\":0,\"moles_1\":0,\"decals_2\":0,\"moles_2\":0,\"pants_1\":0,\"bracelets_2\":0,\"beard_2\":0,\"sex\":0,\"eyebrows_3\":0,\"blush_1\":0,\"lipstick_4\":0,\"torso_2\":0,\"hair_2\":0,\"helmet_2\":0,\"mask_2\":0,\"face\":0,\"sun_2\":0,\"shoes_2\":0,\"arms\":0,\"blush_3\":0,\"skin\":0,\"hair_color_2\":0,\"chain_1\":0,\"blemishes_2\":0,\"beard_4\":0,\"lipstick_2\":0,\"ears_1\":-1,\"chest_2\":0,\"makeup_3\":0,\"makeup_2\":0,\"bproof_2\":0,\"shoes_1\":0,\"mask_1\":0,\"makeup_4\":0,\"hair_1\":0,\"pants_2\":0,\"sun_1\":0,\"bags_1\":0,\"hair_color_1\":0,\"complexion_1\":0,\"eyebrows_2\":0,\"arms_2\":0,\"helmet_1\":-1,\"beard_1\":0,\"eye_color\":0,\"torso_1\":0,\"bracelets_1\":-1,\"lipstick_3\":0,\"blush_2\":0,\"blemishes_1\":0,\"chain_2\":0,\"bags_2\":0,\"ears_2\":0,\"eyebrows_4\":0,\"eyebrows_1\":0,\"tshirt_1\":0,\"chest_1\":0,\"complexion_2\":0,\"chest_3\":0,\"beard_3\":0,\"lipstick_1\":0}', 'fueler', 0, '[]', '{\"y\":2855.8,\"x\":600.8,\"z\":40.0}', -820, 0, 'user', 'Ryan', 'Leeson', '06-10-1993', 'm', '165', '[{\"percent\":22.88,\"val\":228800,\"name\":\"hunger\"},{\"percent\":29.66,\"val\":296600,\"name\":\"thirst\"},{\"percent\":0.0,\"val\":0,\"name\":\"drunk\"}]', NULL, 0, '926-2758', 0),
('steam:11000010e2a62e2', 'license:4bd8348aff04ce40e48df509991d5f1d7a857ceb', 22311, 'Metalhead434', '{\"blush_1\":0,\"eyebrows_3\":0,\"hair_2\":0,\"bodyb_2\":0,\"bproof_2\":1,\"makeup_2\":0,\"makeup_3\":0,\"eyebrows_4\":0,\"helmet_1\":58,\"pants_2\":7,\"age_2\":0,\"torso_1\":94,\"lipstick_2\":0,\"chain_2\":0,\"torso_2\":0,\"decals_2\":0,\"bracelets_2\":0,\"mask_1\":0,\"skin\":0,\"face\":0,\"age_1\":0,\"sun_1\":0,\"makeup_4\":0,\"blemishes_1\":0,\"bags_1\":0,\"bracelets_1\":-1,\"complexion_2\":0,\"sex\":0,\"hair_color_2\":0,\"decals_1\":0,\"lipstick_3\":0,\"chest_2\":0,\"tshirt_1\":15,\"chest_3\":0,\"mask_2\":0,\"blemishes_2\":0,\"beard_2\":10,\"chain_1\":0,\"beard_4\":0,\"blush_3\":0,\"moles_1\":0,\"glasses_1\":9,\"complexion_1\":0,\"helmet_2\":2,\"lipstick_1\":0,\"ears_2\":0,\"tshirt_2\":0,\"makeup_1\":0,\"shoes_2\":0,\"eye_color\":0,\"bproof_1\":7,\"glasses_2\":0,\"shoes_1\":25,\"eyebrows_2\":0,\"bags_2\":0,\"watches_1\":-1,\"ears_1\":-1,\"lipstick_4\":0,\"eyebrows_1\":0,\"sun_2\":0,\"arms_2\":0,\"moles_2\":0,\"watches_2\":0,\"chest_1\":0,\"arms\":30,\"beard_1\":2,\"hair_1\":19,\"blush_2\":0,\"bodyb_1\":0,\"beard_3\":5,\"pants_1\":9,\"hair_color_1\":5}', 'police', 4, '[{\"components\":[],\"label\":\"Nightstick\",\"name\":\"WEAPON_NIGHTSTICK\",\"ammo\":0},{\"components\":[\"clip_default\",\"flashlight\"],\"label\":\"Combat pistol\",\"name\":\"WEAPON_COMBATPISTOL\",\"ammo\":1000},{\"components\":[\"clip_default\",\"flashlight\",\"scope\"],\"label\":\"SMG\",\"name\":\"WEAPON_SMG\",\"ammo\":974},{\"components\":[],\"label\":\"Fire extinguisher\",\"name\":\"WEAPON_FIREEXTINGUISHER\",\"ammo\":2000},{\"components\":[],\"label\":\"Taser\",\"name\":\"WEAPON_STUNGUN\",\"ammo\":1000},{\"components\":[],\"label\":\"Flashlight\",\"name\":\"WEAPON_FLASHLIGHT\",\"ammo\":0}]', '{\"x\":-448.5,\"y\":6012.1,\"z\":31.7}', 424439, 1, 'superadmin', 'Anthony', 'Smith', '1990-05-24', 'm', '184', '[{\"percent\":92.92,\"name\":\"hunger\",\"val\":929200},{\"percent\":94.7125,\"name\":\"thirst\",\"val\":947125},{\"percent\":0.0,\"name\":\"drunk\",\"val\":0}]', NULL, 0, '121-3758', 0),
('steam:11000010fe595d0', 'license:af8d774357cd4d1bca63d28ab84c0f418d353fb7', 0, 'DylanFernandez', '{\"blush_1\":0,\"eyebrows_3\":0,\"hair_2\":0,\"bodyb_2\":0,\"bproof_2\":0,\"makeup_2\":0,\"makeup_3\":0,\"eyebrows_4\":0,\"helmet_1\":-1,\"pants_2\":0,\"age_2\":0,\"torso_1\":0,\"lipstick_2\":0,\"chain_2\":0,\"torso_2\":0,\"decals_2\":0,\"bracelets_2\":0,\"eye_color\":0,\"skin\":0,\"face\":0,\"age_1\":0,\"sun_1\":0,\"makeup_4\":0,\"blemishes_1\":0,\"bags_1\":0,\"bracelets_1\":-1,\"complexion_2\":0,\"sex\":0,\"watches_1\":-1,\"decals_1\":0,\"lipstick_3\":0,\"chest_2\":0,\"tshirt_1\":0,\"chest_3\":0,\"mask_2\":0,\"hair_1\":4,\"beard_2\":0,\"chain_1\":0,\"beard_4\":0,\"blush_3\":0,\"beard_1\":0,\"glasses_1\":0,\"complexion_1\":0,\"pants_1\":0,\"lipstick_1\":0,\"glasses_2\":0,\"tshirt_2\":0,\"makeup_1\":0,\"shoes_2\":0,\"beard_3\":0,\"hair_color_2\":0,\"arms\":0,\"bproof_1\":0,\"blemishes_2\":0,\"bags_2\":0,\"ears_2\":0,\"ears_1\":-1,\"lipstick_4\":0,\"eyebrows_2\":0,\"sun_2\":0,\"arms_2\":0,\"moles_2\":0,\"watches_2\":0,\"chest_1\":0,\"shoes_1\":0,\"mask_1\":0,\"helmet_2\":0,\"moles_1\":0,\"bodyb_1\":0,\"eyebrows_1\":0,\"blush_2\":0,\"hair_color_1\":0}', 'police', 2, '[]', '{\"x\":454.4,\"y\":-1018.2,\"z\":28.4}', 86300, 0, 'user', 'Dylan', 'Fernandez', '1998', 'm', '180', '[{\"name\":\"hunger\",\"percent\":34.94,\"val\":349400},{\"name\":\"thirst\",\"percent\":33.705,\"val\":337050},{\"name\":\"drunk\",\"percent\":0.0,\"val\":0}]', NULL, 0, '249-9884', 0),
('steam:110000111c332ed', 'license:c7b5c1f2e80fc912bddb70417074f3b389c003b6', 10674371, 'Darryl [BC-6]', '{\"bags_1\":0,\"bproof_2\":0,\"eyebrows_3\":0,\"tshirt_2\":0,\"complexion_2\":0,\"bodyb_1\":0,\"chest_1\":0,\"torso_2\":0,\"eyebrows_1\":0,\"age_2\":0,\"ears_2\":0,\"pants_1\":0,\"shoes_2\":0,\"glasses_1\":0,\"watches_1\":-1,\"makeup_4\":0,\"chest_2\":0,\"bracelets_2\":0,\"decals_2\":0,\"watches_2\":0,\"tshirt_1\":0,\"eyebrows_2\":0,\"lipstick_3\":0,\"blemishes_2\":0,\"beard_4\":0,\"chain_2\":0,\"hair_color_2\":0,\"helmet_2\":0,\"arms\":0,\"beard_2\":0,\"sun_2\":0,\"moles_1\":0,\"blush_1\":0,\"lipstick_2\":0,\"decals_1\":0,\"age_1\":0,\"lipstick_4\":0,\"hair_color_1\":0,\"sex\":0,\"makeup_2\":0,\"lipstick_1\":0,\"bodyb_2\":0,\"blush_2\":0,\"makeup_1\":0,\"arms_2\":0,\"hair_2\":0,\"makeup_3\":0,\"complexion_1\":0,\"helmet_1\":-1,\"mask_1\":0,\"eyebrows_4\":0,\"glasses_2\":0,\"mask_2\":0,\"sun_1\":0,\"moles_2\":0,\"beard_1\":0,\"chain_1\":0,\"shoes_1\":0,\"face\":0,\"ears_1\":-1,\"pants_2\":0,\"chest_3\":0,\"torso_1\":0,\"bags_2\":0,\"bproof_1\":0,\"eye_color\":0,\"skin\":0,\"beard_3\":0,\"bracelets_1\":-1,\"hair_1\":0,\"blush_3\":0,\"blemishes_1\":0}', 'police', 4, '[]', '{\"y\":-994.0,\"x\":405.6,\"z\":28.9}', 50717920, 1, 'superadmin', 'James', 'Dunn', '08/29/1987', 'm', '140', '[{\"name\":\"hunger\",\"val\":493800,\"percent\":49.38},{\"name\":\"thirst\",\"val\":495350,\"percent\":49.535},{\"name\":\"drunk\",\"val\":0,\"percent\":0.0}]', NULL, 0, '113-0914', 0),
('steam:110000111d0b1aa', 'license:4c35a379d3f41843a3774eaf606ad2c998c89fd3', 4000000, 'babyfacemel', '{\"beard_4\":0,\"chest_3\":0,\"beard_2\":0,\"shoes_2\":1,\"chain_1\":0,\"blemishes_1\":0,\"watches_2\":0,\"hair_color_2\":19,\"shoes_1\":44,\"tshirt_2\":0,\"moles_2\":3,\"pants_1\":109,\"bodyb_2\":0,\"eye_color\":3,\"lipstick_2\":6,\"makeup_1\":4,\"bproof_2\":0,\"hair_1\":5,\"bodyb_1\":0,\"moles_1\":2,\"ears_2\":0,\"helmet_2\":0,\"mask_2\":0,\"bags_2\":0,\"age_2\":0,\"helmet_1\":-1,\"bags_1\":0,\"bproof_1\":39,\"complexion_1\":0,\"sex\":1,\"watches_1\":-1,\"bracelets_2\":0,\"lipstick_4\":6,\"bracelets_1\":-1,\"eyebrows_1\":12,\"decals_1\":0,\"arms\":68,\"chest_2\":0,\"sun_1\":0,\"pants_2\":0,\"hair_color_1\":35,\"complexion_2\":0,\"age_1\":0,\"torso_1\":118,\"beard_1\":0,\"eyebrows_2\":4,\"hair_2\":2,\"mask_1\":0,\"eyebrows_3\":61,\"decals_2\":0,\"blush_3\":10,\"torso_2\":1,\"eyebrows_4\":0,\"lipstick_1\":3,\"arms_2\":0,\"tshirt_1\":14,\"ears_1\":-1,\"blush_1\":1,\"beard_3\":0,\"chest_1\":0,\"chain_2\":0,\"makeup_2\":6,\"sun_2\":0,\"blemishes_2\":3,\"lipstick_3\":53,\"makeup_3\":19,\"glasses_1\":19,\"glasses_2\":0,\"skin\":28,\"face\":21,\"makeup_4\":13,\"blush_2\":5}', 'taxi', 4, '[{\"name\":\"WEAPON_APPISTOL\",\"components\":[\"clip_extended\",\"suppressor\"],\"label\":\"AP pistol\",\"ammo\":995},{\"name\":\"WEAPON_GRENADE\",\"components\":[],\"label\":\"Grenade\",\"ammo\":25},{\"name\":\"WEAPON_MOLOTOV\",\"components\":[],\"label\":\"Molotov cocktail\",\"ammo\":25},{\"name\":\"WEAPON_PETROLCAN\",\"components\":[],\"label\":\"Jerrycan\",\"ammo\":4292},{\"name\":\"WEAPON_COMPACTLAUNCHER\",\"components\":[],\"label\":\"Compact launcher\",\"ammo\":16},{\"name\":\"WEAPON_FLARE\",\"components\":[],\"label\":\"Flare gun\",\"ammo\":25}]', '{\"x\":94.1,\"y\":6395.5,\"z\":31.2}', 77880, 1, 'superadmin', 'Melaficent', 'Spikes', '1994-109-17', 'f', '170', '[{\"name\":\"hunger\",\"percent\":86.21,\"val\":862100},{\"name\":\"thirst\",\"percent\":89.6875,\"val\":896875},{\"name\":\"drunk\",\"percent\":0.0,\"val\":0}]', NULL, 0, '827-5849', 0),
('steam:1100001139319c1', 'license:fe4031d6bbcd8ce2770862c34c19038ca32d88f0', 222, 'Mito Jr', '{\"makeup_2\":0,\"arms_2\":0,\"eye_color\":0,\"lipstick_1\":0,\"makeup_3\":0,\"age_1\":0,\"face\":4,\"bracelets_2\":0,\"blush_2\":0,\"makeup_4\":0,\"helmet_2\":0,\"arms\":0,\"beard_3\":0,\"pants_1\":0,\"eyebrows_1\":0,\"blemishes_1\":0,\"shoes_2\":3,\"decals_1\":0,\"hair_2\":5,\"hair_color_1\":5,\"bproof_2\":0,\"moles_1\":0,\"bags_1\":0,\"tshirt_2\":0,\"sun_1\":0,\"age_2\":0,\"sex\":0,\"chest_3\":0,\"makeup_1\":0,\"moles_2\":0,\"ears_1\":-1,\"complexion_2\":0,\"chain_2\":0,\"glasses_2\":0,\"hair_1\":2,\"lipstick_2\":0,\"helmet_1\":-1,\"bracelets_1\":-1,\"mask_2\":0,\"bproof_1\":0,\"watches_2\":0,\"bodyb_2\":0,\"hair_color_2\":0,\"chain_1\":0,\"eyebrows_2\":0,\"beard_2\":0,\"mask_1\":0,\"bodyb_1\":0,\"watches_1\":-1,\"complexion_1\":0,\"glasses_1\":0,\"blush_1\":0,\"beard_4\":0,\"bags_2\":0,\"shoes_1\":3,\"skin\":4,\"decals_2\":0,\"ears_2\":0,\"eyebrows_4\":0,\"eyebrows_3\":0,\"lipstick_4\":0,\"chest_2\":0,\"sun_2\":0,\"beard_1\":0,\"lipstick_3\":0,\"blemishes_2\":0,\"torso_1\":0,\"pants_2\":0,\"blush_3\":0,\"torso_2\":0,\"tshirt_1\":0,\"chest_1\":0}', 'police', 2, '[{\"name\":\"WEAPON_PISTOL\",\"label\":\"Pistol\",\"components\":[\"clip_default\"],\"ammo\":862},{\"name\":\"WEAPON_COMBATPISTOL\",\"label\":\"Combat pistol\",\"components\":[\"clip_default\",\"flashlight\"],\"ammo\":862},{\"name\":\"WEAPON_PUMPSHOTGUN\",\"label\":\"Pump shotgun\",\"components\":[],\"ammo\":1090},{\"name\":\"WEAPON_CARBINERIFLE\",\"label\":\"Carbine rifle\",\"components\":[\"clip_default\",\"flashlight\",\"grip\"],\"ammo\":0},{\"name\":\"WEAPON_STUNGUN\",\"label\":\"Taser\",\"components\":[],\"ammo\":1000},{\"name\":\"WEAPON_FLARE\",\"label\":\"Flare gun\",\"components\":[],\"ammo\":19}]', '{\"y\":-1017.5,\"x\":453.5,\"z\":28.5}', 320060, 0, 'user', 'Michael', 'Jameson', '1994-04-16', 'm', '187', '[{\"name\":\"hunger\",\"val\":477600,\"percent\":47.76},{\"name\":\"thirst\",\"val\":483200,\"percent\":48.32},{\"name\":\"drunk\",\"val\":0,\"percent\":0.0}]', NULL, 0, '440-5316', 0),
('steam:110000113ec9482', 'license:33b7d61639501f180e2d2fae3ff22e0b3a8896f6', 9723, 'Mark H. [307][113]', '{\"ears_1\":-1,\"blush_3\":0,\"moles_1\":0,\"age_1\":0,\"glasses_2\":0,\"hair_color_2\":0,\"makeup_1\":0,\"bags_1\":0,\"glasses_1\":0,\"blemishes_2\":0,\"bags_2\":0,\"skin\":0,\"beard_4\":0,\"sun_2\":0,\"bodyb_1\":0,\"lipstick_1\":0,\"blush_1\":0,\"pants_2\":0,\"sex\":0,\"helmet_1\":-1,\"tshirt_1\":0,\"hair_1\":0,\"arms_2\":0,\"complexion_2\":0,\"bproof_1\":0,\"lipstick_2\":0,\"bodyb_2\":0,\"shoes_1\":0,\"eye_color\":0,\"torso_1\":0,\"makeup_2\":0,\"chest_3\":0,\"chest_1\":0,\"blush_2\":0,\"watches_2\":0,\"hair_2\":0,\"bproof_2\":0,\"arms\":0,\"lipstick_4\":0,\"eyebrows_1\":0,\"face\":0,\"lipstick_3\":0,\"mask_1\":0,\"shoes_2\":0,\"helmet_2\":0,\"watches_1\":-1,\"beard_1\":0,\"decals_2\":0,\"moles_2\":0,\"ears_2\":0,\"makeup_4\":0,\"hair_color_1\":0,\"blemishes_1\":0,\"chain_2\":0,\"complexion_1\":0,\"beard_3\":0,\"tshirt_2\":0,\"torso_2\":0,\"decals_1\":0,\"age_2\":0,\"eyebrows_2\":0,\"beard_2\":0,\"chest_2\":0,\"mask_2\":0,\"chain_1\":0,\"sun_1\":0,\"eyebrows_4\":0,\"bracelets_2\":0,\"bracelets_1\":-1,\"pants_1\":0,\"eyebrows_3\":0,\"makeup_3\":0}', 'police', 1, '[{\"label\":\"Nightstick\",\"ammo\":0,\"name\":\"WEAPON_NIGHTSTICK\",\"components\":[]},{\"label\":\"Pistol\",\"ammo\":9951,\"name\":\"WEAPON_PISTOL\",\"components\":[\"clip_default\"]},{\"label\":\"Combat pistol\",\"ammo\":9951,\"name\":\"WEAPON_COMBATPISTOL\",\"components\":[\"clip_default\",\"flashlight\"]},{\"label\":\"Pump shotgun\",\"ammo\":9999,\"name\":\"WEAPON_PUMPSHOTGUN\",\"components\":[]},{\"label\":\"Carbine rifle\",\"ammo\":8,\"name\":\"WEAPON_CARBINERIFLE\",\"components\":[\"clip_default\"]},{\"label\":\"Fire extinguisher\",\"ammo\":0,\"name\":\"WEAPON_FIREEXTINGUISHER\",\"components\":[]},{\"label\":\"Taser\",\"ammo\":9999,\"name\":\"WEAPON_STUNGUN\",\"components\":[]},{\"label\":\"Flashlight\",\"ammo\":0,\"name\":\"WEAPON_FLASHLIGHT\",\"components\":[]},{\"label\":\"Flare gun\",\"ammo\":25,\"name\":\"WEAPON_FLARE\",\"components\":[]}]', '{\"x\":1866.6,\"z\":39.0,\"y\":3691.6}', 42046, 0, 'user', 'Hammil', 'Mark', '1994-04-06', 'm', '160', '[{\"val\":121500,\"percent\":12.15,\"name\":\"hunger\"},{\"val\":216125,\"percent\":21.6125,\"name\":\"thirst\"},{\"val\":0,\"percent\":0.0,\"name\":\"drunk\"}]', NULL, 0, '150-0087', 0),
('steam:11000011531f6b5', 'license:09c4006f557a0c94e7d4f08a5dc23c1914c6737e', 0, 'Don Tony', '{\"chest_3\":0,\"makeup_4\":0,\"blush_3\":0,\"torso_1\":0,\"blemishes_1\":0,\"moles_2\":0,\"complexion_2\":0,\"watches_2\":0,\"bproof_2\":0,\"eyebrows_3\":0,\"beard_1\":0,\"makeup_1\":0,\"tshirt_2\":0,\"lipstick_1\":0,\"glasses_2\":0,\"arms\":5,\"bodyb_1\":0,\"blush_2\":0,\"eye_color\":0,\"face\":0,\"torso_2\":0,\"hair_2\":0,\"pants_2\":0,\"moles_1\":0,\"blemishes_2\":0,\"chain_1\":0,\"lipstick_2\":0,\"pants_1\":0,\"bracelets_2\":0,\"decals_1\":0,\"chain_2\":0,\"eyebrows_4\":0,\"eyebrows_2\":0,\"beard_2\":0,\"sun_2\":0,\"mask_1\":0,\"bodyb_2\":0,\"age_1\":0,\"bproof_1\":0,\"lipstick_3\":0,\"ears_2\":0,\"helmet_1\":-1,\"hair_color_1\":0,\"mask_2\":0,\"beard_4\":0,\"chest_2\":0,\"makeup_3\":0,\"decals_2\":0,\"bracelets_1\":-1,\"lipstick_4\":0,\"age_2\":0,\"hair_color_2\":0,\"arms_2\":0,\"sun_1\":0,\"ears_1\":-1,\"blush_1\":0,\"makeup_2\":0,\"skin\":45,\"complexion_1\":0,\"bags_1\":9,\"beard_3\":0,\"tshirt_1\":5,\"watches_1\":1,\"helmet_2\":0,\"shoes_1\":0,\"eyebrows_1\":10,\"chest_1\":0,\"sex\":0,\"bags_2\":0,\"hair_1\":3,\"glasses_1\":0,\"shoes_2\":6}', 'mechanic', 4, '[{\"ammo\":9,\"label\":\"Pump shotgun\",\"components\":[],\"name\":\"WEAPON_PUMPSHOTGUN\"},{\"ammo\":0,\"label\":\"Parachute\",\"components\":[],\"name\":\"GADGET_PARACHUTE\"}]', '{\"x\":-2594.0,\"y\":2609.7,\"z\":-0.5}', 9340, 0, 'user', 'Tony', 'Montana', '19902712', 'm', '150', '[{\"val\":133800,\"name\":\"hunger\",\"percent\":13.38},{\"val\":225350,\"name\":\"thirst\",\"percent\":22.535},{\"val\":0,\"name\":\"drunk\",\"percent\":0.0}]', NULL, 1, '311-8622', 0),
('steam:1100001156cfac9', 'license:5d9ba217761fbe2cb970db3c8c32ac212826e369', 110, 'Joe', '{\"decals_1\":0,\"lipstick_2\":0,\"torso_2\":0,\"bracelets_2\":0,\"blush_1\":0,\"beard_4\":0,\"chain_2\":0,\"lipstick_4\":0,\"chest_2\":0,\"makeup_1\":0,\"complexion_1\":0,\"bags_1\":0,\"shoes_1\":0,\"sun_1\":0,\"lipstick_1\":0,\"torso_1\":0,\"eyebrows_1\":0,\"tshirt_2\":0,\"blush_2\":0,\"hair_color_1\":0,\"bproof_2\":0,\"lipstick_3\":0,\"helmet_1\":-1,\"face\":0,\"makeup_2\":0,\"arms\":0,\"ears_1\":-1,\"mask_1\":0,\"bags_2\":0,\"decals_2\":0,\"pants_1\":0,\"watches_1\":-1,\"glasses_1\":0,\"age_1\":0,\"ears_2\":0,\"beard_3\":0,\"chest_1\":0,\"chest_3\":0,\"skin\":0,\"eyebrows_4\":0,\"hair_2\":0,\"moles_2\":0,\"beard_2\":0,\"watches_2\":0,\"bodyb_2\":0,\"helmet_2\":0,\"eyebrows_2\":0,\"tshirt_1\":0,\"mask_2\":0,\"complexion_2\":0,\"makeup_4\":0,\"bodyb_1\":0,\"arms_2\":0,\"chain_1\":0,\"blemishes_2\":0,\"shoes_2\":0,\"eye_color\":0,\"bproof_1\":0,\"pants_2\":0,\"eyebrows_3\":0,\"age_2\":0,\"sun_2\":0,\"hair_1\":4,\"glasses_2\":0,\"sex\":0,\"makeup_3\":0,\"beard_1\":0,\"hair_color_2\":0,\"moles_1\":0,\"blemishes_1\":0,\"blush_3\":0,\"bracelets_1\":-1}', 'ambulance', 1, '[{\"components\":[],\"label\":\"Fire extinguisher\",\"name\":\"WEAPON_FIREEXTINGUISHER\",\"ammo\":2000}]', '{\"x\":-314.5,\"y\":-2168.0,\"z\":10.3}', 21740, 0, 'user', 'Joey ', 'Mcboby', '08/12/99', 'm', '200', '[{\"percent\":42.58,\"name\":\"hunger\",\"val\":425800},{\"percent\":44.435,\"name\":\"thirst\",\"val\":444350},{\"percent\":0.0,\"name\":\"drunk\",\"val\":0}]', NULL, 0, '859-9967', 0),
('steam:110000116e48a9b', 'license:afeca7162eb4179bd704399b08706ed34e67f640', 12500, 'FireLieutenant184', '{\"age_2\":0,\"lipstick_2\":0,\"torso_2\":0,\"bracelets_2\":0,\"blush_1\":0,\"beard_4\":0,\"chain_2\":0,\"lipstick_4\":0,\"chest_2\":0,\"makeup_1\":0,\"complexion_1\":0,\"eye_color\":0,\"chain_1\":0,\"sun_1\":0,\"lipstick_1\":0,\"torso_1\":4,\"blush_3\":0,\"tshirt_2\":0,\"blush_2\":0,\"hair_color_1\":0,\"bproof_2\":0,\"lipstick_3\":0,\"helmet_1\":-1,\"face\":10,\"makeup_2\":0,\"arms\":0,\"ears_1\":-1,\"mask_1\":0,\"bags_2\":0,\"decals_2\":0,\"pants_1\":1,\"watches_1\":-1,\"glasses_1\":2,\"age_1\":0,\"sex\":0,\"beard_3\":0,\"chest_1\":0,\"chest_3\":0,\"skin\":0,\"eyebrows_4\":0,\"hair_2\":0,\"moles_2\":0,\"beard_2\":0,\"watches_2\":0,\"bodyb_2\":0,\"helmet_2\":0,\"eyebrows_2\":0,\"tshirt_1\":0,\"mask_2\":0,\"complexion_2\":0,\"makeup_4\":0,\"bodyb_1\":0,\"arms_2\":0,\"decals_1\":0,\"blemishes_2\":0,\"bracelets_1\":-1,\"eyebrows_1\":0,\"eyebrows_3\":0,\"pants_2\":0,\"sun_2\":0,\"ears_2\":0,\"shoes_2\":0,\"hair_1\":2,\"glasses_2\":0,\"shoes_1\":1,\"makeup_3\":0,\"beard_1\":0,\"bproof_1\":0,\"moles_1\":0,\"blemishes_1\":0,\"bags_1\":0,\"hair_color_2\":0}', 'police', 3, '[{\"components\":[],\"name\":\"WEAPON_NIGHTSTICK\",\"ammo\":0,\"label\":\"Nightstick\"},{\"components\":[\"clip_default\"],\"name\":\"WEAPON_PISTOL\",\"ammo\":1000,\"label\":\"Pistol\"},{\"components\":[\"clip_extended\",\"flashlight\"],\"name\":\"WEAPON_COMBATPISTOL\",\"ammo\":1000,\"label\":\"Combat pistol\"},{\"components\":[\"flashlight\"],\"name\":\"WEAPON_PUMPSHOTGUN\",\"ammo\":931,\"label\":\"Pump shotgun\"},{\"components\":[\"clip_default\",\"flashlight\",\"scope\"],\"name\":\"WEAPON_CARBINERIFLE\",\"ammo\":1000,\"label\":\"Carbine rifle\"},{\"components\":[\"clip_extended\",\"flashlight\",\"scope\",\"grip\"],\"name\":\"WEAPON_SPECIALCARBINE\",\"ammo\":1000,\"label\":\"Special carbine\"},{\"components\":[],\"name\":\"WEAPON_SMOKEGRENADE\",\"ammo\":17,\"label\":\"Smoke grenade\"},{\"components\":[],\"name\":\"WEAPON_FIREEXTINGUISHER\",\"ammo\":577,\"label\":\"Fire extinguisher\"},{\"components\":[],\"name\":\"WEAPON_STUNGUN\",\"ammo\":1000,\"label\":\"Taser\"},{\"components\":[],\"name\":\"WEAPON_FLASHLIGHT\",\"ammo\":0,\"label\":\"Flashlight\"},{\"components\":[],\"name\":\"WEAPON_FLARE\",\"ammo\":25,\"label\":\"Flare gun\"}]', '{\"x\":2044.2,\"y\":2599.3,\"z\":53.8}', 123750, 0, 'user', 'Nate', 'Redmond', '19970429', 'm', '160', '[{\"val\":673400,\"percent\":67.34,\"name\":\"hunger\"},{\"val\":730050,\"percent\":73.005,\"name\":\"thirst\"},{\"val\":0,\"percent\":0.0,\"name\":\"drunk\"}]', NULL, 0, '453-2662', 0),
('steam:1100001176cddfb', 'license:afa0ca33eb47c7444aadff124cbe6e62acb38b35', 7000, 'HeliDriverFF', '{\"watches_1\":4,\"sex\":0,\"hair_2\":5,\"makeup_3\":0,\"beard_2\":0,\"helmet_2\":0,\"bodyb_2\":0,\"eyebrows_3\":0,\"skin\":0,\"eye_color\":2,\"eyebrows_2\":0,\"torso_1\":44,\"ears_2\":0,\"bracelets_1\":-1,\"beard_3\":0,\"blush_3\":0,\"makeup_1\":0,\"chain_1\":0,\"tshirt_1\":16,\"arms\":0,\"bproof_1\":0,\"glasses_2\":0,\"hair_color_2\":6,\"face\":0,\"bags_2\":0,\"mask_1\":0,\"lipstick_3\":0,\"beard_4\":0,\"age_1\":0,\"lipstick_4\":0,\"chest_1\":0,\"blush_2\":0,\"eyebrows_1\":0,\"decals_1\":0,\"shoes_2\":0,\"helmet_1\":-1,\"blemishes_1\":0,\"bproof_2\":0,\"chest_2\":0,\"tshirt_2\":0,\"bags_1\":0,\"ears_1\":-1,\"makeup_2\":0,\"moles_2\":0,\"chest_3\":0,\"eyebrows_4\":0,\"sun_2\":0,\"hair_color_1\":18,\"pants_2\":0,\"watches_2\":0,\"hair_1\":11,\"lipstick_2\":0,\"mask_2\":0,\"blush_1\":0,\"moles_1\":0,\"shoes_1\":25,\"pants_1\":47,\"torso_2\":0,\"complexion_1\":0,\"age_2\":0,\"sun_1\":0,\"complexion_2\":0,\"chain_2\":0,\"arms_2\":0,\"glasses_1\":6,\"bracelets_2\":0,\"makeup_4\":0,\"lipstick_1\":0,\"bodyb_1\":0,\"decals_2\":0,\"blemishes_2\":0,\"beard_1\":0}', 'unemployed', 0, '[{\"name\":\"WEAPON_CARBINERIFLE\",\"components\":[\"clip_box\",\"luxary_finish\"],\"label\":\"Carbine rifle\",\"ammo\":738}]', '{\"x\":-734.8,\"z\":51.2,\"y\":5418.6}', 4000, 0, 'user', 'Jesse', 'Anderson', '1985/04/01', 'm', '195', '[{\"percent\":21.0,\"name\":\"hunger\",\"val\":210000},{\"percent\":40.75,\"name\":\"thirst\",\"val\":407500},{\"percent\":0.0,\"name\":\"drunk\",\"val\":0}]', NULL, 0, '822-5198', 0),
('steam:1100001177ff1be', 'license:b2cc893cda2c4c81d56065d4ce26eb74167c322b', 16, 'Elijah P.', '{\"bags_1\":0,\"helmet_1\":-1,\"mask_2\":0,\"tshirt_2\":0,\"glasses_2\":0,\"bodyb_1\":0,\"chest_1\":0,\"torso_2\":15,\"eyebrows_1\":0,\"age_2\":0,\"ears_2\":0,\"pants_2\":0,\"shoes_2\":0,\"glasses_1\":0,\"watches_1\":-1,\"makeup_4\":0,\"chest_2\":0,\"bracelets_2\":0,\"eyebrows_4\":0,\"watches_2\":0,\"tshirt_1\":0,\"eyebrows_2\":0,\"lipstick_3\":0,\"blemishes_2\":0,\"beard_4\":0,\"ears_1\":-1,\"hair_color_2\":0,\"helmet_2\":0,\"arms\":0,\"moles_2\":0,\"sun_2\":0,\"moles_1\":0,\"blush_1\":0,\"lipstick_2\":0,\"decals_1\":0,\"age_1\":0,\"lipstick_4\":0,\"hair_color_1\":2,\"sex\":0,\"makeup_2\":0,\"lipstick_1\":0,\"bodyb_2\":0,\"blush_2\":0,\"beard_3\":0,\"arms_2\":0,\"hair_2\":2,\"makeup_3\":0,\"blemishes_1\":0,\"eyebrows_3\":0,\"mask_1\":0,\"bproof_2\":0,\"complexion_2\":0,\"hair_1\":3,\"sun_1\":0,\"face\":0,\"beard_1\":0,\"beard_2\":0,\"pants_1\":0,\"makeup_1\":0,\"eye_color\":0,\"chain_1\":0,\"chest_3\":0,\"chain_2\":0,\"bags_2\":0,\"shoes_1\":0,\"decals_2\":0,\"skin\":0,\"bracelets_1\":-1,\"bproof_1\":0,\"torso_1\":0,\"blush_3\":0,\"complexion_1\":0}', 'mechanic', 0, '[]', '{\"x\":-262.3,\"y\":6332.4,\"z\":32.4}', 5268, 0, 'user', 'Elijah', 'Perry', '1996/03/24', 'm', '159', '[{\"percent\":30.93,\"name\":\"hunger\",\"val\":309300},{\"percent\":35.6975,\"name\":\"thirst\",\"val\":356975},{\"percent\":0.0,\"name\":\"drunk\",\"val\":0}]', NULL, 0, '722-8375', 0),
('steam:110000118024cc1', 'license:e6bfc55586c08056970c8f3d36af2662e6c7c70d', 0, 'Ron White', NULL, 'unemployed', 0, '[]', '{\"x\":-45.2,\"z\":31.6,\"y\":6301.6}', 0, 0, 'user', '', '', '', '', '', '[{\"val\":998000,\"percent\":99.8,\"name\":\"hunger\"},{\"val\":998500,\"percent\":99.85,\"name\":\"thirst\"},{\"val\":0,\"percent\":0.0,\"name\":\"drunk\"}]', NULL, 0, '521-4579', 0),
('steam:1100001183c7077', 'license:80638247cf95b34b74ffefd30b13a6226b77b3f3', 25000, 'KillerMcFatass', '{\"beard_4\":0,\"chest_3\":0,\"beard_2\":10,\"shoes_2\":0,\"chain_1\":0,\"blemishes_1\":0,\"watches_2\":0,\"hair_color_2\":0,\"shoes_1\":24,\"tshirt_2\":0,\"moles_2\":0,\"pants_1\":76,\"bodyb_2\":0,\"eye_color\":3,\"lipstick_2\":0,\"makeup_1\":0,\"bproof_2\":0,\"hair_1\":46,\"bodyb_1\":0,\"chain_2\":0,\"ears_2\":0,\"beard_3\":0,\"mask_2\":0,\"bags_2\":0,\"age_2\":0,\"sun_2\":0,\"bags_1\":0,\"bproof_1\":0,\"complexion_1\":0,\"sex\":0,\"watches_1\":-1,\"chest_1\":0,\"lipstick_4\":0,\"bracelets_1\":-1,\"eyebrows_1\":23,\"decals_1\":0,\"arms\":1,\"chest_2\":0,\"sun_1\":0,\"pants_2\":0,\"hair_color_1\":0,\"complexion_2\":0,\"age_1\":0,\"torso_1\":6,\"beard_1\":2,\"eyebrows_2\":10,\"ears_1\":24,\"arms_2\":0,\"eyebrows_3\":26,\"decals_2\":0,\"helmet_1\":-1,\"mask_1\":0,\"eyebrows_4\":0,\"hair_2\":0,\"tshirt_1\":14,\"blush_3\":0,\"makeup_2\":0,\"blush_1\":0,\"helmet_2\":0,\"torso_2\":0,\"skin\":0,\"glasses_2\":1,\"moles_1\":0,\"blemishes_2\":0,\"lipstick_3\":0,\"makeup_3\":0,\"glasses_1\":15,\"lipstick_1\":0,\"face\":43,\"bracelets_2\":0,\"makeup_4\":0,\"blush_2\":0}', 'unemployed', 0, '[{\"name\":\"WEAPON_BAT\",\"ammo\":0,\"label\":\"Bat\",\"components\":[]},{\"name\":\"WEAPON_COMBATPISTOL\",\"ammo\":980,\"label\":\"Combat pistol\",\"components\":[\"clip_default\"]},{\"name\":\"WEAPON_PUMPSHOTGUN\",\"ammo\":7,\"label\":\"Pump shotgun\",\"components\":[]}]', '{\"x\":321.4,\"y\":-1389.0,\"z\":31.9}', 17000, 1, 'superadmin', 'Jesse', 'Spikes', '1992-02-25', 'm', '200', '[{\"name\":\"hunger\",\"percent\":49.28,\"val\":492800},{\"name\":\"thirst\",\"percent\":49.46,\"val\":494600},{\"name\":\"drunk\",\"percent\":0.0,\"val\":0}]', NULL, 0, '104-0089', 0),
('steam:110000119e7384d', 'license:792f68387d8c28da436769f43ef9c77a671ff2d7', 0, 'Pro Styx', '{\"watches_1\":-1,\"ears_2\":0,\"blush_2\":0,\"glasses_2\":0,\"helmet_1\":-1,\"beard_4\":0,\"bracelets_1\":-1,\"tshirt_1\":0,\"beard_3\":0,\"bproof_2\":0,\"sun_1\":0,\"decals_1\":0,\"bags_2\":0,\"blush_1\":0,\"eyebrows_1\":0,\"moles_2\":0,\"chain_1\":0,\"lipstick_4\":0,\"makeup_1\":0,\"hair_color_1\":0,\"age_1\":0,\"chain_2\":0,\"lipstick_3\":0,\"moles_1\":0,\"mask_1\":0,\"complexion_2\":0,\"mask_2\":0,\"eye_color\":0,\"ears_1\":-1,\"bproof_1\":0,\"beard_1\":0,\"blemishes_2\":0,\"eyebrows_2\":0,\"age_2\":0,\"sex\":0,\"glasses_1\":0,\"shoes_2\":0,\"shoes_1\":0,\"bracelets_2\":0,\"pants_1\":0,\"blush_3\":0,\"torso_2\":0,\"helmet_2\":0,\"arms_2\":0,\"torso_1\":0,\"skin\":0,\"eyebrows_3\":0,\"bags_1\":0,\"bodyb_2\":0,\"pants_2\":0,\"hair_color_2\":0,\"watches_2\":0,\"hair_2\":0,\"chest_2\":0,\"chest_3\":0,\"lipstick_1\":0,\"beard_2\":0,\"tshirt_2\":0,\"makeup_2\":0,\"makeup_3\":0,\"sun_2\":0,\"decals_2\":0,\"chest_1\":0,\"blemishes_1\":0,\"makeup_4\":0,\"face\":0,\"complexion_1\":0,\"arms\":0,\"bodyb_1\":0,\"eyebrows_4\":0,\"lipstick_2\":0,\"hair_1\":0}', 'unemployed', 0, '[]', '{\"z\":105.5,\"y\":205.4,\"x\":227.1}', 400, 0, 'user', 'Mohamed', 'Salm', '1998-12-19', 'm', '194', '[{\"name\":\"hunger\",\"val\":896600,\"percent\":89.66},{\"name\":\"thirst\",\"val\":922450,\"percent\":92.245},{\"name\":\"drunk\",\"val\":0,\"percent\":0.0}]', NULL, 0, '646-1662', 0);
INSERT INTO `old_users` (`identifier`, `license`, `money`, `name`, `skin`, `job`, `job_grade`, `loadout`, `position`, `bank`, `permission_level`, `group`, `firstname`, `lastname`, `dateofbirth`, `sex`, `height`, `status`, `last_property`, `is_dead`, `phone_number`, `jail_time`) VALUES
('steam:110000132580eb0', 'license:66ad9907077ce29ebeb7234ef771915368ae5d6e', 683, 'K9Marine', NULL, 'miner', 0, '[{\"label\":\"Knife\",\"name\":\"WEAPON_KNIFE\",\"components\":[],\"ammo\":0},{\"label\":\"Nightstick\",\"name\":\"WEAPON_NIGHTSTICK\",\"components\":[],\"ammo\":0},{\"label\":\"Hammer\",\"name\":\"WEAPON_HAMMER\",\"components\":[],\"ammo\":0},{\"label\":\"Bat\",\"name\":\"WEAPON_BAT\",\"components\":[],\"ammo\":0},{\"label\":\"Golf club\",\"name\":\"WEAPON_GOLFCLUB\",\"components\":[],\"ammo\":0},{\"label\":\"Crow bar\",\"name\":\"WEAPON_CROWBAR\",\"components\":[],\"ammo\":0},{\"label\":\"Pistol\",\"name\":\"WEAPON_PISTOL\",\"components\":[\"clip_default\"],\"ammo\":1000},{\"label\":\"Combat pistol\",\"name\":\"WEAPON_COMBATPISTOL\",\"components\":[\"clip_default\"],\"ammo\":1000},{\"label\":\"AP pistol\",\"name\":\"WEAPON_APPISTOL\",\"components\":[\"clip_default\"],\"ammo\":1000},{\"label\":\"Pistol .50\",\"name\":\"WEAPON_PISTOL50\",\"components\":[\"clip_default\"],\"ammo\":1000},{\"label\":\"Heavy revolver\",\"name\":\"WEAPON_REVOLVER\",\"components\":[],\"ammo\":1000},{\"label\":\"Sns pistol\",\"name\":\"WEAPON_SNSPISTOL\",\"components\":[\"clip_default\"],\"ammo\":1000},{\"label\":\"Heavy pistol\",\"name\":\"WEAPON_HEAVYPISTOL\",\"components\":[\"clip_default\"],\"ammo\":1000},{\"label\":\"Vintage pistol\",\"name\":\"WEAPON_VINTAGEPISTOL\",\"components\":[\"clip_default\"],\"ammo\":1000},{\"label\":\"Micro SMG\",\"name\":\"WEAPON_MICROSMG\",\"components\":[\"clip_default\"],\"ammo\":1000},{\"label\":\"SMG\",\"name\":\"WEAPON_SMG\",\"components\":[\"clip_default\"],\"ammo\":1000},{\"label\":\"Assault SMG\",\"name\":\"WEAPON_ASSAULTSMG\",\"components\":[\"clip_default\"],\"ammo\":1000},{\"label\":\"Mini smg\",\"name\":\"WEAPON_MINISMG\",\"components\":[\"clip_default\"],\"ammo\":1000},{\"label\":\"Machine pistol\",\"name\":\"WEAPON_MACHINEPISTOL\",\"components\":[\"clip_default\"],\"ammo\":1000},{\"label\":\"Combat pdw\",\"name\":\"WEAPON_COMBATPDW\",\"components\":[\"clip_default\"],\"ammo\":1000},{\"label\":\"Pump shotgun\",\"name\":\"WEAPON_PUMPSHOTGUN\",\"components\":[],\"ammo\":1000},{\"label\":\"Sawed off shotgun\",\"name\":\"WEAPON_SAWNOFFSHOTGUN\",\"components\":[],\"ammo\":1000},{\"label\":\"Assault shotgun\",\"name\":\"WEAPON_ASSAULTSHOTGUN\",\"components\":[\"clip_default\"],\"ammo\":1000},{\"label\":\"Bullpup shotgun\",\"name\":\"WEAPON_BULLPUPSHOTGUN\",\"components\":[],\"ammo\":1000},{\"label\":\"Heavy shotgun\",\"name\":\"WEAPON_HEAVYSHOTGUN\",\"components\":[\"clip_default\"],\"ammo\":1000},{\"label\":\"Assault rifle\",\"name\":\"WEAPON_ASSAULTRIFLE\",\"components\":[\"clip_default\"],\"ammo\":1000},{\"label\":\"Carbine rifle\",\"name\":\"WEAPON_CARBINERIFLE\",\"components\":[\"clip_default\"],\"ammo\":1000},{\"label\":\"Advanced rifle\",\"name\":\"WEAPON_ADVANCEDRIFLE\",\"components\":[\"clip_default\"],\"ammo\":1000},{\"label\":\"Special carbine\",\"name\":\"WEAPON_SPECIALCARBINE\",\"components\":[\"clip_default\"],\"ammo\":1000},{\"label\":\"Bullpup rifle\",\"name\":\"WEAPON_BULLPUPRIFLE\",\"components\":[\"clip_default\"],\"ammo\":1000},{\"label\":\"Compact rifle\",\"name\":\"WEAPON_COMPACTRIFLE\",\"components\":[\"clip_default\"],\"ammo\":1000},{\"label\":\"MG\",\"name\":\"WEAPON_MG\",\"components\":[\"clip_default\"],\"ammo\":1000},{\"label\":\"Combat MG\",\"name\":\"WEAPON_COMBATMG\",\"components\":[\"clip_default\"],\"ammo\":1000},{\"label\":\"Gusenberg sweeper\",\"name\":\"WEAPON_GUSENBERG\",\"components\":[\"clip_default\"],\"ammo\":1000},{\"label\":\"Sniper rifle\",\"name\":\"WEAPON_SNIPERRIFLE\",\"components\":[\"scope\"],\"ammo\":1000},{\"label\":\"Heavy sniper\",\"name\":\"WEAPON_HEAVYSNIPER\",\"components\":[\"scope_advanced\"],\"ammo\":1000},{\"label\":\"Marksman rifle\",\"name\":\"WEAPON_MARKSMANRIFLE\",\"components\":[\"clip_default\",\"scope\"],\"ammo\":1000},{\"label\":\"Grenade launcher\",\"name\":\"WEAPON_GRENADELAUNCHER\",\"components\":[],\"ammo\":20},{\"label\":\"Rocket launcher\",\"name\":\"WEAPON_RPG\",\"components\":[],\"ammo\":20},{\"label\":\"Minigun\",\"name\":\"WEAPON_MINIGUN\",\"components\":[],\"ammo\":1000},{\"label\":\"Grenade\",\"name\":\"WEAPON_GRENADE\",\"components\":[],\"ammo\":25},{\"label\":\"Sticky bomb\",\"name\":\"WEAPON_STICKYBOMB\",\"components\":[],\"ammo\":25},{\"label\":\"Smoke grenade\",\"name\":\"WEAPON_SMOKEGRENADE\",\"components\":[],\"ammo\":25},{\"label\":\"Bz gas\",\"name\":\"WEAPON_BZGAS\",\"components\":[],\"ammo\":25},{\"label\":\"Molotov cocktail\",\"name\":\"WEAPON_MOLOTOV\",\"components\":[],\"ammo\":25},{\"label\":\"Fire extinguisher\",\"name\":\"WEAPON_FIREEXTINGUISHER\",\"components\":[],\"ammo\":2000},{\"label\":\"Jerrycan\",\"name\":\"WEAPON_PETROLCAN\",\"components\":[],\"ammo\":4500},{\"label\":\"Ball\",\"name\":\"WEAPON_BALL\",\"components\":[],\"ammo\":1},{\"label\":\"Bottle\",\"name\":\"WEAPON_BOTTLE\",\"components\":[],\"ammo\":0},{\"label\":\"Dagger\",\"name\":\"WEAPON_DAGGER\",\"components\":[],\"ammo\":0},{\"label\":\"Firework\",\"name\":\"WEAPON_FIREWORK\",\"components\":[],\"ammo\":20},{\"label\":\"Musket\",\"name\":\"WEAPON_MUSKET\",\"components\":[],\"ammo\":1000},{\"label\":\"Taser\",\"name\":\"WEAPON_STUNGUN\",\"components\":[],\"ammo\":1000},{\"label\":\"Homing launcher\",\"name\":\"WEAPON_HOMINGLAUNCHER\",\"components\":[],\"ammo\":10},{\"label\":\"Proximity mine\",\"name\":\"WEAPON_PROXMINE\",\"components\":[],\"ammo\":5},{\"label\":\"Snow ball\",\"name\":\"WEAPON_SNOWBALL\",\"components\":[],\"ammo\":10},{\"label\":\"Flaregun\",\"name\":\"WEAPON_FLAREGUN\",\"components\":[],\"ammo\":20},{\"label\":\"Marksman pistol\",\"name\":\"WEAPON_MARKSMANPISTOL\",\"components\":[],\"ammo\":1000},{\"label\":\"Knuckledusters\",\"name\":\"WEAPON_KNUCKLE\",\"components\":[],\"ammo\":0},{\"label\":\"Hatchet\",\"name\":\"WEAPON_HATCHET\",\"components\":[],\"ammo\":0},{\"label\":\"Railgun\",\"name\":\"WEAPON_RAILGUN\",\"components\":[],\"ammo\":20},{\"label\":\"Machete\",\"name\":\"WEAPON_MACHETE\",\"components\":[],\"ammo\":0},{\"label\":\"Switchblade\",\"name\":\"WEAPON_SWITCHBLADE\",\"components\":[],\"ammo\":0},{\"label\":\"Double barrel shotgun\",\"name\":\"WEAPON_DBSHOTGUN\",\"components\":[],\"ammo\":1000},{\"label\":\"Auto shotgun\",\"name\":\"WEAPON_AUTOSHOTGUN\",\"components\":[],\"ammo\":1000},{\"label\":\"Battle axe\",\"name\":\"WEAPON_BATTLEAXE\",\"components\":[],\"ammo\":0},{\"label\":\"Compact launcher\",\"name\":\"WEAPON_COMPACTLAUNCHER\",\"components\":[],\"ammo\":20},{\"label\":\"Pipe bomb\",\"name\":\"WEAPON_PIPEBOMB\",\"components\":[],\"ammo\":10},{\"label\":\"Pool cue\",\"name\":\"WEAPON_POOLCUE\",\"components\":[],\"ammo\":0},{\"label\":\"Pipe wrench\",\"name\":\"WEAPON_WRENCH\",\"components\":[],\"ammo\":0},{\"label\":\"Flashlight\",\"name\":\"WEAPON_FLASHLIGHT\",\"components\":[],\"ammo\":0},{\"label\":\"Flare gun\",\"name\":\"WEAPON_FLARE\",\"components\":[],\"ammo\":25},{\"label\":\"Double-Action Revolver\",\"name\":\"WEAPON_DOUBLEACTION\",\"components\":[],\"ammo\":1000}]', '{\"z\":29.1,\"y\":-1758.1,\"x\":-55.1}', 1500, 0, 'user', 'Jak', 'Fulton', '1988-10-10', 'm', '174', '[{\"name\":\"hunger\",\"val\":999000,\"percent\":99.9},{\"name\":\"thirst\",\"val\":999400,\"percent\":99.94},{\"name\":\"drunk\",\"val\":0,\"percent\":0.0}]', NULL, 0, '614-6017', 0),
('steam:110000133d93ea2', 'license:bb1b09de182bf81a18e4eca7a372134cad269c76', 3608, 'Venom8797', '{\"age_2\":0,\"tshirt_1\":0,\"lipstick_2\":0,\"tshirt_2\":0,\"moles_2\":0,\"makeup_1\":0,\"blush_1\":0,\"bracelets_2\":0,\"complexion_1\":0,\"hair_1\":0,\"bags_2\":0,\"sun_2\":0,\"chain_2\":0,\"pants_2\":0,\"bproof_2\":0,\"pants_1\":0,\"arms\":0,\"blemishes_2\":0,\"lipstick_3\":0,\"helmet_1\":-1,\"face\":0,\"bproof_1\":0,\"shoes_1\":0,\"hair_color_1\":0,\"glasses_2\":0,\"decals_2\":0,\"moles_1\":0,\"complexion_2\":0,\"sex\":0,\"sun_1\":0,\"hair_color_2\":0,\"eyebrows_4\":0,\"chest_2\":0,\"lipstick_4\":0,\"chest_1\":0,\"bodyb_2\":0,\"hair_2\":0,\"chest_3\":0,\"ears_2\":0,\"eyebrows_2\":0,\"blush_2\":0,\"arms_2\":0,\"ears_1\":-1,\"eye_color\":0,\"torso_1\":0,\"torso_2\":0,\"eyebrows_1\":0,\"makeup_4\":0,\"glasses_1\":0,\"helmet_2\":0,\"bodyb_1\":0,\"beard_1\":0,\"beard_3\":0,\"chain_1\":0,\"mask_2\":0,\"makeup_2\":0,\"blemishes_1\":0,\"makeup_3\":0,\"decals_1\":0,\"blush_3\":0,\"beard_4\":0,\"age_1\":0,\"eyebrows_3\":0,\"skin\":0,\"watches_1\":-1,\"watches_2\":0,\"bags_1\":0,\"shoes_2\":0,\"mask_1\":0,\"lipstick_1\":0,\"bracelets_1\":-1,\"beard_2\":0}', 'police', 1, '[{\"label\":\"Nightstick\",\"ammo\":0,\"components\":[],\"name\":\"WEAPON_NIGHTSTICK\"},{\"label\":\"Combat pistol\",\"ammo\":9956,\"components\":[\"clip_default\",\"flashlight\"],\"name\":\"WEAPON_COMBATPISTOL\"},{\"label\":\"Pump shotgun\",\"ammo\":9999,\"components\":[\"flashlight\"],\"name\":\"WEAPON_PUMPSHOTGUN\"},{\"label\":\"Fire extinguisher\",\"ammo\":2000,\"components\":[],\"name\":\"WEAPON_FIREEXTINGUISHER\"},{\"label\":\"Taser\",\"ammo\":9999,\"components\":[],\"name\":\"WEAPON_STUNGUN\"},{\"label\":\"Flashlight\",\"ammo\":0,\"components\":[],\"name\":\"WEAPON_FLASHLIGHT\"},{\"label\":\"Flare gun\",\"ammo\":25,\"components\":[],\"name\":\"WEAPON_FLARE\"}]', '{\"x\":1878.1,\"z\":35.9,\"y\":3564.7}', 19629, 0, 'user', 'Nicholas ', 'Silver', '3/17/1999', 'm', '200', '[{\"val\":772700,\"percent\":77.27,\"name\":\"hunger\"},{\"val\":829300,\"percent\":82.93,\"name\":\"thirst\"},{\"val\":0,\"percent\":0.0,\"name\":\"drunk\"}]', NULL, 0, '228-4620', 0),
('steam:110000134e3ca1a', 'license:18b98386826174336f17731ee73f01c954c03029', 197, 'ZG', '{\"bracelets_2\":0,\"beard_1\":0,\"bodyb_2\":0,\"blush_2\":0,\"bags_1\":0,\"bracelets_1\":-1,\"skin\":0,\"hair_1\":0,\"sun_2\":0,\"blush_1\":0,\"chain_2\":0,\"helmet_2\":0,\"sun_1\":0,\"torso_1\":0,\"shoes_1\":0,\"bodyb_1\":0,\"age_1\":0,\"glasses_1\":0,\"lipstick_1\":0,\"complexion_1\":0,\"hair_color_1\":0,\"blush_3\":0,\"chest_2\":0,\"bags_2\":0,\"beard_2\":0,\"makeup_2\":0,\"moles_1\":0,\"chain_1\":0,\"blemishes_2\":0,\"eyebrows_4\":0,\"watches_2\":0,\"watches_1\":-1,\"pants_1\":0,\"mask_1\":0,\"ears_2\":0,\"eyebrows_3\":0,\"ears_1\":-1,\"shoes_2\":0,\"lipstick_4\":0,\"eyebrows_2\":0,\"mask_2\":0,\"eye_color\":0,\"arms_2\":0,\"hair_2\":0,\"age_2\":0,\"arms\":0,\"torso_2\":0,\"bproof_2\":0,\"makeup_1\":0,\"chest_1\":0,\"helmet_1\":-1,\"face\":0,\"moles_2\":0,\"makeup_4\":0,\"glasses_2\":0,\"blemishes_1\":0,\"decals_1\":0,\"beard_4\":0,\"eyebrows_1\":0,\"sex\":0,\"tshirt_1\":0,\"chest_3\":0,\"complexion_2\":0,\"lipstick_3\":0,\"tshirt_2\":0,\"bproof_1\":0,\"decals_2\":0,\"makeup_3\":0,\"pants_2\":0,\"hair_color_2\":0,\"beard_3\":0,\"lipstick_2\":0}', 'miner', 0, '[]', '{\"y\":3550.9,\"x\":1280.5,\"z\":35.1}', -253500, 0, 'user', 'Jones', 'Smih', '04-17-1996', 'm', '140', '[{\"name\":\"hunger\",\"percent\":91.78,\"val\":917800},{\"name\":\"thirst\",\"percent\":93.73,\"val\":937300},{\"name\":\"drunk\",\"percent\":0.0,\"val\":0}]', NULL, 0, '753-9558', 0),
('steam:11000013517b942', 'license:72243fe74f8979354934205ba21e3e6d2303bb6f', 0, 'Detective Monkeyy', NULL, 'unemployed', 0, '[]', '{\"y\":5174.8,\"z\":18.2,\"x\":3337.8}', 0, 0, 'user', '', '', '', '', '', '[{\"val\":981200,\"percent\":98.12,\"name\":\"hunger\"},{\"val\":985900,\"percent\":98.59,\"name\":\"thirst\"},{\"val\":0,\"percent\":0.0,\"name\":\"drunk\"}]', NULL, 0, '183-7756', 0),
('steam:1100001351e28d7', 'license:5c9116145e9b55533bb3f7541fd4415032dd61c2', 0, 'Officer Rodriguez', '{\"ears_1\":-1,\"blush_3\":0,\"moles_1\":0,\"ears_2\":0,\"makeup_4\":0,\"hair_color_2\":0,\"makeup_1\":0,\"torso_2\":0,\"glasses_1\":0,\"blemishes_2\":0,\"helmet_2\":0,\"skin\":0,\"beard_4\":0,\"sun_2\":0,\"bodyb_1\":0,\"lipstick_3\":0,\"blush_1\":0,\"pants_2\":0,\"sex\":0,\"helmet_1\":-1,\"tshirt_1\":0,\"hair_1\":0,\"watches_1\":-1,\"complexion_2\":0,\"bproof_1\":0,\"lipstick_2\":0,\"shoes_2\":0,\"chain_1\":0,\"eye_color\":0,\"torso_1\":0,\"makeup_2\":0,\"chest_3\":0,\"chest_1\":0,\"beard_3\":0,\"watches_2\":0,\"hair_2\":0,\"blemishes_1\":0,\"arms\":0,\"lipstick_4\":0,\"eyebrows_1\":0,\"face\":0,\"shoes_1\":0,\"chain_2\":0,\"bproof_2\":0,\"pants_1\":0,\"blush_2\":0,\"beard_1\":0,\"bodyb_2\":0,\"moles_2\":0,\"decals_2\":0,\"glasses_2\":0,\"hair_color_1\":0,\"eyebrows_2\":0,\"age_1\":0,\"complexion_1\":0,\"bags_1\":0,\"tshirt_2\":0,\"bracelets_1\":-1,\"arms_2\":0,\"decals_1\":0,\"lipstick_1\":0,\"bags_2\":0,\"age_2\":0,\"mask_2\":0,\"chest_2\":0,\"mask_1\":0,\"eyebrows_4\":0,\"bracelets_2\":0,\"beard_2\":0,\"makeup_3\":0,\"eyebrows_3\":0,\"sun_1\":0}', 'lumberjack', 0, '[{\"components\":[],\"name\":\"WEAPON_NIGHTSTICK\",\"ammo\":0,\"label\":\"Nightstick\"},{\"components\":[\"clip_default\",\"flashlight\"],\"name\":\"WEAPON_PISTOL\",\"ammo\":955,\"label\":\"Pistol\"},{\"components\":[\"flashlight\"],\"name\":\"WEAPON_PUMPSHOTGUN\",\"ammo\":1000,\"label\":\"Pump shotgun\"},{\"components\":[\"clip_default\"],\"name\":\"WEAPON_ASSAULTRIFLE\",\"ammo\":9998,\"label\":\"Assault rifle\"},{\"components\":[\"clip_default\",\"flashlight\",\"scope\",\"grip\"],\"name\":\"WEAPON_CARBINERIFLE\",\"ammo\":9998,\"label\":\"Carbine rifle\"},{\"components\":[\"clip_default\"],\"name\":\"WEAPON_ADVANCEDRIFLE\",\"ammo\":9998,\"label\":\"Advanced rifle\"},{\"components\":[\"clip_default\"],\"name\":\"WEAPON_SPECIALCARBINE\",\"ammo\":9998,\"label\":\"Special carbine\"},{\"components\":[\"clip_default\"],\"name\":\"WEAPON_BULLPUPRIFLE\",\"ammo\":9998,\"label\":\"Bullpup rifle\"},{\"components\":[],\"name\":\"WEAPON_FIREEXTINGUISHER\",\"ammo\":1000,\"label\":\"Fire extinguisher\"},{\"components\":[],\"name\":\"WEAPON_STUNGUN\",\"ammo\":1000,\"label\":\"Taser\"},{\"components\":[],\"name\":\"WEAPON_FLASHLIGHT\",\"ammo\":0,\"label\":\"Flashlight\"},{\"components\":[],\"name\":\"WEAPON_FLARE\",\"ammo\":25,\"label\":\"Flare gun\"}]', '{\"z\":82.1,\"y\":220.3,\"x\":822.9}', 650, 0, 'user', 'Brandon', 'Ramos', '05/16/1990', 'm', '160', '[{\"val\":693000,\"name\":\"hunger\",\"percent\":69.3},{\"val\":769750,\"name\":\"thirst\",\"percent\":76.975},{\"val\":0,\"name\":\"drunk\",\"percent\":0.0}]', NULL, 0, '345-5878', 0),
('steam:110000135e316b5', 'license:f5e5df53388c1d09abec13151fbb8be5a75f8ba2', 0, 'FreezyOW', '{\"torso_2\":0,\"bracelets_1\":-1,\"beard_3\":0,\"beard_4\":0,\"complexion_2\":0,\"eyebrows_4\":0,\"shoes_1\":0,\"tshirt_1\":0,\"eye_color\":0,\"tshirt_2\":0,\"face\":0,\"torso_1\":0,\"age_2\":0,\"bags_2\":0,\"skin\":0,\"makeup_1\":0,\"watches_1\":-1,\"bracelets_2\":0,\"bproof_1\":0,\"beard_1\":0,\"chain_2\":0,\"watches_2\":0,\"bodyb_1\":0,\"beard_2\":0,\"glasses_1\":0,\"ears_1\":-1,\"chain_1\":0,\"hair_2\":0,\"makeup_2\":0,\"sun_1\":0,\"sex\":0,\"blush_1\":0,\"bags_1\":0,\"eyebrows_1\":0,\"pants_2\":0,\"blemishes_2\":0,\"eyebrows_2\":0,\"moles_2\":0,\"age_1\":0,\"makeup_3\":0,\"shoes_2\":0,\"hair_1\":0,\"chest_1\":0,\"chest_2\":0,\"sun_2\":0,\"glasses_2\":0,\"lipstick_3\":0,\"mask_2\":0,\"hair_color_1\":0,\"blush_2\":0,\"bproof_2\":0,\"helmet_1\":-1,\"complexion_1\":0,\"hair_color_2\":0,\"moles_1\":0,\"eyebrows_3\":0,\"lipstick_2\":0,\"arms\":0,\"lipstick_4\":0,\"decals_2\":0,\"blemishes_1\":0,\"pants_1\":0,\"ears_2\":0,\"lipstick_1\":0,\"blush_3\":0,\"mask_1\":0,\"arms_2\":0,\"chest_3\":0,\"helmet_2\":0,\"decals_1\":0,\"bodyb_2\":0,\"makeup_4\":0}', 'fisherman', 0, '[]', '{\"x\":897.4,\"y\":-1661.6,\"z\":30.2}', 800, 0, 'user', 'Aaron', 'Freeman', '1993-10-26', 'm', '155', '[{\"val\":346700,\"name\":\"hunger\",\"percent\":34.67},{\"val\":385025,\"name\":\"thirst\",\"percent\":38.5025},{\"val\":0,\"name\":\"drunk\",\"percent\":0.0}]', NULL, 0, '207-0046', 0),
('steam:110000136177a4e', 'license:cc1fb4f4f7ddadf9ac11a23477ceca023b131e15', 0, 'nickbaseball1', NULL, 'unemployed', 0, '[]', '{\"y\":1753.7,\"z\":25.2,\"x\":2550.4}', 750, 0, 'user', 'Nick', 'Stromlund', '20002505', 'm', '150', '[{\"val\":877600,\"percent\":87.76,\"name\":\"hunger\"},{\"val\":908200,\"percent\":90.82,\"name\":\"thirst\"},{\"val\":0,\"percent\":0.0,\"name\":\"drunk\"}]', NULL, 0, '999-6114', 0),
('steam:110000139d9adf9', 'license:ae561a5668e725731bdcf693c98d7fa63be00a59', 0, 'нℓ | C4', NULL, 'unemployed', 0, '[]', '{\"z\":31.6,\"y\":6301.6,\"x\":-45.2}', 200, 0, 'user', '', '', '', '', '', '[{\"name\":\"hunger\",\"val\":978100,\"percent\":97.81},{\"name\":\"thirst\",\"val\":983575,\"percent\":98.3575},{\"name\":\"drunk\",\"val\":0,\"percent\":0.0}]', NULL, 0, '696-4507', 0),
('steam:11000013e33b934', 'license:9631ace593394b20479e2e471799e2099fe5135b', 0, 'Hernandez', NULL, 'mechanic', 0, '[]', '{\"x\":340.8,\"y\":-1394.2,\"z\":32.5}', 65897, 0, 'user', 'ANGEL', 'HERNANDEZ', '1989/07/06', 'm', '170', '[{\"val\":493200,\"percent\":49.32,\"name\":\"hunger\"},{\"val\":494900,\"percent\":49.49,\"name\":\"thirst\"},{\"val\":0,\"percent\":0.0,\"name\":\"drunk\"}]', NULL, 0, '517-1172', 0),
('steam:11000013e7edea3', 'license:713f44ae43ca9c36f67bed492533b040e379ae7f', 0, 'RICHS05', '{\"makeup_3\":0,\"lipstick_2\":0,\"beard_3\":0,\"blush_2\":0,\"watches_1\":-1,\"chain_2\":0,\"sun_1\":0,\"makeup_1\":0,\"blush_3\":0,\"lipstick_1\":0,\"helmet_1\":-1,\"age_2\":0,\"ears_2\":0,\"tshirt_1\":0,\"arms\":0,\"beard_1\":0,\"beard_4\":0,\"sex\":0,\"glasses_1\":0,\"tshirt_2\":0,\"lipstick_4\":0,\"bags_1\":0,\"glasses_2\":0,\"complexion_2\":0,\"hair_2\":0,\"ears_1\":-1,\"pants_2\":0,\"eyebrows_1\":0,\"eyebrows_3\":0,\"beard_2\":0,\"moles_2\":0,\"makeup_4\":0,\"torso_2\":0,\"chest_2\":0,\"torso_1\":0,\"chain_1\":0,\"decals_2\":0,\"bags_2\":0,\"lipstick_3\":0,\"eye_color\":0,\"bproof_2\":0,\"hair_color_2\":0,\"face\":0,\"arms_2\":0,\"blush_1\":0,\"complexion_1\":0,\"bproof_1\":0,\"chest_1\":0,\"sun_2\":0,\"age_1\":0,\"mask_2\":0,\"bracelets_1\":-1,\"blemishes_2\":0,\"shoes_2\":0,\"helmet_2\":0,\"makeup_2\":0,\"hair_color_1\":0,\"chest_3\":0,\"skin\":0,\"hair_1\":0,\"shoes_1\":0,\"eyebrows_2\":0,\"eyebrows_4\":0,\"blemishes_1\":0,\"moles_1\":0,\"decals_1\":0,\"pants_1\":0,\"bodyb_2\":0,\"mask_1\":0,\"bracelets_2\":0,\"bodyb_1\":0,\"watches_2\":0}', 'police', 4, '[{\"components\":[],\"ammo\":0,\"label\":\"Crow bar\",\"name\":\"WEAPON_CROWBAR\"},{\"components\":[\"clip_default\",\"flashlight\"],\"ammo\":988,\"label\":\"Combat pistol\",\"name\":\"WEAPON_COMBATPISTOL\"},{\"components\":[\"clip_extended\",\"flashlight\",\"scope\",\"suppressor\"],\"ammo\":1000,\"label\":\"SMG\",\"name\":\"WEAPON_SMG\"},{\"components\":[\"flashlight\"],\"ammo\":997,\"label\":\"Pump shotgun\",\"name\":\"WEAPON_PUMPSHOTGUN\"},{\"components\":[\"clip_default\"],\"ammo\":965,\"label\":\"Carbine rifle\",\"name\":\"WEAPON_CARBINERIFLE\"},{\"components\":[\"scope_advanced\"],\"ammo\":1000,\"label\":\"Heavy sniper\",\"name\":\"WEAPON_HEAVYSNIPER\"},{\"components\":[],\"ammo\":25,\"label\":\"Smoke grenade\",\"name\":\"WEAPON_SMOKEGRENADE\"},{\"components\":[],\"ammo\":2000,\"label\":\"Fire extinguisher\",\"name\":\"WEAPON_FIREEXTINGUISHER\"},{\"components\":[],\"ammo\":1000,\"label\":\"Taser\",\"name\":\"WEAPON_STUNGUN\"},{\"components\":[],\"ammo\":0,\"label\":\"Flashlight\",\"name\":\"WEAPON_FLASHLIGHT\"},{\"components\":[],\"ammo\":25,\"label\":\"Flare gun\",\"name\":\"WEAPON_FLARE\"}]', '{\"y\":-894.5,\"x\":293.3,\"z\":29.1}', 356671, 1, 'superadmin', 'Steve', 'Erwin', '', 'm', '200', '[{\"percent\":29.2,\"val\":292000,\"name\":\"hunger\"},{\"percent\":34.4,\"val\":344000,\"name\":\"thirst\"},{\"percent\":0.0,\"val\":0,\"name\":\"drunk\"}]', NULL, 0, '771-1741', 0),
('steam:11000013e9d4b99', 'license:aa05f9d538a874cc7279f02b086b3e3aab1f6016', 0, 'beebo', NULL, 'unemployed', 0, '[{\"label\":\"Pistol\",\"name\":\"WEAPON_PISTOL\",\"ammo\":1000,\"components\":[\"clip_default\"]},{\"label\":\"Combat pistol\",\"name\":\"WEAPON_COMBATPISTOL\",\"ammo\":1000,\"components\":[\"clip_default\"]},{\"label\":\"Pistol .50\",\"name\":\"WEAPON_PISTOL50\",\"ammo\":1000,\"components\":[\"clip_default\"]},{\"label\":\"Taser\",\"name\":\"WEAPON_STUNGUN\",\"ammo\":1000,\"components\":[]}]', '{\"z\":47.1,\"x\":2795.6,\"y\":4483.0}', 1400, 0, 'user', 'Jon', 'Arm', '1995 12 1', 'm', '140', '[{\"name\":\"hunger\",\"percent\":70.43,\"val\":704300},{\"name\":\"thirst\",\"percent\":77.8225,\"val\":778225},{\"name\":\"drunk\",\"percent\":0.0,\"val\":0}]', NULL, 0, '562-3039', 0),
('steam:11000013ea40bc0', 'license:49ecd7ddd93aec39c517a6b9fe5144b25762e37c', -500, 'Ur_A_Scrub1', '{\"bags_1\":0,\"age_1\":0,\"lipstick_3\":0,\"ears_2\":0,\"hair_color_2\":0,\"eyebrows_2\":0,\"pants_2\":0,\"eye_color\":0,\"eyebrows_1\":0,\"lipstick_1\":0,\"pants_1\":6,\"watches_1\":-1,\"face\":0,\"blush_2\":0,\"bproof_2\":0,\"bodyb_1\":0,\"glasses_1\":3,\"bracelets_2\":0,\"chain_2\":0,\"chest_3\":0,\"beard_2\":10,\"sun_2\":0,\"sex\":0,\"helmet_2\":0,\"shoes_2\":0,\"tshirt_2\":1,\"complexion_1\":0,\"moles_2\":0,\"beard_1\":10,\"makeup_4\":0,\"beard_4\":0,\"bracelets_1\":-1,\"chain_1\":0,\"age_2\":0,\"sun_1\":0,\"eyebrows_4\":0,\"chest_1\":0,\"decals_1\":0,\"complexion_2\":0,\"arms\":0,\"decals_2\":0,\"blush_1\":0,\"makeup_3\":0,\"bproof_1\":0,\"makeup_1\":0,\"hair_1\":4,\"torso_1\":7,\"hair_2\":0,\"chest_2\":0,\"watches_2\":0,\"helmet_1\":-1,\"lipstick_4\":0,\"mask_2\":0,\"arms_2\":0,\"bags_2\":0,\"eyebrows_3\":0,\"ears_1\":-1,\"makeup_2\":0,\"beard_3\":0,\"hair_color_1\":1,\"lipstick_2\":0,\"blemishes_1\":0,\"blush_3\":0,\"moles_1\":0,\"torso_2\":1,\"bodyb_2\":0,\"tshirt_1\":0,\"glasses_2\":0,\"mask_1\":0,\"blemishes_2\":0,\"shoes_1\":31,\"skin\":0}', 'mechanic', 0, '[{\"label\":\"Pistol\",\"ammo\":0,\"components\":[\"clip_default\"],\"name\":\"WEAPON_PISTOL\"},{\"label\":\"Pump shotgun\",\"ammo\":0,\"components\":[],\"name\":\"WEAPON_PUMPSHOTGUN\"}]', '{\"x\":1799.2,\"z\":41.1,\"y\":3326.4}', 3398, 0, 'user', 'Freddy', 'Lungus', '1997/03/14', 'm', '157', '[{\"val\":124100,\"percent\":12.41,\"name\":\"hunger\"},{\"val\":218075,\"percent\":21.8075,\"name\":\"thirst\"},{\"val\":0,\"percent\":0.0,\"name\":\"drunk\"}]', NULL, 0, '284-7437', 0),
('steam:11000013eaca305', 'license:6ce7fb07728002f3205689fee3303e863d40b7e9', 0, 'gameon.center4', NULL, 'unemployed', 0, '[]', '{\"z\":13.1,\"y\":-1082.9,\"x\":-1712.4}', 200, 0, 'user', '', '', '', '', '', '[{\"name\":\"hunger\",\"val\":959200,\"percent\":95.92},{\"name\":\"thirst\",\"val\":969400,\"percent\":96.94},{\"name\":\"drunk\",\"val\":0,\"percent\":0.0}]', NULL, 0, '416-0570', 0),
('steam:11000013efa68e1', 'license:6bbd64eed21b1d92dd602318cb2a595f787771c2', 0, 'mt3bx77', NULL, 'unemployed', 0, '[]', '{\"z\":131.4,\"y\":2092.2,\"x\":-597.2}', 200, 0, 'user', '', '', '', '', '', '[{\"name\":\"hunger\",\"val\":969200,\"percent\":96.92},{\"name\":\"thirst\",\"val\":976900,\"percent\":97.69},{\"name\":\"drunk\",\"val\":0,\"percent\":0.0}]', NULL, 0, '646-7832', 0),
('steam:11000013f478ad1', 'license:d79df4609122183cbb7954be667f91994fe362f1', 0, '[B-184]N.Taylor', NULL, 'unemployed', 0, '[]', '{\"z\":797.9,\"y\":5604.5,\"x\":501.6}', 0, 0, 'user', '', '', '', '', '', '[{\"val\":999000,\"name\":\"hunger\",\"percent\":99.9},{\"val\":999250,\"name\":\"thirst\",\"percent\":99.925},{\"val\":0,\"name\":\"drunk\",\"percent\":0.0}]', NULL, 0, '753-4849', 0),
('steam:11000013f6b85e1', 'license:20f178f554669182ff41d9912a7008092d0ab19c', 366, 'antoniocolello', '{\"watches_1\":-1,\"sex\":0,\"hair_2\":0,\"makeup_3\":0,\"beard_2\":0,\"moles_1\":0,\"arms_2\":0,\"eyebrows_3\":0,\"skin\":0,\"eye_color\":0,\"watches_2\":0,\"torso_1\":0,\"ears_2\":0,\"bracelets_1\":-1,\"beard_3\":0,\"blush_3\":0,\"makeup_1\":0,\"chain_1\":0,\"tshirt_1\":0,\"arms\":0,\"bproof_1\":0,\"glasses_2\":0,\"hair_color_2\":0,\"face\":0,\"bags_2\":0,\"sun_2\":0,\"lipstick_3\":0,\"beard_4\":0,\"age_1\":0,\"lipstick_4\":0,\"chest_1\":0,\"blush_2\":0,\"eyebrows_1\":0,\"decals_1\":0,\"shoes_2\":0,\"helmet_1\":-1,\"blemishes_1\":0,\"makeup_4\":0,\"chest_2\":0,\"pants_2\":0,\"bags_1\":0,\"ears_1\":-1,\"blush_1\":0,\"moles_2\":0,\"chest_3\":0,\"eyebrows_4\":0,\"hair_1\":0,\"hair_color_1\":0,\"complexion_2\":0,\"bodyb_1\":0,\"beard_1\":0,\"lipstick_2\":0,\"mask_2\":0,\"mask_1\":0,\"bodyb_2\":0,\"shoes_1\":0,\"helmet_2\":0,\"age_2\":0,\"complexion_1\":0,\"bracelets_2\":0,\"sun_1\":0,\"tshirt_2\":0,\"chain_2\":0,\"torso_2\":0,\"glasses_1\":0,\"decals_2\":0,\"bproof_2\":0,\"lipstick_1\":0,\"pants_1\":0,\"makeup_2\":0,\"blemishes_2\":0,\"eyebrows_2\":0}', 'police', 1, '[{\"name\":\"WEAPON_COMBATPISTOL\",\"label\":\"Combat pistol\",\"ammo\":983,\"components\":[\"clip_default\"]},{\"name\":\"WEAPON_PUMPSHOTGUN\",\"label\":\"Pump shotgun\",\"ammo\":10,\"components\":[]},{\"name\":\"WEAPON_CARBINERIFLE\",\"label\":\"Carbine rifle\",\"ammo\":1000,\"components\":[\"clip_default\"]},{\"name\":\"WEAPON_FIREEXTINGUISHER\",\"label\":\"Fire extinguisher\",\"ammo\":2000,\"components\":[]},{\"name\":\"WEAPON_STUNGUN\",\"label\":\"Taser\",\"ammo\":1000,\"components\":[]},{\"name\":\"WEAPON_FLARE\",\"label\":\"Flare gun\",\"ammo\":25,\"components\":[]}]', '{\"x\":500.0,\"y\":-852.6,\"z\":38.2}', 39000, 0, 'user', 'Gregory', 'Stevens', '1950-09-09', 'm', '200', '[{\"name\":\"hunger\",\"percent\":71.43,\"val\":714300},{\"name\":\"thirst\",\"percent\":73.5725,\"val\":735725},{\"name\":\"drunk\",\"percent\":0.0,\"val\":0}]', NULL, 1, '371-8759', 0);

-- --------------------------------------------------------

--
-- Table structure for table `owned_bags`
--

CREATE TABLE `owned_bags` (
  `identifier` varchar(50) DEFAULT NULL,
  `id` int(11) DEFAULT NULL,
  `x` double DEFAULT NULL,
  `y` double DEFAULT NULL,
  `z` double DEFAULT NULL,
  `itemcount` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `owned_bags`
--

INSERT INTO `owned_bags` (`identifier`, `id`, `x`, `y`, `z`, `itemcount`) VALUES
(NULL, 80872, 754.0885620117188, -980.535400390625, 24.163902282714844, 1);

-- --------------------------------------------------------

--
-- Table structure for table `owned_bag_inventory`
--

CREATE TABLE `owned_bag_inventory` (
  `id` int(11) DEFAULT NULL,
  `item` varchar(50) DEFAULT NULL,
  `label` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `count` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `owned_bag_inventory`
--

INSERT INTO `owned_bag_inventory` (`id`, `item`, `label`, `count`) VALUES
(80872, 'jewels', 'Jewels', 10);

-- --------------------------------------------------------

--
-- Table structure for table `owned_properties`
--

CREATE TABLE `owned_properties` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `price` double NOT NULL,
  `rented` int(11) NOT NULL,
  `owner` varchar(60) COLLATE utf8mb4_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `owned_properties`
--

INSERT INTO `owned_properties` (`id`, `name`, `price`, `rented`, `owner`) VALUES
(1, 'TinselTowersApt12', 1700000, 0, 'steam:110000111c332ed'),
(2, 'RichardMajesticApt2', 8500, 1, 'steam:110000134e3ca1a');

-- --------------------------------------------------------

--
-- Table structure for table `owned_vehicles`
--

CREATE TABLE `owned_vehicles` (
  `owner` varchar(22) NOT NULL,
  `plate` varchar(12) NOT NULL,
  `vehicle` longtext DEFAULT NULL,
  `type` varchar(20) NOT NULL DEFAULT 'car',
  `job` varchar(20) NOT NULL DEFAULT '',
  `stored` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `owned_vehicles`
--

INSERT INTO `owned_vehicles` (`owner`, `plate`, `vehicle`, `type`, `job`, `stored`) VALUES
('steam:11000010a01bdb9', 'ARD 313', '{\"modSideSkirt\":-1,\"modTrimA\":-1,\"modArmor\":-1,\"dirtLevel\":0.0,\"modDashboard\":-1,\"modWindows\":-1,\"tyreSmokeColor\":[255,255,255],\"modPlateHolder\":-1,\"modEngineBlock\":-1,\"extras\":{\"1\":false,\"2\":false,\"5\":false,\"3\":false,\"4\":false},\"modAerials\":-1,\"engineHealth\":1000.0,\"modDoorSpeaker\":-1,\"modTransmission\":-1,\"color2\":134,\"modFender\":-1,\"modAPlate\":-1,\"modLivery\":1,\"bodyHealth\":1000.0,\"color1\":134,\"modBrakes\":-1,\"modTank\":-1,\"wheelColor\":156,\"modRoof\":-1,\"modTrunk\":-1,\"modTurbo\":false,\"modAirFilter\":-1,\"modSuspension\":-1,\"windowTint\":-1,\"neonEnabled\":[false,false,false,false],\"modSpoilers\":-1,\"modFrontBumper\":-1,\"modExhaust\":-1,\"plateIndex\":4,\"modEngine\":-1,\"wheels\":1,\"modHydrolic\":-1,\"modArchCover\":-1,\"modRearBumper\":-1,\"modGrille\":-1,\"modSmokeEnabled\":false,\"modFrontWheels\":-1,\"pearlescentColor\":156,\"modXenon\":false,\"model\":1091240186,\"modRightFender\":-1,\"modBackWheels\":-1,\"modVanityPlate\":-1,\"modHood\":-1,\"modSeats\":-1,\"modStruts\":-1,\"modOrnaments\":-1,\"modSpeakers\":-1,\"modSteeringWheel\":-1,\"modShifterLeavers\":-1,\"plate\":\"ARD 313\",\"fuelLevel\":93.7,\"modFrame\":-1,\"modHorns\":-1,\"modDial\":-1,\"neonColor\":[255,0,255],\"modTrimB\":-1}', 'car', 'police', 1),
('steam:110000133d93ea2', 'ARH 860', '{\"modSmokeEnabled\":false,\"plateIndex\":4,\"modSpeakers\":-1,\"modBackWheels\":-1,\"pearlescentColor\":156,\"extras\":{\"11\":false,\"4\":false,\"1\":false,\"2\":false,\"10\":true,\"12\":false,\"7\":true,\"3\":false,\"5\":false,\"6\":false},\"modAPlate\":-1,\"modFrontBumper\":-1,\"modBrakes\":-1,\"modDashboard\":-1,\"modEngine\":-1,\"modTrimA\":-1,\"modShifterLeavers\":-1,\"fuelLevel\":78.6,\"modFender\":-1,\"modXenon\":false,\"modSteeringWheel\":-1,\"modFrontWheels\":-1,\"modStruts\":-1,\"modTrunk\":-1,\"engineHealth\":1000.0,\"tyreSmokeColor\":[255,255,255],\"modPlateHolder\":-1,\"modOrnaments\":-1,\"modAirFilter\":-1,\"modHorns\":-1,\"modRightFender\":-1,\"modLivery\":2,\"color1\":134,\"modDial\":-1,\"modSpoilers\":-1,\"modSuspension\":-1,\"modSeats\":-1,\"color2\":134,\"modGrille\":-1,\"wheels\":1,\"modHood\":-1,\"modWindows\":-1,\"modArchCover\":-1,\"plate\":\"ARH 860\",\"modTransmission\":-1,\"windowTint\":-1,\"modVanityPlate\":-1,\"modTank\":-1,\"modTrimB\":-1,\"modDoorSpeaker\":-1,\"modEngineBlock\":-1,\"dirtLevel\":0.0,\"neonColor\":[255,0,255],\"modSideSkirt\":-1,\"modTurbo\":false,\"wheelColor\":156,\"modArmor\":-1,\"bodyHealth\":1000.0,\"modRoof\":-1,\"modRearBumper\":-1,\"modExhaust\":-1,\"modFrame\":-1,\"neonEnabled\":[false,false,false,false],\"modHydrolic\":-1,\"model\":797325625,\"modAerials\":-1}', 'car', 'police', 1),
('steam:11000010a01bdb9', 'BDD 799', '{\"modTransmission\":-1,\"modDial\":-1,\"modEngineBlock\":-1,\"modHood\":-1,\"modRoof\":-1,\"color2\":0,\"extras\":[],\"tyreSmokeColor\":[255,255,255],\"modGrille\":-1,\"modArmor\":-1,\"modTank\":-1,\"modAPlate\":-1,\"modBackWheels\":-1,\"modDashboard\":-1,\"modRightFender\":-1,\"fuelLevel\":93.4,\"modOrnaments\":-1,\"modFender\":-1,\"engineHealth\":1000.0,\"modShifterLeavers\":-1,\"dirtLevel\":7.3,\"modLivery\":-1,\"modFrame\":-1,\"modHorns\":-1,\"modEngine\":-1,\"bodyHealth\":1000.0,\"modSpeakers\":-1,\"wheelColor\":156,\"modExhaust\":-1,\"pearlescentColor\":38,\"neonEnabled\":[false,false,false,false],\"modSeats\":-1,\"plateIndex\":4,\"modHydrolic\":-1,\"modSideSkirt\":-1,\"modBrakes\":-1,\"modFrontWheels\":-1,\"windowTint\":-1,\"modWindows\":-1,\"neonColor\":[255,0,255],\"modDoorSpeaker\":-1,\"modTrimA\":-1,\"modStruts\":-1,\"wheels\":6,\"modFrontBumper\":-1,\"modSuspension\":-1,\"modAerials\":-1,\"modRearBumper\":-1,\"modArchCover\":-1,\"modSmokeEnabled\":false,\"modTrunk\":-1,\"plate\":\"BDD 799\",\"modSpoilers\":-1,\"modTrimB\":-1,\"modPlateHolder\":-1,\"modSteeringWheel\":-1,\"model\":1131912276,\"modAirFilter\":-1,\"modVanityPlate\":-1,\"color1\":29,\"modXenon\":false,\"modTurbo\":false}', 'car', '', 1),
('steam:11000010e2a62e2', 'BPI 436', '{\"modXenon\":false,\"fuelLevel\":83.2,\"color2\":111,\"modLivery\":0,\"modSeats\":-1,\"modHorns\":-1,\"modFender\":-1,\"modFrame\":-1,\"modPlateHolder\":-1,\"color1\":111,\"modHood\":-1,\"modSideSkirt\":-1,\"modWindows\":-1,\"modAPlate\":-1,\"modOrnaments\":-1,\"tyreSmokeColor\":[255,255,255],\"modSpoilers\":-1,\"modFrontBumper\":-1,\"modRearBumper\":-1,\"modEngineBlock\":-1,\"neonColor\":[255,0,255],\"modStruts\":-1,\"extras\":{\"9\":false,\"8\":false,\"7\":true,\"6\":false,\"5\":false,\"4\":false,\"3\":false,\"2\":false,\"11\":false,\"10\":false,\"1\":true},\"model\":1863871753,\"modAirFilter\":-1,\"modBrakes\":-1,\"modTransmission\":-1,\"pearlescentColor\":0,\"wheelColor\":156,\"modSpeakers\":-1,\"modDashboard\":-1,\"modAerials\":-1,\"plate\":\"BPI 436\",\"modSuspension\":-1,\"wheels\":1,\"modGrille\":-1,\"modTurbo\":false,\"plateIndex\":4,\"modRoof\":-1,\"neonEnabled\":[false,false,false,false],\"bodyHealth\":1000.0,\"modArchCover\":-1,\"modTrimA\":-1,\"engineHealth\":1000.0,\"modRightFender\":-1,\"modTank\":-1,\"modExhaust\":-1,\"modFrontWheels\":-1,\"windowTint\":-1,\"modShifterLeavers\":-1,\"modArmor\":-1,\"modTrunk\":-1,\"modSmokeEnabled\":false,\"modDoorSpeaker\":-1,\"modDial\":-1,\"modHydrolic\":-1,\"modBackWheels\":-1,\"modVanityPlate\":-1,\"modEngine\":-1,\"dirtLevel\":9.0,\"modTrimB\":-1,\"modSteeringWheel\":-1}', 'car', 'police', 1),
('steam:11000010b49a743', 'CMF 528', '{\"modXenon\":false,\"engineHealth\":1000.0,\"color2\":111,\"modLivery\":0,\"pearlescentColor\":0,\"modHorns\":-1,\"dirtLevel\":4.0,\"modOrnaments\":-1,\"modPlateHolder\":-1,\"color1\":111,\"modHood\":-1,\"modSideSkirt\":-1,\"modWindows\":-1,\"modAPlate\":-1,\"fuelLevel\":75.4,\"modGrille\":-1,\"modTrimB\":-1,\"modFrame\":-1,\"modShifterLeavers\":-1,\"modEngineBlock\":-1,\"neonColor\":[255,0,255],\"modSpoilers\":-1,\"modFrontBumper\":-1,\"model\":1863871753,\"modAirFilter\":-1,\"modSeats\":-1,\"modTransmission\":-1,\"plate\":\"CMF 528\",\"wheelColor\":156,\"modSpeakers\":-1,\"modDashboard\":-1,\"modAerials\":-1,\"wheels\":1,\"modSuspension\":-1,\"modTank\":-1,\"tyreSmokeColor\":[255,255,255],\"modTurbo\":false,\"plateIndex\":4,\"modRoof\":-1,\"neonEnabled\":[false,false,false,false],\"bodyHealth\":1000.0,\"modArchCover\":-1,\"modBrakes\":-1,\"modFender\":-1,\"modRightFender\":-1,\"modTrimA\":-1,\"modExhaust\":-1,\"modFrontWheels\":-1,\"windowTint\":-1,\"extras\":{\"9\":false,\"8\":false,\"7\":false,\"6\":false,\"5\":true,\"4\":false,\"3\":false,\"2\":false,\"11\":false,\"10\":false,\"1\":true},\"modArmor\":-1,\"modTrunk\":-1,\"modRearBumper\":-1,\"modDoorSpeaker\":-1,\"modDial\":-1,\"modHydrolic\":-1,\"modBackWheels\":-1,\"modVanityPlate\":-1,\"modEngine\":-1,\"modSteeringWheel\":-1,\"modSmokeEnabled\":false,\"modStruts\":-1}', 'car', 'police', 0),
('steam:11000010b49a743', 'DMU 563', '{\"modXenon\":false,\"engineHealth\":1000.0,\"color2\":134,\"modLivery\":3,\"pearlescentColor\":156,\"modHorns\":-1,\"dirtLevel\":0.0,\"modOrnaments\":-1,\"modPlateHolder\":-1,\"color1\":134,\"modHood\":-1,\"modSideSkirt\":-1,\"modWindows\":-1,\"modAPlate\":-1,\"fuelLevel\":87.3,\"modGrille\":-1,\"modTrimB\":-1,\"modFrame\":-1,\"modShifterLeavers\":-1,\"modEngineBlock\":-1,\"neonColor\":[255,0,255],\"modSpoilers\":-1,\"modFrontBumper\":-1,\"model\":1418298348,\"modAirFilter\":-1,\"modSeats\":-1,\"modTransmission\":-1,\"plate\":\"DMU 563\",\"wheelColor\":156,\"modSpeakers\":-1,\"modDashboard\":-1,\"modAerials\":-1,\"wheels\":1,\"modSuspension\":-1,\"modTank\":-1,\"tyreSmokeColor\":[255,255,255],\"modTurbo\":false,\"plateIndex\":4,\"modRoof\":-1,\"neonEnabled\":[false,false,false,false],\"bodyHealth\":1000.0,\"modArchCover\":-1,\"modBrakes\":-1,\"modFender\":-1,\"modRightFender\":-1,\"modTrimA\":-1,\"modExhaust\":-1,\"modFrontWheels\":-1,\"windowTint\":-1,\"extras\":{\"9\":false,\"8\":false,\"7\":false,\"6\":false,\"5\":false,\"4\":true,\"3\":false,\"2\":false,\"1\":false,\"10\":true,\"12\":false,\"11\":false},\"modArmor\":-1,\"modTrunk\":-1,\"modRearBumper\":-1,\"modDoorSpeaker\":-1,\"modDial\":-1,\"modHydrolic\":-1,\"modBackWheels\":-1,\"modVanityPlate\":-1,\"modEngine\":-1,\"modSteeringWheel\":-1,\"modSmokeEnabled\":false,\"modStruts\":-1}', 'car', 'police', 1),
('steam:11000010b49a743', 'DVP 040', '{\"modXenon\":false,\"engineHealth\":1000.0,\"color2\":134,\"modLivery\":2,\"pearlescentColor\":156,\"modHorns\":-1,\"dirtLevel\":0.0,\"modOrnaments\":-1,\"modPlateHolder\":-1,\"color1\":134,\"modHood\":-1,\"modSideSkirt\":-1,\"modWindows\":-1,\"modAPlate\":-1,\"fuelLevel\":77.4,\"modGrille\":-1,\"modTrimB\":-1,\"modFrame\":-1,\"modShifterLeavers\":-1,\"modEngineBlock\":-1,\"neonColor\":[255,0,255],\"modSpoilers\":-1,\"modFrontBumper\":-1,\"model\":476322742,\"modAirFilter\":-1,\"modSeats\":-1,\"modTransmission\":-1,\"plate\":\"DVP 040\",\"wheelColor\":156,\"modSpeakers\":-1,\"modDashboard\":-1,\"modAerials\":-1,\"wheels\":1,\"modSuspension\":-1,\"modTank\":-1,\"tyreSmokeColor\":[255,255,255],\"modTurbo\":false,\"plateIndex\":4,\"modRoof\":-1,\"neonEnabled\":[false,false,false,false],\"bodyHealth\":1000.0,\"modArchCover\":-1,\"modBrakes\":-1,\"modFender\":-1,\"modRightFender\":-1,\"modTrimA\":-1,\"modExhaust\":-1,\"modFrontWheels\":-1,\"windowTint\":-1,\"extras\":{\"2\":true,\"1\":false,\"6\":false,\"5\":false,\"4\":false,\"3\":false},\"modArmor\":-1,\"modTrunk\":-1,\"modRearBumper\":-1,\"modDoorSpeaker\":-1,\"modDial\":-1,\"modHydrolic\":-1,\"modBackWheels\":-1,\"modVanityPlate\":-1,\"modEngine\":-1,\"modSteeringWheel\":-1,\"modSmokeEnabled\":false,\"modStruts\":-1}', 'car', 'police', 1),
('steam:1100001156cfac9', 'EFI 541', '{\"modHydrolic\":-1,\"modWindows\":-1,\"modSideSkirt\":-1,\"modArmor\":-1,\"color1\":12,\"tyreSmokeColor\":[255,255,255],\"modAirFilter\":-1,\"extras\":[],\"model\":-1513691047,\"modFrontWheels\":-1,\"windowTint\":-1,\"neonColor\":[255,0,255],\"modVanityPlate\":-1,\"modSpoilers\":-1,\"color2\":36,\"pearlescentColor\":134,\"modTransmission\":-1,\"modPlateHolder\":-1,\"wheelColor\":156,\"modStruts\":-1,\"engineHealth\":1000.0,\"modTurbo\":false,\"plate\":\"EFI 541\",\"modExhaust\":-1,\"modFrontBumper\":-1,\"modDial\":-1,\"modBackWheels\":-1,\"modDashboard\":-1,\"fuelLevel\":96.2,\"modTank\":-1,\"plateIndex\":4,\"dirtLevel\":15.0,\"modRightFender\":-1,\"modAPlate\":-1,\"modTrimB\":-1,\"modShifterLeavers\":-1,\"modOrnaments\":-1,\"modRoof\":-1,\"wheels\":0,\"modBrakes\":-1,\"bodyHealth\":985.0,\"modSteeringWheel\":-1,\"modArchCover\":-1,\"modSpeakers\":-1,\"modDoorSpeaker\":-1,\"modEngine\":1,\"modRearBumper\":-1,\"modGrille\":-1,\"neonEnabled\":[false,false,false,false],\"modTrunk\":-1,\"modEngineBlock\":-1,\"modAerials\":-1,\"modSmokeEnabled\":false,\"modSeats\":-1,\"modFender\":-1,\"modSuspension\":-1,\"modFrame\":-1,\"modHorns\":-1,\"modLivery\":4,\"modTrimA\":-1,\"modXenon\":1,\"modHood\":-1}', 'car', '', 1),
('steam:1100001139319c1', 'EJG 262', '{\"modEngineBlock\":-1,\"modTransmission\":-1,\"tyreSmokeColor\":[255,255,255],\"color1\":134,\"modHood\":-1,\"modRightFender\":-1,\"modExhaust\":-1,\"windowTint\":-1,\"modTrimA\":-1,\"plate\":\"EJG 262\",\"plateIndex\":4,\"modEngine\":-1,\"model\":1418298348,\"modAerials\":-1,\"modDoorSpeaker\":-1,\"wheels\":1,\"modWindows\":-1,\"modFender\":-1,\"modTrimB\":-1,\"modGrille\":-1,\"modSpoilers\":-1,\"neonEnabled\":[false,false,false,false],\"modSideSkirt\":-1,\"modVanityPlate\":-1,\"modFrontWheels\":-1,\"modSpeakers\":-1,\"modSuspension\":-1,\"modTank\":-1,\"modRoof\":-1,\"modAirFilter\":-1,\"modOrnaments\":-1,\"engineHealth\":1000.0,\"modPlateHolder\":-1,\"modSmokeEnabled\":false,\"modDial\":-1,\"modBackWheels\":-1,\"dirtLevel\":0.0,\"modShifterLeavers\":-1,\"modSteeringWheel\":-1,\"color2\":134,\"modHydrolic\":-1,\"neonColor\":[255,0,255],\"modBrakes\":-1,\"modFrame\":-1,\"pearlescentColor\":156,\"fuelLevel\":91.8,\"extras\":{\"12\":false,\"1\":false,\"2\":false,\"3\":false,\"4\":false,\"5\":true,\"6\":false,\"7\":false,\"8\":false,\"9\":false,\"11\":false,\"10\":true},\"modLivery\":0,\"modXenon\":false,\"modRearBumper\":-1,\"modSeats\":-1,\"modAPlate\":-1,\"modFrontBumper\":-1,\"bodyHealth\":1000.0,\"modTurbo\":false,\"modHorns\":-1,\"modArchCover\":-1,\"wheelColor\":156,\"modDashboard\":-1,\"modStruts\":-1,\"modTrunk\":-1,\"modArmor\":-1}', 'car', 'police', 1),
('steam:110000111c332ed', 'EQX 253', '{\"modXenon\":false,\"fuelLevel\":81.7,\"color2\":0,\"modLivery\":0,\"plate\":\"EQX 253\",\"modHorns\":-1,\"modFender\":-1,\"modSmokeEnabled\":false,\"modPlateHolder\":-1,\"color1\":111,\"modHood\":-1,\"modTurbo\":false,\"modWindows\":-1,\"modAPlate\":-1,\"modBackWheels\":-1,\"extras\":{\"9\":false,\"8\":false,\"7\":false,\"6\":false,\"5\":false,\"4\":false,\"3\":false,\"2\":false,\"1\":false,\"10\":true},\"modRoof\":-1,\"modFrontBumper\":-1,\"modTrimB\":-1,\"modEngineBlock\":-1,\"neonColor\":[255,0,255],\"modStruts\":-1,\"modSteeringWheel\":-1,\"model\":365223774,\"modFrame\":-1,\"modSeats\":-1,\"modTransmission\":-1,\"bodyHealth\":1000.0,\"wheelColor\":156,\"modSpeakers\":-1,\"modTank\":-1,\"modAerials\":-1,\"windowTint\":-1,\"modSuspension\":-1,\"neonEnabled\":[false,false,false,false],\"modAirFilter\":-1,\"engineHealth\":1000.0,\"plateIndex\":4,\"pearlescentColor\":1,\"modRearBumper\":-1,\"wheels\":0,\"modArchCover\":-1,\"modDashboard\":-1,\"modBrakes\":-1,\"modRightFender\":-1,\"tyreSmokeColor\":[255,255,255],\"modExhaust\":-1,\"modFrontWheels\":-1,\"modGrille\":-1,\"modShifterLeavers\":-1,\"modArmor\":-1,\"modTrunk\":-1,\"dirtLevel\":1.0,\"modDoorSpeaker\":-1,\"modDial\":-1,\"modHydrolic\":-1,\"modTrimA\":-1,\"modVanityPlate\":-1,\"modEngine\":-1,\"modSideSkirt\":-1,\"modOrnaments\":-1,\"modSpoilers\":-1}', 'car', 'police', 1),
('steam:11000010b49a743', 'EWR 797', '{\"modXenon\":false,\"engineHealth\":1000.0,\"color2\":134,\"modLivery\":0,\"pearlescentColor\":156,\"modHorns\":-1,\"dirtLevel\":0.0,\"modOrnaments\":-1,\"modPlateHolder\":-1,\"color1\":134,\"modHood\":-1,\"modSideSkirt\":-1,\"modWindows\":-1,\"modAPlate\":-1,\"fuelLevel\":91.6,\"modGrille\":-1,\"modTrimB\":-1,\"modFrame\":-1,\"modShifterLeavers\":-1,\"modEngineBlock\":-1,\"neonColor\":[255,0,255],\"modSpoilers\":-1,\"modFrontBumper\":-1,\"model\":1745872177,\"modAirFilter\":-1,\"modSeats\":-1,\"modTransmission\":-1,\"plate\":\"EWR 797\",\"wheelColor\":156,\"modSpeakers\":-1,\"modDashboard\":-1,\"modAerials\":-1,\"wheels\":1,\"modSuspension\":-1,\"modTank\":-1,\"tyreSmokeColor\":[255,255,255],\"modTurbo\":false,\"plateIndex\":4,\"modRoof\":-1,\"neonEnabled\":[false,false,false,false],\"bodyHealth\":1000.0,\"modArchCover\":-1,\"modBrakes\":-1,\"modFender\":-1,\"modRightFender\":-1,\"modTrimA\":-1,\"modExhaust\":-1,\"modFrontWheels\":-1,\"windowTint\":-1,\"extras\":{\"9\":false,\"8\":false,\"7\":false,\"6\":false,\"5\":false,\"4\":false,\"3\":false,\"2\":true,\"1\":false,\"10\":false,\"12\":true,\"11\":false},\"modArmor\":-1,\"modTrunk\":-1,\"modRearBumper\":-1,\"modDoorSpeaker\":-1,\"modDial\":-1,\"modHydrolic\":-1,\"modBackWheels\":-1,\"modVanityPlate\":-1,\"modEngine\":-1,\"modSteeringWheel\":-1,\"modSmokeEnabled\":false,\"modStruts\":-1}', 'car', 'police', 1),
('steam:11000010b49a743', 'FGD 260', '{\"modXenon\":false,\"engineHealth\":1000.0,\"color2\":134,\"modLivery\":0,\"pearlescentColor\":156,\"modHorns\":-1,\"dirtLevel\":6.0,\"modOrnaments\":-1,\"modPlateHolder\":-1,\"color1\":134,\"modHood\":-1,\"modSideSkirt\":-1,\"modWindows\":-1,\"modAPlate\":-1,\"fuelLevel\":92.7,\"modGrille\":-1,\"modTrimB\":-1,\"modFrame\":-1,\"modShifterLeavers\":-1,\"modEngineBlock\":-1,\"neonColor\":[255,0,255],\"modSpoilers\":-1,\"modFrontBumper\":-1,\"model\":1091240186,\"modAirFilter\":-1,\"modSeats\":-1,\"modTransmission\":-1,\"plate\":\"FGD 260\",\"wheelColor\":156,\"modSpeakers\":-1,\"modDashboard\":-1,\"modAerials\":-1,\"wheels\":1,\"modSuspension\":-1,\"modTank\":-1,\"tyreSmokeColor\":[255,255,255],\"modTurbo\":false,\"plateIndex\":4,\"modRoof\":-1,\"neonEnabled\":[false,false,false,false],\"bodyHealth\":1000.0,\"modArchCover\":-1,\"modBrakes\":-1,\"modFender\":-1,\"modRightFender\":-1,\"modTrimA\":-1,\"modExhaust\":-1,\"modFrontWheels\":-1,\"windowTint\":-1,\"extras\":{\"2\":false,\"1\":false,\"5\":false,\"4\":false,\"3\":true},\"modArmor\":-1,\"modTrunk\":-1,\"modRearBumper\":-1,\"modDoorSpeaker\":-1,\"modDial\":-1,\"modHydrolic\":-1,\"modBackWheels\":-1,\"modVanityPlate\":-1,\"modEngine\":-1,\"modSteeringWheel\":-1,\"modSmokeEnabled\":false,\"modStruts\":-1}', 'car', 'police', 1),
('steam:11000010b49a743', 'FYX 206', '{\"modXenon\":false,\"engineHealth\":1000.0,\"color2\":134,\"modLivery\":0,\"pearlescentColor\":156,\"modHorns\":-1,\"dirtLevel\":0.0,\"modOrnaments\":-1,\"modPlateHolder\":-1,\"color1\":134,\"modHood\":-1,\"modSideSkirt\":-1,\"modWindows\":-1,\"modAPlate\":-1,\"fuelLevel\":84.3,\"modGrille\":-1,\"modTrimB\":-1,\"modFrame\":-1,\"modShifterLeavers\":-1,\"modEngineBlock\":-1,\"neonColor\":[255,0,255],\"modSpoilers\":-1,\"modFrontBumper\":-1,\"model\":443587546,\"modAirFilter\":-1,\"modSeats\":-1,\"modTransmission\":-1,\"plate\":\"FYX 206\",\"wheelColor\":156,\"modSpeakers\":-1,\"modDashboard\":-1,\"modAerials\":-1,\"wheels\":1,\"modSuspension\":-1,\"modTank\":-1,\"tyreSmokeColor\":[255,255,255],\"modTurbo\":false,\"plateIndex\":4,\"modRoof\":-1,\"neonEnabled\":[false,false,false,false],\"bodyHealth\":1000.0,\"modArchCover\":-1,\"modBrakes\":-1,\"modFender\":-1,\"modRightFender\":-1,\"modTrimA\":-1,\"modExhaust\":-1,\"modFrontWheels\":-1,\"windowTint\":-1,\"extras\":{\"9\":false,\"8\":false,\"7\":false,\"6\":false,\"5\":false,\"4\":false,\"3\":true,\"2\":false,\"1\":false,\"10\":false,\"12\":false,\"11\":true},\"modArmor\":-1,\"modTrunk\":-1,\"modRearBumper\":-1,\"modDoorSpeaker\":-1,\"modDial\":-1,\"modHydrolic\":-1,\"modBackWheels\":-1,\"modVanityPlate\":-1,\"modEngine\":-1,\"modSteeringWheel\":-1,\"modSmokeEnabled\":false,\"modStruts\":-1}', 'car', 'police', 1),
('steam:1100001081d1893', 'GHX 382', '{\"modDial\":-1,\"dirtLevel\":8.0,\"modSeats\":-1,\"engineHealth\":1000.0,\"modVanityPlate\":-1,\"modDoorSpeaker\":-1,\"modSuspension\":-1,\"neonEnabled\":[false,false,false,false],\"modTurbo\":false,\"modTrimB\":-1,\"modRearBumper\":-1,\"modDashboard\":-1,\"modShifterLeavers\":-1,\"modFrame\":-1,\"modSpoilers\":-1,\"modRightFender\":-1,\"modXenon\":false,\"modWindows\":-1,\"pearlescentColor\":89,\"modSpeakers\":-1,\"plate\":\"GHX 382\",\"modFrontWheels\":-1,\"modAerials\":-1,\"modOrnaments\":-1,\"plateIndex\":4,\"fuelLevel\":0.0,\"modTrunk\":-1,\"color1\":88,\"neonColor\":[255,0,255],\"modFender\":-1,\"modRoof\":-1,\"wheelColor\":156,\"extras\":[],\"wheels\":6,\"bodyHealth\":1000.0,\"modEngine\":-1,\"model\":1131912276,\"modHorns\":-1,\"modAirFilter\":-1,\"modHood\":-1,\"modAPlate\":-1,\"windowTint\":-1,\"modEngineBlock\":-1,\"modGrille\":-1,\"modTrimA\":-1,\"modBrakes\":-1,\"modTank\":-1,\"color2\":0,\"modArchCover\":-1,\"modHydrolic\":-1,\"modSteeringWheel\":-1,\"modPlateHolder\":-1,\"tyreSmokeColor\":[255,255,255],\"modLivery\":-1,\"modStruts\":-1,\"modBackWheels\":-1,\"modTransmission\":-1,\"modSmokeEnabled\":false,\"modFrontBumper\":-1,\"modExhaust\":-1,\"modArmor\":-1,\"modSideSkirt\":-1}', 'car', '', 1),
('steam:11000010b49a743', 'GNQ 302', '{\"modXenon\":false,\"engineHealth\":1000.0,\"color2\":134,\"modLivery\":1,\"pearlescentColor\":156,\"modHorns\":-1,\"dirtLevel\":0.0,\"modOrnaments\":-1,\"modPlateHolder\":-1,\"color1\":134,\"modHood\":-1,\"modSideSkirt\":-1,\"modWindows\":-1,\"modAPlate\":-1,\"fuelLevel\":78.2,\"modGrille\":-1,\"modTrimB\":-1,\"modFrame\":-1,\"modShifterLeavers\":-1,\"modEngineBlock\":-1,\"neonColor\":[255,0,255],\"modSpoilers\":-1,\"modFrontBumper\":-1,\"model\":-1059115956,\"modAirFilter\":-1,\"modSeats\":-1,\"modTransmission\":-1,\"plate\":\"GNQ 302\",\"wheelColor\":156,\"modSpeakers\":-1,\"modDashboard\":-1,\"modAerials\":-1,\"wheels\":0,\"modSuspension\":-1,\"modTank\":-1,\"tyreSmokeColor\":[255,255,255],\"modTurbo\":false,\"plateIndex\":4,\"modRoof\":-1,\"neonEnabled\":[false,false,false,false],\"bodyHealth\":1000.0,\"modArchCover\":-1,\"modBrakes\":-1,\"modFender\":-1,\"modRightFender\":-1,\"modTrimA\":-1,\"modExhaust\":-1,\"modFrontWheels\":-1,\"windowTint\":-1,\"extras\":{\"9\":false,\"8\":false,\"7\":false,\"6\":false,\"5\":false,\"4\":false,\"3\":true,\"2\":false,\"1\":false,\"10\":false,\"12\":false,\"11\":false},\"modArmor\":-1,\"modTrunk\":-1,\"modRearBumper\":-1,\"modDoorSpeaker\":-1,\"modDial\":-1,\"modHydrolic\":-1,\"modBackWheels\":-1,\"modVanityPlate\":-1,\"modEngine\":-1,\"modSteeringWheel\":-1,\"modSmokeEnabled\":false,\"modStruts\":-1}', 'car', 'police', 1),
('steam:11000010b49a743', 'GQP 909', '{\"modXenon\":false,\"engineHealth\":1000.0,\"color2\":0,\"modLivery\":0,\"pearlescentColor\":1,\"modHorns\":-1,\"dirtLevel\":2.0,\"modOrnaments\":-1,\"modPlateHolder\":-1,\"color1\":111,\"modHood\":-1,\"modSideSkirt\":-1,\"modWindows\":-1,\"modAPlate\":-1,\"fuelLevel\":90.3,\"modGrille\":-1,\"modTrimB\":-1,\"modFrame\":-1,\"modShifterLeavers\":-1,\"modEngineBlock\":-1,\"neonColor\":[255,0,255],\"modSpoilers\":-1,\"modFrontBumper\":-1,\"model\":-1173789994,\"modAirFilter\":-1,\"modSeats\":-1,\"modTransmission\":-1,\"plate\":\"GQP 909\",\"wheelColor\":156,\"modSpeakers\":-1,\"modDashboard\":-1,\"modAerials\":-1,\"wheels\":0,\"modSuspension\":-1,\"modTank\":-1,\"tyreSmokeColor\":[255,255,255],\"modTurbo\":false,\"plateIndex\":4,\"modRoof\":-1,\"neonEnabled\":[false,false,false,false],\"bodyHealth\":1000.0,\"modArchCover\":-1,\"modBrakes\":-1,\"modFender\":-1,\"modRightFender\":-1,\"modTrimA\":-1,\"modExhaust\":-1,\"modFrontWheels\":-1,\"windowTint\":-1,\"extras\":{\"9\":false,\"8\":false,\"7\":false,\"6\":false,\"5\":false,\"4\":false,\"3\":false,\"2\":false,\"1\":false,\"10\":true,\"12\":false,\"11\":false},\"modArmor\":-1,\"modTrunk\":-1,\"modRearBumper\":-1,\"modDoorSpeaker\":-1,\"modDial\":-1,\"modHydrolic\":-1,\"modBackWheels\":-1,\"modVanityPlate\":-1,\"modEngine\":-1,\"modSteeringWheel\":-1,\"modSmokeEnabled\":false,\"modStruts\":-1}', 'car', 'police', 1),
('steam:11000010b49a743', 'GQU 606', '{\"modXenon\":false,\"engineHealth\":1000.0,\"color2\":134,\"modLivery\":0,\"pearlescentColor\":156,\"modHorns\":-1,\"dirtLevel\":8.0,\"modOrnaments\":-1,\"modPlateHolder\":-1,\"color1\":134,\"modHood\":-1,\"modSideSkirt\":-1,\"modWindows\":-1,\"modAPlate\":-1,\"fuelLevel\":77.5,\"modGrille\":-1,\"modTrimB\":-1,\"modFrame\":-1,\"modShifterLeavers\":-1,\"modEngineBlock\":-1,\"neonColor\":[255,0,255],\"modSpoilers\":-1,\"modFrontBumper\":-1,\"model\":1989051918,\"modAirFilter\":-1,\"modSeats\":-1,\"modTransmission\":-1,\"plate\":\"GQU 606\",\"wheelColor\":156,\"modSpeakers\":-1,\"modDashboard\":-1,\"modAerials\":-1,\"wheels\":3,\"modSuspension\":-1,\"modTank\":-1,\"tyreSmokeColor\":[255,255,255],\"modTurbo\":false,\"plateIndex\":4,\"modRoof\":-1,\"neonEnabled\":[false,false,false,false],\"bodyHealth\":1000.0,\"modArchCover\":-1,\"modBrakes\":-1,\"modFender\":-1,\"modRightFender\":-1,\"modTrimA\":-1,\"modExhaust\":-1,\"modFrontWheels\":-1,\"windowTint\":-1,\"extras\":{\"2\":false,\"1\":false,\"5\":true,\"4\":false,\"3\":false},\"modArmor\":-1,\"modTrunk\":-1,\"modRearBumper\":-1,\"modDoorSpeaker\":-1,\"modDial\":-1,\"modHydrolic\":-1,\"modBackWheels\":-1,\"modVanityPlate\":-1,\"modEngine\":-1,\"modSteeringWheel\":-1,\"modSmokeEnabled\":false,\"modStruts\":-1}', 'car', 'police', 1),
('steam:110000111c332ed', 'HEI 456', '{\"modHydrolic\":-1,\"modWindows\":-1,\"modExhaust\":-1,\"modTransmission\":2,\"color1\":0,\"tyreSmokeColor\":[255,255,255],\"modBrakes\":2,\"extras\":[],\"model\":11251904,\"modSpeakers\":-1,\"windowTint\":1,\"modGrille\":-1,\"modVanityPlate\":-1,\"modSpoilers\":-1,\"color2\":132,\"modEngineBlock\":-1,\"modHood\":-1,\"modPlateHolder\":-1,\"wheelColor\":156,\"neonEnabled\":[false,false,false,false],\"engineHealth\":885.2,\"modSeats\":-1,\"plate\":\"HEI 456\",\"modSideSkirt\":-1,\"modDoorSpeaker\":-1,\"modSuspension\":-1,\"modArchCover\":-1,\"modDashboard\":-1,\"fuelLevel\":79.4,\"modTank\":-1,\"modOrnaments\":-1,\"dirtLevel\":0.0,\"modRightFender\":-1,\"modArmor\":4,\"neonColor\":[255,0,255],\"modShifterLeavers\":-1,\"modAPlate\":-1,\"modRoof\":-1,\"wheels\":6,\"modTrimB\":-1,\"bodyHealth\":921.1,\"modSteeringWheel\":-1,\"modAirFilter\":-1,\"modStruts\":-1,\"plateIndex\":0,\"modEngine\":3,\"modRearBumper\":-1,\"modFrontWheels\":-1,\"modLivery\":-1,\"modTrunk\":-1,\"modFrontBumper\":-1,\"modAerials\":-1,\"modTrimA\":-1,\"modBackWheels\":-1,\"modFender\":-1,\"pearlescentColor\":0,\"modFrame\":-1,\"modHorns\":-1,\"modDial\":-1,\"modTurbo\":1,\"modXenon\":1,\"modSmokeEnabled\":false}', 'car', '', 1),
('steam:110000133d93ea2', 'HUM 131', '{\"modHood\":-1,\"modRearBumper\":-1,\"dirtLevel\":0.0,\"fuelLevel\":81.8,\"color1\":134,\"modAPlate\":-1,\"neonEnabled\":[false,false,false,false],\"color2\":134,\"modFrame\":-1,\"modTurbo\":false,\"modEngine\":-1,\"tyreSmokeColor\":[255,255,255],\"modTrimB\":-1,\"modEngineBlock\":-1,\"modSideSkirt\":-1,\"model\":5917646,\"modRoof\":-1,\"modExhaust\":-1,\"modLivery\":1,\"modSuspension\":-1,\"modTrunk\":-1,\"modBackWheels\":-1,\"modPlateHolder\":-1,\"modSmokeEnabled\":false,\"modTrimA\":-1,\"pearlescentColor\":156,\"extras\":{\"1\":false,\"2\":false,\"3\":true,\"4\":false,\"5\":false,\"6\":false,\"7\":false,\"10\":false,\"11\":false},\"modOrnaments\":-1,\"modGrille\":-1,\"modXenon\":false,\"modDoorSpeaker\":-1,\"plateIndex\":4,\"modFrontWheels\":-1,\"modArchCover\":-1,\"modFender\":-1,\"modSteeringWheel\":-1,\"engineHealth\":1000.0,\"bodyHealth\":1000.0,\"modTank\":-1,\"modDial\":-1,\"neonColor\":[255,0,255],\"modVanityPlate\":-1,\"windowTint\":-1,\"modSpoilers\":-1,\"modStruts\":-1,\"wheels\":1,\"modHorns\":-1,\"modAirFilter\":-1,\"modDashboard\":-1,\"modFrontBumper\":-1,\"modWindows\":-1,\"modAerials\":-1,\"modRightFender\":-1,\"modSpeakers\":-1,\"modHydrolic\":-1,\"plate\":\"HUM 131\",\"modBrakes\":-1,\"modShifterLeavers\":-1,\"modArmor\":-1,\"wheelColor\":156,\"modSeats\":-1,\"modTransmission\":-1}', 'car', 'police', 1),
('steam:1100001081d1893', 'HWQ 197', '{\"modDoorSpeaker\":-1,\"modHood\":-1,\"modHorns\":-1,\"modSteeringWheel\":-1,\"modRightFender\":-1,\"modSideSkirt\":-1,\"windowTint\":-1,\"wheelColor\":156,\"engineHealth\":1000.0,\"modArmor\":-1,\"modFender\":-1,\"modEngineBlock\":-1,\"modAPlate\":-1,\"modTank\":-1,\"modPlateHolder\":-1,\"color1\":6,\"modOrnaments\":-1,\"modBackWheels\":-1,\"modAirFilter\":-1,\"modFrame\":-1,\"pearlescentColor\":111,\"modSpeakers\":-1,\"modSuspension\":-1,\"modWindows\":-1,\"neonEnabled\":[false,false,false,false],\"modSmokeEnabled\":false,\"color2\":0,\"dirtLevel\":11.1,\"modDial\":-1,\"modTrimB\":-1,\"plate\":\"HWQ 197\",\"tyreSmokeColor\":[255,255,255],\"neonColor\":[255,0,255],\"modGrille\":-1,\"modVanityPlate\":-1,\"modRoof\":-1,\"modTrunk\":-1,\"modStruts\":-1,\"plateIndex\":0,\"modSeats\":-1,\"modArchCover\":-1,\"modFrontBumper\":-1,\"modBrakes\":-1,\"modSpoilers\":-1,\"model\":-344943009,\"bodyHealth\":1000.0,\"extras\":{\"10\":false,\"12\":true},\"modShifterLeavers\":-1,\"modRearBumper\":-1,\"modFrontWheels\":-1,\"modTurbo\":false,\"modHydrolic\":-1,\"modXenon\":false,\"modEngine\":-1,\"modTrimA\":-1,\"modExhaust\":-1,\"modAerials\":-1,\"fuelLevel\":92.6,\"modTransmission\":-1,\"modDashboard\":-1,\"wheels\":0,\"modLivery\":-1}', 'car', '', 1),
('steam:110000111c332ed', 'HXT 145', '{\"modFrontBumper\":-1,\"modTurbo\":false,\"modHorns\":-1,\"plateIndex\":4,\"modStruts\":-1,\"modSteeringWheel\":-1,\"modSeats\":-1,\"modGrille\":-1,\"tyreSmokeColor\":[255,255,255],\"modDashboard\":-1,\"modOrnaments\":-1,\"modEngine\":3,\"neonColor\":[255,0,255],\"modFender\":-1,\"modExhaust\":-1,\"modTrimB\":-1,\"modXenon\":1,\"neonEnabled\":[false,false,false,false],\"modTransmission\":2,\"modVanityPlate\":-1,\"modFrontWheels\":3,\"color1\":111,\"modBrakes\":2,\"modSideSkirt\":-1,\"pearlescentColor\":1,\"modArchCover\":-1,\"modHood\":-1,\"modAirFilter\":-1,\"modAPlate\":-1,\"modRearBumper\":-1,\"modWindows\":-1,\"wheelColor\":156,\"modTrimA\":-1,\"windowTint\":4,\"modHydrolic\":-1,\"modArmor\":4,\"extras\":{\"2\":false,\"1\":false,\"12\":true,\"3\":false,\"10\":false,\"9\":false,\"4\":false,\"11\":true,\"6\":false,\"5\":false,\"8\":false,\"7\":false},\"modSpeakers\":-1,\"modBackWheels\":-1,\"engineHealth\":1000.0,\"wheels\":1,\"plate\":\"HXT 145\",\"modSmokeEnabled\":false,\"modRoof\":-1,\"modSpoilers\":-1,\"modDoorSpeaker\":-1,\"bodyHealth\":1000.0,\"modDial\":-1,\"modFrame\":-1,\"model\":-1173789994,\"modTank\":-1,\"modTrunk\":-1,\"color2\":0,\"modLivery\":0,\"modEngineBlock\":-1,\"modPlateHolder\":-1,\"modRightFender\":-1,\"dirtLevel\":4.1,\"modShifterLeavers\":-1,\"modSuspension\":3,\"fuelLevel\":91.7,\"modAerials\":-1}', 'car', 'police', 1),
('steam:1100001139319c1', 'ICD 254', '{\"modEngineBlock\":-1,\"modTransmission\":-1,\"tyreSmokeColor\":[255,255,255],\"color1\":134,\"modHood\":-1,\"modRightFender\":-1,\"modExhaust\":-1,\"windowTint\":-1,\"modTrimA\":-1,\"plate\":\"ICD 254\",\"plateIndex\":4,\"modEngine\":-1,\"model\":-1616359240,\"modAerials\":-1,\"modDoorSpeaker\":-1,\"wheels\":1,\"modWindows\":-1,\"modFender\":-1,\"modTrimB\":-1,\"modGrille\":-1,\"modSpoilers\":-1,\"neonEnabled\":[false,false,false,false],\"modSideSkirt\":-1,\"modVanityPlate\":-1,\"modFrontWheels\":-1,\"modSpeakers\":-1,\"modSuspension\":-1,\"modTank\":-1,\"modRoof\":-1,\"modAirFilter\":-1,\"modOrnaments\":-1,\"engineHealth\":1000.0,\"modPlateHolder\":-1,\"modSmokeEnabled\":false,\"modDial\":-1,\"modBackWheels\":-1,\"dirtLevel\":0.0,\"modShifterLeavers\":-1,\"modSteeringWheel\":-1,\"color2\":134,\"modHydrolic\":-1,\"neonColor\":[255,0,255],\"modBrakes\":-1,\"modFrame\":-1,\"pearlescentColor\":156,\"fuelLevel\":79.1,\"extras\":{\"12\":false,\"1\":false,\"2\":false,\"3\":true,\"4\":false,\"5\":false,\"6\":false,\"7\":false,\"8\":false,\"9\":false,\"11\":false,\"10\":true},\"modLivery\":2,\"modXenon\":false,\"modRearBumper\":-1,\"modSeats\":-1,\"modAPlate\":-1,\"modFrontBumper\":-1,\"bodyHealth\":1000.0,\"modTurbo\":false,\"modHorns\":-1,\"modArchCover\":-1,\"wheelColor\":156,\"modDashboard\":-1,\"modStruts\":-1,\"modTrunk\":-1,\"modArmor\":-1}', 'car', 'police', 1),
('steam:11000010b49a743', 'IES 134', '{\"modXenon\":false,\"engineHealth\":1000.0,\"color2\":134,\"modLivery\":0,\"pearlescentColor\":156,\"modHorns\":-1,\"dirtLevel\":0.0,\"modOrnaments\":-1,\"modPlateHolder\":-1,\"color1\":134,\"modHood\":-1,\"modSideSkirt\":-1,\"modWindows\":-1,\"modAPlate\":-1,\"fuelLevel\":95.0,\"modGrille\":-1,\"modTrimB\":-1,\"modFrame\":-1,\"modShifterLeavers\":-1,\"modEngineBlock\":-1,\"neonColor\":[255,0,255],\"modSpoilers\":-1,\"modFrontBumper\":-1,\"model\":598074270,\"modAirFilter\":-1,\"modSeats\":-1,\"modTransmission\":-1,\"plate\":\"IES 134\",\"wheelColor\":156,\"modSpeakers\":-1,\"modDashboard\":-1,\"modAerials\":-1,\"wheels\":1,\"modSuspension\":-1,\"modTank\":-1,\"tyreSmokeColor\":[255,255,255],\"modTurbo\":false,\"plateIndex\":4,\"modRoof\":-1,\"neonEnabled\":[false,false,false,false],\"bodyHealth\":1000.0,\"modArchCover\":-1,\"modBrakes\":-1,\"modFender\":-1,\"modRightFender\":-1,\"modTrimA\":-1,\"modExhaust\":-1,\"modFrontWheels\":-1,\"windowTint\":-1,\"extras\":{\"9\":false,\"8\":false,\"7\":false,\"6\":false,\"5\":false,\"4\":true,\"3\":false,\"2\":false,\"11\":true,\"10\":false,\"1\":false},\"modArmor\":-1,\"modTrunk\":-1,\"modRearBumper\":-1,\"modDoorSpeaker\":-1,\"modDial\":-1,\"modHydrolic\":-1,\"modBackWheels\":-1,\"modVanityPlate\":-1,\"modEngine\":-1,\"modSteeringWheel\":-1,\"modSmokeEnabled\":false,\"modStruts\":-1}', 'car', 'police', 1),
('steam:110000113ec9482', 'IHD 662', '{\"modXenon\":false,\"fuelLevel\":85.2,\"color2\":134,\"modLivery\":0,\"modGrille\":-1,\"modSmokeEnabled\":false,\"modFender\":-1,\"modFrontWheels\":-1,\"modPlateHolder\":-1,\"modHorns\":-1,\"modHood\":-1,\"modTurbo\":false,\"modWindows\":-1,\"color1\":134,\"modSpoilers\":-1,\"modRearBumper\":-1,\"wheels\":1,\"modFrontBumper\":-1,\"plate\":\"IHD 662\",\"modEngineBlock\":-1,\"neonColor\":[255,0,255],\"modStruts\":-1,\"modTank\":-1,\"model\":1745872177,\"modFrame\":-1,\"modSeats\":-1,\"modTransmission\":-1,\"pearlescentColor\":156,\"wheelColor\":156,\"modSpeakers\":-1,\"modDashboard\":-1,\"modAerials\":-1,\"modOrnaments\":-1,\"modSuspension\":-1,\"modBackWheels\":-1,\"modAPlate\":-1,\"modAirFilter\":-1,\"plateIndex\":4,\"modRoof\":-1,\"modTrimB\":-1,\"bodyHealth\":1000.0,\"modArchCover\":-1,\"modBrakes\":-1,\"engineHealth\":1000.0,\"modRightFender\":-1,\"extras\":{\"9\":false,\"8\":false,\"7\":false,\"6\":false,\"5\":false,\"4\":true,\"3\":false,\"2\":false,\"1\":false,\"10\":false,\"12\":true,\"11\":false},\"modExhaust\":-1,\"modSteeringWheel\":-1,\"windowTint\":-1,\"modShifterLeavers\":-1,\"modArmor\":-1,\"modTrunk\":-1,\"dirtLevel\":0.0,\"modDoorSpeaker\":-1,\"modDial\":-1,\"modHydrolic\":-1,\"modTrimA\":-1,\"modVanityPlate\":-1,\"modEngine\":-1,\"neonEnabled\":[false,false,false,false],\"tyreSmokeColor\":[255,255,255],\"modSideSkirt\":-1}', 'car', 'police', 1),
('steam:11000010b49a743', 'IWL 116', '{\"modXenon\":false,\"engineHealth\":1000.0,\"color2\":134,\"modLivery\":3,\"pearlescentColor\":156,\"modHorns\":-1,\"dirtLevel\":0.0,\"modOrnaments\":-1,\"modPlateHolder\":-1,\"color1\":134,\"modHood\":-1,\"modSideSkirt\":-1,\"modWindows\":-1,\"modAPlate\":-1,\"fuelLevel\":90.1,\"modGrille\":-1,\"modTrimB\":-1,\"modFrame\":-1,\"modShifterLeavers\":-1,\"modEngineBlock\":-1,\"neonColor\":[255,0,255],\"modSpoilers\":-1,\"modFrontBumper\":-1,\"model\":1027958099,\"modAirFilter\":-1,\"modSeats\":-1,\"modTransmission\":-1,\"plate\":\"IWL 116\",\"wheelColor\":156,\"modSpeakers\":-1,\"modDashboard\":-1,\"modAerials\":-1,\"wheels\":1,\"modSuspension\":-1,\"modTank\":-1,\"tyreSmokeColor\":[255,255,255],\"modTurbo\":false,\"plateIndex\":4,\"modRoof\":-1,\"neonEnabled\":[false,false,false,false],\"bodyHealth\":1000.0,\"modArchCover\":-1,\"modBrakes\":-1,\"modFender\":-1,\"modRightFender\":-1,\"modTrimA\":-1,\"modExhaust\":-1,\"modFrontWheels\":-1,\"windowTint\":-1,\"extras\":{\"9\":false,\"8\":false,\"7\":false,\"6\":false,\"5\":true,\"4\":false,\"3\":false,\"2\":false,\"1\":false,\"10\":true,\"12\":false,\"11\":false},\"modArmor\":-1,\"modTrunk\":-1,\"modRearBumper\":-1,\"modDoorSpeaker\":-1,\"modDial\":-1,\"modHydrolic\":-1,\"modBackWheels\":-1,\"modVanityPlate\":-1,\"modEngine\":-1,\"modSteeringWheel\":-1,\"modSmokeEnabled\":false,\"modStruts\":-1}', 'car', 'police', 1),
('steam:11000010a01bdb9', 'JCU 911', '{\"modFrontWheels\":-1,\"modShifterLeavers\":-1,\"modBrakes\":-1,\"neonColor\":[255,0,255],\"modTransmission\":-1,\"modHood\":-1,\"modTurbo\":false,\"modTrimB\":-1,\"modXenon\":false,\"plateIndex\":4,\"modArmor\":-1,\"modOrnaments\":-1,\"modRoof\":-1,\"color1\":134,\"modSeats\":-1,\"modHydrolic\":-1,\"tyreSmokeColor\":[255,255,255],\"modVanityPlate\":-1,\"modTank\":-1,\"modWindows\":-1,\"fuelLevel\":77.2,\"model\":1171614426,\"modDial\":-1,\"modHorns\":-1,\"windowTint\":-1,\"modSpeakers\":-1,\"color2\":134,\"modEngine\":-1,\"modRightFender\":-1,\"modSmokeEnabled\":false,\"modTrunk\":-1,\"modExhaust\":-1,\"modSpoilers\":-1,\"modSteeringWheel\":-1,\"modGrille\":-1,\"engineHealth\":1000.0,\"modLivery\":1,\"extras\":{\"1\":false},\"modPlateHolder\":-1,\"neonEnabled\":[false,false,false,false],\"modEngineBlock\":-1,\"modArchCover\":-1,\"modFender\":-1,\"modRearBumper\":-1,\"modAPlate\":-1,\"modAerials\":-1,\"modStruts\":-1,\"modSideSkirt\":-1,\"dirtLevel\":4.0,\"modFrontBumper\":-1,\"modAirFilter\":-1,\"modFrame\":-1,\"modTrimA\":-1,\"plate\":\"JCU 911\",\"pearlescentColor\":156,\"wheelColor\":156,\"modDoorSpeaker\":-1,\"modDashboard\":-1,\"bodyHealth\":1000.0,\"modBackWheels\":-1,\"wheels\":0,\"modSuspension\":-1}', 'car', 'ambulance', 1),
('steam:110000133d93ea2', 'JHE 148', '{\"modOrnaments\":-1,\"modSuspension\":-1,\"modDoorSpeaker\":-1,\"modPlateHolder\":-1,\"engineHealth\":1000.0,\"modSteeringWheel\":-1,\"modHood\":-1,\"modTurbo\":false,\"modBrakes\":-1,\"modRearBumper\":-1,\"wheelColor\":156,\"plate\":\"JHE 148\",\"bodyHealth\":1000.0,\"modTransmission\":-1,\"modTrunk\":-1,\"modFrontBumper\":-1,\"modLivery\":-1,\"modArchCover\":-1,\"dirtLevel\":9.0,\"pearlescentColor\":111,\"modHorns\":-1,\"modDashboard\":-1,\"modFender\":-1,\"modAerials\":-1,\"modSmokeEnabled\":false,\"modSpeakers\":-1,\"modSideSkirt\":-1,\"modTrimA\":-1,\"modWindows\":-1,\"modHydrolic\":-1,\"modBackWheels\":-1,\"color2\":88,\"modArmor\":-1,\"neonEnabled\":[false,false,false,false],\"modSeats\":-1,\"tyreSmokeColor\":[255,255,255],\"wheels\":6,\"modTank\":-1,\"extras\":[],\"modFrame\":-1,\"plateIndex\":0,\"neonColor\":[255,0,255],\"modAPlate\":-1,\"modStruts\":-1,\"windowTint\":-1,\"modXenon\":false,\"color1\":5,\"fuelLevel\":87.7,\"modRoof\":-1,\"modAirFilter\":-1,\"modExhaust\":-1,\"modFrontWheels\":-1,\"model\":-634879114,\"modVanityPlate\":-1,\"modEngineBlock\":-1,\"modGrille\":-1,\"modEngine\":-1,\"modShifterLeavers\":-1,\"modSpoilers\":-1,\"modTrimB\":-1,\"modDial\":-1,\"modRightFender\":-1}', 'car', '', 1),
('steam:110000133d93ea2', 'JLU 868', '{\"modHood\":-1,\"modRearBumper\":-1,\"dirtLevel\":0.0,\"fuelLevel\":94.1,\"color1\":134,\"modAPlate\":-1,\"neonEnabled\":[false,false,false,false],\"color2\":134,\"modFrame\":-1,\"modTurbo\":false,\"modEngine\":-1,\"tyreSmokeColor\":[255,255,255],\"modTrimB\":-1,\"modEngineBlock\":-1,\"modSideSkirt\":-1,\"model\":1027958099,\"modSmokeEnabled\":false,\"modLivery\":2,\"modVanityPlate\":-1,\"modTrunk\":-1,\"modRoof\":-1,\"modBackWheels\":-1,\"modPlateHolder\":-1,\"extras\":{\"1\":false,\"2\":false,\"3\":true,\"4\":false,\"5\":false,\"6\":false,\"7\":false,\"8\":false,\"9\":false,\"10\":false,\"11\":false,\"12\":true},\"modTrimA\":-1,\"pearlescentColor\":156,\"modFrontWheels\":-1,\"modAerials\":-1,\"modGrille\":-1,\"bodyHealth\":1000.0,\"modDoorSpeaker\":-1,\"plateIndex\":4,\"modXenon\":false,\"modArchCover\":-1,\"modFender\":-1,\"modSteeringWheel\":-1,\"engineHealth\":1000.0,\"modArmor\":-1,\"modExhaust\":-1,\"modDial\":-1,\"neonColor\":[255,0,255],\"modTank\":-1,\"plate\":\"JLU 868\",\"modSuspension\":-1,\"modStruts\":-1,\"wheels\":1,\"modHorns\":-1,\"modAirFilter\":-1,\"modSpoilers\":-1,\"modFrontBumper\":-1,\"modWindows\":-1,\"modOrnaments\":-1,\"modRightFender\":-1,\"modSpeakers\":-1,\"modHydrolic\":-1,\"windowTint\":-1,\"modBrakes\":-1,\"modDashboard\":-1,\"modShifterLeavers\":-1,\"wheelColor\":156,\"modSeats\":-1,\"modTransmission\":-1}', 'car', 'police', 1),
('steam:110000133d93ea2', 'JTG 510', '{\"modHood\":-1,\"modRearBumper\":-1,\"dirtLevel\":0.0,\"fuelLevel\":81.8,\"color1\":134,\"modAPlate\":-1,\"neonEnabled\":[false,false,false,false],\"color2\":134,\"modFrame\":-1,\"modTurbo\":false,\"modEngine\":-1,\"tyreSmokeColor\":[255,255,255],\"modTrimB\":-1,\"modEngineBlock\":-1,\"modSideSkirt\":-1,\"model\":5917646,\"modRoof\":-1,\"modExhaust\":-1,\"modLivery\":1,\"modSuspension\":-1,\"modTrunk\":-1,\"modBackWheels\":-1,\"modPlateHolder\":-1,\"modSmokeEnabled\":false,\"modTrimA\":-1,\"pearlescentColor\":156,\"extras\":{\"1\":false,\"2\":false,\"3\":true,\"4\":false,\"5\":false,\"6\":false,\"7\":false,\"10\":false,\"11\":false},\"modOrnaments\":-1,\"modGrille\":-1,\"modXenon\":false,\"modDoorSpeaker\":-1,\"plateIndex\":4,\"modFrontWheels\":-1,\"modArchCover\":-1,\"modFender\":-1,\"modSteeringWheel\":-1,\"engineHealth\":1000.0,\"bodyHealth\":1000.0,\"modTank\":-1,\"modDial\":-1,\"neonColor\":[255,0,255],\"modVanityPlate\":-1,\"windowTint\":-1,\"modSpoilers\":-1,\"modStruts\":-1,\"wheels\":1,\"modHorns\":-1,\"modAirFilter\":-1,\"modDashboard\":-1,\"modFrontBumper\":-1,\"modWindows\":-1,\"modAerials\":-1,\"modRightFender\":-1,\"modSpeakers\":-1,\"modHydrolic\":-1,\"plate\":\"JTG 510\",\"modBrakes\":-1,\"modShifterLeavers\":-1,\"modArmor\":-1,\"wheelColor\":156,\"modSeats\":-1,\"modTransmission\":-1}', 'car', 'police', 1),
('steam:110000113ec9482', 'KPC 906', '{\"modRearBumper\":-1,\"modArmor\":-1,\"windowTint\":-1,\"modLivery\":1,\"fuelLevel\":90.7,\"plate\":\"KPC 906\",\"modSpoilers\":-1,\"color2\":134,\"modDial\":-1,\"tyreSmokeColor\":[255,255,255],\"modSteeringWheel\":-1,\"modVanityPlate\":-1,\"modSeats\":-1,\"modFrontWheels\":-1,\"modDoorSpeaker\":-1,\"modHydrolic\":-1,\"modBrakes\":-1,\"neonColor\":[255,0,255],\"modStruts\":-1,\"color1\":134,\"bodyHealth\":1000.0,\"modTrunk\":-1,\"plateIndex\":4,\"modFrame\":-1,\"modTrimA\":-1,\"pearlescentColor\":156,\"dirtLevel\":0.0,\"modAirFilter\":-1,\"modTurbo\":false,\"modTransmission\":-1,\"modRightFender\":-1,\"modSuspension\":-1,\"modHorns\":-1,\"modPlateHolder\":-1,\"modSpeakers\":-1,\"modSideSkirt\":-1,\"modTrimB\":-1,\"modFrontBumper\":-1,\"modWindows\":-1,\"wheelColor\":156,\"wheels\":1,\"modExhaust\":-1,\"modEngine\":-1,\"engineHealth\":1000.0,\"modGrille\":-1,\"modRoof\":-1,\"modSmokeEnabled\":false,\"extras\":{\"5\":false,\"4\":false,\"3\":false,\"2\":true,\"1\":false},\"modHood\":-1,\"modDashboard\":-1,\"modXenon\":false,\"modFender\":-1,\"neonEnabled\":[false,false,false,false],\"modShifterLeavers\":-1,\"modBackWheels\":-1,\"modOrnaments\":-1,\"model\":1091240186,\"modTank\":-1,\"modArchCover\":-1,\"modEngineBlock\":-1,\"modAPlate\":-1,\"modAerials\":-1}', 'car', 'police', 1),
('steam:11000010b49a743', 'KXO 628', '{\"modXenon\":false,\"engineHealth\":1000.0,\"color2\":134,\"modLivery\":1,\"pearlescentColor\":156,\"modHorns\":-1,\"dirtLevel\":0.0,\"modOrnaments\":-1,\"modPlateHolder\":-1,\"color1\":134,\"modHood\":-1,\"modSideSkirt\":-1,\"modWindows\":-1,\"modAPlate\":-1,\"fuelLevel\":88.4,\"modGrille\":-1,\"modTrimB\":-1,\"modFrame\":-1,\"modShifterLeavers\":-1,\"modEngineBlock\":-1,\"neonColor\":[255,0,255],\"modSpoilers\":-1,\"modFrontBumper\":-1,\"model\":1357997983,\"modAirFilter\":-1,\"modSeats\":-1,\"modTransmission\":-1,\"plate\":\"KXO 628\",\"wheelColor\":156,\"modSpeakers\":-1,\"modDashboard\":-1,\"modAerials\":-1,\"wheels\":1,\"modSuspension\":-1,\"modTank\":-1,\"tyreSmokeColor\":[255,255,255],\"modTurbo\":false,\"plateIndex\":4,\"modRoof\":-1,\"neonEnabled\":[false,false,false,false],\"bodyHealth\":1000.0,\"modArchCover\":-1,\"modBrakes\":-1,\"modFender\":-1,\"modRightFender\":-1,\"modTrimA\":-1,\"modExhaust\":-1,\"modFrontWheels\":-1,\"windowTint\":-1,\"extras\":{\"9\":false,\"8\":false,\"7\":false,\"6\":false,\"5\":false,\"4\":false,\"3\":false,\"2\":false,\"1\":true,\"10\":true,\"12\":false,\"11\":false},\"modArmor\":-1,\"modTrunk\":-1,\"modRearBumper\":-1,\"modDoorSpeaker\":-1,\"modDial\":-1,\"modHydrolic\":-1,\"modBackWheels\":-1,\"modVanityPlate\":-1,\"modEngine\":-1,\"modSteeringWheel\":-1,\"modSmokeEnabled\":false,\"modStruts\":-1}', 'car', 'police', 1),
('steam:110000133d93ea2', 'LAK 226', '{\"modDoorSpeaker\":-1,\"modSeats\":-1,\"modEngine\":-1,\"dirtLevel\":0.0,\"modShifterLeavers\":-1,\"modTrimA\":-1,\"modEngineBlock\":-1,\"modHydrolic\":-1,\"modAPlate\":-1,\"modPlateHolder\":-1,\"windowTint\":-1,\"modAerials\":-1,\"fuelLevel\":81.0,\"modAirFilter\":-1,\"modVanityPlate\":-1,\"modXenon\":false,\"modSpeakers\":-1,\"modSmokeEnabled\":false,\"modArchCover\":-1,\"neonEnabled\":[false,false,false,false],\"modDial\":-1,\"modHorns\":-1,\"modFrame\":-1,\"modArmor\":-1,\"tyreSmokeColor\":[255,255,255],\"wheels\":1,\"modRightFender\":-1,\"modWindows\":-1,\"modBrakes\":-1,\"modOrnaments\":-1,\"modLivery\":0,\"plateIndex\":4,\"modStruts\":-1,\"pearlescentColor\":156,\"extras\":{\"9\":false,\"10\":false,\"7\":false,\"8\":false,\"5\":false,\"6\":true,\"3\":false,\"4\":false,\"1\":false,\"2\":false,\"12\":true,\"11\":false},\"modExhaust\":-1,\"modTransmission\":-1,\"bodyHealth\":1000.0,\"modTank\":-1,\"modHood\":-1,\"modTrunk\":-1,\"modFrontWheels\":-1,\"modGrille\":-1,\"wheelColor\":156,\"neonColor\":[255,0,255],\"modRearBumper\":-1,\"modFrontBumper\":-1,\"modDashboard\":-1,\"model\":1027958099,\"modSuspension\":-1,\"modSideSkirt\":-1,\"modRoof\":-1,\"color2\":134,\"modTurbo\":false,\"color1\":134,\"modBackWheels\":-1,\"plate\":\"LAK 226\",\"engineHealth\":1000.0,\"modTrimB\":-1,\"modSteeringWheel\":-1,\"modFender\":-1,\"modSpoilers\":-1}', 'car', 'police', 1),
('steam:1100001139319c1', 'LFN 404', '{\"modEngineBlock\":-1,\"modTransmission\":-1,\"tyreSmokeColor\":[255,255,255],\"color1\":134,\"modHood\":-1,\"modRightFender\":-1,\"modExhaust\":-1,\"windowTint\":-1,\"modTrimA\":-1,\"plate\":\"LFN 404\",\"plateIndex\":4,\"modEngine\":-1,\"model\":1745872177,\"modAerials\":-1,\"modDoorSpeaker\":-1,\"wheels\":1,\"modWindows\":-1,\"modFender\":-1,\"modTrimB\":-1,\"modGrille\":-1,\"modSpoilers\":-1,\"neonEnabled\":[false,false,false,false],\"modSideSkirt\":-1,\"modVanityPlate\":-1,\"modFrontWheels\":-1,\"modSpeakers\":-1,\"modSuspension\":-1,\"modTank\":-1,\"modRoof\":-1,\"modAirFilter\":-1,\"modOrnaments\":-1,\"engineHealth\":1000.0,\"modPlateHolder\":-1,\"modSmokeEnabled\":false,\"modDial\":-1,\"modBackWheels\":-1,\"dirtLevel\":0.0,\"modShifterLeavers\":-1,\"modSteeringWheel\":-1,\"color2\":134,\"modHydrolic\":-1,\"neonColor\":[255,0,255],\"modBrakes\":-1,\"modFrame\":-1,\"pearlescentColor\":156,\"fuelLevel\":92.3,\"extras\":{\"12\":false,\"1\":false,\"2\":false,\"3\":false,\"4\":false,\"5\":false,\"6\":false,\"7\":true,\"8\":false,\"9\":false,\"11\":false,\"10\":false},\"modLivery\":1,\"modXenon\":false,\"modRearBumper\":-1,\"modSeats\":-1,\"modAPlate\":-1,\"modFrontBumper\":-1,\"bodyHealth\":1000.0,\"modTurbo\":false,\"modHorns\":-1,\"modArchCover\":-1,\"wheelColor\":156,\"modDashboard\":-1,\"modStruts\":-1,\"modTrunk\":-1,\"modArmor\":-1}', 'car', 'police', 1),
('steam:1100001081d1893', 'LIL 631', '{\"pearlescentColor\":156,\"color2\":134,\"modBrakes\":-1,\"modSmokeEnabled\":false,\"modRightFender\":-1,\"engineHealth\":1000.0,\"modFrontWheels\":-1,\"modLivery\":1,\"modHood\":-1,\"extras\":{\"1\":false,\"2\":false,\"7\":true,\"12\":false,\"9\":false,\"10\":false,\"11\":false,\"8\":false,\"5\":false,\"6\":false,\"3\":false,\"4\":false},\"modDoorSpeaker\":-1,\"color1\":134,\"tyreSmokeColor\":[255,255,255],\"modAerials\":-1,\"modSideSkirt\":-1,\"modRoof\":-1,\"modTrimB\":-1,\"modWindows\":-1,\"modTurbo\":false,\"wheels\":0,\"windowTint\":-1,\"modTrunk\":-1,\"modArmor\":-1,\"modTransmission\":-1,\"modSuspension\":-1,\"modPlateHolder\":-1,\"modVanityPlate\":-1,\"modBackWheels\":-1,\"modHorns\":-1,\"fuelLevel\":88.9,\"modEngineBlock\":-1,\"modAPlate\":-1,\"modExhaust\":-1,\"modArchCover\":-1,\"modFrontBumper\":-1,\"modSteeringWheel\":-1,\"neonColor\":[255,0,255],\"modFrame\":-1,\"model\":-1059115956,\"modSpoilers\":-1,\"modSeats\":-1,\"modSpeakers\":-1,\"dirtLevel\":0.0,\"modGrille\":-1,\"modAirFilter\":-1,\"modOrnaments\":-1,\"modXenon\":false,\"neonEnabled\":[false,false,false,false],\"modHydrolic\":-1,\"bodyHealth\":1000.0,\"modEngine\":-1,\"modDashboard\":-1,\"plateIndex\":4,\"modStruts\":-1,\"modTank\":-1,\"plate\":\"LIL 631\",\"modFender\":-1,\"modDial\":-1,\"wheelColor\":156,\"modShifterLeavers\":-1,\"modRearBumper\":-1,\"modTrimA\":-1}', 'car', 'police', 1),
('steam:11000010e2a62e2', 'MBS 016', '{\"modXenon\":false,\"fuelLevel\":77.4,\"color2\":156,\"modLivery\":0,\"modSeats\":-1,\"modHorns\":-1,\"modFender\":-1,\"modFrame\":-1,\"modPlateHolder\":-1,\"color1\":156,\"modHood\":-1,\"modSideSkirt\":-1,\"modWindows\":-1,\"modAPlate\":-1,\"modOrnaments\":-1,\"tyreSmokeColor\":[255,255,255],\"modSpoilers\":-1,\"modFrontBumper\":-1,\"modRearBumper\":-1,\"modEngineBlock\":-1,\"neonColor\":[255,0,255],\"modStruts\":-1,\"extras\":{\"9\":false,\"8\":true,\"7\":true,\"6\":true,\"5\":true,\"4\":true,\"3\":true,\"2\":true,\"11\":true,\"10\":true,\"1\":true},\"model\":-272237023,\"modAirFilter\":-1,\"modBrakes\":-1,\"modTransmission\":-1,\"pearlescentColor\":0,\"wheelColor\":0,\"modSpeakers\":-1,\"modDashboard\":-1,\"modAerials\":-1,\"plate\":\"MBS 016\",\"modSuspension\":-1,\"wheels\":1,\"modGrille\":-1,\"modTurbo\":false,\"plateIndex\":4,\"modRoof\":-1,\"neonEnabled\":[false,false,false,false],\"bodyHealth\":1000.0,\"modArchCover\":-1,\"modTrimA\":-1,\"engineHealth\":1000.0,\"modRightFender\":-1,\"modTank\":-1,\"modExhaust\":-1,\"modFrontWheels\":-1,\"windowTint\":-1,\"modShifterLeavers\":-1,\"modArmor\":-1,\"modTrunk\":-1,\"modSmokeEnabled\":false,\"modDoorSpeaker\":-1,\"modDial\":-1,\"modHydrolic\":-1,\"modBackWheels\":-1,\"modVanityPlate\":-1,\"modEngine\":-1,\"dirtLevel\":3.0,\"modTrimB\":-1,\"modSteeringWheel\":-1}', 'car', 'police', 1),
('steam:11000010b49a743', 'MIK 819', '{\"modXenon\":false,\"engineHealth\":1000.0,\"color2\":0,\"modLivery\":0,\"pearlescentColor\":1,\"modHorns\":-1,\"dirtLevel\":0.0,\"modOrnaments\":-1,\"modPlateHolder\":-1,\"color1\":111,\"modHood\":-1,\"modSideSkirt\":-1,\"modWindows\":-1,\"modAPlate\":-1,\"fuelLevel\":94.4,\"modGrille\":-1,\"modTrimB\":-1,\"modFrame\":-1,\"modShifterLeavers\":-1,\"modEngineBlock\":-1,\"neonColor\":[255,0,255],\"modSpoilers\":-1,\"modFrontBumper\":-1,\"model\":365223774,\"modAirFilter\":-1,\"modSeats\":-1,\"modTransmission\":-1,\"plate\":\"MIK 819\",\"wheelColor\":156,\"modSpeakers\":-1,\"modDashboard\":-1,\"modAerials\":-1,\"wheels\":0,\"modSuspension\":-1,\"modTank\":-1,\"tyreSmokeColor\":[255,255,255],\"modTurbo\":false,\"plateIndex\":4,\"modRoof\":-1,\"neonEnabled\":[false,false,false,false],\"bodyHealth\":1000.0,\"modArchCover\":-1,\"modBrakes\":-1,\"modFender\":-1,\"modRightFender\":-1,\"modTrimA\":-1,\"modExhaust\":-1,\"modFrontWheels\":-1,\"windowTint\":-1,\"extras\":{\"9\":false,\"8\":false,\"7\":false,\"6\":true,\"5\":false,\"4\":false,\"3\":false,\"2\":false,\"1\":false,\"10\":false},\"modArmor\":-1,\"modTrunk\":-1,\"modRearBumper\":-1,\"modDoorSpeaker\":-1,\"modDial\":-1,\"modHydrolic\":-1,\"modBackWheels\":-1,\"modVanityPlate\":-1,\"modEngine\":-1,\"modSteeringWheel\":-1,\"modSmokeEnabled\":false,\"modStruts\":-1}', 'car', 'police', 1);
INSERT INTO `owned_vehicles` (`owner`, `plate`, `vehicle`, `type`, `job`, `stored`) VALUES
('steam:11000010e2a62e2', 'MVP 765', '{\"modSmokeEnabled\":false,\"modFrontBumper\":-1,\"modSpeakers\":-1,\"plateIndex\":4,\"pearlescentColor\":0,\"tyreSmokeColor\":[255,255,255],\"modAPlate\":-1,\"modRearBumper\":-1,\"modBrakes\":-1,\"modDashboard\":-1,\"modEngine\":-1,\"modTrimA\":-1,\"modTrunk\":-1,\"fuelLevel\":86.6,\"modFender\":-1,\"modXenon\":false,\"modSteeringWheel\":-1,\"modWindows\":-1,\"modStruts\":-1,\"modExhaust\":-1,\"engineHealth\":1000.0,\"modAirFilter\":-1,\"modPlateHolder\":-1,\"dirtLevel\":0.0,\"modLivery\":1,\"modHorns\":-1,\"modRightFender\":-1,\"modDial\":-1,\"color1\":156,\"modSeats\":-1,\"modSpoilers\":-1,\"modSuspension\":-1,\"modFrontWheels\":-1,\"modHood\":-1,\"modGrille\":-1,\"wheels\":1,\"extras\":{\"11\":true,\"4\":true,\"1\":true,\"2\":true,\"10\":true,\"9\":false,\"3\":true,\"7\":true,\"8\":true,\"5\":true,\"6\":true},\"modShifterLeavers\":-1,\"modVanityPlate\":-1,\"plate\":\"MVP 765\",\"modTransmission\":-1,\"windowTint\":-1,\"color2\":156,\"modHydrolic\":-1,\"modFrame\":-1,\"modDoorSpeaker\":-1,\"modEngineBlock\":-1,\"modOrnaments\":-1,\"neonColor\":[255,0,255],\"modSideSkirt\":-1,\"modTurbo\":false,\"wheelColor\":0,\"modArchCover\":-1,\"bodyHealth\":1000.0,\"modBackWheels\":-1,\"modTank\":-1,\"modArmor\":-1,\"modRoof\":-1,\"neonEnabled\":[false,false,false,false],\"modTrimB\":-1,\"model\":-272237023,\"modAerials\":-1}', 'car', 'police', 1),
('steam:11000010b49a743', 'MXN 342', '{\"modXenon\":false,\"engineHealth\":1000.0,\"color2\":134,\"modLivery\":0,\"pearlescentColor\":156,\"modHorns\":-1,\"dirtLevel\":0.0,\"modOrnaments\":-1,\"modPlateHolder\":-1,\"color1\":134,\"modHood\":-1,\"modSideSkirt\":-1,\"modWindows\":-1,\"modAPlate\":-1,\"fuelLevel\":95.0,\"modGrille\":-1,\"modTrimB\":-1,\"modFrame\":-1,\"modShifterLeavers\":-1,\"modEngineBlock\":-1,\"neonColor\":[255,0,255],\"modSpoilers\":-1,\"modFrontBumper\":-1,\"model\":598074270,\"modAirFilter\":-1,\"modSeats\":-1,\"modTransmission\":-1,\"plate\":\"MXN 342\",\"wheelColor\":156,\"modSpeakers\":-1,\"modDashboard\":-1,\"modAerials\":-1,\"wheels\":1,\"modSuspension\":-1,\"modTank\":-1,\"tyreSmokeColor\":[255,255,255],\"modTurbo\":false,\"plateIndex\":4,\"modRoof\":-1,\"neonEnabled\":[false,false,false,false],\"bodyHealth\":1000.0,\"modArchCover\":-1,\"modBrakes\":-1,\"modFender\":-1,\"modRightFender\":-1,\"modTrimA\":-1,\"modExhaust\":-1,\"modFrontWheels\":-1,\"windowTint\":-1,\"extras\":{\"9\":false,\"8\":false,\"7\":false,\"6\":false,\"5\":false,\"4\":true,\"3\":false,\"2\":false,\"11\":true,\"10\":false,\"1\":false},\"modArmor\":-1,\"modTrunk\":-1,\"modRearBumper\":-1,\"modDoorSpeaker\":-1,\"modDial\":-1,\"modHydrolic\":-1,\"modBackWheels\":-1,\"modVanityPlate\":-1,\"modEngine\":-1,\"modSteeringWheel\":-1,\"modSmokeEnabled\":false,\"modStruts\":-1}', 'car', 'police', 1),
('steam:1100001081d1893', 'NZT 687', '{\"modXenon\":false,\"engineHealth\":1000.0,\"color2\":156,\"modLivery\":1,\"modGrille\":-1,\"modHorns\":-1,\"modFender\":-1,\"modAirFilter\":-1,\"modPlateHolder\":-1,\"modSmokeEnabled\":false,\"modHood\":-1,\"modSideSkirt\":-1,\"modWindows\":-1,\"color1\":156,\"modSpoilers\":-1,\"modTank\":-1,\"dirtLevel\":2.0,\"modFrame\":-1,\"modAPlate\":-1,\"modEngineBlock\":-1,\"neonColor\":[255,0,255],\"modStruts\":-1,\"modTrimA\":-1,\"model\":-1744779546,\"modTrimB\":-1,\"modSeats\":-1,\"modTransmission\":-1,\"bodyHealth\":1000.0,\"wheelColor\":0,\"modSpeakers\":-1,\"modDashboard\":-1,\"modAerials\":-1,\"plate\":\"NZT 687\",\"modSuspension\":-1,\"tyreSmokeColor\":[255,255,255],\"modFrontWheels\":-1,\"modTurbo\":false,\"plateIndex\":4,\"modRoof\":-1,\"neonEnabled\":[false,false,false,false],\"wheels\":0,\"modArchCover\":-1,\"modRearBumper\":-1,\"modBrakes\":-1,\"modRightFender\":-1,\"modShifterLeavers\":-1,\"modExhaust\":-1,\"modSteeringWheel\":-1,\"windowTint\":-1,\"extras\":{\"2\":false,\"1\":false,\"7\":false,\"6\":false,\"5\":true,\"4\":false,\"3\":false},\"modArmor\":-1,\"modTrunk\":-1,\"fuelLevel\":94.2,\"modDoorSpeaker\":-1,\"modDial\":-1,\"modHydrolic\":-1,\"modBackWheels\":-1,\"modVanityPlate\":-1,\"modEngine\":-1,\"modFrontBumper\":-1,\"modOrnaments\":-1,\"pearlescentColor\":0}', 'car', 'police', 1),
('steam:11000010b49a743', 'PBV 032', '{\"modXenon\":false,\"engineHealth\":1000.0,\"color2\":134,\"modLivery\":0,\"pearlescentColor\":156,\"modHorns\":-1,\"dirtLevel\":6.0,\"modOrnaments\":-1,\"modPlateHolder\":-1,\"color1\":134,\"modHood\":-1,\"modSideSkirt\":-1,\"modWindows\":-1,\"modAPlate\":-1,\"fuelLevel\":92.7,\"modGrille\":-1,\"modTrimB\":-1,\"modFrame\":-1,\"modShifterLeavers\":-1,\"modEngineBlock\":-1,\"neonColor\":[255,0,255],\"modSpoilers\":-1,\"modFrontBumper\":-1,\"model\":1091240186,\"modAirFilter\":-1,\"modSeats\":-1,\"modTransmission\":-1,\"plate\":\"PBV 032\",\"wheelColor\":156,\"modSpeakers\":-1,\"modDashboard\":-1,\"modAerials\":-1,\"wheels\":1,\"modSuspension\":-1,\"modTank\":-1,\"tyreSmokeColor\":[255,255,255],\"modTurbo\":false,\"plateIndex\":4,\"modRoof\":-1,\"neonEnabled\":[false,false,false,false],\"bodyHealth\":1000.0,\"modArchCover\":-1,\"modBrakes\":-1,\"modFender\":-1,\"modRightFender\":-1,\"modTrimA\":-1,\"modExhaust\":-1,\"modFrontWheels\":-1,\"windowTint\":-1,\"extras\":{\"2\":false,\"1\":false,\"5\":false,\"4\":false,\"3\":true},\"modArmor\":-1,\"modTrunk\":-1,\"modRearBumper\":-1,\"modDoorSpeaker\":-1,\"modDial\":-1,\"modHydrolic\":-1,\"modBackWheels\":-1,\"modVanityPlate\":-1,\"modEngine\":-1,\"modSteeringWheel\":-1,\"modSmokeEnabled\":false,\"modStruts\":-1}', 'car', 'police', 1),
('steam:11000010b49a743', 'PSD 704', '{\"modXenon\":false,\"engineHealth\":1000.0,\"color2\":134,\"modLivery\":1,\"pearlescentColor\":156,\"modHorns\":-1,\"dirtLevel\":0.0,\"modOrnaments\":-1,\"modPlateHolder\":-1,\"color1\":134,\"modHood\":-1,\"modSideSkirt\":-1,\"modWindows\":-1,\"modAPlate\":-1,\"fuelLevel\":88.4,\"modGrille\":-1,\"modTrimB\":-1,\"modFrame\":-1,\"modShifterLeavers\":-1,\"modEngineBlock\":-1,\"neonColor\":[255,0,255],\"modSpoilers\":-1,\"modFrontBumper\":-1,\"model\":1357997983,\"modAirFilter\":-1,\"modSeats\":-1,\"modTransmission\":-1,\"plate\":\"PSD 704\",\"wheelColor\":156,\"modSpeakers\":-1,\"modDashboard\":-1,\"modAerials\":-1,\"wheels\":1,\"modSuspension\":-1,\"modTank\":-1,\"tyreSmokeColor\":[255,255,255],\"modTurbo\":false,\"plateIndex\":4,\"modRoof\":-1,\"neonEnabled\":[false,false,false,false],\"bodyHealth\":1000.0,\"modArchCover\":-1,\"modBrakes\":-1,\"modFender\":-1,\"modRightFender\":-1,\"modTrimA\":-1,\"modExhaust\":-1,\"modFrontWheels\":-1,\"windowTint\":-1,\"extras\":{\"9\":false,\"8\":false,\"7\":false,\"6\":false,\"5\":false,\"4\":false,\"3\":false,\"2\":false,\"1\":true,\"10\":true,\"12\":false,\"11\":false},\"modArmor\":-1,\"modTrunk\":-1,\"modRearBumper\":-1,\"modDoorSpeaker\":-1,\"modDial\":-1,\"modHydrolic\":-1,\"modBackWheels\":-1,\"modVanityPlate\":-1,\"modEngine\":-1,\"modSteeringWheel\":-1,\"modSmokeEnabled\":false,\"modStruts\":-1}', 'car', 'police', 1),
('steam:11000010b49a743', 'REE 665', '{\"modXenon\":false,\"engineHealth\":1000.0,\"color2\":0,\"modLivery\":0,\"pearlescentColor\":156,\"modHorns\":-1,\"dirtLevel\":1.0,\"modOrnaments\":-1,\"modPlateHolder\":-1,\"color1\":0,\"modHood\":-1,\"modSideSkirt\":-1,\"modWindows\":-1,\"modAPlate\":-1,\"fuelLevel\":77.8,\"modGrille\":-1,\"modTrimB\":-1,\"modFrame\":-1,\"modShifterLeavers\":-1,\"modEngineBlock\":-1,\"neonColor\":[255,0,255],\"modSpoilers\":-1,\"modFrontBumper\":-1,\"model\":-1371035692,\"modAirFilter\":-1,\"modSeats\":-1,\"modTransmission\":-1,\"plate\":\"REE 665\",\"wheelColor\":156,\"modSpeakers\":-1,\"modDashboard\":-1,\"modAerials\":-1,\"wheels\":0,\"modSuspension\":-1,\"modTank\":-1,\"tyreSmokeColor\":[255,255,255],\"modTurbo\":false,\"plateIndex\":4,\"modRoof\":-1,\"neonEnabled\":[false,false,false,false],\"bodyHealth\":1000.0,\"modArchCover\":-1,\"modBrakes\":-1,\"modFender\":-1,\"modRightFender\":-1,\"modTrimA\":-1,\"modExhaust\":-1,\"modFrontWheels\":-1,\"windowTint\":-1,\"extras\":{\"9\":false,\"8\":false,\"7\":false,\"6\":false,\"5\":false,\"4\":false,\"3\":true,\"2\":false,\"1\":true,\"10\":false,\"12\":false,\"11\":false},\"modArmor\":-1,\"modTrunk\":-1,\"modRearBumper\":-1,\"modDoorSpeaker\":-1,\"modDial\":-1,\"modHydrolic\":-1,\"modBackWheels\":-1,\"modVanityPlate\":-1,\"modEngine\":-1,\"modSteeringWheel\":-1,\"modSmokeEnabled\":false,\"modStruts\":-1}', 'car', 'police', 1),
('steam:1100001139319c1', 'REV 811', '{\"modHood\":-1,\"modRearBumper\":-1,\"dirtLevel\":0.0,\"fuelLevel\":79.0,\"color1\":134,\"modAPlate\":-1,\"neonEnabled\":[false,false,false,false],\"color2\":134,\"modFrame\":-1,\"modTurbo\":false,\"modEngine\":-1,\"tyreSmokeColor\":[255,255,255],\"modTrimB\":-1,\"modEngineBlock\":-1,\"modSideSkirt\":-1,\"modFender\":-1,\"modSmokeEnabled\":false,\"bodyHealth\":1000.0,\"modLivery\":1,\"modSuspension\":-1,\"modTrunk\":-1,\"modBackWheels\":-1,\"modPlateHolder\":-1,\"pearlescentColor\":156,\"modTrimA\":-1,\"modSpoilers\":-1,\"modOrnaments\":-1,\"modAerials\":-1,\"modGrille\":-1,\"extras\":{\"1\":false,\"2\":false,\"3\":true,\"4\":false,\"5\":false},\"model\":1091240186,\"plateIndex\":4,\"modFrontWheels\":-1,\"modArchCover\":-1,\"modXenon\":false,\"modDoorSpeaker\":-1,\"engineHealth\":1000.0,\"modArmor\":-1,\"modSteeringWheel\":-1,\"modDial\":-1,\"neonColor\":[255,0,255],\"windowTint\":-1,\"modExhaust\":-1,\"modTransmission\":-1,\"modStruts\":-1,\"wheels\":1,\"modHorns\":-1,\"modTank\":-1,\"modAirFilter\":-1,\"modDashboard\":-1,\"modWindows\":-1,\"modVanityPlate\":-1,\"modRightFender\":-1,\"modSpeakers\":-1,\"modHydrolic\":-1,\"plate\":\"REV 811\",\"modBrakes\":-1,\"modShifterLeavers\":-1,\"modRoof\":-1,\"wheelColor\":156,\"modSeats\":-1,\"modFrontBumper\":-1}', 'car', 'police', 1),
('steam:11000010e2a62e2', 'RJO 879', '{\"modSmokeEnabled\":false,\"modFrontBumper\":-1,\"modSpeakers\":-1,\"plateIndex\":4,\"pearlescentColor\":1,\"tyreSmokeColor\":[255,255,255],\"modAPlate\":-1,\"modRearBumper\":-1,\"modBrakes\":-1,\"modDashboard\":-1,\"modEngine\":-1,\"modTrimA\":-1,\"modTrunk\":-1,\"fuelLevel\":90.1,\"modFender\":-1,\"modXenon\":false,\"modSteeringWheel\":-1,\"modWindows\":-1,\"modStruts\":-1,\"modExhaust\":-1,\"engineHealth\":1000.0,\"modAirFilter\":-1,\"modPlateHolder\":-1,\"dirtLevel\":3.0,\"modLivery\":1,\"modHorns\":-1,\"modRightFender\":-1,\"modDial\":-1,\"color1\":111,\"modSeats\":-1,\"modSpoilers\":-1,\"modSuspension\":-1,\"modFrontWheels\":-1,\"modHood\":-1,\"modGrille\":-1,\"wheels\":0,\"extras\":{\"11\":true,\"4\":false,\"1\":true,\"2\":false,\"10\":false,\"3\":false,\"9\":false,\"12\":false,\"7\":false,\"8\":false,\"5\":false,\"6\":false},\"modShifterLeavers\":-1,\"modVanityPlate\":-1,\"plate\":\"RJO 879\",\"modTransmission\":-1,\"windowTint\":-1,\"color2\":0,\"modHydrolic\":-1,\"modFrame\":-1,\"modDoorSpeaker\":-1,\"modEngineBlock\":-1,\"modOrnaments\":-1,\"neonColor\":[255,0,255],\"modSideSkirt\":-1,\"modTurbo\":false,\"wheelColor\":156,\"modArchCover\":-1,\"bodyHealth\":1000.0,\"modBackWheels\":-1,\"modTank\":-1,\"modArmor\":-1,\"modRoof\":-1,\"neonEnabled\":[false,false,false,false],\"modTrimB\":-1,\"model\":-1173789994,\"modAerials\":-1}', 'car', 'police', 1),
('steam:110000113ec9482', 'RTJ 281', '{\"modXenon\":false,\"fuelLevel\":75.5,\"color2\":134,\"modLivery\":0,\"modGrille\":-1,\"modSmokeEnabled\":false,\"modFender\":-1,\"modFrontWheels\":-1,\"modPlateHolder\":-1,\"modHorns\":-1,\"modHood\":-1,\"modTurbo\":false,\"modWindows\":-1,\"color1\":134,\"modSpoilers\":-1,\"modRearBumper\":-1,\"wheels\":1,\"modFrontBumper\":-1,\"plate\":\"RTJ 281\",\"modEngineBlock\":-1,\"neonColor\":[255,0,255],\"modStruts\":-1,\"modTank\":-1,\"model\":-1616359240,\"modFrame\":-1,\"modSeats\":-1,\"modTransmission\":-1,\"pearlescentColor\":156,\"wheelColor\":156,\"modSpeakers\":-1,\"modDashboard\":-1,\"modAerials\":-1,\"modOrnaments\":-1,\"modSuspension\":-1,\"modBackWheels\":-1,\"modAPlate\":-1,\"modAirFilter\":-1,\"plateIndex\":4,\"modRoof\":-1,\"modTrimB\":-1,\"bodyHealth\":998.0,\"modArchCover\":-1,\"modBrakes\":-1,\"engineHealth\":1000.0,\"modRightFender\":-1,\"extras\":{\"9\":false,\"8\":false,\"7\":false,\"6\":false,\"5\":false,\"4\":false,\"3\":true,\"2\":false,\"1\":false,\"10\":true,\"12\":false,\"11\":false},\"modExhaust\":-1,\"modSteeringWheel\":-1,\"windowTint\":-1,\"modShifterLeavers\":-1,\"modArmor\":-1,\"modTrunk\":-1,\"dirtLevel\":0.0,\"modDoorSpeaker\":-1,\"modDial\":-1,\"modHydrolic\":-1,\"modTrimA\":-1,\"modVanityPlate\":-1,\"modEngine\":-1,\"neonEnabled\":[false,false,false,false],\"tyreSmokeColor\":[255,255,255],\"modSideSkirt\":-1}', 'car', 'police', 1),
('steam:11000010b49a743', 'TNJ 840', '{\"modXenon\":false,\"engineHealth\":1000.0,\"color2\":156,\"modLivery\":0,\"pearlescentColor\":0,\"modHorns\":-1,\"dirtLevel\":3.0,\"modOrnaments\":-1,\"modPlateHolder\":-1,\"color1\":156,\"modHood\":-1,\"modSideSkirt\":-1,\"modWindows\":-1,\"modAPlate\":-1,\"fuelLevel\":79.8,\"modGrille\":-1,\"modTrimB\":-1,\"modFrame\":-1,\"modShifterLeavers\":-1,\"modEngineBlock\":-1,\"neonColor\":[255,0,255],\"modSpoilers\":-1,\"modFrontBumper\":-1,\"model\":-272237023,\"modAirFilter\":-1,\"modSeats\":-1,\"modTransmission\":-1,\"plate\":\"TNJ 840\",\"wheelColor\":0,\"modSpeakers\":-1,\"modDashboard\":-1,\"modAerials\":-1,\"wheels\":1,\"modSuspension\":-1,\"modTank\":-1,\"tyreSmokeColor\":[255,255,255],\"modTurbo\":false,\"plateIndex\":4,\"modRoof\":-1,\"neonEnabled\":[false,false,false,false],\"bodyHealth\":1000.0,\"modArchCover\":-1,\"modBrakes\":-1,\"modFender\":-1,\"modRightFender\":-1,\"modTrimA\":-1,\"modExhaust\":-1,\"modFrontWheels\":-1,\"windowTint\":-1,\"extras\":{\"9\":false,\"8\":true,\"7\":true,\"6\":true,\"5\":true,\"4\":true,\"3\":true,\"2\":true,\"11\":true,\"10\":true,\"1\":true},\"modArmor\":-1,\"modTrunk\":-1,\"modRearBumper\":-1,\"modDoorSpeaker\":-1,\"modDial\":-1,\"modHydrolic\":-1,\"modBackWheels\":-1,\"modVanityPlate\":-1,\"modEngine\":-1,\"modSteeringWheel\":-1,\"modSmokeEnabled\":false,\"modStruts\":-1}', 'car', 'police', 1),
('steam:1100001081d1893', 'UED 687', '{\"modShifterLeavers\":-1,\"windowTint\":-1,\"plate\":\"UED 687\",\"modFender\":-1,\"modHydrolic\":-1,\"bodyHealth\":1000.0,\"modVanityPlate\":-1,\"neonColor\":[255,0,255],\"modXenon\":false,\"modStruts\":-1,\"wheels\":1,\"modBackWheels\":-1,\"modFrontBumper\":-1,\"modAerials\":-1,\"modRightFender\":-1,\"neonEnabled\":[false,false,false,false],\"modFrame\":-1,\"modRearBumper\":-1,\"modEngineBlock\":-1,\"modDoorSpeaker\":-1,\"modAirFilter\":-1,\"modSmokeEnabled\":false,\"modGrille\":-1,\"dirtLevel\":5.0,\"modDial\":-1,\"modEngine\":-1,\"modSuspension\":-1,\"modExhaust\":-1,\"modTransmission\":-1,\"modTrimA\":-1,\"tyreSmokeColor\":[255,255,255],\"modSteeringWheel\":-1,\"modHood\":-1,\"model\":1027958099,\"modBrakes\":-1,\"extras\":{\"7\":false,\"8\":false,\"9\":false,\"12\":false,\"3\":false,\"4\":false,\"5\":false,\"6\":false,\"1\":false,\"2\":true,\"10\":false,\"11\":true},\"modSeats\":-1,\"modSideSkirt\":-1,\"modSpeakers\":-1,\"modTrunk\":-1,\"modArmor\":-1,\"modRoof\":-1,\"modTurbo\":false,\"color2\":134,\"modPlateHolder\":-1,\"modLivery\":3,\"engineHealth\":1000.0,\"wheelColor\":156,\"modAPlate\":-1,\"modSpoilers\":-1,\"modWindows\":-1,\"plateIndex\":4,\"modTank\":-1,\"modFrontWheels\":-1,\"pearlescentColor\":156,\"modArchCover\":-1,\"color1\":134,\"modHorns\":-1,\"modOrnaments\":-1,\"fuelLevel\":82.2,\"modDashboard\":-1,\"modTrimB\":-1}', 'car', 'police', 1),
('steam:11000010b49a743', 'UVM 860', '{\"modXenon\":false,\"engineHealth\":1000.0,\"color2\":134,\"modLivery\":0,\"pearlescentColor\":156,\"modHorns\":-1,\"dirtLevel\":1.0,\"modOrnaments\":-1,\"modPlateHolder\":-1,\"color1\":134,\"modHood\":-1,\"modSideSkirt\":-1,\"modWindows\":-1,\"modAPlate\":-1,\"fuelLevel\":78.8,\"modGrille\":-1,\"modTrimB\":-1,\"modFrame\":-1,\"modShifterLeavers\":-1,\"modEngineBlock\":-1,\"neonColor\":[255,0,255],\"modSpoilers\":-1,\"modFrontBumper\":-1,\"model\":161178935,\"modAirFilter\":-1,\"modSeats\":-1,\"modTransmission\":-1,\"plate\":\"UVM 860\",\"wheelColor\":156,\"modSpeakers\":-1,\"modDashboard\":-1,\"modAerials\":-1,\"wheels\":1,\"modSuspension\":-1,\"modTank\":-1,\"tyreSmokeColor\":[255,255,255],\"modTurbo\":false,\"plateIndex\":4,\"modRoof\":-1,\"neonEnabled\":[false,false,false,false],\"bodyHealth\":1000.0,\"modArchCover\":-1,\"modBrakes\":-1,\"modFender\":-1,\"modRightFender\":-1,\"modTrimA\":-1,\"modExhaust\":-1,\"modFrontWheels\":-1,\"windowTint\":-1,\"extras\":{\"9\":false,\"8\":false,\"7\":false,\"6\":false,\"5\":false,\"4\":false,\"3\":false,\"2\":false,\"1\":false,\"10\":true,\"12\":false,\"11\":false},\"modArmor\":-1,\"modTrunk\":-1,\"modRearBumper\":-1,\"modDoorSpeaker\":-1,\"modDial\":-1,\"modHydrolic\":-1,\"modBackWheels\":-1,\"modVanityPlate\":-1,\"modEngine\":-1,\"modSteeringWheel\":-1,\"modSmokeEnabled\":false,\"modStruts\":-1}', 'car', 'police', 1),
('steam:110000133d93ea2', 'UVX 186', '{\"modHood\":-1,\"modRearBumper\":-1,\"dirtLevel\":0.0,\"fuelLevel\":94.1,\"color1\":134,\"modAPlate\":-1,\"neonEnabled\":[false,false,false,false],\"color2\":134,\"modFrame\":-1,\"modTurbo\":false,\"modEngine\":-1,\"tyreSmokeColor\":[255,255,255],\"modTrimB\":-1,\"modEngineBlock\":-1,\"modSideSkirt\":-1,\"model\":1027958099,\"modSmokeEnabled\":false,\"modLivery\":2,\"modVanityPlate\":-1,\"modTrunk\":-1,\"modRoof\":-1,\"modBackWheels\":-1,\"modPlateHolder\":-1,\"extras\":{\"1\":false,\"2\":false,\"3\":true,\"4\":false,\"5\":false,\"6\":false,\"7\":false,\"8\":false,\"9\":false,\"10\":false,\"11\":false,\"12\":true},\"modTrimA\":-1,\"pearlescentColor\":156,\"modFrontWheels\":-1,\"modAerials\":-1,\"modGrille\":-1,\"bodyHealth\":1000.0,\"modDoorSpeaker\":-1,\"plateIndex\":4,\"modXenon\":false,\"modArchCover\":-1,\"modFender\":-1,\"modSteeringWheel\":-1,\"engineHealth\":1000.0,\"modArmor\":-1,\"modExhaust\":-1,\"modDial\":-1,\"neonColor\":[255,0,255],\"modTank\":-1,\"plate\":\"UVX 186\",\"modSuspension\":-1,\"modStruts\":-1,\"wheels\":1,\"modHorns\":-1,\"modAirFilter\":-1,\"modSpoilers\":-1,\"modFrontBumper\":-1,\"modWindows\":-1,\"modOrnaments\":-1,\"modRightFender\":-1,\"modSpeakers\":-1,\"modHydrolic\":-1,\"windowTint\":-1,\"modBrakes\":-1,\"modDashboard\":-1,\"modShifterLeavers\":-1,\"wheelColor\":156,\"modSeats\":-1,\"modTransmission\":-1}', 'car', 'police', 1),
('steam:11000010b49a743', 'UWP 006', '{\"modTurbo\":false,\"modSpeakers\":-1,\"bodyHealth\":1000.0,\"modFrontBumper\":-1,\"modLivery\":1,\"modShifterLeavers\":-1,\"modXenon\":false,\"modDashboard\":-1,\"modStruts\":-1,\"fuelLevel\":85.2,\"modHorns\":-1,\"plateIndex\":4,\"modSeats\":-1,\"modHood\":-1,\"modRearBumper\":-1,\"wheels\":1,\"modDial\":-1,\"engineHealth\":1000.0,\"modFrontWheels\":-1,\"neonEnabled\":[false,false,false,false],\"plate\":\"UWP 006\",\"modBrakes\":-1,\"color2\":111,\"modRoof\":-1,\"tyreSmokeColor\":[255,255,255],\"wheelColor\":156,\"modAPlate\":-1,\"modTrimA\":-1,\"modArchCover\":-1,\"color1\":111,\"modRightFender\":-1,\"modSpoilers\":-1,\"modSuspension\":-1,\"modAirFilter\":-1,\"neonColor\":[255,0,255],\"extras\":{\"9\":false,\"8\":false,\"7\":false,\"6\":false,\"1\":true,\"11\":false,\"10\":false,\"5\":true,\"4\":false,\"3\":false,\"2\":false},\"modBackWheels\":-1,\"modOrnaments\":-1,\"modSideSkirt\":-1,\"modWindows\":-1,\"modTransmission\":-1,\"modDoorSpeaker\":-1,\"modAerials\":-1,\"modVanityPlate\":-1,\"modEngineBlock\":-1,\"model\":1863871753,\"modHydrolic\":-1,\"modFender\":-1,\"pearlescentColor\":0,\"modExhaust\":-1,\"modSmokeEnabled\":false,\"dirtLevel\":6.0,\"modGrille\":-1,\"modEngine\":-1,\"modFrame\":-1,\"windowTint\":-1,\"modTrimB\":-1,\"modArmor\":-1,\"modTrunk\":-1,\"modSteeringWheel\":-1,\"modPlateHolder\":-1,\"modTank\":-1}', 'car', 'police', 1),
('steam:110000111c332ed', 'VUT 533', '{\"modXenon\":false,\"fuelLevel\":79.7,\"color2\":156,\"modLivery\":0,\"modShifterLeavers\":-1,\"modSmokeEnabled\":false,\"modFender\":-1,\"modFrame\":-1,\"modPlateHolder\":-1,\"color1\":156,\"modHood\":-1,\"modSideSkirt\":-1,\"modWindows\":-1,\"modSpoilers\":-1,\"modSeats\":-1,\"dirtLevel\":1.0,\"modGrille\":-1,\"modFrontBumper\":-1,\"modFrontWheels\":-1,\"modEngineBlock\":-1,\"neonColor\":[255,0,255],\"modStruts\":-1,\"modAirFilter\":-1,\"model\":-272237023,\"bodyHealth\":1000.0,\"modBrakes\":-1,\"modTransmission\":-1,\"modOrnaments\":-1,\"wheelColor\":0,\"modSpeakers\":-1,\"modDashboard\":-1,\"modAerials\":-1,\"engineHealth\":1000.0,\"modSuspension\":-1,\"modAPlate\":-1,\"modHorns\":-1,\"modTurbo\":false,\"plateIndex\":4,\"pearlescentColor\":0,\"modTrimB\":-1,\"wheels\":1,\"modArchCover\":-1,\"modTank\":-1,\"modTrimA\":-1,\"modRightFender\":-1,\"modRoof\":-1,\"modExhaust\":-1,\"modSteeringWheel\":-1,\"windowTint\":-1,\"extras\":{\"9\":false,\"8\":true,\"7\":true,\"6\":true,\"5\":true,\"4\":true,\"3\":true,\"2\":true,\"1\":true,\"10\":true,\"11\":true},\"modArmor\":-1,\"modTrunk\":-1,\"modRearBumper\":-1,\"modDoorSpeaker\":-1,\"modDial\":-1,\"modHydrolic\":-1,\"modBackWheels\":-1,\"modVanityPlate\":-1,\"modEngine\":-1,\"neonEnabled\":[false,false,false,false],\"plate\":\"VUT 533\",\"tyreSmokeColor\":[255,255,255]}', 'car', 'police', 1),
('steam:11000010b49a743', 'VUW 714', '{\"modXenon\":false,\"engineHealth\":1000.0,\"color2\":134,\"modLivery\":2,\"pearlescentColor\":156,\"modHorns\":-1,\"dirtLevel\":0.0,\"modOrnaments\":-1,\"modPlateHolder\":-1,\"color1\":134,\"modHood\":-1,\"modSideSkirt\":-1,\"modWindows\":-1,\"modAPlate\":-1,\"fuelLevel\":75.2,\"modGrille\":-1,\"modTrimB\":-1,\"modFrame\":-1,\"modShifterLeavers\":-1,\"modEngineBlock\":-1,\"neonColor\":[255,0,255],\"modSpoilers\":-1,\"modFrontBumper\":-1,\"model\":797325625,\"modAirFilter\":-1,\"modSeats\":-1,\"modTransmission\":-1,\"plate\":\"VUW 714\",\"wheelColor\":156,\"modSpeakers\":-1,\"modDashboard\":-1,\"modAerials\":-1,\"wheels\":1,\"modSuspension\":-1,\"modTank\":-1,\"tyreSmokeColor\":[255,255,255],\"modTurbo\":false,\"plateIndex\":4,\"modRoof\":-1,\"neonEnabled\":[false,false,false,false],\"bodyHealth\":1000.0,\"modArchCover\":-1,\"modBrakes\":-1,\"modFender\":-1,\"modRightFender\":-1,\"modTrimA\":-1,\"modExhaust\":-1,\"modFrontWheels\":-1,\"windowTint\":-1,\"extras\":{\"7\":false,\"6\":false,\"5\":true,\"4\":false,\"3\":false,\"2\":false,\"1\":false,\"10\":true,\"12\":false,\"11\":false},\"modArmor\":-1,\"modTrunk\":-1,\"modRearBumper\":-1,\"modDoorSpeaker\":-1,\"modDial\":-1,\"modHydrolic\":-1,\"modBackWheels\":-1,\"modVanityPlate\":-1,\"modEngine\":-1,\"modSteeringWheel\":-1,\"modSmokeEnabled\":false,\"modStruts\":-1}', 'car', 'police', 1),
('steam:1100001139319c1', 'WMI 327', '{\"modSmokeEnabled\":false,\"plateIndex\":4,\"modSpeakers\":-1,\"modFrame\":-1,\"pearlescentColor\":0,\"extras\":{\"3\":false,\"4\":false,\"1\":false,\"2\":true,\"5\":false},\"modAPlate\":-1,\"modAirFilter\":-1,\"modBrakes\":-1,\"modDashboard\":-1,\"modEngine\":-1,\"modTrimA\":-1,\"modShifterLeavers\":-1,\"fuelLevel\":92.7,\"modFender\":-1,\"modXenon\":false,\"modSteeringWheel\":-1,\"modHood\":-1,\"modStruts\":-1,\"modFrontBumper\":-1,\"engineHealth\":1000.0,\"tyreSmokeColor\":[255,255,255],\"modPlateHolder\":-1,\"dirtLevel\":0.0,\"modLivery\":0,\"modHorns\":-1,\"modRightFender\":-1,\"modDial\":-1,\"color1\":156,\"modSeats\":-1,\"modSpoilers\":-1,\"modSuspension\":-1,\"modOrnaments\":-1,\"modFrontWheels\":-1,\"modGrille\":-1,\"wheels\":1,\"modWindows\":-1,\"modVanityPlate\":-1,\"modExhaust\":-1,\"plate\":\"WMI 327\",\"modTransmission\":-1,\"windowTint\":-1,\"color2\":156,\"modTank\":-1,\"modTrimB\":-1,\"modDoorSpeaker\":-1,\"modEngineBlock\":-1,\"modTurbo\":false,\"neonColor\":[255,0,255],\"modSideSkirt\":-1,\"modTrunk\":-1,\"wheelColor\":0,\"modArmor\":-1,\"bodyHealth\":1000.0,\"modBackWheels\":-1,\"modRearBumper\":-1,\"modHydrolic\":-1,\"modRoof\":-1,\"neonEnabled\":[false,false,false,false],\"modArchCover\":-1,\"model\":-1661555510,\"modAerials\":-1}', 'car', 'police', 1),
('steam:110000113ec9482', 'WND 516', '{\"modRearBumper\":-1,\"modArmor\":-1,\"windowTint\":-1,\"modLivery\":0,\"fuelLevel\":89.8,\"plate\":\"WND 516\",\"modSpoilers\":-1,\"color2\":0,\"modDial\":-1,\"tyreSmokeColor\":[255,255,255],\"modSteeringWheel\":-1,\"modVanityPlate\":-1,\"modSeats\":-1,\"modFrontWheels\":-1,\"modDoorSpeaker\":-1,\"modHydrolic\":-1,\"modBrakes\":-1,\"neonColor\":[255,0,255],\"modStruts\":-1,\"color1\":0,\"bodyHealth\":1000.0,\"modTrunk\":-1,\"plateIndex\":4,\"modFrame\":-1,\"modTrimA\":-1,\"pearlescentColor\":156,\"dirtLevel\":0.0,\"modAirFilter\":-1,\"modTurbo\":false,\"modTransmission\":-1,\"modRightFender\":-1,\"modSuspension\":-1,\"modHorns\":-1,\"modPlateHolder\":-1,\"modSpeakers\":-1,\"modSideSkirt\":-1,\"modTrimB\":-1,\"modFrontBumper\":-1,\"modWindows\":-1,\"wheelColor\":156,\"wheels\":0,\"modExhaust\":-1,\"modEngine\":-1,\"engineHealth\":1000.0,\"modGrille\":-1,\"modRoof\":-1,\"modSmokeEnabled\":false,\"extras\":{\"5\":false,\"4\":false,\"3\":false,\"2\":false,\"9\":false,\"8\":false,\"7\":true,\"6\":false,\"1\":false,\"12\":false,\"10\":false,\"11\":false},\"modHood\":-1,\"modDashboard\":-1,\"modXenon\":false,\"modFender\":-1,\"neonEnabled\":[false,false,false,false],\"modShifterLeavers\":-1,\"modBackWheels\":-1,\"modOrnaments\":-1,\"model\":-1371035692,\"modTank\":-1,\"modArchCover\":-1,\"modEngineBlock\":-1,\"modAPlate\":-1,\"modAerials\":-1}', 'car', 'police', 1),
('steam:110000111c332ed', 'WNL 429', '{\"modFrontBumper\":-1,\"modTurbo\":false,\"modHorns\":-1,\"plateIndex\":4,\"modStruts\":-1,\"modSteeringWheel\":-1,\"engineHealth\":995.9,\"modGrille\":-1,\"tyreSmokeColor\":[255,255,255],\"modDashboard\":-1,\"modOrnaments\":-1,\"modEngine\":3,\"modTrunk\":-1,\"modFender\":-1,\"modExhaust\":-1,\"modTrimB\":-1,\"modXenon\":1,\"color1\":141,\"modTransmission\":2,\"modVanityPlate\":-1,\"modFrontWheels\":3,\"modAPlate\":-1,\"modBrakes\":2,\"modSideSkirt\":-1,\"neonColor\":[255,0,255],\"neonEnabled\":[false,false,false,false],\"modHood\":-1,\"modAirFilter\":-1,\"modSmokeEnabled\":false,\"pearlescentColor\":0,\"modWindows\":-1,\"wheelColor\":156,\"modTrimA\":-1,\"modDoorSpeaker\":-1,\"modHydrolic\":-1,\"modArmor\":4,\"extras\":{\"2\":false,\"1\":false,\"4\":false,\"3\":false,\"5\":true},\"modSpeakers\":-1,\"modBackWheels\":-1,\"modArchCover\":-1,\"wheels\":1,\"modAerials\":-1,\"modPlateHolder\":-1,\"modRoof\":-1,\"modSpoilers\":-1,\"plate\":\"WNL 429\",\"bodyHealth\":997.3,\"modDial\":-1,\"modFrame\":-1,\"model\":1989051918,\"modTank\":-1,\"windowTint\":1,\"color2\":134,\"modLivery\":0,\"modEngineBlock\":-1,\"modSeats\":-1,\"modRightFender\":-1,\"dirtLevel\":3.2,\"modShifterLeavers\":-1,\"modSuspension\":-1,\"fuelLevel\":64.0,\"modRearBumper\":-1}', 'car', 'police', 1),
('steam:110000133d93ea2', 'XHN 367', '{\"wheelColor\":156,\"modAerials\":-1,\"modPlateHolder\":-1,\"modFrontBumper\":-1,\"modFender\":-1,\"modTrimA\":-1,\"modAirFilter\":-1,\"bodyHealth\":971.4,\"modTank\":-1,\"modEngine\":-1,\"modSeats\":-1,\"modSideSkirt\":-1,\"plate\":\"XHN 367\",\"modSteeringWheel\":-1,\"modXenon\":false,\"neonEnabled\":[false,false,false,false],\"modSpoilers\":-1,\"modFrame\":-1,\"modSmokeEnabled\":false,\"plateIndex\":1,\"modFrontWheels\":-1,\"modOrnaments\":-1,\"modGrille\":-1,\"model\":-634879114,\"wheels\":6,\"neonColor\":[255,0,255],\"modArmor\":-1,\"modStruts\":-1,\"modVanityPlate\":-1,\"modRightFender\":-1,\"modTransmission\":-1,\"tyreSmokeColor\":[255,255,255],\"modRearBumper\":-1,\"modShifterLeavers\":-1,\"extras\":[],\"modHydrolic\":-1,\"fuelLevel\":82.9,\"modExhaust\":-1,\"modEngineBlock\":-1,\"modDial\":-1,\"modTurbo\":false,\"modArchCover\":-1,\"color1\":143,\"modBackWheels\":-1,\"engineHealth\":969.1,\"windowTint\":-1,\"modLivery\":-1,\"modSpeakers\":-1,\"modRoof\":-1,\"modDashboard\":-1,\"color2\":23,\"modDoorSpeaker\":-1,\"modBrakes\":-1,\"modTrimB\":-1,\"pearlescentColor\":8,\"modHorns\":-1,\"modTrunk\":-1,\"dirtLevel\":8.1,\"modAPlate\":-1,\"modWindows\":-1,\"modSuspension\":-1,\"modHood\":-1}', 'car', '', 1),
('steam:11000010b49a743', 'XYY 238', '{\"modXenon\":false,\"engineHealth\":1000.0,\"color2\":134,\"modLivery\":0,\"pearlescentColor\":156,\"modHorns\":-1,\"dirtLevel\":0.0,\"modOrnaments\":-1,\"modPlateHolder\":-1,\"color1\":134,\"modHood\":-1,\"modSideSkirt\":-1,\"modWindows\":-1,\"modAPlate\":-1,\"fuelLevel\":90.6,\"modGrille\":-1,\"modTrimB\":-1,\"modFrame\":-1,\"modShifterLeavers\":-1,\"modEngineBlock\":-1,\"neonColor\":[255,0,255],\"modSpoilers\":-1,\"modFrontBumper\":-1,\"model\":-1739648114,\"modAirFilter\":-1,\"modSeats\":-1,\"modTransmission\":-1,\"plate\":\"XYY 238\",\"wheelColor\":156,\"modSpeakers\":-1,\"modDashboard\":-1,\"modAerials\":-1,\"wheels\":1,\"modSuspension\":-1,\"modTank\":-1,\"tyreSmokeColor\":[255,255,255],\"modTurbo\":false,\"plateIndex\":4,\"modRoof\":-1,\"neonEnabled\":[false,false,false,false],\"bodyHealth\":1000.0,\"modArchCover\":-1,\"modBrakes\":-1,\"modFender\":-1,\"modRightFender\":-1,\"modTrimA\":-1,\"modExhaust\":-1,\"modFrontWheels\":-1,\"windowTint\":-1,\"extras\":{\"2\":false,\"1\":false,\"6\":false,\"5\":false,\"4\":true,\"3\":false},\"modArmor\":-1,\"modTrunk\":-1,\"modRearBumper\":-1,\"modDoorSpeaker\":-1,\"modDial\":-1,\"modHydrolic\":-1,\"modBackWheels\":-1,\"modVanityPlate\":-1,\"modEngine\":-1,\"modSteeringWheel\":-1,\"modSmokeEnabled\":false,\"modStruts\":-1}', 'car', 'police', 1),
('steam:11000010e2a62e2', 'YAA 564', '{\"modXenon\":false,\"fuelLevel\":92.3,\"color2\":0,\"modLivery\":0,\"modSeats\":-1,\"modHorns\":-1,\"modFender\":-1,\"modFrame\":-1,\"modPlateHolder\":-1,\"color1\":111,\"modHood\":-1,\"modSideSkirt\":-1,\"modWindows\":-1,\"modAPlate\":-1,\"modOrnaments\":-1,\"tyreSmokeColor\":[255,255,255],\"modSpoilers\":-1,\"modFrontBumper\":-1,\"modRearBumper\":-1,\"modEngineBlock\":-1,\"neonColor\":[255,0,255],\"modStruts\":-1,\"extras\":{\"9\":false,\"8\":true,\"7\":false,\"6\":false,\"5\":false,\"4\":false,\"3\":false,\"2\":false,\"1\":false,\"10\":false,\"12\":false,\"11\":true},\"model\":-1173789994,\"modAirFilter\":-1,\"modBrakes\":-1,\"modTransmission\":-1,\"pearlescentColor\":1,\"wheelColor\":156,\"modSpeakers\":-1,\"modDashboard\":-1,\"modAerials\":-1,\"plate\":\"YAA 564\",\"modSuspension\":-1,\"wheels\":0,\"modGrille\":-1,\"modTurbo\":false,\"plateIndex\":4,\"modRoof\":-1,\"neonEnabled\":[false,false,false,false],\"bodyHealth\":1000.0,\"modArchCover\":-1,\"modTrimA\":-1,\"engineHealth\":1000.0,\"modRightFender\":-1,\"modTank\":-1,\"modExhaust\":-1,\"modFrontWheels\":-1,\"windowTint\":-1,\"modShifterLeavers\":-1,\"modArmor\":-1,\"modTrunk\":-1,\"modSmokeEnabled\":false,\"modDoorSpeaker\":-1,\"modDial\":-1,\"modHydrolic\":-1,\"modBackWheels\":-1,\"modVanityPlate\":-1,\"modEngine\":-1,\"dirtLevel\":2.0,\"modTrimB\":-1,\"modSteeringWheel\":-1}', 'car', 'police', 1),
('steam:1100001081d1893', 'YHR 689', '{\"pearlescentColor\":0,\"color2\":156,\"modBrakes\":-1,\"modSmokeEnabled\":false,\"modRightFender\":-1,\"engineHealth\":1000.0,\"modFrontWheels\":-1,\"modLivery\":1,\"modHood\":-1,\"extras\":{\"1\":true,\"2\":true,\"7\":true,\"9\":false,\"10\":true,\"11\":true,\"8\":true,\"5\":true,\"6\":true,\"3\":true,\"4\":true},\"modDoorSpeaker\":-1,\"color1\":156,\"tyreSmokeColor\":[255,255,255],\"modAerials\":-1,\"modSideSkirt\":-1,\"modRoof\":-1,\"modTrimB\":-1,\"modWindows\":-1,\"modTurbo\":false,\"wheels\":1,\"windowTint\":-1,\"modTrunk\":-1,\"modArmor\":-1,\"modTransmission\":-1,\"modSuspension\":-1,\"modPlateHolder\":-1,\"modVanityPlate\":-1,\"modBackWheels\":-1,\"modHorns\":-1,\"fuelLevel\":75.7,\"modEngineBlock\":-1,\"modAPlate\":-1,\"modExhaust\":-1,\"modArchCover\":-1,\"modFrontBumper\":-1,\"modSteeringWheel\":-1,\"neonColor\":[255,0,255],\"modFrame\":-1,\"model\":-272237023,\"modSpoilers\":-1,\"modSeats\":-1,\"modSpeakers\":-1,\"dirtLevel\":0.0,\"modGrille\":-1,\"modAirFilter\":-1,\"modOrnaments\":-1,\"modXenon\":false,\"neonEnabled\":[false,false,false,false],\"modHydrolic\":-1,\"bodyHealth\":1000.0,\"modEngine\":-1,\"modDashboard\":-1,\"plateIndex\":4,\"modStruts\":-1,\"modTank\":-1,\"plate\":\"YHR 689\",\"modFender\":-1,\"modDial\":-1,\"wheelColor\":0,\"modShifterLeavers\":-1,\"modRearBumper\":-1,\"modTrimA\":-1}', 'car', 'police', 1),
('steam:11000010b49a743', 'YRN 866', '{\"modXenon\":false,\"engineHealth\":1000.0,\"color2\":134,\"modLivery\":1,\"pearlescentColor\":156,\"modHorns\":-1,\"dirtLevel\":0.0,\"modOrnaments\":-1,\"modPlateHolder\":-1,\"color1\":134,\"modHood\":-1,\"modSideSkirt\":-1,\"modWindows\":-1,\"modAPlate\":-1,\"fuelLevel\":84.5,\"modGrille\":-1,\"modTrimB\":-1,\"modFrame\":-1,\"modShifterLeavers\":-1,\"modEngineBlock\":-1,\"neonColor\":[255,0,255],\"modSpoilers\":-1,\"modFrontBumper\":-1,\"model\":-1616359240,\"modAirFilter\":-1,\"modSeats\":-1,\"modTransmission\":-1,\"plate\":\"YRN 866\",\"wheelColor\":156,\"modSpeakers\":-1,\"modDashboard\":-1,\"modAerials\":-1,\"wheels\":1,\"modSuspension\":-1,\"modTank\":-1,\"tyreSmokeColor\":[255,255,255],\"modTurbo\":false,\"plateIndex\":4,\"modRoof\":-1,\"neonEnabled\":[false,false,false,false],\"bodyHealth\":1000.0,\"modArchCover\":-1,\"modBrakes\":-1,\"modFender\":-1,\"modRightFender\":-1,\"modTrimA\":-1,\"modExhaust\":-1,\"modFrontWheels\":-1,\"windowTint\":-1,\"extras\":{\"9\":false,\"8\":true,\"7\":true,\"6\":false,\"5\":false,\"4\":false,\"3\":false,\"2\":false,\"1\":false,\"10\":false,\"12\":true,\"11\":false},\"modArmor\":-1,\"modTrunk\":-1,\"modRearBumper\":-1,\"modDoorSpeaker\":-1,\"modDial\":-1,\"modHydrolic\":-1,\"modBackWheels\":-1,\"modVanityPlate\":-1,\"modEngine\":-1,\"modSteeringWheel\":-1,\"modSmokeEnabled\":false,\"modStruts\":-1}', 'car', 'police', 1),
('steam:1100001139319c1', 'YVR 134', '{\"modSmokeEnabled\":false,\"plateIndex\":4,\"modSpeakers\":-1,\"modFrame\":-1,\"pearlescentColor\":156,\"extras\":{\"3\":false,\"4\":false,\"1\":false,\"2\":false,\"5\":false},\"modAPlate\":-1,\"modAirFilter\":-1,\"modBrakes\":-1,\"modDashboard\":-1,\"modEngine\":-1,\"modTrimA\":-1,\"modShifterLeavers\":-1,\"fuelLevel\":84.4,\"modFender\":-1,\"modXenon\":false,\"modSteeringWheel\":-1,\"modHood\":-1,\"modStruts\":-1,\"modFrontBumper\":-1,\"engineHealth\":1000.0,\"tyreSmokeColor\":[255,255,255],\"modPlateHolder\":-1,\"dirtLevel\":4.0,\"modLivery\":0,\"modHorns\":-1,\"modRightFender\":-1,\"modDial\":-1,\"color1\":134,\"modSeats\":-1,\"modSpoilers\":-1,\"modSuspension\":-1,\"modOrnaments\":-1,\"modFrontWheels\":-1,\"modGrille\":-1,\"wheels\":3,\"modWindows\":-1,\"modVanityPlate\":-1,\"modExhaust\":-1,\"plate\":\"YVR 134\",\"modTransmission\":-1,\"windowTint\":-1,\"color2\":134,\"modTank\":-1,\"modTrimB\":-1,\"modDoorSpeaker\":-1,\"modEngineBlock\":-1,\"modTurbo\":false,\"neonColor\":[255,0,255],\"modSideSkirt\":-1,\"modTrunk\":-1,\"wheelColor\":156,\"modArmor\":-1,\"bodyHealth\":1000.0,\"modBackWheels\":-1,\"modRearBumper\":-1,\"modHydrolic\":-1,\"modRoof\":-1,\"neonEnabled\":[false,false,false,false],\"modArchCover\":-1,\"model\":1989051918,\"modAerials\":-1}', 'car', 'police', 1),
('steam:1100001081d1893', 'ZEG 942', '{\"modRearBumper\":-1,\"modArmor\":-1,\"neonColor\":[255,0,255],\"modLivery\":2,\"engineHealth\":1000.0,\"modSteeringWheel\":-1,\"modSpoilers\":-1,\"color2\":134,\"modDial\":-1,\"modXenon\":false,\"modWindows\":-1,\"modVanityPlate\":-1,\"modSeats\":-1,\"modFrontWheels\":-1,\"modDoorSpeaker\":-1,\"modHydrolic\":-1,\"tyreSmokeColor\":[255,255,255],\"modPlateHolder\":-1,\"plate\":\"ZEG 942\",\"modHorns\":-1,\"modTrunk\":-1,\"modSmokeEnabled\":false,\"plateIndex\":4,\"modEngine\":-1,\"modAirFilter\":-1,\"pearlescentColor\":156,\"dirtLevel\":0.0,\"modTurbo\":false,\"modFender\":-1,\"modTransmission\":-1,\"modRightFender\":-1,\"bodyHealth\":1000.0,\"modBrakes\":-1,\"modSpeakers\":-1,\"modDashboard\":-1,\"modSideSkirt\":-1,\"modTrimB\":-1,\"modFrontBumper\":-1,\"modExhaust\":-1,\"wheelColor\":156,\"wheels\":1,\"modSuspension\":-1,\"windowTint\":-1,\"modTrimA\":-1,\"modGrille\":-1,\"fuelLevel\":81.6,\"modFrame\":-1,\"extras\":{\"5\":false,\"4\":false,\"3\":false,\"2\":true,\"9\":false,\"8\":false,\"7\":false,\"6\":false,\"1\":false,\"10\":false,\"11\":false},\"modHood\":-1,\"color1\":134,\"modStruts\":-1,\"modRoof\":-1,\"neonEnabled\":[false,false,false,false],\"modShifterLeavers\":-1,\"modBackWheels\":-1,\"modOrnaments\":-1,\"model\":598074270,\"modTank\":-1,\"modArchCover\":-1,\"modEngineBlock\":-1,\"modAPlate\":-1,\"modAerials\":-1}', 'car', 'police', 1),
('steam:1100001139319c1', 'ZIR 437', '{\"color2\":134,\"modFender\":-1,\"plate\":\"ZIR 437\",\"modArchCover\":-1,\"modHood\":-1,\"modDoorSpeaker\":-1,\"neonColor\":[255,0,255],\"modEngineBlock\":-1,\"bodyHealth\":1000.0,\"modHydrolic\":-1,\"modTank\":-1,\"modOrnaments\":-1,\"modSpoilers\":-1,\"modAerials\":-1,\"modFrontWheels\":-1,\"modAirFilter\":-1,\"modTrunk\":-1,\"neonEnabled\":[false,false,false,false],\"modRearBumper\":-1,\"tyreSmokeColor\":[255,255,255],\"modSeats\":-1,\"modGrille\":-1,\"model\":-1059115956,\"modWindows\":-1,\"modBackWheels\":-1,\"modFrontBumper\":-1,\"wheelColor\":156,\"modTransmission\":-1,\"modFrame\":-1,\"modVanityPlate\":-1,\"modStruts\":-1,\"modExhaust\":-1,\"plateIndex\":4,\"modTrimB\":-1,\"pearlescentColor\":156,\"color1\":134,\"modTrimA\":-1,\"modHorns\":-1,\"modRoof\":-1,\"modAPlate\":-1,\"modXenon\":false,\"wheels\":0,\"modSmokeEnabled\":false,\"modShifterLeavers\":-1,\"modTurbo\":false,\"fuelLevel\":94.7,\"modSpeakers\":-1,\"modBrakes\":-1,\"windowTint\":-1,\"engineHealth\":1000.0,\"dirtLevel\":2.0,\"modSuspension\":-1,\"modArmor\":-1,\"modPlateHolder\":-1,\"modLivery\":0,\"modSideSkirt\":-1,\"modDial\":-1,\"modSteeringWheel\":-1,\"modDashboard\":-1,\"modEngine\":-1,\"extras\":{\"8\":false,\"9\":false,\"12\":false,\"11\":false,\"10\":false,\"1\":false,\"2\":true,\"3\":false,\"4\":false,\"5\":false,\"6\":false,\"7\":false},\"modRightFender\":-1}', 'car', 'police', 1),
('steam:11000010b49a743', 'ZIZ 845', '{\"modXenon\":false,\"engineHealth\":1000.0,\"color2\":156,\"modLivery\":0,\"pearlescentColor\":0,\"modHorns\":-1,\"dirtLevel\":1.0,\"modOrnaments\":-1,\"modPlateHolder\":-1,\"color1\":156,\"modHood\":-1,\"modSideSkirt\":-1,\"modWindows\":-1,\"modAPlate\":-1,\"fuelLevel\":86.3,\"modGrille\":-1,\"modTrimB\":-1,\"modFrame\":-1,\"modShifterLeavers\":-1,\"modEngineBlock\":-1,\"neonColor\":[255,0,255],\"modSpoilers\":-1,\"modFrontBumper\":-1,\"model\":515485475,\"modAirFilter\":-1,\"modSeats\":-1,\"modTransmission\":-1,\"plate\":\"ZIZ 845\",\"wheelColor\":0,\"modSpeakers\":-1,\"modDashboard\":-1,\"modAerials\":-1,\"wheels\":1,\"modSuspension\":-1,\"modTank\":-1,\"tyreSmokeColor\":[255,255,255],\"modTurbo\":false,\"plateIndex\":4,\"modRoof\":-1,\"neonEnabled\":[false,false,false,false],\"bodyHealth\":1000.0,\"modArchCover\":-1,\"modBrakes\":-1,\"modFender\":-1,\"modRightFender\":-1,\"modTrimA\":-1,\"modExhaust\":-1,\"modFrontWheels\":-1,\"windowTint\":-1,\"extras\":{\"9\":false,\"8\":true,\"7\":true,\"6\":true,\"5\":true,\"4\":true,\"3\":true,\"2\":true,\"1\":true},\"modArmor\":-1,\"modTrunk\":-1,\"modRearBumper\":-1,\"modDoorSpeaker\":-1,\"modDial\":-1,\"modHydrolic\":-1,\"modBackWheels\":-1,\"modVanityPlate\":-1,\"modEngine\":-1,\"modSteeringWheel\":-1,\"modSmokeEnabled\":false,\"modStruts\":-1}', 'car', 'police', 1),
('steam:110000113ec9482', 'ZTH 450', '{\"color2\":134,\"modFender\":-1,\"plate\":\"ZTH 450\",\"modArchCover\":-1,\"modHood\":-1,\"modDoorSpeaker\":-1,\"neonColor\":[255,0,255],\"modEngineBlock\":-1,\"bodyHealth\":1000.0,\"modPlateHolder\":-1,\"model\":-1059115956,\"modOrnaments\":-1,\"modTransmission\":-1,\"modAerials\":-1,\"modTurbo\":false,\"engineHealth\":1000.0,\"modTrunk\":-1,\"neonEnabled\":[false,false,false,false],\"modRearBumper\":-1,\"tyreSmokeColor\":[255,255,255],\"modSeats\":-1,\"modDashboard\":-1,\"modAirFilter\":-1,\"modSpoilers\":-1,\"modBackWheels\":-1,\"modFrontBumper\":-1,\"wheelColor\":156,\"fuelLevel\":81.8,\"modFrame\":-1,\"modVanityPlate\":-1,\"modStruts\":-1,\"modExhaust\":-1,\"plateIndex\":4,\"modTrimB\":-1,\"pearlescentColor\":156,\"color1\":134,\"modTrimA\":-1,\"modTank\":-1,\"modRoof\":-1,\"modArmor\":-1,\"modXenon\":false,\"wheels\":0,\"modSmokeEnabled\":false,\"modDial\":-1,\"modWindows\":-1,\"modHydrolic\":-1,\"modSpeakers\":-1,\"modEngine\":-1,\"windowTint\":-1,\"modHorns\":-1,\"dirtLevel\":3.0,\"modSuspension\":-1,\"modShifterLeavers\":-1,\"modFrontWheels\":-1,\"modLivery\":0,\"modSideSkirt\":-1,\"modBrakes\":-1,\"modSteeringWheel\":-1,\"modAPlate\":-1,\"modGrille\":-1,\"extras\":{\"8\":true,\"9\":false,\"12\":false,\"11\":false,\"10\":false,\"1\":false,\"2\":false,\"3\":false,\"4\":false,\"5\":false,\"6\":false,\"7\":true},\"modRightFender\":-1}', 'car', 'police', 1);

-- --------------------------------------------------------

--
-- Table structure for table `phone_app_chat`
--

CREATE TABLE `phone_app_chat` (
  `id` int(11) NOT NULL,
  `channel` varchar(20) NOT NULL,
  `message` varchar(255) NOT NULL,
  `time` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `phone_app_chat`
--

INSERT INTO `phone_app_chat` (`id`, `channel`, `message`, `time`) VALUES
(28, 'drugs', 'Test', '2020-03-14 18:45:59'),
(29, 'drugs', 'Hi anyone looking for coke', '2020-03-14 18:46:27'),
(30, 'killkillian', 'yesssssss', '2020-03-14 18:47:23'),
(31, 'killkillian', 'I agree', '2020-03-14 18:47:34'),
(32, 'killkillian', 'When can we set this up?', '2020-03-14 18:47:58'),
(33, 'killkillian', 'like now', '2020-03-14 18:48:07');

-- --------------------------------------------------------

--
-- Table structure for table `phone_calls`
--

CREATE TABLE `phone_calls` (
  `id` int(11) NOT NULL,
  `owner` varchar(10) NOT NULL COMMENT 'Name',
  `num` varchar(10) NOT NULL COMMENT 'Phone Number',
  `incoming` int(11) NOT NULL COMMENT 'Define Incoming Call',
  `time` timestamp NOT NULL DEFAULT current_timestamp(),
  `accepts` int(11) NOT NULL COMMENT 'Accept Call'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `phone_calls`
--

INSERT INTO `phone_calls` (`id`, `owner`, `num`, `incoming`, `time`, `accepts`) VALUES
(122, '827-5849', '1130914', 1, '2020-02-06 04:46:45', 0),
(125, '827-5849', '1130914', 1, '2020-02-06 04:49:29', 0),
(127, '113-0914', '827-5849', 1, '2020-02-06 04:53:06', 1),
(128, '827-5849', '113-0914', 0, '2020-02-06 04:53:06', 1),
(129, '827-5849', '113-0914', 1, '2020-02-06 04:54:49', 0),
(130, '113-0914', '827-5849', 0, '2020-02-06 04:54:49', 0),
(131, '440-5316', '7711741', 1, '2020-02-08 02:23:58', 0),
(132, '104-0089', '827-5849', 0, '2020-02-08 02:24:39', 1),
(133, '827-5849', '104-0089', 1, '2020-02-08 02:24:39', 1),
(134, '104-0089', '827-5849', 1, '2020-02-08 03:55:28', 1),
(135, '827-5849', '104-0089', 0, '2020-02-08 03:55:28', 1),
(136, '104-0089', '827-5849', 1, '2020-02-08 04:39:40', 1),
(137, '827-5849', '104-0089', 0, '2020-02-08 04:39:40', 1),
(138, '104-0089', '827-5849', 1, '2020-02-08 04:41:08', 1),
(139, '827-5849', '104-0089', 0, '2020-02-08 04:41:08', 1),
(140, '104-0089', '827-5849', 1, '2020-02-08 04:52:23', 1),
(141, '827-5849', '104-0089', 0, '2020-02-08 04:52:23', 1),
(142, '104-0089', '827-5849', 1, '2020-02-08 04:54:47', 1),
(143, '827-5849', '104-0089', 0, '2020-02-08 04:54:47', 1),
(144, '827-5849', '104-0089', 0, '2020-02-08 04:55:01', 1),
(145, '104-0089', '827-5849', 1, '2020-02-08 04:55:01', 1),
(146, '104-0089', '827-5849', 1, '2020-02-08 04:56:51', 1),
(147, '827-5849', '104-0089', 0, '2020-02-08 04:56:51', 1),
(148, '104-0089', '827-5849', 1, '2020-02-08 04:59:32', 0),
(149, '827-5849', '104-0089', 0, '2020-02-08 04:59:32', 0),
(150, '104-0089', '827-5849', 1, '2020-02-08 04:59:44', 1),
(151, '827-5849', '104-0089', 0, '2020-02-08 04:59:44', 1),
(152, '104-0089', '827-5849', 1, '2020-02-08 05:02:04', 0),
(153, '827-5849', '104-0089', 0, '2020-02-08 05:02:04', 0),
(154, '104-0089', '827-5849', 0, '2020-02-08 05:05:00', 1),
(155, '827-5849', '104-0089', 1, '2020-02-08 05:05:00', 1),
(156, '104-0089', '827-5849', 1, '2020-02-08 05:06:01', 1),
(157, '827-5849', '104-0089', 0, '2020-02-08 05:06:01', 1),
(158, '104-0089', '827-5849', 1, '2020-02-08 05:06:17', 1),
(159, '827-5849', '104-0089', 0, '2020-02-08 05:06:17', 1),
(160, '822-5198', '911', 1, '2020-02-08 05:24:12', 0),
(161, '822-5198', '911', 1, '2020-02-08 05:26:49', 0),
(162, '827-5849', '104-0089', 1, '2020-02-10 00:07:02', 0),
(163, '104-0089', '827-5849', 0, '2020-02-10 00:07:02', 0),
(164, '104-0089', '753-9558', 1, '2020-02-10 01:24:50', 0),
(165, '753-9558', '104-0089', 0, '2020-02-10 01:24:50', 0),
(166, '753-9558', '104-0089', 1, '2020-02-10 01:25:05', 1),
(167, '104-0089', '753-9558', 0, '2020-02-10 01:25:05', 1),
(168, '113-0914', '121-3758', 1, '2020-02-10 01:49:20', 1),
(169, '121-3758', '113-0914', 0, '2020-02-10 01:49:20', 1),
(170, '104-0089', '753-9558', 1, '2020-02-10 02:32:56', 1),
(171, '753-9558', '104-0089', 0, '2020-02-10 02:32:56', 1),
(172, '753-9558', '104-0089', 1, '2020-02-10 02:40:02', 1),
(173, '104-0089', '753-9558', 0, '2020-02-10 02:40:02', 1),
(174, '753-9558', '104-0089', 1, '2020-02-10 02:42:31', 1),
(175, '104-0089', '753-9558', 0, '2020-02-10 02:42:31', 1),
(176, '440-5316', '771-1741', 1, '2020-02-23 03:43:38', 0),
(177, '771-1741', '440-5316', 0, '2020-02-23 03:43:38', 0),
(178, '440-5316', '771-1741', 1, '2020-02-23 03:44:12', 0),
(179, '771-1741', '440-5316', 0, '2020-02-23 03:44:12', 0),
(180, '531-3502', '873-7104', 1, '2020-03-14 18:42:20', 1),
(182, '228-4620', '8599967', 1, '2020-03-16 02:50:57', 0),
(183, '873-7104', '531-3502', 1, '2020-03-16 03:20:54', 0),
(184, '531-3502', '873-7104', 0, '2020-03-16 03:20:54', 0),
(185, '873-7104', '531-3502', 1, '2020-03-16 03:34:45', 0),
(186, '531-3502', '873-7104', 0, '2020-03-16 03:34:45', 0),
(187, '873-7104', '531-3502', 0, '2020-03-16 03:35:01', 1),
(188, '531-3502', '873-7104', 1, '2020-03-16 03:35:01', 1),
(189, '722-8375', '911', 1, '2020-03-23 00:47:31', 0),
(190, '288-7796', '911', 1, '2020-03-25 02:49:31', 0),
(191, '104-0089', '753-9558', 1, '2020-03-25 03:38:21', 0),
(192, '427-1473', '1130914', 1, '2020-03-26 01:50:58', 0),
(193, '427-1473', '113-0914', 1, '2020-03-26 01:53:01', 0),
(194, '113-0914', '427-1473', 0, '2020-03-26 01:53:01', 0);

-- --------------------------------------------------------

--
-- Table structure for table `phone_messages`
--

CREATE TABLE `phone_messages` (
  `id` int(11) NOT NULL,
  `transmitter` varchar(10) NOT NULL,
  `receiver` varchar(10) NOT NULL,
  `message` varchar(255) NOT NULL DEFAULT '0',
  `time` timestamp NOT NULL DEFAULT current_timestamp(),
  `isRead` int(11) NOT NULL DEFAULT 0,
  `owner` int(11) NOT NULL DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `phone_messages`
--

INSERT INTO `phone_messages` (`id`, `transmitter`, `receiver`, `message`, `time`, `isRead`, `owner`) VALUES
(184, 'ambulance', '859-9967', 'Alert (906-8228): Medical attention required: unconscious citizen!', '2020-03-16 01:51:40', 1, 0),
(185, 'ambulance', '859-9967', 'GPS: 2630.1691894531, 3691.6782226563', '2020-03-16 01:51:40', 1, 0),
(186, 'ambulance', '859-9967', 'Alert (906-8228): Medical attention required: unconscious citizen!', '2020-03-16 01:56:48', 1, 0),
(187, 'ambulance', '859-9967', 'GPS: 2630.1677246094, 3691.6760253906', '2020-03-16 01:56:48', 1, 0),
(301, '113-0914', '427-1473', 'yo where are u', '2020-03-26 01:51:39', 1, 1),
(188, 'mechanic', '873-7104', 'Alert (873-7104): Test', '2020-03-16 02:01:46', 1, 0),
(189, 'mechanic', '873-7104', 'GPS: 1969.6055908203, 3715.1791992188', '2020-03-16 02:01:46', 1, 0),
(190, 'mechanic', '873-7104', 'Alert (531-3502): Need a tow for 2 burnt vehicles if possible. sandy gas station next to air port', '2020-03-16 02:04:32', 1, 0),
(191, 'mechanic', '873-7104', 'GPS: 1773.1000976563, 3354.5939941406', '2020-03-16 02:04:32', 1, 0),
(192, 'ambulance', '859-9967', 'Alert (873-7104): Medical attention required: unconscious citizen!', '2020-03-16 02:15:35', 1, 0),
(193, 'ambulance', '859-9967', 'GPS: 1973.9881591797, 3713.5246582031', '2020-03-16 02:15:35', 1, 0),
(194, 'ambulance', '859-9967', 'Alert (873-7104): Medical attention required: unconscious citizen!', '2020-03-16 02:21:29', 1, 0),
(195, 'ambulance', '859-9967', 'GPS: 1973.9876708984, 3713.5219726563', '2020-03-16 02:21:29', 1, 0),
(196, 'ambulance', '859-9967', 'Alert (873-7104): Medical attention required: unconscious citizen!', '2020-03-16 03:00:56', 1, 0),
(197, 'ambulance', '859-9967', 'GPS: 1857.5554199219, 3675.9580078125', '2020-03-16 03:00:56', 1, 0),
(198, 'ambulance', '859-9967', 'Alert (906-8228): Medical attention required: unconscious citizen!', '2020-03-16 03:01:24', 1, 0),
(199, 'ambulance', '859-9967', 'GPS: 1857.4631347656, 3676.5405273438', '2020-03-16 03:01:24', 1, 0),
(200, 'ambulance', '859-9967', 'Alert (121-3758): Medical attention required: unconscious citizen!', '2020-03-16 03:03:37', 1, 0),
(201, 'ambulance', '859-9967', 'GPS: 1864.1466064453, 3690.080078125', '2020-03-16 03:03:37', 1, 0),
(202, 'ambulance', '859-9967', 'Alert (121-3758): Medical attention required: unconscious citizen!', '2020-03-16 03:08:44', 1, 0),
(203, 'ambulance', '859-9967', 'GPS: 1839.4696044922, 3700.0751953125', '2020-03-16 03:08:44', 1, 0),
(204, '873-7104', '531-3502', 'I put peace time in effect, since they want shootouts every 5 seconds', '2020-03-16 03:21:19', 1, 0),
(205, '531-3502', '873-7104', 'I put peace time in effect, since they want shootouts every 5 seconds', '2020-03-16 03:21:19', 1, 1),
(206, '531-3502', '873-7104', 'i have let the radio peps know.', '2020-03-16 03:22:14', 1, 0),
(207, '873-7104', '531-3502', 'i have let the radio peps know.', '2020-03-16 03:22:15', 1, 1),
(208, '531-3502', '873-7104', 'can i drop you to support?', '2020-03-16 03:22:22', 1, 0),
(209, '873-7104', '531-3502', 'can i drop you to support?', '2020-03-16 03:22:22', 1, 1),
(210, '873-7104', '531-3502', 'Yes', '2020-03-16 03:22:27', 1, 0),
(211, '531-3502', '873-7104', 'Yes', '2020-03-16 03:22:27', 1, 1),
(212, 'police', '906-8228', 'Alert (228-4620): Chrome Karin Sultan Speeding Down Joshua Headed Towards Panarama ', '2020-03-18 01:31:52', 0, 0),
(213, 'police', '906-8228', 'GPS: 1399.3961181641, 3518.8703613281', '2020-03-18 01:31:52', 0, 0),
(214, 'police', '873-7104', 'Alert (228-4620): Chrome Karin Sultan Speeding Down Joshua Headed Towards Panarama ', '2020-03-18 01:31:52', 1, 0),
(215, 'police', '873-7104', 'GPS: 1399.3961181641, 3518.8703613281', '2020-03-18 01:31:52', 1, 0),
(300, '427-1473', '113-0914', 'yo where are u', '2020-03-26 01:51:39', 0, 0),
(216, 'police', '873-7104', 'Alert (531-3502): naked man and woman around race track.', '2020-03-18 02:31:31', 1, 0),
(217, 'police', '228-4620', 'Alert (531-3502): naked man and woman around race track.', '2020-03-18 02:31:31', 1, 0),
(218, 'police', '228-4620', 'GPS: 1162.1770019531, 316.05249023438', '2020-03-18 02:31:31', 1, 0),
(219, 'police', '873-7104', 'GPS: 1162.1770019531, 316.05249023438', '2020-03-18 02:31:31', 1, 0),
(220, 'police', '150-0087', 'Alert (531-3502): two individuals having too much of a good time, and need to be escorted out of yellow jacket.', '2020-03-18 03:00:02', 0, 0),
(221, 'police', '228-4620', 'GPS: 1991.4073486328, 3047.8264160156', '2020-03-18 03:00:02', 1, 0),
(222, 'police', '150-0087', 'GPS: 1991.4073486328, 3047.8264160156', '2020-03-18 03:00:02', 0, 0),
(223, 'police', '228-4620', 'Alert (531-3502): two individuals having too much of a good time, and need to be escorted out of yellow jacket.', '2020-03-18 03:00:02', 1, 0),
(224, 'police', '150-0087', 'Alert (873-7104): My son was just hit by a vehicle come quick', '2020-03-18 03:24:34', 0, 0),
(225, 'police', '150-0087', 'GPS: 1900.6015625, 3772.8037109375', '2020-03-18 03:24:34', 0, 0),
(226, 'police', '228-4620', 'Alert (873-7104): My son was just hit by a vehicle come quick', '2020-03-18 03:24:34', 1, 0),
(227, 'police', '228-4620', 'GPS: 1900.6015625, 3772.8037109375', '2020-03-18 03:24:34', 1, 0),
(228, 'police', '150-0087', 'GPS: -338.92028808594, 6067.0317382813', '2020-03-19 01:08:35', 0, 0),
(229, 'police', '150-0087', 'Alert (859-9967): OMG There is  Officer shot at 1036 is is bleeding out please come i am a off duty ems', '2020-03-19 01:08:35', 0, 0),
(230, 'police', '150-0087', 'GPS: 2209.6096191406, 3253.4418945313', '2020-03-19 01:59:59', 0, 0),
(231, 'police', '150-0087', 'Alert (228-4620): there is a drunk man in a fathom FQ2 and he is on smoke tree road ', '2020-03-19 01:59:59', 0, 0),
(232, 'ambulance', '859-9967', 'Alert (531-3502): Medical attention required: unconscious citizen!', '2020-03-20 00:45:41', 0, 0),
(233, 'ambulance', '859-9967', 'GPS: 228.96144104004, -1637.2814941406', '2020-03-20 00:45:41', 0, 0),
(234, 'ambulance', '873-7104', 'Alert (531-3502): Medical attention required: unconscious citizen!', '2020-03-20 00:45:41', 0, 0),
(235, 'ambulance', '873-7104', 'GPS: 228.96144104004, -1637.2814941406', '2020-03-20 00:45:41', 0, 0),
(236, 'mechanic', '722-8375', 'Alert (531-3502): need a tow for PD', '2020-03-20 02:14:50', 1, 0),
(237, 'mechanic', '722-8375', 'GPS: -1274.0433349609, -703.03558349609', '2020-03-20 02:14:50', 1, 0),
(238, '14', '150-0087', 'yoo', '2020-03-20 02:37:11', 1, 1),
(239, 'police', '150-0087', 'GPS: -133.93397521973, 6213.267578125', '2020-03-21 00:33:25', 0, 0),
(240, 'police', '150-0087', 'Alert (228-4620): Help! I just saw a motorcylist crash on the great oceacn highway near 1027 send officers!', '2020-03-21 00:33:25', 0, 0),
(241, 'police', '121-3758', 'Alert (228-4620): Help! I just saw a motorcylist crash on the great oceacn highway near 1027 send officers!', '2020-03-21 00:33:25', 1, 0),
(242, 'police', '121-3758', 'GPS: -133.93397521973, 6213.267578125', '2020-03-21 00:33:25', 1, 0),
(243, 'police', '121-3758', 'Alert (228-4620): I need police to 1060 Paleto Blvd this guy is vadalising my store please send officers!', '2020-03-21 00:52:22', 1, 0),
(244, 'police', '121-3758', 'GPS: 5.2939047813416, 6510.8461914063', '2020-03-21 00:52:22', 1, 0),
(245, 'police', '150-0087', 'Alert (228-4620): I need police to 1060 Paleto Blvd this guy is vadalising my store please send officers!', '2020-03-21 00:52:22', 0, 0),
(246, 'police', '150-0087', 'GPS: 5.2939047813416, 6510.8461914063', '2020-03-21 00:52:22', 0, 0),
(247, '555-235-85', '859-9967', 'lawyer', '2020-03-21 01:07:40', 1, 1),
(248, '555-235-85', '859-9967', 'i need you help', '2020-03-21 01:08:31', 1, 1),
(249, '555-235-85', '859-9967', 'i have been aressted', '2020-03-21 01:08:42', 1, 1),
(250, 'taxi', '873-7104', 'Alert (531-3502): I need a ride from the vanilla unicorn. ', '2020-03-21 01:13:11', 1, 0),
(251, 'taxi', '873-7104', 'GPS: 128.78703308105, -1304.1221923828', '2020-03-21 01:13:11', 1, 0),
(252, 'mechanic', '722-8375', 'hay tow in need what is you location', '2020-03-21 01:29:40', 1, 1),
(253, 'police', '531-3502', 'Alert (228-4620): Help I have no brakes I am in a black granger on joshua road headed towards sandy from 68 send officers Please!', '2020-03-21 01:37:29', 0, 0),
(254, 'police', '531-3502', 'GPS: 746.79248046875, 2530.8830566406', '2020-03-21 01:37:29', 0, 0),
(255, 'police', '121-3758', 'Alert (228-4620): Help I have no brakes I am in a black granger on joshua road headed towards sandy from 68 send officers Please!', '2020-03-21 01:37:29', 1, 0),
(256, 'police', '121-3758', 'GPS: 746.79248046875, 2530.8830566406', '2020-03-21 01:37:29', 1, 0),
(257, 'police', '873-7104', 'Alert (228-4620): Help I have no brakes I am in a black granger on joshua road headed towards sandy from 68 send officers Please!', '2020-03-21 01:37:29', 1, 0),
(258, 'police', '873-7104', 'GPS: 746.79248046875, 2530.8830566406', '2020-03-21 01:37:29', 1, 0),
(259, 'police', '150-0087', 'Alert (228-4620): Help I have no brakes I am in a black granger on joshua road headed towards sandy from 68 send officers Please!', '2020-03-21 01:37:29', 0, 0),
(260, 'police', '150-0087', 'GPS: 746.79248046875, 2530.8830566406', '2020-03-21 01:37:29', 0, 0),
(261, 'police', '121-3758', 'Alert (859-9967): my friends black granger isnt stoping', '2020-03-21 01:37:31', 1, 0),
(262, 'police', '121-3758', 'GPS: 766.95941162109, 2497.7058105469', '2020-03-21 01:37:31', 1, 0),
(263, 'police', '531-3502', 'Alert (859-9967): my friends black granger isnt stoping', '2020-03-21 01:37:31', 0, 0),
(264, 'police', '531-3502', 'GPS: 766.95941162109, 2497.7058105469', '2020-03-21 01:37:31', 0, 0),
(265, 'police', '873-7104', 'Alert (859-9967): my friends black granger isnt stoping', '2020-03-21 01:37:31', 1, 0),
(266, 'police', '873-7104', 'GPS: 766.95941162109, 2497.7058105469', '2020-03-21 01:37:31', 1, 0),
(267, 'police', '150-0087', 'Alert (859-9967): my friends black granger isnt stoping', '2020-03-21 01:37:31', 0, 0),
(268, 'police', '150-0087', 'GPS: 766.95941162109, 2497.7058105469', '2020-03-21 01:37:31', 0, 0),
(269, 'police', '873-7104', 'Alert (859-9967): help i crashed my big red truck on panorama dr please send help', '2020-03-21 01:46:46', 0, 0),
(270, 'police', '873-7104', 'GPS: 1623.6856689453, 3662.9099121094', '2020-03-21 01:46:46', 0, 0),
(271, 'police', '121-3758', 'Alert (859-9967): help i crashed my big red truck on panorama dr please send help', '2020-03-21 01:46:46', 1, 0),
(272, 'police', '121-3758', 'GPS: 1623.6856689453, 3662.9099121094', '2020-03-21 01:46:46', 1, 0),
(273, 'police', '531-3502', 'Alert (859-9967): help i crashed my big red truck on panorama dr please send help', '2020-03-21 01:46:46', 0, 0),
(274, 'police', '531-3502', 'GPS: 1623.6856689453, 3662.9099121094', '2020-03-21 01:46:46', 0, 0),
(275, 'police', '150-0087', 'Alert (859-9967): help i crashed my big red truck on panorama dr please send help', '2020-03-21 01:46:46', 0, 0),
(276, 'police', '150-0087', 'GPS: 1623.6856689453, 3662.9099121094', '2020-03-21 01:46:46', 0, 0),
(277, 'police', '531-3502', 'Alert (228-4620): There is someone losing blood at the Sandy shores police station they are passed out and missing an eyeball send officers and EMS!', '2020-03-21 02:06:39', 0, 0),
(278, 'police', '531-3502', 'GPS: 1828.6112060547, 3653.357421875', '2020-03-21 02:06:39', 0, 0),
(279, 'police', '873-7104', 'Alert (228-4620): There is someone losing blood at the Sandy shores police station they are passed out and missing an eyeball send officers and EMS!', '2020-03-21 02:06:39', 0, 0),
(280, 'police', '121-3758', 'GPS: 1828.6112060547, 3653.357421875', '2020-03-21 02:06:39', 1, 0),
(281, 'police', '121-3758', 'Alert (228-4620): There is someone losing blood at the Sandy shores police station they are passed out and missing an eyeball send officers and EMS!', '2020-03-21 02:06:39', 1, 0),
(282, 'police', '873-7104', 'GPS: 1828.6112060547, 3653.357421875', '2020-03-21 02:06:39', 0, 0),
(283, 'police', '150-0087', 'GPS: 1828.6112060547, 3653.357421875', '2020-03-21 02:06:39', 0, 0),
(284, 'police', '150-0087', 'Alert (228-4620): There is someone losing blood at the Sandy shores police station they are passed out and missing an eyeball send officers and EMS!', '2020-03-21 02:06:39', 0, 0),
(285, '555-235-85', '859-9967', 'hey', '2020-03-21 02:26:17', 1, 1),
(286, 'police', '121-3758', 'Alert (859-9967): i crashed my big truck into a pull and i need help t is on 1056 on paleto blvd', '2020-03-23 00:46:16', 1, 0),
(287, 'police', '121-3758', 'GPS: -78.986602783203, 6443.9321289063', '2020-03-23 00:46:16', 1, 0),
(288, '7228375', '859-9967', 'hey', '2020-03-23 00:54:32', 1, 1),
(289, 'police', '121-3758', 'Alert (859-9967): there is some a guy at 1028 unloading boxs to a aboaned place', '2020-03-23 01:15:58', 1, 0),
(290, 'police', '121-3758', 'GPS: -172.28961181641, 6260.4291992188', '2020-03-23 01:15:58', 1, 0),
(291, '7228375', '859-9967', 'do you want to join the mafia', '2020-03-23 01:20:58', 1, 1),
(292, 'police', '771-1741', 'Alert (562-3039): there is a sherriff just sitting at 3025 with his lights on', '2020-03-25 01:39:02', 1, 0),
(293, 'police', '531-3502', 'Alert (562-3039): there is a sherriff just sitting at 3025 with his lights on', '2020-03-25 01:39:02', 0, 0),
(294, 'police', '873-7104', 'Alert (562-3039): there is a sherriff just sitting at 3025 with his lights on', '2020-03-25 01:39:02', 0, 0),
(295, 'police', '531-3502', 'GPS: 1423.7905273438, 3625.091796875', '2020-03-25 01:39:02', 0, 0),
(296, 'police', '873-7104', 'GPS: 1423.7905273438, 3625.091796875', '2020-03-25 01:39:02', 0, 0),
(297, 'police', '827-5849', 'Alert (562-3039): there is a sherriff just sitting at 3025 with his lights on', '2020-03-25 01:39:02', 1, 0),
(298, 'police', '827-5849', 'GPS: 1423.7905273438, 3625.091796875', '2020-03-25 01:39:02', 1, 0),
(299, 'police', '771-1741', 'GPS: 1423.7905273438, 3625.091796875', '2020-03-25 01:39:02', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `phone_users_contacts`
--

CREATE TABLE `phone_users_contacts` (
  `id` int(11) NOT NULL,
  `identifier` varchar(60) CHARACTER SET utf8mb4 DEFAULT NULL,
  `number` varchar(10) CHARACTER SET utf8mb4 DEFAULT NULL,
  `display` varchar(64) CHARACTER SET utf8mb4 NOT NULL DEFAULT '-1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `phone_users_contacts`
--

INSERT INTO `phone_users_contacts` (`id`, `identifier`, `number`, `display`) VALUES
(7, 'steam:110000111d0b1aa', '113-0914', 'Darryl'),
(9, 'steam:110000111c332ed', '827-5849', 'Melody'),
(22, 'steam:110000111c332ed', '113-0914', 'Killian'),
(11, 'steam:110000105ed368b', '531-3502', 'AC'),
(13, 'steam:110000105ed368b', '440-5316', 'Sen Corporal Miek'),
(14, 'steam:110000111d0b1aa', '104-0089', 'Jessi'),
(15, 'steam:1100001183c7077', '827-5849', 'Mel'),
(16, 'steam:1100001139319c1', '771-1741', 'Shane SASP'),
(17, 'steam:1100001183c7077', '753-9558', 'Jones'),
(18, 'steam:110000111c332ed', '121-3758', 'metalhead'),
(19, 'steam:11000011531f6b5', '2887796', 'Doe aka T Callahan'),
(20, 'steam:1100001081d1893', '873-7104', 'Sticky'),
(21, 'steam:11000010a01bdb9', '531-3502', 'TwinkToes');

-- --------------------------------------------------------

--
-- Table structure for table `properties`
--

CREATE TABLE `properties` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `label` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `entering` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `exit` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `inside` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `outside` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `ipls` varchar(255) COLLATE utf8mb4_bin DEFAULT '[]',
  `gateway` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `is_single` int(11) DEFAULT NULL,
  `is_room` int(11) DEFAULT NULL,
  `is_gateway` int(11) DEFAULT NULL,
  `room_menu` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `price` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `properties`
--

INSERT INTO `properties` (`id`, `name`, `label`, `entering`, `exit`, `inside`, `outside`, `ipls`, `gateway`, `is_single`, `is_room`, `is_gateway`, `room_menu`, `price`) VALUES
(1, 'WhispymoundDrive', '2677 Whispymound Drive', '{\"y\":564.89,\"z\":182.959,\"x\":119.384}', '{\"x\":117.347,\"y\":559.506,\"z\":183.304}', '{\"y\":557.032,\"z\":183.301,\"x\":118.037}', '{\"y\":567.798,\"z\":182.131,\"x\":119.249}', '[]', NULL, 1, 1, 0, '{\"x\":118.748,\"y\":566.573,\"z\":175.697}', 1500000),
(2, 'NorthConkerAvenue2045', '2045 North Conker Avenue', '{\"x\":372.796,\"y\":428.327,\"z\":144.685}', '{\"x\":373.548,\"y\":422.982,\"z\":144.907},', '{\"y\":420.075,\"z\":145.904,\"x\":372.161}', '{\"x\":372.454,\"y\":432.886,\"z\":143.443}', '[]', NULL, 1, 1, 0, '{\"x\":377.349,\"y\":429.422,\"z\":137.3}', 1500000),
(3, 'RichardMajesticApt2', 'Richard Majestic, Apt 2', '{\"y\":-379.165,\"z\":37.961,\"x\":-936.363}', '{\"y\":-365.476,\"z\":113.274,\"x\":-913.097}', '{\"y\":-367.637,\"z\":113.274,\"x\":-918.022}', '{\"y\":-382.023,\"z\":37.961,\"x\":-943.626}', '[]', NULL, 1, 1, 0, '{\"x\":-927.554,\"y\":-377.744,\"z\":112.674}', 1700000),
(4, 'NorthConkerAvenue2044', '2044 North Conker Avenue', '{\"y\":440.8,\"z\":146.702,\"x\":346.964}', '{\"y\":437.456,\"z\":148.394,\"x\":341.683}', '{\"y\":435.626,\"z\":148.394,\"x\":339.595}', '{\"x\":350.535,\"y\":443.329,\"z\":145.764}', '[]', NULL, 1, 1, 0, '{\"x\":337.726,\"y\":436.985,\"z\":140.77}', 1500000),
(5, 'WildOatsDrive', '3655 Wild Oats Drive', '{\"y\":502.696,\"z\":136.421,\"x\":-176.003}', '{\"y\":497.817,\"z\":136.653,\"x\":-174.349}', '{\"y\":495.069,\"z\":136.666,\"x\":-173.331}', '{\"y\":506.412,\"z\":135.0664,\"x\":-177.927}', '[]', NULL, 1, 1, 0, '{\"x\":-174.725,\"y\":493.095,\"z\":129.043}', 1500000),
(6, 'HillcrestAvenue2862', '2862 Hillcrest Avenue', '{\"y\":596.58,\"z\":142.641,\"x\":-686.554}', '{\"y\":591.988,\"z\":144.392,\"x\":-681.728}', '{\"y\":590.608,\"z\":144.392,\"x\":-680.124}', '{\"y\":599.019,\"z\":142.059,\"x\":-689.492}', '[]', NULL, 1, 1, 0, '{\"x\":-680.46,\"y\":588.6,\"z\":136.769}', 1500000),
(7, 'LowEndApartment', 'Appartement de base', '{\"y\":-1078.735,\"z\":28.4031,\"x\":292.528}', '{\"y\":-1007.152,\"z\":-102.002,\"x\":265.845}', '{\"y\":-1002.802,\"z\":-100.008,\"x\":265.307}', '{\"y\":-1078.669,\"z\":28.401,\"x\":296.738}', '[]', NULL, 1, 1, 0, '{\"x\":265.916,\"y\":-999.38,\"z\":-100.008}', 562500),
(8, 'MadWayneThunder', '2113 Mad Wayne Thunder', '{\"y\":454.955,\"z\":96.462,\"x\":-1294.433}', '{\"x\":-1289.917,\"y\":449.541,\"z\":96.902}', '{\"y\":446.322,\"z\":96.899,\"x\":-1289.642}', '{\"y\":455.453,\"z\":96.517,\"x\":-1298.851}', '[]', NULL, 1, 1, 0, '{\"x\":-1287.306,\"y\":455.901,\"z\":89.294}', 1500000),
(9, 'HillcrestAvenue2874', '2874 Hillcrest Avenue', '{\"x\":-853.346,\"y\":696.678,\"z\":147.782}', '{\"y\":690.875,\"z\":151.86,\"x\":-859.961}', '{\"y\":688.361,\"z\":151.857,\"x\":-859.395}', '{\"y\":701.628,\"z\":147.773,\"x\":-855.007}', '[]', NULL, 1, 1, 0, '{\"x\":-858.543,\"y\":697.514,\"z\":144.253}', 1500000),
(10, 'HillcrestAvenue2868', '2868 Hillcrest Avenue', '{\"y\":620.494,\"z\":141.588,\"x\":-752.82}', '{\"y\":618.62,\"z\":143.153,\"x\":-759.317}', '{\"y\":617.629,\"z\":143.153,\"x\":-760.789}', '{\"y\":621.281,\"z\":141.254,\"x\":-750.919}', '[]', NULL, 1, 1, 0, '{\"x\":-762.504,\"y\":618.992,\"z\":135.53}', 1500000),
(11, 'TinselTowersApt12', 'Tinsel Towers, Apt 42', '{\"y\":37.025,\"z\":42.58,\"x\":-618.299}', '{\"y\":58.898,\"z\":97.2,\"x\":-603.301}', '{\"y\":58.941,\"z\":97.2,\"x\":-608.741}', '{\"y\":30.603,\"z\":42.524,\"x\":-620.017}', '[]', NULL, 1, 1, 0, '{\"x\":-622.173,\"y\":54.585,\"z\":96.599}', 1700000),
(12, 'MiltonDrive', 'Milton Drive', '{\"x\":-775.17,\"y\":312.01,\"z\":84.658}', NULL, NULL, '{\"x\":-775.346,\"y\":306.776,\"z\":84.7}', '[]', NULL, 0, 0, 1, NULL, 0),
(13, 'Modern1Apartment', 'Appartement Moderne 1', NULL, '{\"x\":-784.194,\"y\":323.636,\"z\":210.997}', '{\"x\":-779.751,\"y\":323.385,\"z\":210.997}', NULL, '[\"apa_v_mp_h_01_a\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-766.661,\"y\":327.672,\"z\":210.396}', 1300000),
(14, 'Modern2Apartment', 'Appartement Moderne 2', NULL, '{\"x\":-786.8663,\"y\":315.764,\"z\":186.913}', '{\"x\":-781.808,\"y\":315.866,\"z\":186.913}', NULL, '[\"apa_v_mp_h_01_c\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-795.735,\"y\":326.757,\"z\":186.313}', 1300000),
(15, 'Modern3Apartment', 'Appartement Moderne 3', NULL, '{\"x\":-774.012,\"y\":342.042,\"z\":195.686}', '{\"x\":-779.057,\"y\":342.063,\"z\":195.686}', NULL, '[\"apa_v_mp_h_01_b\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-765.386,\"y\":330.782,\"z\":195.08}', 1300000),
(16, 'Mody1Apartment', 'Appartement Mode 1', NULL, '{\"x\":-784.194,\"y\":323.636,\"z\":210.997}', '{\"x\":-779.751,\"y\":323.385,\"z\":210.997}', NULL, '[\"apa_v_mp_h_02_a\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-766.615,\"y\":327.878,\"z\":210.396}', 1300000),
(17, 'Mody2Apartment', 'Appartement Mode 2', NULL, '{\"x\":-786.8663,\"y\":315.764,\"z\":186.913}', '{\"x\":-781.808,\"y\":315.866,\"z\":186.913}', NULL, '[\"apa_v_mp_h_02_c\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-795.297,\"y\":327.092,\"z\":186.313}', 1300000),
(18, 'Mody3Apartment', 'Appartement Mode 3', NULL, '{\"x\":-774.012,\"y\":342.042,\"z\":195.686}', '{\"x\":-779.057,\"y\":342.063,\"z\":195.686}', NULL, '[\"apa_v_mp_h_02_b\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-765.303,\"y\":330.932,\"z\":195.085}', 1300000),
(19, 'Vibrant1Apartment', 'Appartement Vibrant 1', NULL, '{\"x\":-784.194,\"y\":323.636,\"z\":210.997}', '{\"x\":-779.751,\"y\":323.385,\"z\":210.997}', NULL, '[\"apa_v_mp_h_03_a\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-765.885,\"y\":327.641,\"z\":210.396}', 1300000),
(20, 'Vibrant2Apartment', 'Appartement Vibrant 2', NULL, '{\"x\":-786.8663,\"y\":315.764,\"z\":186.913}', '{\"x\":-781.808,\"y\":315.866,\"z\":186.913}', NULL, '[\"apa_v_mp_h_03_c\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-795.607,\"y\":327.344,\"z\":186.313}', 1300000),
(21, 'Vibrant3Apartment', 'Appartement Vibrant 3', NULL, '{\"x\":-774.012,\"y\":342.042,\"z\":195.686}', '{\"x\":-779.057,\"y\":342.063,\"z\":195.686}', NULL, '[\"apa_v_mp_h_03_b\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-765.525,\"y\":330.851,\"z\":195.085}', 1300000),
(22, 'Sharp1Apartment', 'Appartement Persan 1', NULL, '{\"x\":-784.194,\"y\":323.636,\"z\":210.997}', '{\"x\":-779.751,\"y\":323.385,\"z\":210.997}', NULL, '[\"apa_v_mp_h_04_a\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-766.527,\"y\":327.89,\"z\":210.396}', 1300000),
(23, 'Sharp2Apartment', 'Appartement Persan 2', NULL, '{\"x\":-786.8663,\"y\":315.764,\"z\":186.913}', '{\"x\":-781.808,\"y\":315.866,\"z\":186.913}', NULL, '[\"apa_v_mp_h_04_c\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-795.642,\"y\":326.497,\"z\":186.313}', 1300000),
(24, 'Sharp3Apartment', 'Appartement Persan 3', NULL, '{\"x\":-774.012,\"y\":342.042,\"z\":195.686}', '{\"x\":-779.057,\"y\":342.063,\"z\":195.686}', NULL, '[\"apa_v_mp_h_04_b\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-765.503,\"y\":331.318,\"z\":195.085}', 1300000),
(25, 'Monochrome1Apartment', 'Appartement Monochrome 1', NULL, '{\"x\":-784.194,\"y\":323.636,\"z\":210.997}', '{\"x\":-779.751,\"y\":323.385,\"z\":210.997}', NULL, '[\"apa_v_mp_h_05_a\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-766.289,\"y\":328.086,\"z\":210.396}', 1300000),
(26, 'Monochrome2Apartment', 'Appartement Monochrome 2', NULL, '{\"x\":-786.8663,\"y\":315.764,\"z\":186.913}', '{\"x\":-781.808,\"y\":315.866,\"z\":186.913}', NULL, '[\"apa_v_mp_h_05_c\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-795.692,\"y\":326.762,\"z\":186.313}', 1300000),
(27, 'Monochrome3Apartment', 'Appartement Monochrome 3', NULL, '{\"x\":-774.012,\"y\":342.042,\"z\":195.686}', '{\"x\":-779.057,\"y\":342.063,\"z\":195.686}', NULL, '[\"apa_v_mp_h_05_b\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-765.094,\"y\":330.976,\"z\":195.085}', 1300000),
(28, 'Seductive1Apartment', 'Appartement Séduisant 1', NULL, '{\"x\":-784.194,\"y\":323.636,\"z\":210.997}', '{\"x\":-779.751,\"y\":323.385,\"z\":210.997}', NULL, '[\"apa_v_mp_h_06_a\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-766.263,\"y\":328.104,\"z\":210.396}', 1300000),
(29, 'Seductive2Apartment', 'Appartement Séduisant 2', NULL, '{\"x\":-786.8663,\"y\":315.764,\"z\":186.913}', '{\"x\":-781.808,\"y\":315.866,\"z\":186.913}', NULL, '[\"apa_v_mp_h_06_c\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-795.655,\"y\":326.611,\"z\":186.313}', 1300000),
(30, 'Seductive3Apartment', 'Appartement Séduisant 3', NULL, '{\"x\":-774.012,\"y\":342.042,\"z\":195.686}', '{\"x\":-779.057,\"y\":342.063,\"z\":195.686}', NULL, '[\"apa_v_mp_h_06_b\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-765.3,\"y\":331.414,\"z\":195.085}', 1300000),
(31, 'Regal1Apartment', 'Appartement Régal 1', NULL, '{\"x\":-784.194,\"y\":323.636,\"z\":210.997}', '{\"x\":-779.751,\"y\":323.385,\"z\":210.997}', NULL, '[\"apa_v_mp_h_07_a\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-765.956,\"y\":328.257,\"z\":210.396}', 1300000),
(32, 'Regal2Apartment', 'Appartement Régal 2', NULL, '{\"x\":-786.8663,\"y\":315.764,\"z\":186.913}', '{\"x\":-781.808,\"y\":315.866,\"z\":186.913}', NULL, '[\"apa_v_mp_h_07_c\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-795.545,\"y\":326.659,\"z\":186.313}', 1300000),
(33, 'Regal3Apartment', 'Appartement Régal 3', NULL, '{\"x\":-774.012,\"y\":342.042,\"z\":195.686}', '{\"x\":-779.057,\"y\":342.063,\"z\":195.686}', NULL, '[\"apa_v_mp_h_07_b\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-765.087,\"y\":331.429,\"z\":195.123}', 1300000),
(34, 'Aqua1Apartment', 'Appartement Aqua 1', NULL, '{\"x\":-784.194,\"y\":323.636,\"z\":210.997}', '{\"x\":-779.751,\"y\":323.385,\"z\":210.997}', NULL, '[\"apa_v_mp_h_08_a\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-766.187,\"y\":328.47,\"z\":210.396}', 1300000),
(35, 'Aqua2Apartment', 'Appartement Aqua 2', NULL, '{\"x\":-786.8663,\"y\":315.764,\"z\":186.913}', '{\"x\":-781.808,\"y\":315.866,\"z\":186.913}', NULL, '[\"apa_v_mp_h_08_c\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-795.658,\"y\":326.563,\"z\":186.313}', 1300000),
(36, 'Aqua3Apartment', 'Appartement Aqua 3', NULL, '{\"x\":-774.012,\"y\":342.042,\"z\":195.686}', '{\"x\":-779.057,\"y\":342.063,\"z\":195.686}', NULL, '[\"apa_v_mp_h_08_b\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-765.287,\"y\":331.084,\"z\":195.086}', 1300000),
(37, 'IntegrityWay', '4 Integrity Way', '{\"x\":-47.804,\"y\":-585.867,\"z\":36.956}', NULL, NULL, '{\"x\":-54.178,\"y\":-583.762,\"z\":35.798}', '[]', NULL, 0, 0, 1, NULL, 0),
(38, 'IntegrityWay28', '4 Integrity Way - Apt 28', NULL, '{\"x\":-31.409,\"y\":-594.927,\"z\":79.03}', '{\"x\":-26.098,\"y\":-596.909,\"z\":79.03}', NULL, '[]', 'IntegrityWay', 0, 1, 0, '{\"x\":-11.923,\"y\":-597.083,\"z\":78.43}', 1700000),
(39, 'IntegrityWay30', '4 Integrity Way - Apt 30', NULL, '{\"x\":-17.702,\"y\":-588.524,\"z\":89.114}', '{\"x\":-16.21,\"y\":-582.569,\"z\":89.114}', NULL, '[]', 'IntegrityWay', 0, 1, 0, '{\"x\":-26.327,\"y\":-588.384,\"z\":89.123}', 1700000),
(40, 'DellPerroHeights', 'Dell Perro Heights', '{\"x\":-1447.06,\"y\":-538.28,\"z\":33.74}', NULL, NULL, '{\"x\":-1440.022,\"y\":-548.696,\"z\":33.74}', '[]', NULL, 0, 0, 1, NULL, 0),
(41, 'DellPerroHeightst4', 'Dell Perro Heights - Apt 28', NULL, '{\"x\":-1452.125,\"y\":-540.591,\"z\":73.044}', '{\"x\":-1455.435,\"y\":-535.79,\"z\":73.044}', NULL, '[]', 'DellPerroHeights', 0, 1, 0, '{\"x\":-1467.058,\"y\":-527.571,\"z\":72.443}', 1700000),
(42, 'DellPerroHeightst7', 'Dell Perro Heights - Apt 30', NULL, '{\"x\":-1451.562,\"y\":-523.535,\"z\":55.928}', '{\"x\":-1456.02,\"y\":-519.209,\"z\":55.929}', NULL, '[]', 'DellPerroHeights', 0, 1, 0, '{\"x\":-1457.026,\"y\":-530.219,\"z\":55.937}', 1700000);

-- --------------------------------------------------------

--
-- Table structure for table `rented_vehicles`
--

CREATE TABLE `rented_vehicles` (
  `vehicle` varchar(60) COLLATE utf8mb4_bin NOT NULL,
  `plate` varchar(12) COLLATE utf8mb4_bin NOT NULL,
  `player_name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `base_price` int(11) NOT NULL,
  `rent_price` int(11) NOT NULL,
  `owner` varchar(22) COLLATE utf8mb4_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- --------------------------------------------------------

--
-- Table structure for table `shops`
--

CREATE TABLE `shops` (
  `id` int(11) NOT NULL,
  `store` varchar(100) COLLATE utf8mb4_bin NOT NULL,
  `item` varchar(100) COLLATE utf8mb4_bin NOT NULL,
  `price` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `shops`
--

INSERT INTO `shops` (`id`, `store`, `item`, `price`) VALUES
(1, 'TwentyFourSeven', 'bread', 30),
(2, 'TwentyFourSeven', 'water', 15),
(3, 'RobsLiquor', 'bread', 30),
(4, 'RobsLiquor', 'water', 15),
(5, 'LTDgasoline', 'bread', 30),
(6, 'LTDgasoline', 'water', 15);

-- --------------------------------------------------------

--
-- Table structure for table `society_moneywash`
--

CREATE TABLE `society_moneywash` (
  `id` int(11) NOT NULL,
  `identifier` varchar(60) COLLATE utf8mb4_bin NOT NULL,
  `society` varchar(60) COLLATE utf8mb4_bin NOT NULL,
  `amount` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- --------------------------------------------------------

--
-- Table structure for table `trunk_inventory`
--

CREATE TABLE `trunk_inventory` (
  `id` int(11) NOT NULL,
  `plate` varchar(8) NOT NULL,
  `data` text NOT NULL,
  `owned` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `trunk_inventory`
--

INSERT INTO `trunk_inventory` (`id`, `plate`, `data`, `owned`) VALUES
(3, 'OIL 252 ', '{\"weapons\":[{\"ammo\":24,\"label\":\"Sticky bomb\",\"name\":\"WEAPON_STICKYBOMB\"}],\"coffre\":[{\"name\":\"marijuana\",\"count\":95}]}', 1);

-- --------------------------------------------------------

--
-- Table structure for table `twitter_accounts`
--

CREATE TABLE `twitter_accounts` (
  `id` int(11) NOT NULL,
  `username` varchar(50) CHARACTER SET utf8 NOT NULL DEFAULT '0',
  `password` varchar(50) COLLATE utf8mb4_bin NOT NULL DEFAULT '0',
  `avatar_url` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `twitter_accounts`
--

INSERT INTO `twitter_accounts` (`id`, `username`, `password`, `avatar_url`) VALUES
(38, 'JamesonSASP', 'Hagan1100', 'https://townsquare.media/site/704/files/2019/10/state-trooper.jpg?w=980&q=75'),
(39, '22pcross', 'Prince87', 'https://i.imgur.com/'),
(40, 'Lord Godrick', 'charlesd46san', NULL),
(41, 'sticky', 'stoner420', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `twitter_likes`
--

CREATE TABLE `twitter_likes` (
  `id` int(11) NOT NULL,
  `authorId` int(11) DEFAULT NULL,
  `tweetId` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `twitter_likes`
--

INSERT INTO `twitter_likes` (`id`, `authorId`, `tweetId`) VALUES
(137, 41, 172);

-- --------------------------------------------------------

--
-- Table structure for table `twitter_tweets`
--

CREATE TABLE `twitter_tweets` (
  `id` int(11) NOT NULL,
  `authorId` int(11) NOT NULL,
  `realUser` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `message` varchar(256) COLLATE utf8mb4_unicode_ci NOT NULL,
  `time` timestamp NOT NULL DEFAULT current_timestamp(),
  `likes` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `twitter_tweets`
--

INSERT INTO `twitter_tweets` (`id`, `authorId`, `realUser`, `message`, `time`, `likes`) VALUES
(170, 38, 'steam:1100001139319c1', 'Ready to Work!', '2020-02-08 02:22:51', 0),
(171, 39, 'steam:110000111c332ed', 'out on patrol', '2020-02-10 01:46:10', 0),
(172, 40, 'steam:1100001081d1893', 'I am contemplating going after someone.', '2020-03-14 18:50:06', 1),
(173, 41, 'steam:11000010a01bdb9', '@Lord Godrick Ill assist', '2020-03-14 18:50:54', 0),
(174, 40, 'steam:1100001081d1893', '@sticky Yessssssss!', '2020-03-14 18:51:14', 0),
(175, 41, 'steam:11000010a01bdb9', '@Lord Godrick Let me know when and where', '2020-03-14 18:51:45', 0);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `identifier` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `license` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL,
  `money` int(11) DEFAULT NULL,
  `name` varchar(50) COLLATE utf8mb4_bin DEFAULT '',
  `skin` longtext COLLATE utf8mb4_bin DEFAULT NULL,
  `job` varchar(50) COLLATE utf8mb4_bin DEFAULT 'unemployed',
  `job_grade` int(11) DEFAULT 0,
  `loadout` longtext COLLATE utf8mb4_bin DEFAULT NULL,
  `position` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `bank` int(11) DEFAULT 5000,
  `permission_level` int(11) DEFAULT NULL,
  `group` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL,
  `firstname` varchar(50) COLLATE utf8mb4_bin DEFAULT '',
  `lastname` varchar(50) COLLATE utf8mb4_bin DEFAULT '',
  `dateofbirth` varchar(25) COLLATE utf8mb4_bin DEFAULT '',
  `sex` varchar(10) COLLATE utf8mb4_bin DEFAULT '',
  `height` varchar(5) COLLATE utf8mb4_bin DEFAULT '',
  `status` longtext COLLATE utf8mb4_bin DEFAULT NULL,
  `last_property` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `is_dead` tinyint(1) DEFAULT 0,
  `phone_number` varchar(10) COLLATE utf8mb4_bin DEFAULT NULL,
  `jail_time` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`identifier`, `license`, `money`, `name`, `skin`, `job`, `job_grade`, `loadout`, `position`, `bank`, `permission_level`, `group`, `firstname`, `lastname`, `dateofbirth`, `sex`, `height`, `status`, `last_property`, `is_dead`, `phone_number`, `jail_time`) VALUES
('steam:11000010a01bdb9', 'license:cb5765d5073314f0d95e9e69ab51cf5cc5e16d7d', 0, 'stickybombz', '{\"eyebrows_1\":0,\"lipstick_1\":0,\"pants_1\":0,\"sun_1\":0,\"chain_2\":0,\"hair_2\":0,\"shoes_1\":0,\"eye_color\":0,\"bodyb_1\":0,\"mask_2\":0,\"moles_1\":0,\"watches_1\":-1,\"blemishes_2\":0,\"chest_1\":0,\"makeup_4\":0,\"sex\":0,\"eyebrows_2\":0,\"torso_1\":0,\"eyebrows_3\":0,\"watches_2\":0,\"mask_1\":0,\"bodyb_2\":0,\"makeup_3\":0,\"eyebrows_4\":0,\"ears_2\":0,\"pants_2\":0,\"bracelets_2\":0,\"helmet_2\":0,\"face\":0,\"torso_2\":0,\"glasses_2\":0,\"tshirt_2\":0,\"bags_1\":0,\"helmet_1\":-1,\"age_2\":0,\"shoes_2\":0,\"lipstick_3\":0,\"bags_2\":0,\"decals_2\":0,\"bracelets_1\":-1,\"lipstick_2\":0,\"blush_2\":0,\"beard_4\":0,\"arms_2\":0,\"blush_3\":0,\"hair_color_2\":0,\"beard_3\":0,\"chest_3\":0,\"makeup_1\":0,\"complexion_2\":0,\"skin\":0,\"bproof_1\":0,\"moles_2\":0,\"glasses_1\":0,\"blemishes_1\":0,\"hair_color_1\":0,\"ears_1\":-1,\"tshirt_1\":0,\"makeup_2\":0,\"sun_2\":0,\"beard_1\":0,\"blush_1\":0,\"lipstick_4\":0,\"chest_2\":0,\"chain_1\":0,\"decals_1\":0,\"age_1\":0,\"complexion_1\":0,\"hair_1\":0,\"bproof_2\":0,\"beard_2\":0,\"arms\":0}', 'unemployed', 0, '[{\"label\":\"Bat\",\"ammo\":0,\"components\":[],\"name\":\"WEAPON_BAT\"},{\"label\":\"Pistol\",\"ammo\":83,\"components\":[\"clip_default\"],\"name\":\"WEAPON_PISTOL\"},{\"label\":\"Heavy revolver\",\"ammo\":83,\"components\":[],\"name\":\"WEAPON_REVOLVER\"},{\"label\":\"Bz gas\",\"ammo\":24,\"components\":[],\"name\":\"WEAPON_BZGAS\"},{\"label\":\"Taser\",\"ammo\":42,\"components\":[],\"name\":\"WEAPON_STUNGUN\"}]', '{\"x\":351.3,\"y\":-1578.0,\"z\":29.3}', 10589, 100, 'superadmin', 'Tommie', 'Pickles', '1988-12-28', 'm', '172', '[{\"val\":316600,\"percent\":31.66,\"name\":\"hunger\"},{\"val\":487375,\"percent\":48.7375,\"name\":\"thirst\"},{\"val\":0,\"percent\":0.0,\"name\":\"drunk\"}]', NULL, 0, '827-5849', 0),
('steam:1100001081d1893', 'license:335b97a11f294fa23e5ab4446ddf5f9d9452b275', 10000, '456', '{\"eyebrows_1\":0,\"lipstick_1\":0,\"pants_1\":0,\"sun_1\":0,\"chain_2\":0,\"hair_2\":0,\"blush_1\":0,\"eye_color\":0,\"bodyb_1\":0,\"mask_2\":0,\"moles_1\":0,\"watches_1\":-1,\"blemishes_2\":0,\"chest_1\":0,\"makeup_4\":0,\"sex\":0,\"eyebrows_2\":0,\"blemishes_1\":0,\"eyebrows_3\":0,\"watches_2\":0,\"mask_1\":0,\"bodyb_2\":0,\"makeup_3\":0,\"eyebrows_4\":0,\"age_1\":0,\"pants_2\":0,\"bracelets_2\":0,\"helmet_2\":0,\"face\":0,\"torso_2\":0,\"glasses_2\":0,\"arms\":0,\"bags_1\":0,\"helmet_1\":-1,\"age_2\":0,\"shoes_2\":0,\"lipstick_3\":0,\"bags_2\":0,\"decals_2\":0,\"bracelets_1\":-1,\"lipstick_2\":0,\"blush_2\":0,\"beard_4\":0,\"arms_2\":0,\"blush_3\":0,\"complexion_1\":0,\"beard_3\":0,\"chest_3\":0,\"makeup_1\":0,\"complexion_2\":0,\"skin\":0,\"bproof_1\":0,\"moles_2\":0,\"glasses_1\":0,\"tshirt_2\":0,\"hair_color_1\":0,\"ears_1\":-1,\"beard_2\":0,\"makeup_2\":0,\"sun_2\":0,\"beard_1\":0,\"decals_1\":0,\"shoes_1\":0,\"lipstick_4\":0,\"chain_1\":0,\"ears_2\":0,\"torso_1\":0,\"tshirt_1\":0,\"hair_1\":0,\"hair_color_2\":0,\"bproof_2\":0,\"chest_2\":0}', 'police', 6, '[{\"label\":\"Nightstick\",\"components\":[],\"ammo\":0,\"name\":\"WEAPON_NIGHTSTICK\"},{\"label\":\"Pistol\",\"components\":[\"clip_default\"],\"ammo\":76,\"name\":\"WEAPON_PISTOL\"},{\"label\":\"Combat pistol\",\"components\":[\"clip_default\"],\"ammo\":76,\"name\":\"WEAPON_COMBATPISTOL\"},{\"label\":\"Pump shotgun\",\"components\":[],\"ammo\":19,\"name\":\"WEAPON_PUMPSHOTGUN\"},{\"label\":\"Taser\",\"components\":[],\"ammo\":100,\"name\":\"WEAPON_STUNGUN\"},{\"label\":\"Flashlight\",\"components\":[],\"ammo\":0,\"name\":\"WEAPON_FLASHLIGHT\"},{\"label\":\"Flare gun\",\"components\":[],\"ammo\":17,\"name\":\"WEAPON_FLARE\"}]', '{\"x\":-438.1,\"y\":6023.2,\"z\":31.5}', 224914, 100, 'superadmin', 'Godrick', 'Twinkletoes', '1994-09-12', 'm', '200', '[{\"val\":857800,\"percent\":85.78,\"name\":\"hunger\"},{\"val\":893350,\"percent\":89.335,\"name\":\"thirst\"},{\"val\":0,\"percent\":0.0,\"name\":\"drunk\"}]', NULL, 0, '531-3502', 0),
('steam:110000133d93ea2', 'license:bb1b09de182bf81a18e4eca7a372134cad269c76', 0, 'Venom8797', NULL, 'mechanic', 3, '[]', '{\"x\":2047.0,\"y\":3733.6,\"z\":32.8}', 12648, 0, 'user', 'Grant ', 'Cummings', '1990/03/17', 'm', '150', '[{\"val\":701000,\"percent\":70.1,\"name\":\"hunger\"},{\"val\":775525,\"percent\":77.5525,\"name\":\"thirst\"},{\"val\":0,\"percent\":0.0,\"name\":\"drunk\"}]', NULL, 0, '771-1741', 0),
('steam:1100001139319c1', 'license:fe4031d6bbcd8ce2770862c34c19038ca32d88f0', 497, 'Mito Jr', '{\"ears_2\":0,\"complexion_1\":0,\"blush_1\":0,\"lipstick_1\":0,\"eye_color\":0,\"shoes_2\":0,\"glasses_1\":0,\"lipstick_3\":0,\"age_1\":0,\"sun_2\":0,\"eyebrows_4\":0,\"blemishes_1\":0,\"makeup_4\":0,\"glasses_2\":0,\"mask_1\":0,\"tshirt_2\":0,\"moles_2\":0,\"bodyb_1\":0,\"bodyb_2\":0,\"chest_2\":0,\"bags_1\":0,\"beard_4\":0,\"chain_1\":0,\"complexion_2\":0,\"bags_2\":0,\"ears_1\":-1,\"decals_2\":0,\"lipstick_2\":0,\"chest_3\":0,\"makeup_1\":0,\"skin\":4,\"age_2\":0,\"chain_2\":0,\"moles_1\":0,\"beard_2\":0,\"eyebrows_3\":0,\"hair_color_1\":3,\"torso_2\":0,\"beard_1\":0,\"bracelets_1\":-1,\"pants_2\":0,\"helmet_2\":0,\"chest_1\":0,\"shoes_1\":2,\"hair_1\":2,\"eyebrows_1\":0,\"blush_2\":0,\"pants_1\":0,\"makeup_2\":0,\"sun_1\":0,\"blemishes_2\":0,\"bracelets_2\":0,\"mask_2\":0,\"arms_2\":0,\"sex\":0,\"bproof_1\":0,\"tshirt_1\":0,\"torso_1\":0,\"watches_2\":0,\"blush_3\":0,\"hair_color_2\":6,\"watches_1\":-1,\"makeup_3\":0,\"decals_1\":0,\"arms\":0,\"helmet_1\":-1,\"beard_3\":0,\"bproof_2\":0,\"eyebrows_2\":0,\"lipstick_4\":0,\"hair_2\":5,\"face\":4}', 'police', 2, '[{\"ammo\":0,\"name\":\"WEAPON_NIGHTSTICK\",\"components\":[],\"label\":\"Nightstick\"},{\"ammo\":129,\"name\":\"WEAPON_COMBATPISTOL\",\"components\":[\"clip_default\",\"flashlight\"],\"label\":\"Combat pistol\"},{\"ammo\":131,\"name\":\"WEAPON_PUMPSHOTGUN\",\"components\":[\"flashlight\"],\"label\":\"Pump shotgun\"},{\"ammo\":100,\"name\":\"WEAPON_FIREEXTINGUISHER\",\"components\":[],\"label\":\"Fire extinguisher\"},{\"ammo\":100,\"name\":\"WEAPON_STUNGUN\",\"components\":[],\"label\":\"Taser\"},{\"ammo\":0,\"name\":\"WEAPON_FLASHLIGHT\",\"components\":[],\"label\":\"Flashlight\"},{\"ammo\":25,\"name\":\"WEAPON_FLARE\",\"components\":[],\"label\":\"Flare gun\"}]', '{\"x\":2040.9,\"y\":3163.7,\"z\":45.3}', 16048, 0, 'user', 'Michael', 'Thomas', '1994-06-12', 'm', '176', '[{\"val\":740500,\"name\":\"hunger\",\"percent\":74.05},{\"val\":805600,\"name\":\"thirst\",\"percent\":80.56},{\"val\":0,\"name\":\"drunk\",\"percent\":0.0}]', NULL, 0, '873-7104', 0),
('steam:1100001156cfac9', 'license:5d9ba217761fbe2cb970db3c8c32ac212826e369', 350, 'Joe', NULL, 'unemployed', 0, '[{\"label\":\"Pistol\",\"name\":\"WEAPON_PISTOL\",\"components\":[\"clip_default\"],\"ammo\":18},{\"label\":\"Machete\",\"name\":\"WEAPON_MACHETE\",\"components\":[],\"ammo\":0}]', '{\"x\":2393.0,\"y\":3800.0,\"z\":37.7}', 6400, 0, 'user', 'Joey ', 'Mcboby', '08-12-99', 'm', '188', '[{\"val\":443000,\"name\":\"hunger\",\"percent\":44.3},{\"val\":457250,\"name\":\"thirst\",\"percent\":45.725},{\"val\":0,\"name\":\"drunk\",\"percent\":0.0}]', NULL, 1, '562-3039', 0),
('steam:11000013ea40bc0', 'license:49ecd7ddd93aec39c517a6b9fe5144b25762e37c', 8324, 'Ur_A_Scrub1', '{\"bracelets_1\":-1,\"complexion_1\":0,\"blush_1\":0,\"lipstick_1\":0,\"eye_color\":0,\"shoes_2\":0,\"glasses_1\":0,\"lipstick_3\":0,\"age_1\":0,\"sun_2\":0,\"eyebrows_4\":0,\"blemishes_1\":0,\"makeup_4\":0,\"glasses_2\":0,\"mask_1\":0,\"tshirt_2\":0,\"moles_2\":0,\"bodyb_1\":0,\"bproof_2\":0,\"chest_2\":0,\"lipstick_4\":0,\"beard_4\":0,\"chain_1\":0,\"complexion_2\":0,\"bags_2\":0,\"sun_1\":0,\"decals_2\":0,\"lipstick_2\":0,\"sex\":0,\"makeup_1\":0,\"skin\":0,\"age_2\":0,\"chain_2\":0,\"moles_1\":0,\"beard_2\":0,\"eyebrows_3\":0,\"pants_1\":19,\"torso_2\":0,\"eyebrows_2\":0,\"beard_3\":0,\"pants_2\":0,\"mask_2\":0,\"chest_1\":0,\"shoes_1\":15,\"hair_1\":22,\"eyebrows_1\":0,\"blush_2\":0,\"ears_2\":0,\"beard_1\":11,\"bags_1\":0,\"ears_1\":-1,\"bracelets_2\":0,\"face\":0,\"arms_2\":0,\"chest_3\":0,\"bproof_1\":0,\"tshirt_1\":16,\"torso_1\":0,\"bodyb_2\":0,\"helmet_2\":0,\"hair_color_2\":0,\"watches_1\":-1,\"makeup_3\":0,\"decals_1\":0,\"arms\":0,\"hair_color_1\":0,\"helmet_1\":-1,\"watches_2\":0,\"makeup_2\":0,\"blush_3\":0,\"blemishes_2\":0,\"hair_2\":0}', 'unemployed', 0, '[{\"label\":\"Combat pistol\",\"components\":[\"clip_default\"],\"ammo\":991,\"name\":\"WEAPON_COMBATPISTOL\"},{\"label\":\"Pump shotgun\",\"components\":[],\"ammo\":20,\"name\":\"WEAPON_PUMPSHOTGUN\"}]', '{\"x\":1441.2,\"y\":3498.4,\"z\":35.8}', 1200, 0, 'user', 'Freddy', 'Lungus', '1997/03/14', 'm', '157', '[{\"val\":469800,\"percent\":46.98,\"name\":\"hunger\"},{\"val\":477350,\"percent\":47.735,\"name\":\"thirst\"},{\"val\":0,\"percent\":0.0,\"name\":\"drunk\"}]', NULL, 0, '104-0089', 0),
('steam:110000106197c1a', 'license:a8f78a1b293e00697b6d5296a0cbbe4cef9b08c0', 0, 'billsrock35', '{\"bracelets_1\":-1,\"complexion_1\":0,\"blush_1\":0,\"lipstick_1\":0,\"eye_color\":0,\"shoes_2\":0,\"glasses_1\":0,\"lipstick_3\":0,\"age_1\":0,\"sun_2\":0,\"eyebrows_4\":0,\"blemishes_1\":0,\"makeup_4\":0,\"glasses_2\":0,\"mask_1\":0,\"helmet_1\":-1,\"moles_2\":0,\"helmet_2\":0,\"bproof_2\":0,\"chest_2\":0,\"lipstick_4\":0,\"beard_4\":0,\"chain_1\":0,\"complexion_2\":0,\"bags_2\":0,\"ears_1\":-1,\"decals_2\":0,\"lipstick_2\":0,\"sex\":0,\"makeup_1\":0,\"skin\":0,\"age_2\":0,\"chain_2\":0,\"moles_1\":0,\"beard_2\":0,\"eyebrows_3\":0,\"pants_1\":0,\"watches_2\":0,\"makeup_3\":0,\"tshirt_2\":0,\"pants_2\":0,\"shoes_1\":0,\"chest_1\":0,\"mask_2\":0,\"hair_1\":0,\"eyebrows_1\":0,\"blush_2\":0,\"blemishes_2\":0,\"ears_2\":0,\"beard_3\":0,\"torso_2\":0,\"bracelets_2\":0,\"sun_1\":0,\"arms_2\":0,\"bodyb_1\":0,\"bproof_1\":0,\"tshirt_1\":0,\"torso_1\":0,\"chest_3\":0,\"face\":0,\"watches_1\":-1,\"hair_color_2\":0,\"eyebrows_2\":0,\"decals_1\":0,\"arms\":0,\"hair_color_1\":0,\"blush_3\":0,\"beard_1\":0,\"makeup_2\":0,\"bodyb_2\":0,\"bags_1\":0,\"hair_2\":0}', 'unemployed', 0, '[{\"ammo\":225,\"name\":\"WEAPON_PISTOL\",\"components\":[\"clip_default\"],\"label\":\"Pistol\"},{\"ammo\":225,\"name\":\"WEAPON_COMBATPISTOL\",\"components\":[\"clip_default\"],\"label\":\"Combat pistol\"},{\"ammo\":225,\"name\":\"WEAPON_PISTOL50\",\"components\":[\"clip_default\"],\"label\":\"Pistol .50\"},{\"ammo\":225,\"name\":\"WEAPON_REVOLVER\",\"components\":[],\"label\":\"Heavy revolver\"},{\"ammo\":225,\"name\":\"WEAPON_HEAVYPISTOL\",\"components\":[\"clip_default\"],\"label\":\"Heavy pistol\"},{\"ammo\":42,\"name\":\"WEAPON_SMG\",\"components\":[\"clip_default\"],\"label\":\"SMG\"},{\"ammo\":10,\"name\":\"WEAPON_PUMPSHOTGUN\",\"components\":[],\"label\":\"Pump shotgun\"},{\"ammo\":126,\"name\":\"WEAPON_ASSAULTRIFLE\",\"components\":[\"clip_default\"],\"label\":\"Assault rifle\"},{\"ammo\":126,\"name\":\"WEAPON_CARBINERIFLE\",\"components\":[\"clip_default\"],\"label\":\"Carbine rifle\"},{\"ammo\":126,\"name\":\"WEAPON_ADVANCEDRIFLE\",\"components\":[\"clip_default\"],\"label\":\"Advanced rifle\"},{\"ammo\":42,\"name\":\"WEAPON_SNIPERRIFLE\",\"components\":[\"scope\"],\"label\":\"Sniper rifle\"},{\"ammo\":25,\"name\":\"WEAPON_SMOKEGRENADE\",\"components\":[],\"label\":\"Smoke grenade\"},{\"ammo\":0,\"name\":\"WEAPON_MACHETE\",\"components\":[],\"label\":\"Machete\"},{\"ammo\":20,\"name\":\"WEAPON_FLARE\",\"components\":[],\"label\":\"Flare gun\"}]', '{\"x\":2056.7,\"y\":3186.2,\"z\":47.4}', 133586, 0, 'user', 'Austin', 'Nocera', '1995-06-09', 'm', '200', '[{\"val\":489500,\"name\":\"hunger\",\"percent\":48.95},{\"val\":492125,\"name\":\"thirst\",\"percent\":49.2125},{\"val\":0,\"name\":\"drunk\",\"percent\":0.0}]', NULL, 0, '288-7796', 0),
('steam:110000116e48a9b', 'license:afeca7162eb4179bd704399b08706ed34e67f640', 0, 'FireLieutenant184', '{\"makeup_3\":0,\"beard_4\":0,\"blemishes_2\":0,\"blemishes_1\":0,\"lipstick_1\":0,\"chest_3\":0,\"mask_1\":0,\"lipstick_3\":0,\"makeup_1\":0,\"eyebrows_3\":0,\"pants_1\":0,\"blush_1\":0,\"sun_2\":0,\"lipstick_4\":0,\"eyebrows_4\":0,\"torso_2\":0,\"eyebrows_1\":0,\"eyebrows_2\":0,\"helmet_2\":0,\"pants_2\":0,\"complexion_2\":0,\"face\":0,\"shoes_1\":0,\"blush_2\":0,\"complexion_1\":0,\"beard_3\":0,\"blush_3\":0,\"makeup_4\":0,\"chain_1\":0,\"tshirt_1\":0,\"eye_color\":0,\"glasses_2\":0,\"bracelets_2\":0,\"watches_1\":-1,\"helmet_1\":-1,\"decals_1\":0,\"age_1\":0,\"glasses_1\":0,\"watches_2\":0,\"bodyb_2\":0,\"sex\":0,\"shoes_2\":0,\"bracelets_1\":-1,\"bodyb_1\":0,\"skin\":0,\"hair_1\":0,\"hair_color_1\":0,\"bags_2\":0,\"bproof_2\":0,\"beard_2\":0,\"sun_1\":0,\"decals_2\":0,\"chain_2\":0,\"torso_1\":0,\"chest_1\":0,\"bags_1\":0,\"moles_1\":0,\"hair_2\":0,\"moles_2\":0,\"ears_2\":0,\"makeup_2\":0,\"arms_2\":0,\"lipstick_2\":0,\"tshirt_2\":0,\"age_2\":0,\"hair_color_2\":0,\"beard_1\":0,\"ears_1\":-1,\"arms\":0,\"bproof_1\":0,\"chest_2\":0,\"mask_2\":0}', 'unemployed', 0, '[]', '{\"x\":406.9,\"z\":29.3,\"y\":-993.8}', 5400, 0, 'user', 'Nate', 'Redmond', '1997-04-29', 'm', '160', '[{\"val\":482300,\"name\":\"hunger\",\"percent\":48.23},{\"val\":486725,\"name\":\"thirst\",\"percent\":48.6725},{\"val\":0,\"name\":\"drunk\",\"percent\":0.0}]', NULL, 0, '906-8228', 0),
('steam:11000010b49a743', 'license:e5d2acf163c043923bfb9d0a3117fb456b890766', 0, 'killian2134', '{\"watches_2\":0,\"moles_2\":0,\"torso_1\":0,\"bracelets_1\":-1,\"hair_color_1\":0,\"beard_3\":0,\"bags_1\":0,\"pants_1\":0,\"lipstick_3\":0,\"chain_2\":0,\"beard_2\":0,\"beard_4\":0,\"blemishes_2\":0,\"eyebrows_4\":0,\"ears_2\":0,\"lipstick_2\":0,\"blush_2\":0,\"chain_1\":0,\"shoes_2\":0,\"bproof_2\":0,\"chest_1\":0,\"hair_1\":4,\"bproof_1\":0,\"sun_1\":0,\"complexion_2\":0,\"ears_1\":-1,\"watches_1\":-1,\"beard_1\":0,\"eye_color\":0,\"chest_3\":0,\"torso_2\":1,\"makeup_1\":0,\"sun_2\":0,\"arms_2\":0,\"age_1\":0,\"bracelets_2\":0,\"arms\":0,\"glasses_2\":0,\"lipstick_1\":0,\"mask_2\":0,\"moles_1\":0,\"bodyb_2\":0,\"helmet_1\":-1,\"bags_2\":0,\"blush_3\":0,\"makeup_2\":0,\"decals_2\":0,\"makeup_4\":0,\"mask_1\":0,\"sex\":0,\"eyebrows_1\":0,\"tshirt_1\":0,\"bodyb_1\":0,\"eyebrows_2\":0,\"shoes_1\":0,\"lipstick_4\":0,\"eyebrows_3\":0,\"helmet_2\":0,\"blemishes_1\":0,\"glasses_1\":0,\"complexion_1\":0,\"age_2\":0,\"face\":0,\"chest_2\":0,\"makeup_3\":0,\"skin\":0,\"tshirt_2\":1,\"hair_color_2\":0,\"pants_2\":0,\"decals_1\":0,\"blush_1\":0,\"hair_2\":0}', 'police', 4, '[{\"label\":\"Knife\",\"components\":[],\"ammo\":0,\"name\":\"WEAPON_KNIFE\"},{\"label\":\"Nightstick\",\"components\":[],\"ammo\":0,\"name\":\"WEAPON_NIGHTSTICK\"},{\"label\":\"Combat pistol\",\"components\":[\"clip_default\",\"flashlight\"],\"ammo\":1000,\"name\":\"WEAPON_COMBATPISTOL\"},{\"label\":\"SMG\",\"components\":[\"clip_extended\",\"flashlight\",\"scope\"],\"ammo\":1000,\"name\":\"WEAPON_SMG\"},{\"label\":\"Pump shotgun\",\"components\":[\"flashlight\"],\"ammo\":1000,\"name\":\"WEAPON_PUMPSHOTGUN\"},{\"label\":\"Carbine rifle\",\"components\":[\"clip_default\",\"flashlight\",\"scope\",\"grip\"],\"ammo\":1000,\"name\":\"WEAPON_CARBINERIFLE\"},{\"label\":\"Heavy sniper\",\"components\":[\"scope_advanced\"],\"ammo\":1000,\"name\":\"WEAPON_HEAVYSNIPER\"},{\"label\":\"Bz gas\",\"components\":[],\"ammo\":25,\"name\":\"WEAPON_BZGAS\"},{\"label\":\"Fire extinguisher\",\"components\":[],\"ammo\":2000,\"name\":\"WEAPON_FIREEXTINGUISHER\"},{\"label\":\"Taser\",\"components\":[],\"ammo\":1000,\"name\":\"WEAPON_STUNGUN\"},{\"label\":\"Flashlight\",\"components\":[],\"ammo\":0,\"name\":\"WEAPON_FLASHLIGHT\"},{\"label\":\"Flare gun\",\"components\":[],\"ammo\":25,\"name\":\"WEAPON_FLARE\"}]', '{\"x\":1637.6,\"y\":3564.6,\"z\":34.8}', 11263, 100, 'superadmin', 'Killian', 'Oconnell', '19960529', 'm', '175', '[{\"val\":696400,\"percent\":69.64,\"name\":\"hunger\"},{\"val\":772300,\"percent\":77.23,\"name\":\"thirst\"},{\"val\":0,\"percent\":0.0,\"name\":\"drunk\"}]', NULL, 0, '113-0914', 0),
('steam:110000111c332ed', 'license:c7b5c1f2e80fc912bddb70417074f3b389c003b6', 0, 'Darryl [BC-6]', '{\"bags_2\":0,\"helmet_1\":-1,\"skin\":0,\"tshirt_1\":0,\"eyebrows_1\":0,\"mask_2\":0,\"makeup_4\":0,\"lipstick_1\":0,\"complexion_2\":0,\"beard_3\":0,\"age_2\":0,\"chest_1\":0,\"bodyb_1\":0,\"chain_2\":0,\"chest_3\":0,\"age_1\":0,\"beard_4\":0,\"arms_2\":0,\"moles_1\":0,\"moles_2\":0,\"bracelets_1\":-1,\"complexion_1\":0,\"tshirt_2\":0,\"bracelets_2\":0,\"lipstick_3\":0,\"sex\":0,\"decals_1\":0,\"arms\":0,\"bags_1\":0,\"blush_1\":0,\"torso_1\":0,\"face\":0,\"makeup_3\":0,\"hair_color_1\":0,\"glasses_2\":0,\"eye_color\":0,\"sun_1\":0,\"lipstick_4\":0,\"hair_color_2\":0,\"bproof_2\":0,\"blush_2\":0,\"watches_2\":0,\"makeup_2\":0,\"pants_1\":0,\"ears_2\":0,\"lipstick_2\":0,\"makeup_1\":0,\"decals_2\":0,\"mask_1\":0,\"hair_2\":0,\"blemishes_2\":0,\"shoes_2\":0,\"torso_2\":0,\"sun_2\":0,\"glasses_1\":0,\"beard_1\":0,\"hair_1\":0,\"blush_3\":0,\"bodyb_2\":0,\"chain_1\":0,\"shoes_1\":0,\"eyebrows_4\":0,\"bproof_1\":0,\"beard_2\":0,\"helmet_2\":0,\"eyebrows_3\":0,\"blemishes_1\":0,\"chest_2\":0,\"pants_2\":0,\"ears_1\":-1,\"eyebrows_2\":0,\"watches_1\":-1}', 'police', 4, '[{\"label\":\"Pump shotgun\",\"components\":[],\"ammo\":10,\"name\":\"WEAPON_PUMPSHOTGUN\"}]', '{\"x\":1635.2,\"y\":3568.3,\"z\":35.3}', 12282, 0, 'user', 'Darryl', 'Normil', '1987-29-08', 'm', '160', '[{\"val\":799300,\"percent\":79.93,\"name\":\"hunger\"},{\"val\":848950,\"percent\":84.895,\"name\":\"thirst\"},{\"val\":0,\"percent\":0.0,\"name\":\"drunk\"}]', NULL, 0, '427-1473', 0);

-- --------------------------------------------------------

--
-- Table structure for table `user_accounts`
--

CREATE TABLE `user_accounts` (
  `id` int(11) NOT NULL,
  `identifier` varchar(22) COLLATE utf8mb4_bin NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `money` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `user_accounts`
--

INSERT INTO `user_accounts` (`id`, `identifier`, `name`, `money`) VALUES
(2, 'steam:110000111d0b1aa', 'black_money', 0),
(3, 'steam:1100001081d1893', 'black_money', 0),
(4, 'steam:11000010b49a743', 'black_money', 0),
(5, 'steam:11000013e7edea3', 'black_money', 0),
(6, 'steam:110000111c332ed', 'black_money', 0),
(7, 'steam:11000010a01bdb9', 'black_money', 0),
(8, 'steam:11000013e9d4b99', 'black_money', 0),
(9, 'steam:110000107941620', 'black_money', 0),
(10, 'steam:11000010fe595d0', 'black_money', 0),
(11, 'steam:11000010e2a62e2', 'black_money', 0),
(12, 'steam:11000010b88b452', 'black_money', 0),
(13, 'steam:110000105ed368b', 'black_money', 0),
(14, 'steam:1100001183c7077', 'black_money', 0),
(15, 'steam:1100001139319c1', 'black_money', 0),
(16, 'steam:110000132580eb0', 'black_money', 0),
(17, 'steam:110000119e7384d', 'black_money', 0),
(18, 'steam:110000139d9adf9', 'black_money', 0),
(19, 'steam:11000013eaca305', 'black_money', 0),
(20, 'steam:11000013efa68e1', 'black_money', 0),
(21, 'steam:1100001176cddfb', 'black_money', 0),
(22, 'steam:11000013f6b85e1', 'black_money', 0),
(23, 'steam:11000013e33b934', 'black_money', 0),
(24, 'steam:11000013517b942', 'black_money', 0),
(25, 'steam:110000134e3ca1a', 'black_money', 0),
(26, 'steam:1100001013142e0', 'black_money', 0),
(27, 'steam:110000133d93ea2', 'black_money', 0),
(28, 'steam:11000011531f6b5', 'black_money', 0),
(29, 'steam:11000010cfb10c1', 'black_money', 0),
(30, 'steam:110000135e316b5', 'black_money', 0),
(31, 'steam:1100001156cfac9', 'black_money', 0),
(32, 'steam:110000136177a4e', 'black_money', 0),
(33, 'steam:110000116e48a9b', 'black_money', 0),
(34, 'steam:110000106197c1a', 'black_money', 0),
(35, 'steam:110000113ec9482', 'black_money', 0),
(36, 'steam:1100001351e28d7', 'black_money', 0),
(37, 'steam:11000013f478ad1', 'black_money', 0),
(38, 'steam:11000010d651395', 'black_money', 0),
(39, 'steam:1100001177ff1be', 'black_money', 0),
(40, 'steam:11000013ea40bc0', 'black_money', 0),
(41, 'steam:110000118024cc1', 'black_money', 0);

-- --------------------------------------------------------

--
-- Table structure for table `user_inventory`
--

CREATE TABLE `user_inventory` (
  `id` int(11) NOT NULL,
  `identifier` varchar(22) COLLATE utf8mb4_bin NOT NULL,
  `item` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `count` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `user_inventory`
--

INSERT INTO `user_inventory` (`id`, `identifier`, `item`, `count`) VALUES
(1, 'steam:11000010a01bdb9', 'fish', 0),
(2, 'steam:11000010a01bdb9', 'nugget', 0),
(3, 'steam:11000010a01bdb9', 'iron', 0),
(4, 'steam:11000010a01bdb9', 'cheese', 0),
(5, 'steam:11000010a01bdb9', 'pizza', 0),
(6, 'steam:11000010a01bdb9', 'fixkit', 20),
(7, 'steam:11000010a01bdb9', 'fakepee', 0),
(8, 'steam:11000010a01bdb9', 'petrol', 0),
(9, 'steam:11000010a01bdb9', 'drugtest', 0),
(10, 'steam:11000010a01bdb9', 'dabs', 0),
(11, 'steam:11000010a01bdb9', 'ctomato', 0),
(12, 'steam:11000010a01bdb9', 'cutted_wood', 0),
(13, 'steam:11000010a01bdb9', 'gazbottle', 0),
(14, 'steam:11000010a01bdb9', 'washed_stone', 0),
(15, 'steam:11000010a01bdb9', 'heroine', 0),
(16, 'steam:11000010a01bdb9', 'bag', 0),
(17, 'steam:11000010a01bdb9', 'gold', 0),
(18, 'steam:11000010a01bdb9', 'diamond', 0),
(19, 'steam:11000010a01bdb9', 'vbread', 0),
(20, 'steam:11000010a01bdb9', 'opium_pooch', 0),
(21, 'steam:11000010a01bdb9', 'bread', 0),
(22, 'steam:11000010a01bdb9', 'fvburger', 0),
(23, 'steam:11000010a01bdb9', 'ephedrine', 0),
(24, 'steam:11000010a01bdb9', 'coke_pooch', 0),
(25, 'steam:11000010a01bdb9', 'medikit', 20),
(26, 'steam:11000010a01bdb9', 'beer', 0),
(27, 'steam:11000010a01bdb9', 'pastacarbonara', 0),
(28, 'steam:11000010a01bdb9', 'cigarett', 0),
(29, 'steam:11000010a01bdb9', 'coffee', 20),
(30, 'steam:11000010a01bdb9', 'blowpipe', 0),
(31, 'steam:11000010a01bdb9', 'carotool', 0),
(32, 'steam:11000010a01bdb9', 'cannabis', 0),
(33, 'steam:11000010a01bdb9', 'marabou', 0),
(34, 'steam:11000010a01bdb9', 'pcp', 0),
(35, 'steam:11000010a01bdb9', 'coca', 0),
(36, 'steam:11000010a01bdb9', 'fixtool', 0),
(37, 'steam:11000010a01bdb9', 'vodka', 0),
(38, 'steam:11000010a01bdb9', 'narcan', 0),
(39, 'steam:11000010a01bdb9', 'wool', 0),
(40, 'steam:11000010a01bdb9', 'lettuce', 0),
(41, 'steam:11000010a01bdb9', 'marijuana', 0),
(42, 'steam:11000010a01bdb9', 'wood', 0),
(43, 'steam:11000010a01bdb9', 'meth', 0),
(44, 'steam:11000010a01bdb9', 'meth_pooch', 0),
(45, 'steam:11000010a01bdb9', 'copper', 0),
(46, 'steam:11000010a01bdb9', 'weed_pooch', 0),
(47, 'steam:11000010a01bdb9', 'essence', 0),
(48, 'steam:11000010a01bdb9', 'weed', 0),
(49, 'steam:11000010a01bdb9', 'fabric', 0),
(50, 'steam:11000010a01bdb9', 'cocacola', 0),
(51, 'steam:11000010a01bdb9', 'water', 0),
(52, 'steam:11000010a01bdb9', 'vhamburger', 0),
(53, 'steam:11000010a01bdb9', 'tomato', 0),
(54, 'steam:11000010a01bdb9', 'poppy', 0),
(55, 'steam:11000010a01bdb9', 'breathalyzer', 0),
(56, 'steam:11000010a01bdb9', 'tequila', 0),
(57, 'steam:11000010a01bdb9', 'fanta', 0),
(58, 'steam:11000010a01bdb9', 'sprite', 0),
(59, 'steam:11000010a01bdb9', 'clothe', 0),
(60, 'steam:11000010a01bdb9', 'armour', 10),
(61, 'steam:11000010a01bdb9', 'donut', 20),
(62, 'steam:11000010a01bdb9', 'potato', 0),
(63, 'steam:11000010a01bdb9', 'shotgun_shells', 0),
(64, 'steam:11000010a01bdb9', 'whiskey', 0),
(65, 'steam:11000010a01bdb9', 'radio', 1),
(66, 'steam:11000010a01bdb9', 'jewels', 0),
(67, 'steam:11000010a01bdb9', 'chips', 0),
(68, 'steam:11000010a01bdb9', 'bandage', 0),
(69, 'steam:11000010a01bdb9', 'stone', 0),
(70, 'steam:11000010a01bdb9', 'packaged_plank', 0),
(71, 'steam:11000010a01bdb9', 'fburger', 0),
(72, 'steam:11000010a01bdb9', 'packaged_chicken', 0),
(73, 'steam:11000010a01bdb9', 'opium', 0),
(74, 'steam:11000010a01bdb9', 'coke', 0),
(75, 'steam:11000010a01bdb9', 'lotteryticket', 0),
(76, 'steam:11000010a01bdb9', 'cheesebows', 9),
(77, 'steam:11000010a01bdb9', 'ephedra', 0),
(78, 'steam:11000010a01bdb9', 'lighter', 0),
(79, 'steam:11000010a01bdb9', 'nuggets4', 0),
(80, 'steam:11000010a01bdb9', 'alive_chicken', 0),
(81, 'steam:11000010a01bdb9', 'painkiller', 0),
(82, 'steam:11000010a01bdb9', 'macka', 0),
(83, 'steam:11000010a01bdb9', 'ccheese', 0),
(84, 'steam:11000010a01bdb9', 'slaughtered_chicken', 0),
(85, 'steam:11000010a01bdb9', 'carokit', 0),
(86, 'steam:11000010a01bdb9', 'clip', 20),
(87, 'steam:11000010a01bdb9', 'burger', 0),
(88, 'steam:11000010a01bdb9', 'loka', 9),
(89, 'steam:11000010a01bdb9', 'nuggets10', 0),
(90, 'steam:11000010a01bdb9', 'crack', 0),
(91, 'steam:11000010a01bdb9', 'petrol_raffin', 0),
(92, 'steam:11000010a01bdb9', 'clettuce', 0),
(93, 'steam:11000010a01bdb9', 'shamburger', 0),
(94, 'steam:1100001081d1893', 'fish', 0),
(95, 'steam:1100001081d1893', 'iron', 0),
(96, 'steam:1100001081d1893', 'nugget', 0),
(97, 'steam:1100001081d1893', 'cheese', 0),
(98, 'steam:1100001081d1893', 'pizza', 0),
(99, 'steam:1100001081d1893', 'fixkit', 4),
(100, 'steam:1100001081d1893', 'fakepee', 0),
(101, 'steam:1100001081d1893', 'drugtest', 0),
(102, 'steam:1100001081d1893', 'dabs', 0),
(103, 'steam:1100001081d1893', 'petrol', 0),
(104, 'steam:1100001081d1893', 'ctomato', 0),
(105, 'steam:1100001081d1893', 'washed_stone', 0),
(106, 'steam:1100001081d1893', 'heroine', 0),
(107, 'steam:1100001081d1893', 'cutted_wood', 0),
(108, 'steam:1100001081d1893', 'gazbottle', 0),
(109, 'steam:1100001081d1893', 'bag', 0),
(110, 'steam:1100001081d1893', 'gold', 0),
(111, 'steam:1100001081d1893', 'diamond', 0),
(112, 'steam:1100001081d1893', 'vbread', 0),
(113, 'steam:1100001081d1893', 'opium_pooch', 0),
(114, 'steam:1100001081d1893', 'bread', 0),
(115, 'steam:1100001081d1893', 'fvburger', 0),
(116, 'steam:1100001081d1893', 'ephedrine', 0),
(117, 'steam:1100001081d1893', 'coke_pooch', 0),
(118, 'steam:1100001081d1893', 'medikit', 3),
(119, 'steam:1100001081d1893', 'pastacarbonara', 0),
(120, 'steam:1100001081d1893', 'cigarett', 0),
(121, 'steam:1100001081d1893', 'beer', 0),
(122, 'steam:1100001081d1893', 'carotool', 0),
(123, 'steam:1100001081d1893', 'coffee', 9),
(124, 'steam:1100001081d1893', 'blowpipe', 0),
(125, 'steam:1100001081d1893', 'cannabis', 0),
(126, 'steam:1100001081d1893', 'coca', 0),
(127, 'steam:1100001081d1893', 'marabou', 0),
(128, 'steam:1100001081d1893', 'fixtool', 0),
(129, 'steam:1100001081d1893', 'pcp', 0),
(130, 'steam:1100001081d1893', 'narcan', 0),
(131, 'steam:1100001081d1893', 'vodka', 0),
(132, 'steam:1100001081d1893', 'wool', 0),
(133, 'steam:1100001081d1893', 'lettuce', 0),
(134, 'steam:1100001081d1893', 'marijuana', 0),
(135, 'steam:1100001081d1893', 'wood', 0),
(136, 'steam:1100001081d1893', 'meth', 0),
(137, 'steam:1100001081d1893', 'meth_pooch', 0),
(138, 'steam:1100001081d1893', 'copper', 0),
(139, 'steam:1100001081d1893', 'weed_pooch', 0),
(140, 'steam:1100001081d1893', 'weed', 0),
(141, 'steam:1100001081d1893', 'essence', 0),
(142, 'steam:1100001081d1893', 'fabric', 0),
(143, 'steam:1100001081d1893', 'cocacola', 0),
(144, 'steam:1100001081d1893', 'water', 0),
(145, 'steam:1100001081d1893', 'vhamburger', 0),
(146, 'steam:1100001081d1893', 'poppy', 0),
(147, 'steam:1100001081d1893', 'tomato', 0),
(148, 'steam:1100001081d1893', 'tequila', 0),
(149, 'steam:1100001081d1893', 'breathalyzer', 0),
(150, 'steam:1100001081d1893', 'fanta', 0),
(151, 'steam:1100001081d1893', 'sprite', 0),
(152, 'steam:1100001081d1893', 'armour', 3),
(153, 'steam:1100001081d1893', 'clothe', 0),
(154, 'steam:1100001081d1893', 'donut', 9),
(155, 'steam:1100001081d1893', 'potato', 0),
(156, 'steam:1100001081d1893', 'shotgun_shells', 0),
(157, 'steam:1100001081d1893', 'whiskey', 0),
(158, 'steam:1100001081d1893', 'radio', 1),
(159, 'steam:1100001081d1893', 'jewels', 0),
(160, 'steam:1100001081d1893', 'chips', 0),
(161, 'steam:1100001081d1893', 'bandage', 0),
(162, 'steam:1100001081d1893', 'stone', 0),
(163, 'steam:1100001081d1893', 'packaged_plank', 0),
(164, 'steam:1100001081d1893', 'packaged_chicken', 0),
(165, 'steam:1100001081d1893', 'fburger', 0),
(166, 'steam:1100001081d1893', 'opium', 0),
(167, 'steam:1100001081d1893', 'coke', 0),
(168, 'steam:1100001081d1893', 'lotteryticket', 0),
(169, 'steam:1100001081d1893', 'cheesebows', 0),
(170, 'steam:1100001081d1893', 'ephedra', 0),
(171, 'steam:1100001081d1893', 'nuggets4', 0),
(172, 'steam:1100001081d1893', 'lighter', 0),
(173, 'steam:1100001081d1893', 'alive_chicken', 0),
(174, 'steam:1100001081d1893', 'macka', 0),
(175, 'steam:1100001081d1893', 'slaughtered_chicken', 0),
(176, 'steam:1100001081d1893', 'painkiller', 0),
(177, 'steam:1100001081d1893', 'ccheese', 0),
(178, 'steam:1100001081d1893', 'carokit', 0),
(179, 'steam:1100001081d1893', 'clip', 5),
(180, 'steam:1100001081d1893', 'burger', 0),
(181, 'steam:1100001081d1893', 'loka', 0),
(182, 'steam:1100001081d1893', 'nuggets10', 0),
(183, 'steam:1100001081d1893', 'crack', 0),
(184, 'steam:1100001081d1893', 'petrol_raffin', 0),
(185, 'steam:1100001081d1893', 'clettuce', 0),
(186, 'steam:1100001081d1893', 'shamburger', 0),
(187, 'steam:110000133d93ea2', 'fish', 0),
(188, 'steam:110000133d93ea2', 'iron', 0),
(189, 'steam:110000133d93ea2', 'nugget', 0),
(190, 'steam:110000133d93ea2', 'cheese', 0),
(191, 'steam:110000133d93ea2', 'pizza', 0),
(192, 'steam:110000133d93ea2', 'fixkit', 0),
(193, 'steam:110000133d93ea2', 'fakepee', 0),
(194, 'steam:110000133d93ea2', 'petrol', 0),
(195, 'steam:110000133d93ea2', 'drugtest', 0),
(196, 'steam:110000133d93ea2', 'dabs', 0),
(197, 'steam:110000133d93ea2', 'ctomato', 0),
(198, 'steam:110000133d93ea2', 'cutted_wood', 0),
(199, 'steam:110000133d93ea2', 'washed_stone', 0),
(200, 'steam:110000133d93ea2', 'gazbottle', 0),
(201, 'steam:110000133d93ea2', 'heroine', 0),
(202, 'steam:110000133d93ea2', 'bag', 0),
(203, 'steam:110000133d93ea2', 'gold', 0),
(204, 'steam:110000133d93ea2', 'diamond', 0),
(205, 'steam:110000133d93ea2', 'vbread', 0),
(206, 'steam:110000133d93ea2', 'opium_pooch', 0),
(207, 'steam:110000133d93ea2', 'bread', 48),
(208, 'steam:110000133d93ea2', 'fvburger', 0),
(209, 'steam:110000133d93ea2', 'medikit', 0),
(210, 'steam:110000133d93ea2', 'ephedrine', 0),
(211, 'steam:110000133d93ea2', 'coke_pooch', 0),
(212, 'steam:110000133d93ea2', 'beer', 0),
(213, 'steam:110000133d93ea2', 'pastacarbonara', 0),
(214, 'steam:110000133d93ea2', 'cigarett', 0),
(215, 'steam:110000133d93ea2', 'carotool', 0),
(216, 'steam:110000133d93ea2', 'coffee', 0),
(217, 'steam:110000133d93ea2', 'cannabis', 0),
(218, 'steam:110000133d93ea2', 'blowpipe', 0),
(219, 'steam:110000133d93ea2', 'coca', 0),
(220, 'steam:110000133d93ea2', 'fixtool', 0),
(221, 'steam:110000133d93ea2', 'marabou', 0),
(222, 'steam:110000133d93ea2', 'pcp', 0),
(223, 'steam:110000133d93ea2', 'narcan', 0),
(224, 'steam:110000133d93ea2', 'vodka', 0),
(225, 'steam:110000133d93ea2', 'wool', 0),
(226, 'steam:110000133d93ea2', 'lettuce', 0),
(227, 'steam:110000133d93ea2', 'marijuana', 0),
(228, 'steam:110000133d93ea2', 'wood', 0),
(229, 'steam:110000133d93ea2', 'meth', 0),
(230, 'steam:110000133d93ea2', 'meth_pooch', 0),
(231, 'steam:110000133d93ea2', 'copper', 0),
(232, 'steam:110000133d93ea2', 'weed_pooch', 0),
(233, 'steam:110000133d93ea2', 'essence', 0),
(234, 'steam:110000133d93ea2', 'weed', 0),
(235, 'steam:110000133d93ea2', 'fabric', 0),
(236, 'steam:110000133d93ea2', 'vhamburger', 0),
(237, 'steam:110000133d93ea2', 'cocacola', 48),
(238, 'steam:110000133d93ea2', 'water', 0),
(239, 'steam:110000133d93ea2', 'poppy', 0),
(240, 'steam:110000133d93ea2', 'tequila', 0),
(241, 'steam:110000133d93ea2', 'tomato', 0),
(242, 'steam:110000133d93ea2', 'fanta', 0),
(243, 'steam:110000133d93ea2', 'breathalyzer', 0),
(244, 'steam:110000133d93ea2', 'clothe', 0),
(245, 'steam:110000133d93ea2', 'sprite', 0),
(246, 'steam:110000133d93ea2', 'donut', 0),
(247, 'steam:110000133d93ea2', 'armour', 0),
(248, 'steam:110000133d93ea2', 'potato', 0),
(249, 'steam:110000133d93ea2', 'shotgun_shells', 0),
(250, 'steam:110000133d93ea2', 'whiskey', 0),
(251, 'steam:110000133d93ea2', 'radio', 2),
(252, 'steam:110000133d93ea2', 'jewels', 0),
(253, 'steam:110000133d93ea2', 'chips', 0),
(254, 'steam:110000133d93ea2', 'stone', 0),
(255, 'steam:110000133d93ea2', 'packaged_plank', 0),
(256, 'steam:110000133d93ea2', 'bandage', 0),
(257, 'steam:110000133d93ea2', 'fburger', 0),
(258, 'steam:110000133d93ea2', 'packaged_chicken', 0),
(259, 'steam:110000133d93ea2', 'opium', 0),
(260, 'steam:110000133d93ea2', 'coke', 0),
(261, 'steam:110000133d93ea2', 'lotteryticket', 0),
(262, 'steam:110000133d93ea2', 'ephedra', 0),
(263, 'steam:110000133d93ea2', 'cheesebows', 0),
(264, 'steam:110000133d93ea2', 'lighter', 0),
(265, 'steam:110000133d93ea2', 'nuggets4', 0),
(266, 'steam:110000133d93ea2', 'alive_chicken', 0),
(267, 'steam:110000133d93ea2', 'macka', 0),
(268, 'steam:110000133d93ea2', 'slaughtered_chicken', 0),
(269, 'steam:110000133d93ea2', 'painkiller', 0),
(270, 'steam:110000133d93ea2', 'carokit', 0),
(271, 'steam:110000133d93ea2', 'clip', 0),
(272, 'steam:110000133d93ea2', 'burger', 0),
(273, 'steam:110000133d93ea2', 'loka', 0),
(274, 'steam:110000133d93ea2', 'ccheese', 0),
(275, 'steam:110000133d93ea2', 'nuggets10', 0),
(276, 'steam:110000133d93ea2', 'crack', 0),
(277, 'steam:110000133d93ea2', 'petrol_raffin', 0),
(278, 'steam:110000133d93ea2', 'clettuce', 0),
(279, 'steam:110000133d93ea2', 'shamburger', 0),
(280, 'steam:1100001139319c1', 'fish', 0),
(281, 'steam:1100001139319c1', 'iron', 0),
(282, 'steam:1100001139319c1', 'nugget', 0),
(283, 'steam:1100001139319c1', 'cheese', 0),
(284, 'steam:1100001139319c1', 'pizza', 0),
(285, 'steam:1100001139319c1', 'fixkit', 65),
(286, 'steam:1100001139319c1', 'fakepee', 0),
(287, 'steam:1100001139319c1', 'drugtest', 0),
(288, 'steam:1100001139319c1', 'petrol', 0),
(289, 'steam:1100001139319c1', 'dabs', 0),
(290, 'steam:1100001139319c1', 'ctomato', 0),
(291, 'steam:1100001139319c1', 'cutted_wood', 0),
(292, 'steam:1100001139319c1', 'washed_stone', 0),
(293, 'steam:1100001139319c1', 'heroine', 0),
(294, 'steam:1100001139319c1', 'gazbottle', 0),
(295, 'steam:1100001139319c1', 'bag', 0),
(296, 'steam:1100001139319c1', 'gold', 0),
(297, 'steam:1100001139319c1', 'diamond', 0),
(298, 'steam:1100001139319c1', 'vbread', 0),
(299, 'steam:1100001139319c1', 'opium_pooch', 0),
(300, 'steam:1100001139319c1', 'bread', 19),
(301, 'steam:1100001139319c1', 'fvburger', 0),
(302, 'steam:1100001139319c1', 'ephedrine', 0),
(303, 'steam:1100001139319c1', 'coke_pooch', 0),
(304, 'steam:1100001139319c1', 'medikit', 29),
(305, 'steam:1100001139319c1', 'beer', 0),
(306, 'steam:1100001139319c1', 'pastacarbonara', 0),
(307, 'steam:1100001139319c1', 'carotool', 0),
(308, 'steam:1100001139319c1', 'coffee', 9),
(309, 'steam:1100001139319c1', 'cigarett', 0),
(310, 'steam:1100001139319c1', 'blowpipe', 0),
(311, 'steam:1100001139319c1', 'cannabis', 0),
(312, 'steam:1100001139319c1', 'fixtool', 0),
(313, 'steam:1100001139319c1', 'marabou', 0),
(314, 'steam:1100001139319c1', 'pcp', 0),
(315, 'steam:1100001139319c1', 'narcan', 0),
(316, 'steam:1100001139319c1', 'coca', 0),
(317, 'steam:1100001139319c1', 'vodka', 0),
(318, 'steam:1100001139319c1', 'wool', 0),
(319, 'steam:1100001139319c1', 'lettuce', 0),
(320, 'steam:1100001139319c1', 'marijuana', 0),
(321, 'steam:1100001139319c1', 'wood', 0),
(322, 'steam:1100001139319c1', 'meth', 0),
(323, 'steam:1100001139319c1', 'meth_pooch', 0),
(324, 'steam:1100001139319c1', 'copper', 0),
(325, 'steam:1100001139319c1', 'weed_pooch', 0),
(326, 'steam:1100001139319c1', 'essence', 0),
(327, 'steam:1100001139319c1', 'weed', 0),
(328, 'steam:1100001139319c1', 'fabric', 0),
(329, 'steam:1100001139319c1', 'cocacola', 0),
(330, 'steam:1100001139319c1', 'water', 10),
(331, 'steam:1100001139319c1', 'vhamburger', 0),
(332, 'steam:1100001139319c1', 'poppy', 0),
(333, 'steam:1100001139319c1', 'tomato', 0),
(334, 'steam:1100001139319c1', 'tequila', 0),
(335, 'steam:1100001139319c1', 'breathalyzer', 0),
(336, 'steam:1100001139319c1', 'fanta', 0),
(337, 'steam:1100001139319c1', 'sprite', 0),
(338, 'steam:1100001139319c1', 'clothe', 0),
(339, 'steam:1100001139319c1', 'armour', 28),
(340, 'steam:1100001139319c1', 'donut', 0),
(341, 'steam:1100001139319c1', 'potato', 0),
(342, 'steam:1100001139319c1', 'shotgun_shells', 0),
(343, 'steam:1100001139319c1', 'whiskey', 0),
(344, 'steam:1100001139319c1', 'radio', 1),
(345, 'steam:1100001139319c1', 'jewels', 0),
(346, 'steam:1100001139319c1', 'chips', 0),
(347, 'steam:1100001139319c1', 'stone', 0),
(348, 'steam:1100001139319c1', 'packaged_plank', 0),
(349, 'steam:1100001139319c1', 'fburger', 0),
(350, 'steam:1100001139319c1', 'bandage', 0),
(351, 'steam:1100001139319c1', 'packaged_chicken', 0),
(352, 'steam:1100001139319c1', 'opium', 0),
(353, 'steam:1100001139319c1', 'coke', 0),
(354, 'steam:1100001139319c1', 'lotteryticket', 0),
(355, 'steam:1100001139319c1', 'cheesebows', 0),
(356, 'steam:1100001139319c1', 'ephedra', 0),
(357, 'steam:1100001139319c1', 'lighter', 0),
(358, 'steam:1100001139319c1', 'nuggets4', 0),
(359, 'steam:1100001139319c1', 'alive_chicken', 0),
(360, 'steam:1100001139319c1', 'macka', 0),
(361, 'steam:1100001139319c1', 'slaughtered_chicken', 0),
(362, 'steam:1100001139319c1', 'painkiller', 0),
(363, 'steam:1100001139319c1', 'carokit', 0),
(364, 'steam:1100001139319c1', 'clip', 30),
(365, 'steam:1100001139319c1', 'ccheese', 0),
(366, 'steam:1100001139319c1', 'burger', 0),
(367, 'steam:1100001139319c1', 'loka', 0),
(368, 'steam:1100001139319c1', 'nuggets10', 0),
(369, 'steam:1100001139319c1', 'petrol_raffin', 0),
(370, 'steam:1100001139319c1', 'crack', 0),
(371, 'steam:1100001139319c1', 'clettuce', 0),
(372, 'steam:1100001139319c1', 'shamburger', 0),
(373, 'steam:1100001156cfac9', 'petrol', 0),
(374, 'steam:1100001156cfac9', 'lighter', 0),
(375, 'steam:1100001156cfac9', 'alive_chicken', 0),
(376, 'steam:1100001156cfac9', 'petrol_raffin', 0),
(377, 'steam:1100001156cfac9', 'ephedrine', 0),
(378, 'steam:1100001156cfac9', 'diamond', 0),
(379, 'steam:1100001156cfac9', 'heroine', 0),
(380, 'steam:1100001156cfac9', 'pizza', 0),
(381, 'steam:1100001156cfac9', 'fabric', 0),
(382, 'steam:1100001156cfac9', 'donut', 0),
(383, 'steam:1100001156cfac9', 'burger', 0),
(384, 'steam:1100001156cfac9', 'cannabis', 0),
(385, 'steam:1100001156cfac9', 'pcp', 0),
(386, 'steam:1100001156cfac9', 'coke_pooch', 0),
(387, 'steam:1100001156cfac9', 'poppy', 0),
(388, 'steam:1100001156cfac9', 'iron', 0),
(389, 'steam:1100001156cfac9', 'loka', 0),
(390, 'steam:1100001156cfac9', 'fixkit', 0),
(391, 'steam:1100001156cfac9', 'cigarett', 0),
(392, 'steam:1100001156cfac9', 'fburger', 0),
(393, 'steam:1100001156cfac9', 'meth_pooch', 0),
(394, 'steam:1100001156cfac9', 'gazbottle', 0),
(395, 'steam:1100001156cfac9', 'fanta', 0),
(396, 'steam:1100001156cfac9', 'drugtest', 0),
(397, 'steam:1100001156cfac9', 'copper', 0),
(398, 'steam:1100001156cfac9', 'carotool', 0),
(399, 'steam:1100001156cfac9', 'coca', 0),
(400, 'steam:1100001156cfac9', 'macka', 0),
(401, 'steam:1100001156cfac9', 'chips', 0),
(402, 'steam:1100001156cfac9', 'wool', 0),
(403, 'steam:1100001156cfac9', 'wood', 0),
(404, 'steam:1100001156cfac9', 'lotteryticket', 0),
(405, 'steam:1100001156cfac9', 'potato', 0),
(406, 'steam:1100001156cfac9', 'weed_pooch', 0),
(407, 'steam:1100001156cfac9', 'fish', 0),
(408, 'steam:1100001156cfac9', 'ephedra', 0),
(409, 'steam:1100001156cfac9', 'clothe', 0),
(410, 'steam:1100001156cfac9', 'cocacola', 0),
(411, 'steam:1100001156cfac9', 'tomato', 0),
(412, 'steam:1100001156cfac9', 'shotgun_shells', 0),
(413, 'steam:1100001156cfac9', 'weed', 0),
(414, 'steam:1100001156cfac9', 'water', 0),
(415, 'steam:1100001156cfac9', 'beer', 0),
(416, 'steam:1100001156cfac9', 'whiskey', 0),
(417, 'steam:1100001156cfac9', 'vodka', 0),
(418, 'steam:1100001156cfac9', 'vhamburger', 0),
(419, 'steam:1100001156cfac9', 'vbread', 0),
(420, 'steam:1100001156cfac9', 'fakepee', 0),
(421, 'steam:1100001156cfac9', 'fixtool', 0),
(422, 'steam:1100001156cfac9', 'marijuana', 0),
(423, 'steam:1100001156cfac9', 'crack', 0),
(424, 'steam:1100001156cfac9', 'stone', 0),
(425, 'steam:1100001156cfac9', 'sprite', 0),
(426, 'steam:1100001156cfac9', 'carokit', 0),
(427, 'steam:1100001156cfac9', 'dabs', 0),
(428, 'steam:1100001156cfac9', 'shamburger', 0),
(429, 'steam:1100001156cfac9', 'radio', 0),
(430, 'steam:1100001156cfac9', 'washed_stone', 0),
(431, 'steam:1100001156cfac9', 'opium', 0),
(432, 'steam:1100001156cfac9', 'essence', 0),
(433, 'steam:1100001156cfac9', 'pastacarbonara', 0),
(434, 'steam:1100001156cfac9', 'painkiller', 0),
(435, 'steam:1100001156cfac9', 'meth', 0),
(436, 'steam:1100001156cfac9', 'ctomato', 0),
(437, 'steam:1100001156cfac9', 'packaged_plank', 0),
(438, 'steam:1100001156cfac9', 'medikit', 0),
(439, 'steam:1100001156cfac9', 'cheese', 0),
(440, 'steam:1100001156cfac9', 'packaged_chicken', 0),
(441, 'steam:1100001156cfac9', 'opium_pooch', 0),
(442, 'steam:1100001156cfac9', 'armour', 0),
(443, 'steam:1100001156cfac9', 'nuggets4', 0),
(444, 'steam:1100001156cfac9', 'bandage', 0),
(445, 'steam:1100001156cfac9', 'nuggets10', 0),
(446, 'steam:1100001156cfac9', 'blowpipe', 0),
(447, 'steam:1100001156cfac9', 'cutted_wood', 0),
(448, 'steam:1100001156cfac9', 'bread', 0),
(449, 'steam:1100001156cfac9', 'narcan', 0),
(450, 'steam:1100001156cfac9', 'clettuce', 0),
(451, 'steam:1100001156cfac9', 'nugget', 0),
(452, 'steam:1100001156cfac9', 'clip', 0),
(453, 'steam:1100001156cfac9', 'marabou', 0),
(454, 'steam:1100001156cfac9', 'cheesebows', 0),
(455, 'steam:1100001156cfac9', 'coke', 0),
(456, 'steam:1100001156cfac9', 'fvburger', 0),
(457, 'steam:1100001156cfac9', 'bag', 0),
(458, 'steam:1100001156cfac9', 'gold', 0),
(459, 'steam:1100001156cfac9', 'breathalyzer', 0),
(460, 'steam:1100001156cfac9', 'slaughtered_chicken', 0),
(461, 'steam:1100001156cfac9', 'tequila', 0),
(462, 'steam:1100001156cfac9', 'ccheese', 0),
(463, 'steam:1100001156cfac9', 'jewels', 0),
(464, 'steam:1100001156cfac9', 'coffee', 0),
(465, 'steam:1100001156cfac9', 'lettuce', 0),
(466, 'steam:11000013ea40bc0', 'petrol', 0),
(467, 'steam:11000013ea40bc0', 'lighter', 0),
(468, 'steam:11000013ea40bc0', 'alive_chicken', 0),
(469, 'steam:11000013ea40bc0', 'petrol_raffin', 0),
(470, 'steam:11000013ea40bc0', 'ephedrine', 0),
(471, 'steam:11000013ea40bc0', 'diamond', 0),
(472, 'steam:11000013ea40bc0', 'heroine', 0),
(473, 'steam:11000013ea40bc0', 'pizza', 50),
(474, 'steam:11000013ea40bc0', 'fabric', 0),
(475, 'steam:11000013ea40bc0', 'burger', 0),
(476, 'steam:11000013ea40bc0', 'donut', 0),
(477, 'steam:11000013ea40bc0', 'cannabis', 0),
(478, 'steam:11000013ea40bc0', 'coke_pooch', 0),
(479, 'steam:11000013ea40bc0', 'poppy', 0),
(480, 'steam:11000013ea40bc0', 'pcp', 0),
(481, 'steam:11000013ea40bc0', 'fixkit', 0),
(482, 'steam:11000013ea40bc0', 'iron', 0),
(483, 'steam:11000013ea40bc0', 'loka', 0),
(484, 'steam:11000013ea40bc0', 'fburger', 0),
(485, 'steam:11000013ea40bc0', 'cigarett', 0),
(486, 'steam:11000013ea40bc0', 'gazbottle', 0),
(487, 'steam:11000013ea40bc0', 'carotool', 0),
(488, 'steam:11000013ea40bc0', 'meth_pooch', 0),
(489, 'steam:11000013ea40bc0', 'copper', 0),
(490, 'steam:11000013ea40bc0', 'fanta', 0),
(491, 'steam:11000013ea40bc0', 'drugtest', 0),
(492, 'steam:11000013ea40bc0', 'coca', 0),
(493, 'steam:11000013ea40bc0', 'macka', 0),
(494, 'steam:11000013ea40bc0', 'wool', 0),
(495, 'steam:11000013ea40bc0', 'wood', 0),
(496, 'steam:11000013ea40bc0', 'chips', 0),
(497, 'steam:11000013ea40bc0', 'lotteryticket', 0),
(498, 'steam:11000013ea40bc0', 'potato', 0),
(499, 'steam:11000013ea40bc0', 'fish', 0),
(500, 'steam:11000013ea40bc0', 'weed_pooch', 0),
(501, 'steam:11000013ea40bc0', 'clothe', 0),
(502, 'steam:11000013ea40bc0', 'ephedra', 0),
(503, 'steam:11000013ea40bc0', 'cocacola', 12),
(504, 'steam:11000013ea40bc0', 'shotgun_shells', 0),
(505, 'steam:11000013ea40bc0', 'tomato', 0),
(506, 'steam:11000013ea40bc0', 'weed', 0),
(507, 'steam:11000013ea40bc0', 'beer', 0),
(508, 'steam:11000013ea40bc0', 'water', 0),
(509, 'steam:11000013ea40bc0', 'vodka', 0),
(510, 'steam:11000013ea40bc0', 'whiskey', 0),
(511, 'steam:11000013ea40bc0', 'vhamburger', 0),
(512, 'steam:11000013ea40bc0', 'vbread', 0),
(513, 'steam:11000013ea40bc0', 'fixtool', 0),
(514, 'steam:11000013ea40bc0', 'fakepee', 0),
(515, 'steam:11000013ea40bc0', 'crack', 0),
(516, 'steam:11000013ea40bc0', 'marijuana', 0),
(517, 'steam:11000013ea40bc0', 'stone', 0),
(518, 'steam:11000013ea40bc0', 'sprite', 50),
(519, 'steam:11000013ea40bc0', 'carokit', 0),
(520, 'steam:11000013ea40bc0', 'dabs', 0),
(521, 'steam:11000013ea40bc0', 'shamburger', 0),
(522, 'steam:11000013ea40bc0', 'washed_stone', 0),
(523, 'steam:11000013ea40bc0', 'radio', 0),
(524, 'steam:11000013ea40bc0', 'opium', 0),
(525, 'steam:11000013ea40bc0', 'essence', 0),
(526, 'steam:11000013ea40bc0', 'pastacarbonara', 0),
(527, 'steam:11000013ea40bc0', 'painkiller', 0),
(528, 'steam:11000013ea40bc0', 'meth', 0),
(529, 'steam:11000013ea40bc0', 'ctomato', 0),
(530, 'steam:11000013ea40bc0', 'packaged_plank', 0),
(531, 'steam:11000013ea40bc0', 'medikit', 0),
(532, 'steam:11000013ea40bc0', 'cheese', 0),
(533, 'steam:11000013ea40bc0', 'packaged_chicken', 0),
(534, 'steam:11000013ea40bc0', 'opium_pooch', 0),
(535, 'steam:11000013ea40bc0', 'armour', 0),
(536, 'steam:11000013ea40bc0', 'nuggets4', 0),
(537, 'steam:11000013ea40bc0', 'bandage', 0),
(538, 'steam:11000013ea40bc0', 'nuggets10', 0),
(539, 'steam:11000013ea40bc0', 'blowpipe', 0),
(540, 'steam:11000013ea40bc0', 'nugget', 0),
(541, 'steam:11000013ea40bc0', 'clettuce', 0),
(542, 'steam:11000013ea40bc0', 'bread', 0),
(543, 'steam:11000013ea40bc0', 'narcan', 0),
(544, 'steam:11000013ea40bc0', 'cutted_wood', 0),
(545, 'steam:11000013ea40bc0', 'clip', 0),
(546, 'steam:11000013ea40bc0', 'cheesebows', 9),
(547, 'steam:11000013ea40bc0', 'marabou', 0),
(548, 'steam:11000013ea40bc0', 'fvburger', 0),
(549, 'steam:11000013ea40bc0', 'coke', 0),
(550, 'steam:11000013ea40bc0', 'bag', 0),
(551, 'steam:11000013ea40bc0', 'gold', 0),
(552, 'steam:11000013ea40bc0', 'slaughtered_chicken', 0),
(553, 'steam:11000013ea40bc0', 'breathalyzer', 0),
(554, 'steam:11000013ea40bc0', 'tequila', 0),
(555, 'steam:11000013ea40bc0', 'coffee', 0),
(556, 'steam:11000013ea40bc0', 'jewels', 0),
(557, 'steam:11000013ea40bc0', 'lettuce', 0),
(558, 'steam:11000013ea40bc0', 'ccheese', 0),
(559, 'steam:110000106197c1a', 'petrol', 0),
(560, 'steam:110000106197c1a', 'lighter', 0),
(561, 'steam:110000106197c1a', 'alive_chicken', 0),
(562, 'steam:110000106197c1a', 'petrol_raffin', 0),
(563, 'steam:110000106197c1a', 'ephedrine', 0),
(564, 'steam:110000106197c1a', 'diamond', 0),
(565, 'steam:110000106197c1a', 'heroine', 0),
(566, 'steam:110000106197c1a', 'pizza', 0),
(567, 'steam:110000106197c1a', 'fabric', 0),
(568, 'steam:110000106197c1a', 'donut', 0),
(569, 'steam:110000106197c1a', 'burger', 0),
(570, 'steam:110000106197c1a', 'cannabis', 0),
(571, 'steam:110000106197c1a', 'pcp', 0),
(572, 'steam:110000106197c1a', 'poppy', 0),
(573, 'steam:110000106197c1a', 'coke_pooch', 0),
(574, 'steam:110000106197c1a', 'fixkit', 0),
(575, 'steam:110000106197c1a', 'loka', 0),
(576, 'steam:110000106197c1a', 'fburger', 0),
(577, 'steam:110000106197c1a', 'iron', 0),
(578, 'steam:110000106197c1a', 'cigarett', 0),
(579, 'steam:110000106197c1a', 'gazbottle', 0),
(580, 'steam:110000106197c1a', 'meth_pooch', 0),
(581, 'steam:110000106197c1a', 'fanta', 0),
(582, 'steam:110000106197c1a', 'drugtest', 0),
(583, 'steam:110000106197c1a', 'copper', 0),
(584, 'steam:110000106197c1a', 'coca', 0),
(585, 'steam:110000106197c1a', 'carotool', 0),
(586, 'steam:110000106197c1a', 'macka', 0),
(587, 'steam:110000106197c1a', 'chips', 0),
(588, 'steam:110000106197c1a', 'wool', 0),
(589, 'steam:110000106197c1a', 'wood', 0),
(590, 'steam:110000106197c1a', 'lotteryticket', 0),
(591, 'steam:110000106197c1a', 'potato', 0),
(592, 'steam:110000106197c1a', 'fish', 0),
(593, 'steam:110000106197c1a', 'weed_pooch', 0),
(594, 'steam:110000106197c1a', 'clothe', 0),
(595, 'steam:110000106197c1a', 'ephedra', 0),
(596, 'steam:110000106197c1a', 'cocacola', 0),
(597, 'steam:110000106197c1a', 'shotgun_shells', 0),
(598, 'steam:110000106197c1a', 'tomato', 0),
(599, 'steam:110000106197c1a', 'weed', 0),
(600, 'steam:110000106197c1a', 'water', 0),
(601, 'steam:110000106197c1a', 'beer', 0),
(602, 'steam:110000106197c1a', 'vodka', 0),
(603, 'steam:110000106197c1a', 'whiskey', 0),
(604, 'steam:110000106197c1a', 'vhamburger', 0),
(605, 'steam:110000106197c1a', 'fakepee', 0),
(606, 'steam:110000106197c1a', 'fixtool', 0),
(607, 'steam:110000106197c1a', 'vbread', 0),
(608, 'steam:110000106197c1a', 'marijuana', 0),
(609, 'steam:110000106197c1a', 'crack', 0),
(610, 'steam:110000106197c1a', 'sprite', 0),
(611, 'steam:110000106197c1a', 'stone', 0),
(612, 'steam:110000106197c1a', 'carokit', 0),
(613, 'steam:110000106197c1a', 'dabs', 0),
(614, 'steam:110000106197c1a', 'shamburger', 0),
(615, 'steam:110000106197c1a', 'washed_stone', 0),
(616, 'steam:110000106197c1a', 'radio', 0),
(617, 'steam:110000106197c1a', 'opium', 0),
(618, 'steam:110000106197c1a', 'essence', 0),
(619, 'steam:110000106197c1a', 'pastacarbonara', 0),
(620, 'steam:110000106197c1a', 'painkiller', 0),
(621, 'steam:110000106197c1a', 'meth', 0),
(622, 'steam:110000106197c1a', 'ctomato', 0),
(623, 'steam:110000106197c1a', 'medikit', 0),
(624, 'steam:110000106197c1a', 'packaged_plank', 0),
(625, 'steam:110000106197c1a', 'cheese', 0),
(626, 'steam:110000106197c1a', 'packaged_chicken', 0),
(627, 'steam:110000106197c1a', 'opium_pooch', 0),
(628, 'steam:110000106197c1a', 'armour', 0),
(629, 'steam:110000106197c1a', 'nuggets4', 0),
(630, 'steam:110000106197c1a', 'bandage', 0),
(631, 'steam:110000106197c1a', 'nuggets10', 0),
(632, 'steam:110000106197c1a', 'blowpipe', 0),
(633, 'steam:110000106197c1a', 'cutted_wood', 0),
(634, 'steam:110000106197c1a', 'nugget', 0),
(635, 'steam:110000106197c1a', 'bread', 0),
(636, 'steam:110000106197c1a', 'narcan', 0),
(637, 'steam:110000106197c1a', 'clip', 0),
(638, 'steam:110000106197c1a', 'clettuce', 0),
(639, 'steam:110000106197c1a', 'marabou', 0),
(640, 'steam:110000106197c1a', 'cheesebows', 0),
(641, 'steam:110000106197c1a', 'fvburger', 0),
(642, 'steam:110000106197c1a', 'coke', 0),
(643, 'steam:110000106197c1a', 'bag', 0),
(644, 'steam:110000106197c1a', 'gold', 0),
(645, 'steam:110000106197c1a', 'slaughtered_chicken', 0),
(646, 'steam:110000106197c1a', 'tequila', 0),
(647, 'steam:110000106197c1a', 'coffee', 0),
(648, 'steam:110000106197c1a', 'jewels', 0),
(649, 'steam:110000106197c1a', 'breathalyzer', 0),
(650, 'steam:110000106197c1a', 'lettuce', 0),
(651, 'steam:110000106197c1a', 'ccheese', 0),
(652, 'steam:110000116e48a9b', 'pcp', 0),
(653, 'steam:110000116e48a9b', 'crack', 0),
(654, 'steam:110000116e48a9b', 'cocacola', 0),
(655, 'steam:110000116e48a9b', 'vodka', 0),
(656, 'steam:110000116e48a9b', 'washed_stone', 0),
(657, 'steam:110000116e48a9b', 'donut', 0),
(658, 'steam:110000116e48a9b', 'meth', 0),
(659, 'steam:110000116e48a9b', 'fixkit', 0),
(660, 'steam:110000116e48a9b', 'petrol_raffin', 0),
(661, 'steam:110000116e48a9b', 'coffee', 0),
(662, 'steam:110000116e48a9b', 'tomato', 0),
(663, 'steam:110000116e48a9b', 'dabs', 0),
(664, 'steam:110000116e48a9b', 'cheesebows', 0),
(665, 'steam:110000116e48a9b', 'essence', 0),
(666, 'steam:110000116e48a9b', 'lighter', 0),
(667, 'steam:110000116e48a9b', 'beer', 0),
(668, 'steam:110000116e48a9b', 'packaged_chicken', 0),
(669, 'steam:110000116e48a9b', 'iron', 0),
(670, 'steam:110000116e48a9b', 'clothe', 0),
(671, 'steam:110000116e48a9b', 'painkiller', 0),
(672, 'steam:110000116e48a9b', 'weed', 0),
(673, 'steam:110000116e48a9b', 'ephedrine', 0),
(674, 'steam:110000116e48a9b', 'potato', 0),
(675, 'steam:110000116e48a9b', 'narcan', 0),
(676, 'steam:110000116e48a9b', 'nuggets10', 0),
(677, 'steam:110000116e48a9b', 'cannabis', 0),
(678, 'steam:110000116e48a9b', 'chips', 0),
(679, 'steam:110000116e48a9b', 'meth_pooch', 0),
(680, 'steam:110000116e48a9b', 'cheese', 0),
(681, 'steam:110000116e48a9b', 'petrol', 0),
(682, 'steam:110000116e48a9b', 'nuggets4', 0),
(683, 'steam:110000116e48a9b', 'fabric', 0),
(684, 'steam:110000116e48a9b', 'coke_pooch', 0),
(685, 'steam:110000116e48a9b', 'clip', 0),
(686, 'steam:110000116e48a9b', 'blowpipe', 0),
(687, 'steam:110000116e48a9b', 'bread', 0),
(688, 'steam:110000116e48a9b', 'marijuana', 0),
(689, 'steam:110000116e48a9b', 'clettuce', 0),
(690, 'steam:110000116e48a9b', 'armour', 0),
(691, 'steam:110000116e48a9b', 'packaged_plank', 0),
(692, 'steam:110000116e48a9b', 'drugtest', 0),
(693, 'steam:110000116e48a9b', 'bag', 0),
(694, 'steam:110000116e48a9b', 'lettuce', 0),
(695, 'steam:110000116e48a9b', 'medikit', 0),
(696, 'steam:110000116e48a9b', 'nugget', 0),
(697, 'steam:110000116e48a9b', 'gold', 0),
(698, 'steam:110000116e48a9b', 'copper', 0),
(699, 'steam:110000116e48a9b', 'wool', 0),
(700, 'steam:110000116e48a9b', 'shotgun_shells', 0),
(701, 'steam:110000116e48a9b', 'whiskey', 0),
(702, 'steam:110000116e48a9b', 'diamond', 0),
(703, 'steam:110000116e48a9b', 'water', 0),
(704, 'steam:110000116e48a9b', 'vhamburger', 0),
(705, 'steam:110000116e48a9b', 'fish', 0),
(706, 'steam:110000116e48a9b', 'breathalyzer', 0),
(707, 'steam:110000116e48a9b', 'tequila', 0),
(708, 'steam:110000116e48a9b', 'stone', 0),
(709, 'steam:110000116e48a9b', 'sprite', 0),
(710, 'steam:110000116e48a9b', 'fakepee', 0),
(711, 'steam:110000116e48a9b', 'wood', 0),
(712, 'steam:110000116e48a9b', 'shamburger', 0),
(713, 'steam:110000116e48a9b', 'slaughtered_chicken', 0),
(714, 'steam:110000116e48a9b', 'coke', 0),
(715, 'steam:110000116e48a9b', 'poppy', 0),
(716, 'steam:110000116e48a9b', 'fburger', 0),
(717, 'steam:110000116e48a9b', 'pizza', 0),
(718, 'steam:110000116e48a9b', 'macka', 0),
(719, 'steam:110000116e48a9b', 'gazbottle', 0),
(720, 'steam:110000116e48a9b', 'opium', 0),
(721, 'steam:110000116e48a9b', 'burger', 0),
(722, 'steam:110000116e48a9b', 'carotool', 0),
(723, 'steam:110000116e48a9b', 'cutted_wood', 0),
(724, 'steam:110000116e48a9b', 'ephedra', 0),
(725, 'steam:110000116e48a9b', 'cigarett', 0),
(726, 'steam:110000116e48a9b', 'ccheese', 0),
(727, 'steam:110000116e48a9b', 'fixtool', 0),
(728, 'steam:110000116e48a9b', 'marabou', 0),
(729, 'steam:110000116e48a9b', 'lotteryticket', 0),
(730, 'steam:110000116e48a9b', 'pastacarbonara', 0),
(731, 'steam:110000116e48a9b', 'bandage', 0),
(732, 'steam:110000116e48a9b', 'alive_chicken', 0),
(733, 'steam:110000116e48a9b', 'loka', 0),
(734, 'steam:110000116e48a9b', 'jewels', 0),
(735, 'steam:110000116e48a9b', 'vbread', 0),
(736, 'steam:110000116e48a9b', 'carokit', 0),
(737, 'steam:110000116e48a9b', 'weed_pooch', 0),
(738, 'steam:110000116e48a9b', 'coca', 0),
(739, 'steam:110000116e48a9b', 'opium_pooch', 0),
(740, 'steam:110000116e48a9b', 'fvburger', 0),
(741, 'steam:110000116e48a9b', 'fanta', 0),
(742, 'steam:110000116e48a9b', 'heroine', 0),
(743, 'steam:110000116e48a9b', 'ctomato', 0),
(744, 'steam:110000116e48a9b', 'radio', 0),
(745, 'steam:11000010b49a743', 'fburger', 0),
(746, 'steam:11000010b49a743', 'chips', 0),
(747, 'steam:11000010b49a743', 'essence', 0),
(748, 'steam:11000010b49a743', 'petrol_raffin', 0),
(749, 'steam:11000010b49a743', 'lettuce', 0),
(750, 'steam:11000010b49a743', 'ctomato', 0),
(751, 'steam:11000010b49a743', 'coke', 0),
(752, 'steam:11000010b49a743', 'ccheese', 0),
(753, 'steam:11000010b49a743', 'gazbottle', 0),
(754, 'steam:11000010b49a743', 'fixtool', 0),
(755, 'steam:11000010b49a743', 'beer', 0),
(756, 'steam:11000010b49a743', 'fish', 0),
(757, 'steam:11000010b49a743', 'meth', 0),
(758, 'steam:11000010b49a743', 'carotool', 0),
(759, 'steam:11000010b49a743', 'cannabis', 0),
(760, 'steam:11000010b49a743', 'fanta', 0),
(761, 'steam:11000010b49a743', 'bag', 0),
(762, 'steam:11000010b49a743', 'fvburger', 0),
(763, 'steam:11000010b49a743', 'macka', 0),
(764, 'steam:11000010b49a743', 'loka', 0),
(765, 'steam:11000010b49a743', 'alive_chicken', 0),
(766, 'steam:11000010b49a743', 'cheese', 0),
(767, 'steam:11000010b49a743', 'potato', 0),
(768, 'steam:11000010b49a743', 'cigarett', 0),
(769, 'steam:11000010b49a743', 'clettuce', 0),
(770, 'steam:11000010b49a743', 'fixkit', 10),
(771, 'steam:11000010b49a743', 'marabou', 0),
(772, 'steam:11000010b49a743', 'carokit', 0),
(773, 'steam:11000010b49a743', 'cocacola', 0),
(774, 'steam:11000010b49a743', 'shotgun_shells', 0),
(775, 'steam:11000010b49a743', 'jewels', 0),
(776, 'steam:11000010b49a743', 'bread', 0),
(777, 'steam:11000010b49a743', 'narcan', 0),
(778, 'steam:11000010b49a743', 'clothe', 0),
(779, 'steam:11000010b49a743', 'stone', 0),
(780, 'steam:11000010b49a743', 'opium_pooch', 0),
(781, 'steam:11000010b49a743', 'iron', 0),
(782, 'steam:11000010b49a743', 'petrol', 0),
(783, 'steam:11000010b49a743', 'water', 0),
(784, 'steam:11000010b49a743', 'marijuana', 0),
(785, 'steam:11000010b49a743', 'wood', 0),
(786, 'steam:11000010b49a743', 'breathalyzer', 0),
(787, 'steam:11000010b49a743', 'cutted_wood', 0),
(788, 'steam:11000010b49a743', 'wool', 0),
(789, 'steam:11000010b49a743', 'whiskey', 0),
(790, 'steam:11000010b49a743', 'weed_pooch', 0),
(791, 'steam:11000010b49a743', 'weed', 0),
(792, 'steam:11000010b49a743', 'washed_stone', 0),
(793, 'steam:11000010b49a743', 'nuggets4', 0),
(794, 'steam:11000010b49a743', 'coffee', 20),
(795, 'steam:11000010b49a743', 'vodka', 0),
(796, 'steam:11000010b49a743', 'vbread', 0),
(797, 'steam:11000010b49a743', 'tomato', 0),
(798, 'steam:11000010b49a743', 'donut', 14),
(799, 'steam:11000010b49a743', 'fakepee', 0),
(800, 'steam:11000010b49a743', 'tequila', 0),
(801, 'steam:11000010b49a743', 'fabric', 0),
(802, 'steam:11000010b49a743', 'dabs', 0),
(803, 'steam:11000010b49a743', 'pcp', 0),
(804, 'steam:11000010b49a743', 'crack', 0),
(805, 'steam:11000010b49a743', 'sprite', 0),
(806, 'steam:11000010b49a743', 'radio', 1),
(807, 'steam:11000010b49a743', 'painkiller', 0),
(808, 'steam:11000010b49a743', 'packaged_plank', 0),
(809, 'steam:11000010b49a743', 'shamburger', 0),
(810, 'steam:11000010b49a743', 'copper', 0),
(811, 'steam:11000010b49a743', 'armour', 25),
(812, 'steam:11000010b49a743', 'ephedrine', 0),
(813, 'steam:11000010b49a743', 'pizza', 0),
(814, 'steam:11000010b49a743', 'pastacarbonara', 0),
(815, 'steam:11000010b49a743', 'vhamburger', 0),
(816, 'steam:11000010b49a743', 'slaughtered_chicken', 0),
(817, 'steam:11000010b49a743', 'opium', 0),
(818, 'steam:11000010b49a743', 'packaged_chicken', 0),
(819, 'steam:11000010b49a743', 'cheesebows', 0),
(820, 'steam:11000010b49a743', 'coke_pooch', 0),
(821, 'steam:11000010b49a743', 'coca', 0),
(822, 'steam:11000010b49a743', 'nuggets10', 0),
(823, 'steam:11000010b49a743', 'nugget', 0),
(824, 'steam:11000010b49a743', 'bandage', 0),
(825, 'steam:11000010b49a743', 'diamond', 0),
(826, 'steam:11000010b49a743', 'ephedra', 0),
(827, 'steam:11000010b49a743', 'meth_pooch', 0),
(828, 'steam:11000010b49a743', 'medikit', 20),
(829, 'steam:11000010b49a743', 'drugtest', 0),
(830, 'steam:11000010b49a743', 'blowpipe', 0),
(831, 'steam:11000010b49a743', 'heroine', 0),
(832, 'steam:11000010b49a743', 'clip', 6),
(833, 'steam:11000010b49a743', 'lotteryticket', 0),
(834, 'steam:11000010b49a743', 'burger', 0),
(835, 'steam:11000010b49a743', 'gold', 0),
(836, 'steam:11000010b49a743', 'poppy', 0),
(837, 'steam:11000010b49a743', 'lighter', 0),
(838, 'steam:110000111c332ed', 'clip', 0),
(839, 'steam:110000111c332ed', 'chips', 3),
(840, 'steam:110000111c332ed', 'coca', 0),
(841, 'steam:110000111c332ed', 'loka', 0),
(842, 'steam:110000111c332ed', 'cannabis', 0),
(843, 'steam:110000111c332ed', 'potato', 0),
(844, 'steam:110000111c332ed', 'heroine', 0),
(845, 'steam:110000111c332ed', 'clothe', 0),
(846, 'steam:110000111c332ed', 'opium_pooch', 0),
(847, 'steam:110000111c332ed', 'beer', 0),
(848, 'steam:110000111c332ed', 'fanta', 0),
(849, 'steam:110000111c332ed', 'iron', 0),
(850, 'steam:110000111c332ed', 'fvburger', 0),
(851, 'steam:110000111c332ed', 'washed_stone', 0),
(852, 'steam:110000111c332ed', 'pastacarbonara', 0),
(853, 'steam:110000111c332ed', 'armour', 0),
(854, 'steam:110000111c332ed', 'water', 0),
(855, 'steam:110000111c332ed', 'bag', 0),
(856, 'steam:110000111c332ed', 'pizza', 1),
(857, 'steam:110000111c332ed', 'cheese', 0),
(858, 'steam:110000111c332ed', 'coffee', 0),
(859, 'steam:110000111c332ed', 'nuggets10', 0),
(860, 'steam:110000111c332ed', 'blowpipe', 0),
(861, 'steam:110000111c332ed', 'cigarett', 1),
(862, 'steam:110000111c332ed', 'fixkit', 1),
(863, 'steam:110000111c332ed', 'coke_pooch', 0),
(864, 'steam:110000111c332ed', 'tequila', 0),
(865, 'steam:110000111c332ed', 'jewels', 0),
(866, 'steam:110000111c332ed', 'fish', 0),
(867, 'steam:110000111c332ed', 'cutted_wood', 0),
(868, 'steam:110000111c332ed', 'gold', 0),
(869, 'steam:110000111c332ed', 'lettuce', 0),
(870, 'steam:110000111c332ed', 'cheesebows', 0),
(871, 'steam:110000111c332ed', 'marijuana', 0),
(872, 'steam:110000111c332ed', 'fabric', 0),
(873, 'steam:110000111c332ed', 'slaughtered_chicken', 0),
(874, 'steam:110000111c332ed', 'packaged_chicken', 0),
(875, 'steam:110000111c332ed', 'donut', 0),
(876, 'steam:110000111c332ed', 'carotool', 0),
(877, 'steam:110000111c332ed', 'stone', 0),
(878, 'steam:110000111c332ed', 'cocacola', 0),
(879, 'steam:110000111c332ed', 'marabou', 0),
(880, 'steam:110000111c332ed', 'ephedra', 0),
(881, 'steam:110000111c332ed', 'bread', 3),
(882, 'steam:110000111c332ed', 'nuggets4', 0),
(883, 'steam:110000111c332ed', 'dabs', 0),
(884, 'steam:110000111c332ed', 'tomato', 0),
(885, 'steam:110000111c332ed', 'diamond', 0),
(886, 'steam:110000111c332ed', 'ctomato', 0),
(887, 'steam:110000111c332ed', 'vodka', 0),
(888, 'steam:110000111c332ed', 'carokit', 0),
(889, 'steam:110000111c332ed', 'gazbottle', 0),
(890, 'steam:110000111c332ed', 'macka', 0),
(891, 'steam:110000111c332ed', 'weed_pooch', 0),
(892, 'steam:110000111c332ed', 'essence', 0),
(893, 'steam:110000111c332ed', 'weed', 0),
(894, 'steam:110000111c332ed', 'pcp', 0),
(895, 'steam:110000111c332ed', 'shotgun_shells', 0),
(896, 'steam:110000111c332ed', 'petrol', 0),
(897, 'steam:110000111c332ed', 'bandage', 0),
(898, 'steam:110000111c332ed', 'wood', 0),
(899, 'steam:110000111c332ed', 'vhamburger', 0),
(900, 'steam:110000111c332ed', 'ccheese', 0),
(901, 'steam:110000111c332ed', 'vbread', 0),
(902, 'steam:110000111c332ed', 'copper', 0),
(903, 'steam:110000111c332ed', 'meth_pooch', 0),
(904, 'steam:110000111c332ed', 'radio', 1),
(905, 'steam:110000111c332ed', 'shamburger', 0),
(906, 'steam:110000111c332ed', 'alive_chicken', 0),
(907, 'steam:110000111c332ed', 'burger', 0),
(908, 'steam:110000111c332ed', 'breathalyzer', 0),
(909, 'steam:110000111c332ed', 'painkiller', 0),
(910, 'steam:110000111c332ed', 'fakepee', 0),
(911, 'steam:110000111c332ed', 'meth', 0),
(912, 'steam:110000111c332ed', 'fixtool', 0),
(913, 'steam:110000111c332ed', 'poppy', 0),
(914, 'steam:110000111c332ed', 'ephedrine', 0),
(915, 'steam:110000111c332ed', 'petrol_raffin', 0),
(916, 'steam:110000111c332ed', 'packaged_plank', 0),
(917, 'steam:110000111c332ed', 'opium', 0),
(918, 'steam:110000111c332ed', 'narcan', 0),
(919, 'steam:110000111c332ed', 'wool', 0),
(920, 'steam:110000111c332ed', 'clettuce', 0),
(921, 'steam:110000111c332ed', 'nugget', 0),
(922, 'steam:110000111c332ed', 'sprite', 3),
(923, 'steam:110000111c332ed', 'fburger', 0),
(924, 'steam:110000111c332ed', 'medikit', 0),
(925, 'steam:110000111c332ed', 'drugtest', 0),
(926, 'steam:110000111c332ed', 'lighter', 0),
(927, 'steam:110000111c332ed', 'whiskey', 0),
(928, 'steam:110000111c332ed', 'crack', 0),
(929, 'steam:110000111c332ed', 'coke', 0),
(930, 'steam:110000111c332ed', 'lotteryticket', 2);

-- --------------------------------------------------------

--
-- Table structure for table `user_licenses`
--

CREATE TABLE `user_licenses` (
  `id` int(11) NOT NULL,
  `type` varchar(60) COLLATE utf8mb4_bin NOT NULL,
  `owner` varchar(60) COLLATE utf8mb4_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `user_licenses`
--

INSERT INTO `user_licenses` (`id`, `type`, `owner`) VALUES
(1, 'dmv', 'steam:110000132580eb0');

-- --------------------------------------------------------

--
-- Table structure for table `vehicles`
--

CREATE TABLE `vehicles` (
  `name` varchar(60) COLLATE utf8mb4_bin NOT NULL,
  `model` varchar(60) COLLATE utf8mb4_bin NOT NULL,
  `price` int(11) NOT NULL,
  `category` varchar(60) COLLATE utf8mb4_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `vehicles`
--

INSERT INTO `vehicles` (`name`, `model`, `price`, `category`) VALUES
('Dodge Charger', '16charger', 35000, 'super'),
('Lambo Huracan', '18performante', 250000, 'super'),
('Ferrari', '599gtox', 155000, 'super'),
('Akuma', 'AKUMA', 7500, 'motorcycles'),
('Adder', 'adder', 1100000, 'super'),
('Alpha', 'alpha', 60000, 'sports'),
('Ardent', 'ardent', 1150000, 'sportsclassics'),
('Asea', 'asea', 5500, 'sedans'),
('Autarch', 'autarch', 1955000, 'super'),
('Avarus', 'avarus', 18000, 'motorcycles'),
('Bagger', 'bagger', 13500, 'motorcycles'),
('Baller', 'baller2', 40000, 'suvs'),
('Baller Sport', 'baller3', 60000, 'suvs'),
('Banshee', 'banshee', 70000, 'sports'),
('Banshee 900R', 'banshee2', 255000, 'super'),
('Bati 801', 'bati', 12000, 'motorcycles'),
('Bati 801RR', 'bati2', 19000, 'motorcycles'),
('Bestia GTS', 'bestiagts', 55000, 'sports'),
('BF400', 'bf400', 6500, 'motorcycles'),
('Bf Injection', 'bfinjection', 16000, 'offroad'),
('Bifta', 'bifta', 12000, 'offroad'),
('Bison', 'bison', 45000, 'vans'),
('Blade', 'blade', 15000, 'muscle'),
('Blazer', 'blazer', 6500, 'offroad'),
('Blazer Sport', 'blazer4', 8500, 'offroad'),
('blazer5', 'blazer5', 1755600, 'offroad'),
('Blista', 'blista', 8000, 'compacts'),
('BMX (velo)', 'bmx', 160, 'motorcycles'),
('Bobcat XL', 'bobcatxl', 32000, 'vans'),
('Brawler', 'brawler', 45000, 'offroad'),
('Brioso R/A', 'brioso', 18000, 'compacts'),
('Btype', 'btype', 62000, 'sportsclassics'),
('Btype Hotroad', 'btype2', 155000, 'sportsclassics'),
('Btype Luxe', 'btype3', 85000, 'sportsclassics'),
('Buccaneer', 'buccaneer', 18000, 'muscle'),
('Buccaneer Rider', 'buccaneer2', 24000, 'muscle'),
('Buffalo', 'buffalo', 12000, 'sports'),
('Buffalo S', 'buffalo2', 20000, 'sports'),
('Bullet', 'bullet', 90000, 'super'),
('Burrito', 'burrito3', 19000, 'vans'),
('Camper', 'camper', 42000, 'vans'),
('Carbonizzare', 'carbonizzare', 75000, 'sports'),
('Carbon RS', 'carbonrs', 18000, 'motorcycles'),
('Casco', 'casco', 30000, 'sportsclassics'),
('Cavalcade', 'cavalcade2', 55000, 'suvs'),
('Cheetah', 'cheetah', 375000, 'super'),
('Chimera', 'chimera', 38000, 'motorcycles'),
('Chino', 'chino', 15000, 'muscle'),
('Chino Luxe', 'chino2', 19000, 'muscle'),
('Cliffhanger', 'cliffhanger', 9500, 'motorcycles'),
('Cognoscenti Cabrio', 'cogcabrio', 55000, 'coupes'),
('Cognoscenti', 'cognoscenti', 55000, 'sedans'),
('Comet', 'comet2', 65000, 'sports'),
('Comet 5', 'comet5', 1145000, 'sports'),
('Contender', 'contender', 70000, 'suvs'),
('Coquette', 'coquette', 65000, 'sports'),
('Coquette Classic', 'coquette2', 40000, 'sportsclassics'),
('Coquette BlackFin', 'coquette3', 55000, 'muscle'),
('Corvette', 'corvettezr1', 75000, 'super'),
('Cruiser (velo)', 'cruiser', 510, 'motorcycles'),
('Cyclone', 'cyclone', 1890000, 'super'),
('Daemon', 'daemon', 11500, 'motorcycles'),
('Daemon High', 'daemon2', 13500, 'motorcycles'),
('Defiler', 'defiler', 9800, 'motorcycles'),
('Deluxo', 'deluxo', 4721500, 'sportsclassics'),
('Dominator', 'dominator', 35000, 'muscle'),
('Double T', 'double', 28000, 'motorcycles'),
('Dubsta', 'dubsta', 45000, 'suvs'),
('Dubsta Luxuary', 'dubsta2', 60000, 'suvs'),
('Bubsta 6x6', 'dubsta3', 120000, 'offroad'),
('Dukes', 'dukes', 28000, 'muscle'),
('Dune Buggy', 'dune', 8000, 'offroad'),
('Elegy', 'elegy2', 38500, 'sports'),
('Emperor', 'emperor', 8500, 'sedans'),
('Enduro', 'enduro', 5500, 'motorcycles'),
('Entity XF', 'entityxf', 425000, 'super'),
('Esskey', 'esskey', 4200, 'motorcycles'),
('Exemplar', 'exemplar', 32000, 'coupes'),
('F620', 'f620', 40000, 'coupes'),
('Faction', 'faction', 20000, 'muscle'),
('Faction Rider', 'faction2', 30000, 'muscle'),
('Faction XL', 'faction3', 40000, 'muscle'),
('Faggio', 'faggio', 1900, 'motorcycles'),
('Vespa', 'faggio2', 2800, 'motorcycles'),
('Felon', 'felon', 42000, 'coupes'),
('Felon GT', 'felon2', 55000, 'coupes'),
('Feltzer', 'feltzer2', 55000, 'sports'),
('Stirling GT', 'feltzer3', 65000, 'sportsclassics'),
('Fixter (velo)', 'fixter', 225, 'motorcycles'),
('FMJ', 'fmj', 185000, 'super'),
('Fhantom', 'fq2', 17000, 'suvs'),
('Fugitive', 'fugitive', 12000, 'sedans'),
('Furore GT', 'furoregt', 45000, 'sports'),
('Fusilade', 'fusilade', 40000, 'sports'),
('Gargoyle', 'gargoyle', 16500, 'motorcycles'),
('Gauntlet', 'gauntlet', 30000, 'muscle'),
('Gang Burrito', 'gburrito', 45000, 'vans'),
('Burrito', 'gburrito2', 29000, 'vans'),
('Glendale', 'glendale', 6500, 'sedans'),
('Grabger', 'granger', 50000, 'suvs'),
('Gresley', 'gresley', 47500, 'suvs'),
('GT 500', 'gt500', 785000, 'sportsclassics'),
('Guardian', 'guardian', 45000, 'offroad'),
('Hakuchou', 'hakuchou', 31000, 'motorcycles'),
('Hakuchou Sport', 'hakuchou2', 55000, 'motorcycles'),
('Hermes', 'hermes', 535000, 'muscle'),
('Hexer', 'hexer', 12000, 'motorcycles'),
('Hotknife', 'hotknife', 125000, 'muscle'),
('Huntley S', 'huntley', 40000, 'suvs'),
('Hustler', 'hustler', 625000, 'muscle'),
('Infernus', 'infernus', 180000, 'super'),
('Innovation', 'innovation', 23500, 'motorcycles'),
('Intruder', 'intruder', 7500, 'sedans'),
('Issi', 'issi2', 10000, 'compacts'),
('Jackal', 'jackal', 38000, 'coupes'),
('Jester', 'jester', 65000, 'sports'),
('Jester(Racecar)', 'jester2', 135000, 'sports'),
('Journey', 'journey', 6500, 'vans'),
('Kamacho', 'kamacho', 345000, 'offroad'),
('Khamelion', 'khamelion', 38000, 'sports'),
('Kuruma', 'kuruma', 30000, 'sports'),
('Lambo Svj', 'lamboavj', 400000, 'super'),
('Landstalker', 'landstalker', 35000, 'suvs'),
('Rmodlb750sv', 'lb750sv', 400000, 'super'),
('RE-7B', 'le7b', 325000, 'super'),
('Lynx', 'lynx', 40000, 'sports'),
('Mamba', 'mamba', 70000, 'sports'),
('Manana', 'manana', 12800, 'sportsclassics'),
('Manchez', 'manchez', 5300, 'motorcycles'),
('Massacro', 'massacro', 65000, 'sports'),
('Massacro(Racecar)', 'massacro2', 130000, 'sports'),
('Mesa', 'mesa', 16000, 'suvs'),
('Mesa Trail', 'mesa3', 40000, 'suvs'),
('Minivan', 'minivan', 13000, 'vans'),
('Monroe', 'monroe', 55000, 'sportsclassics'),
('The Liberator', 'monster', 210000, 'offroad'),
('Moonbeam', 'moonbeam', 18000, 'vans'),
('Moonbeam Rider', 'moonbeam2', 35000, 'vans'),
('Nemesis', 'nemesis', 5800, 'motorcycles'),
('Neon', 'neon', 1500000, 'sports'),
('Nightblade', 'nightblade', 35000, 'motorcycles'),
('Nightshade', 'nightshade', 65000, 'muscle'),
('9F', 'ninef', 65000, 'sports'),
('9F Cabrio', 'ninef2', 80000, 'sports'),
('Omnis', 'omnis', 35000, 'sports'),
('Oppressor', 'oppressor', 3524500, 'super'),
('Oracle XS', 'oracle2', 35000, 'coupes'),
('Osiris', 'osiris', 160000, 'super'),
('Panto', 'panto', 10000, 'compacts'),
('Paradise', 'paradise', 19000, 'vans'),
('Pariah', 'pariah', 1420000, 'sports'),
('Patriot', 'patriot', 55000, 'suvs'),
('PCJ-600', 'pcj', 6200, 'motorcycles'),
('Penumbra', 'penumbra', 28000, 'sports'),
('Pfister', 'pfister811', 85000, 'super'),
('Phoenix', 'phoenix', 12500, 'muscle'),
('Picador', 'picador', 18000, 'muscle'),
('Pigalle', 'pigalle', 20000, 'sportsclassics'),
('Prairie', 'prairie', 12000, 'compacts'),
('Premier', 'premier', 8000, 'sedans'),
('Primo Custom', 'primo2', 14000, 'sedans'),
('X80 Proto', 'prototipo', 2500000, 'super'),
('Radius', 'radi', 29000, 'suvs'),
('raiden', 'raiden', 1375000, 'sports'),
('Dodge Ram', 'ram2500', 30000, 'super'),
('Rapid GT', 'rapidgt', 35000, 'sports'),
('Rapid GT Convertible', 'rapidgt2', 45000, 'sports'),
('Rapid GT3', 'rapidgt3', 885000, 'sportsclassics'),
('Reaper', 'reaper', 150000, 'super'),
('Rebel', 'rebel2', 35000, 'offroad'),
('Regina', 'regina', 5000, 'sedans'),
('Retinue', 'retinue', 615000, 'sportsclassics'),
('Revolter', 'revolter', 1610000, 'sports'),
('riata', 'riata', 380000, 'offroad'),
('Amg C63', 'rmodamgc63', 56000, 'super'),
('Amg GT63', 'rmodgt63', 65000, 'super'),
('Ford Mustang ', 'rmodmustang', 40000, 'super'),
('Rocoto', 'rocoto', 45000, 'suvs'),
('Ruffian', 'ruffian', 6800, 'motorcycles'),
('Ruiner 2', 'ruiner2', 5745600, 'muscle'),
('Rumpo', 'rumpo', 15000, 'vans'),
('Rumpo Trail', 'rumpo3', 19500, 'vans'),
('Sabre Turbo', 'sabregt', 20000, 'muscle'),
('Sabre GT', 'sabregt2', 25000, 'muscle'),
('Sanchez', 'sanchez', 5300, 'motorcycles'),
('Sanchez Sport', 'sanchez2', 5300, 'motorcycles'),
('Sanctus', 'sanctus', 25000, 'motorcycles'),
('Sandking', 'sandking', 55000, 'offroad'),
('Savestra', 'savestra', 990000, 'sportsclassics'),
('SC 1', 'sc1', 1603000, 'super'),
('Schafter', 'schafter2', 25000, 'sedans'),
('Schafter V12', 'schafter3', 50000, 'sports'),
('Scorcher (velo)', 'scorcher', 280, 'motorcycles'),
('Seminole', 'seminole', 25000, 'suvs'),
('Sentinel', 'sentinel', 32000, 'coupes'),
('Sentinel XS', 'sentinel2', 40000, 'coupes'),
('Sentinel3', 'sentinel3', 650000, 'sports'),
('Seven 70', 'seven70', 39500, 'sports'),
('ETR1', 'sheava', 220000, 'super'),
('Shotaro Concept', 'shotaro', 320000, 'motorcycles'),
('Slam Van', 'slamvan3', 11500, 'muscle'),
('Sovereign', 'sovereign', 22000, 'motorcycles'),
('Stinger', 'stinger', 80000, 'sportsclassics'),
('Stinger GT', 'stingergt', 75000, 'sportsclassics'),
('Streiter', 'streiter', 500000, 'sports'),
('Stretch', 'stretch', 90000, 'sedans'),
('Stromberg', 'stromberg', 3185350, 'sports'),
('Sultan', 'sultan', 15000, 'sports'),
('Sultan RS', 'sultanrs', 65000, 'super'),
('Super Diamond', 'superd', 130000, 'sedans'),
('Surano', 'surano', 50000, 'sports'),
('Surfer', 'surfer', 12000, 'vans'),
('T20', 't20', 300000, 'super'),
('Tailgater', 'tailgater', 30000, 'sedans'),
('Tampa', 'tampa', 16000, 'muscle'),
('Drift Tampa', 'tampa2', 80000, 'sports'),
('Thrust', 'thrust', 24000, 'motorcycles'),
('Tri bike (velo)', 'tribike3', 520, 'motorcycles'),
('Trophy Truck', 'trophytruck', 60000, 'offroad'),
('Trophy Truck Limited', 'trophytruck2', 80000, 'offroad'),
('Tropos', 'tropos', 40000, 'sports'),
('Turismo R', 'turismor', 350000, 'super'),
('Tyrus', 'tyrus', 600000, 'super'),
('Vacca', 'vacca', 120000, 'super'),
('Vader', 'vader', 7200, 'motorcycles'),
('Verlierer', 'verlierer2', 70000, 'sports'),
('Vigero', 'vigero', 12500, 'muscle'),
('Virgo', 'virgo', 14000, 'muscle'),
('Viseris', 'viseris', 875000, 'sportsclassics'),
('Visione', 'visione', 2250000, 'super'),
('Voltic', 'voltic', 90000, 'super'),
('Voltic 2', 'voltic2', 3830400, 'super'),
('Voodoo', 'voodoo', 7200, 'muscle'),
('Vortex', 'vortex', 9800, 'motorcycles'),
('Warrener', 'warrener', 4000, 'sedans'),
('Washington', 'washington', 9000, 'sedans'),
('Windsor', 'windsor', 95000, 'coupes'),
('Windsor Drop', 'windsor2', 125000, 'coupes'),
('Woflsbane', 'wolfsbane', 9000, 'motorcycles'),
('XLS', 'xls', 32000, 'suvs'),
('Yosemite', 'yosemite', 485000, 'muscle'),
('Youga', 'youga', 10800, 'vans'),
('Youga Luxuary', 'youga2', 14500, 'vans'),
('Z190', 'z190', 900000, 'sportsclassics'),
('Zentorno', 'zentorno', 1500000, 'super'),
('Zion', 'zion', 36000, 'coupes'),
('Zion Cabrio', 'zion2', 45000, 'coupes'),
('Zombie', 'zombiea', 9500, 'motorcycles'),
('Zombie Luxuary', 'zombieb', 12000, 'motorcycles'),
('Z-Type', 'ztype', 220000, 'sportsclassics');

-- --------------------------------------------------------

--
-- Table structure for table `vehicle_categories`
--

CREATE TABLE `vehicle_categories` (
  `name` varchar(60) COLLATE utf8mb4_bin NOT NULL,
  `label` varchar(60) COLLATE utf8mb4_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `vehicle_categories`
--

INSERT INTO `vehicle_categories` (`name`, `label`) VALUES
('compacts', 'Compacts'),
('coupes', 'Coupes'),
('motorcycles', 'Motos'),
('muscle', 'Muscle'),
('offroad', 'Off Road'),
('sedans', 'Sedans'),
('sports', 'Sports'),
('sportsclassics', 'Sports Classics'),
('super', 'Super'),
('suvs', 'SUVs'),
('vans', 'Vans');

-- --------------------------------------------------------

--
-- Table structure for table `vehicle_sold`
--

CREATE TABLE `vehicle_sold` (
  `client` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `model` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `plate` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `soldby` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `date` varchar(50) COLLATE utf8mb4_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- --------------------------------------------------------

--
-- Table structure for table `weashops`
--

CREATE TABLE `weashops` (
  `id` int(11) NOT NULL,
  `zone` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `item` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `price` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `weashops`
--

INSERT INTO `weashops` (`id`, `zone`, `item`, `price`) VALUES
(1, 'GunShop', 'WEAPON_PISTOL', 400),
(2, 'GunShop', 'WEAPON_FLASHLIGHT', 15),
(3, 'GunShop', 'WEAPON_MACHETE', 50),
(4, 'GunShop', 'WEAPON_NIGHTSTICK', 50),
(5, 'GunShop', 'WEAPON_BAT', 25),
(6, 'GunShop', 'WEAPON_STUNGUN', 60),
(7, 'GunShop', 'WEAPON_PUMPSHOTGUN', 3500),
(8, 'GunShop', 'WEAPON_ASSAULTRIFLE', 15000),
(9, 'GunShop', 'WEAPON_SNIPERRIFLE', 30000),
(10, 'GunShop', 'WEAPON_BZGAS', 500),
(11, 'GunShop', 'WEAPON_FIREEXTINGUISHER', 25),
(12, 'GunShop', 'WEAPON_BALL', 10),
(13, 'GunShop', 'WEAPON_SMOKEGRENADE', 500),
(14, 'GunShop', 'WEAPON_CARBINERIFLE', 13000),
(15, 'GunShop', 'WEAPON_ADVANCEDRIFLE', 15000),
(16, 'GunShop', 'WEAPON_SMG', 10000),
(17, 'GunShop', 'WEAPON_COMBATPISTOL', 500),
(18, 'GunShop', 'WEAPON_CROWBAR', 30),
(19, 'GunShop', 'WEAPON_FLARE', 15),
(20, 'GunShop', 'WEAPON_FLAREGUN', 500),
(21, 'GunShop', 'WEAPON_HEAVYPISTOL', 300),
(22, 'GunShop', 'WEAPON_PISTOL50', 1000),
(23, 'GunShop', 'WEAPON_REVOLVER', 300),
(24, 'GunShop', 'WEAPON_WRENCH', 20),
(25, 'BlackWeashop', 'WEAPON_SPECIALCARBINE', 10000),
(26, 'BlackWeashop', 'WEAPON_FIREWORK', 10000),
(27, 'BlackWeashop', 'WEAPON_HEAVYSNIPER', 35000),
(28, 'BlackWeashop', 'WEAPON_MINIGUN', 75000),
(29, 'BlackWeashop', 'WEAPON_RAILGUN', 80000),
(30, 'BlackWeashop', 'WEAPON_STICKYBOMB', 500),
(31, 'BlackWeashop', 'WEAPON_ASSAULTSHOTGUN', 3000),
(32, 'BlackWeashop', 'WEAPON_COMPACTRIFLE', 3500),
(33, 'BlackWeashop', 'WEAPON_DBSHOTGUN', 2500),
(34, 'BlackWeashop', 'WEAPON_HEAVYSHOTGUN', 5000),
(35, 'BlackWeashop', 'WEAPON_SWITCHBLADE', 200),
(36, 'BlackWeashop', 'WEAPON_KNUCKLE', 100),
(37, 'BlackWeashop', 'WEAPON_GRENADE', 500),
(38, 'BlackWeashop', 'WEAPON_MICROSMG', 4500),
(39, 'BlackWeashop', 'WEAPON_APPISTOL', 3000);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `addon_account`
--
ALTER TABLE `addon_account`
  ADD PRIMARY KEY (`name`);

--
-- Indexes for table `addon_account_data`
--
ALTER TABLE `addon_account_data`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `index_addon_account_data_account_name_owner` (`account_name`,`owner`),
  ADD KEY `index_addon_account_data_account_name` (`account_name`);

--
-- Indexes for table `addon_inventory`
--
ALTER TABLE `addon_inventory`
  ADD PRIMARY KEY (`name`);

--
-- Indexes for table `addon_inventory_items`
--
ALTER TABLE `addon_inventory_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `index_addon_inventory_items_inventory_name_name` (`inventory_name`,`name`),
  ADD KEY `index_addon_inventory_items_inventory_name_name_owner` (`inventory_name`,`name`,`owner`),
  ADD KEY `index_addon_inventory_inventory_name` (`inventory_name`);

--
-- Indexes for table `billing`
--
ALTER TABLE `billing`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cardealer_vehicles`
--
ALTER TABLE `cardealer_vehicles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `characters`
--
ALTER TABLE `characters`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `datastore`
--
ALTER TABLE `datastore`
  ADD PRIMARY KEY (`name`);

--
-- Indexes for table `datastore_data`
--
ALTER TABLE `datastore_data`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `index_datastore_data_name_owner` (`name`,`owner`),
  ADD KEY `index_datastore_data_name` (`name`);

--
-- Indexes for table `fine_types`
--
ALTER TABLE `fine_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `glovebox_inventory`
--
ALTER TABLE `glovebox_inventory`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `plate` (`plate`);

--
-- Indexes for table `items`
--
ALTER TABLE `items`
  ADD PRIMARY KEY (`name`);

--
-- Indexes for table `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`name`);

--
-- Indexes for table `job_grades`
--
ALTER TABLE `job_grades`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `licenses`
--
ALTER TABLE `licenses`
  ADD PRIMARY KEY (`type`);

--
-- Indexes for table `old_owned_vehicles`
--
ALTER TABLE `old_owned_vehicles`
  ADD PRIMARY KEY (`plate`);

--
-- Indexes for table `old_users`
--
ALTER TABLE `old_users`
  ADD PRIMARY KEY (`identifier`);

--
-- Indexes for table `owned_properties`
--
ALTER TABLE `owned_properties`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `owned_vehicles`
--
ALTER TABLE `owned_vehicles`
  ADD PRIMARY KEY (`plate`);

--
-- Indexes for table `phone_app_chat`
--
ALTER TABLE `phone_app_chat`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `phone_calls`
--
ALTER TABLE `phone_calls`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `phone_messages`
--
ALTER TABLE `phone_messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `phone_users_contacts`
--
ALTER TABLE `phone_users_contacts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `properties`
--
ALTER TABLE `properties`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rented_vehicles`
--
ALTER TABLE `rented_vehicles`
  ADD PRIMARY KEY (`plate`);

--
-- Indexes for table `shops`
--
ALTER TABLE `shops`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `society_moneywash`
--
ALTER TABLE `society_moneywash`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trunk_inventory`
--
ALTER TABLE `trunk_inventory`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `plate` (`plate`);

--
-- Indexes for table `twitter_accounts`
--
ALTER TABLE `twitter_accounts`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `twitter_likes`
--
ALTER TABLE `twitter_likes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_twitter_likes_twitter_accounts` (`authorId`),
  ADD KEY `FK_twitter_likes_twitter_tweets` (`tweetId`);

--
-- Indexes for table `twitter_tweets`
--
ALTER TABLE `twitter_tweets`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_twitter_tweets_twitter_accounts` (`authorId`);

--
-- Indexes for table `user_accounts`
--
ALTER TABLE `user_accounts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_inventory`
--
ALTER TABLE `user_inventory`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_licenses`
--
ALTER TABLE `user_licenses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vehicles`
--
ALTER TABLE `vehicles`
  ADD PRIMARY KEY (`model`);

--
-- Indexes for table `vehicle_categories`
--
ALTER TABLE `vehicle_categories`
  ADD PRIMARY KEY (`name`);

--
-- Indexes for table `vehicle_sold`
--
ALTER TABLE `vehicle_sold`
  ADD PRIMARY KEY (`plate`);

--
-- Indexes for table `weashops`
--
ALTER TABLE `weashops`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `addon_account_data`
--
ALTER TABLE `addon_account_data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=132;

--
-- AUTO_INCREMENT for table `addon_inventory_items`
--
ALTER TABLE `addon_inventory_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `billing`
--
ALTER TABLE `billing`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `cardealer_vehicles`
--
ALTER TABLE `cardealer_vehicles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `characters`
--
ALTER TABLE `characters`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `datastore_data`
--
ALTER TABLE `datastore_data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=211;

--
-- AUTO_INCREMENT for table `fine_types`
--
ALTER TABLE `fine_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;

--
-- AUTO_INCREMENT for table `glovebox_inventory`
--
ALTER TABLE `glovebox_inventory`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66;

--
-- AUTO_INCREMENT for table `job_grades`
--
ALTER TABLE `job_grades`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT for table `owned_properties`
--
ALTER TABLE `owned_properties`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `phone_app_chat`
--
ALTER TABLE `phone_app_chat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `phone_calls`
--
ALTER TABLE `phone_calls`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=195;

--
-- AUTO_INCREMENT for table `phone_messages`
--
ALTER TABLE `phone_messages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=302;

--
-- AUTO_INCREMENT for table `phone_users_contacts`
--
ALTER TABLE `phone_users_contacts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `properties`
--
ALTER TABLE `properties`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT for table `shops`
--
ALTER TABLE `shops`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `society_moneywash`
--
ALTER TABLE `society_moneywash`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trunk_inventory`
--
ALTER TABLE `trunk_inventory`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `twitter_accounts`
--
ALTER TABLE `twitter_accounts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT for table `twitter_likes`
--
ALTER TABLE `twitter_likes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=138;

--
-- AUTO_INCREMENT for table `twitter_tweets`
--
ALTER TABLE `twitter_tweets`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=176;

--
-- AUTO_INCREMENT for table `user_accounts`
--
ALTER TABLE `user_accounts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT for table `user_inventory`
--
ALTER TABLE `user_inventory`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=931;

--
-- AUTO_INCREMENT for table `user_licenses`
--
ALTER TABLE `user_licenses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `weashops`
--
ALTER TABLE `weashops`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=72;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `twitter_likes`
--
ALTER TABLE `twitter_likes`
  ADD CONSTRAINT `FK_twitter_likes_twitter_accounts` FOREIGN KEY (`authorId`) REFERENCES `twitter_accounts` (`id`),
  ADD CONSTRAINT `FK_twitter_likes_twitter_tweets` FOREIGN KEY (`tweetId`) REFERENCES `twitter_tweets` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `twitter_tweets`
--
ALTER TABLE `twitter_tweets`
  ADD CONSTRAINT `FK_twitter_tweets_twitter_accounts` FOREIGN KEY (`authorId`) REFERENCES `twitter_accounts` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
